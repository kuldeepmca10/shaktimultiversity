<?php
// Merchant key here as provided by Payu

define('sPAYU_MERCHANT_ID', '5909346');
define('sPAYU_MERCHANT_KEY', '7lUtugoU');
define('sPAYU_SALT', 'qzC1RpcKUt');
define('sPAYU_PAYMENT_URL', 'https://secure.payu.in/_payment'); // https://test.payu.in  // https://test.payu.in
define('sPAYU_SUCCESS_URL', 'http://localhost/shaktimultiversity/payu/thanks.php');
define('sPAYU_FAILURE_URL', 'http://localhost/shaktimultiversity/payu/failure.php');


// End point - change to https://secure.payu.in for LIVE mode
$PAYU_BASE_URL = "https://test.payu.in";

$action = '';
$aData = array();
$aData['transction_id'] = '99999999';
$aData['first_name'] = 'Kuldee';
$aData['email'] = 'Kuldeep.singh@kleward.com';
$aData['amount'] = '10';
$aData['productinfo'] = 'We';
$aData['phone'] = '838838383';

$posted = array(
    'key' => sPAYU_MERCHANT_KEY, 'hash' => '', 'txnid' => $aData['transction_id'], 'amount' => (int) $aData['amount'],
    'firstname' => $aData['first_name'], 'email' => $aData['email'], 'phone' => $aData['phone'], 'productinfo' => $aData['productinfo'],
    'surl' => sPAYU_SUCCESS_URL, 'furl' => sPAYU_FAILURE_URL, 'service_provider' => 'payu_paisa',
    'lastname' => '', 'curl' => '', 'address1' => '', 'address2' => '',
    'city' => '', 'state' => '', 'country' => '', 'zipcode' => '',
    'udf1' => '', 'udf2' => '', 'udf3' => '', 'udf4' => '',
    'udf5' => '', 'pg' => '');

if (!empty($posted)) {
    //print_r($_POST);
    foreach ($posted as $key => $value) {
        $posted[$key] = $value;
    }
}

$formError = 0;

if (empty($posted['txnid'])) {
    // Generate random transaction id
    $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
} else {
    $txnid = $posted['txnid'];
}
$hash = '';
// Hash Sequence
$hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
if (empty($posted['hash']) && sizeof($posted) > 0) {
    if (
            empty($posted['key']) || empty($posted['txnid']) || empty($posted['amount']) || empty($posted['firstname']) || empty($posted['email']) || empty($posted['phone']) || empty($posted['productinfo']) || empty($posted['surl']) || empty($posted['furl']) || empty($posted['service_provider'])
    ) {
        $formError = 1;
    } else {
        //$posted['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
        $hashVarsSeq = explode('|', $hashSequence);
        $hash_string = '';
        foreach ($hashVarsSeq as $hash_var) {
            $hash_string .= isset($posted[$hash_var]) ? $posted[$hash_var] : '';
            $hash_string .= '|';
        }

        $hash_string .= sPAYU_SALT;


        $hash = strtolower(hash('sha512', $hash_string));
    }
} elseif (!empty($posted['hash'])) {
    $hash = $posted['hash'];
}
?>
<html>
    <head>
        <script>
            var hash = '<?php echo $hash ?>';
            function submitPayuForm() {
                if (hash == '') {
                    return;
                }
                var payuForm = document.forms.payuForm;
                payuForm.submit();
            }
        </script>
    </head>
    <body onload="submitPayuForm()">
        <h2>PayU Form</h2>
        <br/>
        <?php if ($formError) { ?>

            <span style="color:red">Please fill all mandatory fields.</span>
            <br/>
            <br/>
        <?php } ?>
        <form action="<?php echo sPAYU_PAYMENT_URL; ?>" method="post" name="payuForm">
            <input type="hidden" name="key" value="<?php echo sPAYU_MERCHANT_KEY ?>" />
            <input type="hidden" name="hash" value="<?php echo $hash ?>"/>
            <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
            <input type="hidden" name="firstname" value="<?php echo $posted['firstname'] ?>" />
            <input type="hidden" name="amount" value="<?php echo $posted['amount'] ?>" />
            <input type="hidden" name="email" value="<?php echo $posted['email'] ?>" />
            <input type="hidden" name="phone" value="<?php echo $posted['phone'] ?>" />
            <input type="hidden" name="productinfo" value="<?php echo $posted['productinfo'] ?>" />
            <input type="hidden" name="surl" value="<?php echo $posted['surl'] ?>" />
            <input type="hidden" name="furl" value="<?php echo $posted['furl'] ?>" />
            <input type="hidden" name="service_provider" value="payu_paisa" size="64" />

        </form>
    </body>
</html>
