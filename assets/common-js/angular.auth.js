/** Auth Factory **/
app.factory('Auth', function ($window, $http) {
    return{
        setUser: function (u) {
            $window.localStorage['name'] = u.name;
            $window.localStorage['email'] = u.email;
        },
        isLoggedIn: function () {
            return (typeof $window.localStorage.name != "undefined" && $window.localStorage.name != '');
        },
        deleteUser: function () {
            angular.forEach($window.localStorage, function (item, key) {
                $window.localStorage.removeItem(key);
            });
        },
        getUser: function () {
            var uname = '', uemail = '';
            if (this.isLoggedIn()) {
                uname = $window.localStorage.name;
                uemail = $window.localStorage.email;
            }

            return {name: uname, email: uemail};
        },
        redirectLogged: function () {
            if (this.isLoggedIn()) {
                redirect("dashboard");
            }
        },
        redirectNotLogged: function () {
            if (!this.isLoggedIn()) {
                redirect("");
            }
        },
        logout: function () {
            var ob = this;
            $http({url: 'auth/logout'}).success(
                    function (res) {
                        ob.deleteUser();
                        ob.redirectNotLogged();
                    }
            );
        },
        check_res: function (res) {
            if (typeof res == "object") {
                if (typeof res.NotLogged != "undefined") {
                    this.logout();
                }
            }

            return res;
        }
    }
});

//EOF