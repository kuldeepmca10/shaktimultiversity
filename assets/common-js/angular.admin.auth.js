/** Auth Factory **/
app.factory('Auth', function () {
    return{
        check_res: function (res) {
            if (typeof res == "object") {
                if (typeof res.NotLogged != "undefined") {
                    redirect(ADM_URL);
                }
            }
            return res;
        }
    }
});

//EOF