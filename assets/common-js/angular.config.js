var app = angular.module('app', ['ui.router', 'ngAnimate', 'oc.lazyLoad']);

/** Set header type for http post **/
app.config(function ($httpProvider, $httpParamSerializerJQLikeProvider) {
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=utf-8';
    $httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
});

/** Global Functions and Variables **/
app.run(function ($rootScope, $sce) {
    $rootScope.SITE_NAME = 'Shakti Multiversity';

    $rootScope.renderHtml = function (htmlCode) {
        return $sce.trustAsHtml(htmlCode);
    }

    $rootScope.ngrange = function (start, end) {
        var a = [];
        for (i = start; i <= end; i++) {
            a.push(i);
        }
        return a;
    }

    /** Date Format **/
    $rootScope.get_date = function (d) {
        return d.substring(0, 10);
    }

    /** Remove empty data **/
    $rootScope.remove_empty = function (d) {
        angular.forEach(d, function (v, k) {
            if (typeof v == "string") {
                if (!v.trim()) {
                    delete d[k];
                }
            }
        });
        return d;
    }

    /** Status Lookup **/
    $rootScope.status_lookup = function () {
        var a = [{k: 1, v: 'Active'}, {k: 0, v: 'Inactive'}];
        return a;
    }

    /** Status Lookup **/
    $rootScope.country_phonecode = function () {
        var a = [{k: 91, v: 'India'}, {k: 0, v: 'USA'}];
        return a;
    }
});

/** Pagination Directive **/
app.directive('dirMyPaging', function () {
    return {
        template: function (elem, attr) {
            return '<a ng-repeat="n in ngrange(1, page.total_pages)" ng-click="' + attr.fn + '($event, n)" class="{{page.cur_page==n?\'act\':\'\'}}">{{n}}</a>'
        },
        replace: true,
    };
});
//EOF