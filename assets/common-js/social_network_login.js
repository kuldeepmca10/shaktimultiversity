/** FB Login **/
function myFacebookLogin(redirect) {
    if (typeof redirect == "undefined" || !redirect) {
        redirect = "";
    }

    FB.login(
            function () {
                FB.api('/me?fields=name,email,id', function (response) {
                    if (typeof response.id != "undefined") {
                        if (response.id) {
                            var fbdata = JSON.stringify(response);
                            var form_el = '<form id="fbloginform" method="post" action="' + SITE_URL + 'user/fb_login"><input name="fb_id" value="' + response.id + '"><input name="fb_email" value="' + response.email + '"><input name="fb_name" value="' + response.name + '"><input name="redirect" value="' + redirect + '"></form>';
                            $("body").append(form_el);
                            $("#fbloginform").submit();
                        }
                    }
                });
            },
            {scope: 'publish_actions'}
    );
}

/** Google Plus Login **/
var gpluslogin_redirect = '';
function googleAuth(redirect) {
    gpluslogin_redirect = redirect;

    gapi.auth.signIn({
        callback: gPSignInCallback,
        clientid: "649371344297-g9jkhuhmhqofkpoeb4ehsjg51smblq5l.apps.googleusercontent.com",
        cookiepolicy: "single_host_origin",
        requestvisibleactions: "http://schema.org/AddAction",
        scope: "https://www.googleapis.com/auth/plus.login email"
    })
}

function gPSignInCallback(e) {
    if (e["status"]["signed_in"]) {
        gapi.client.load("plus", "v1", function () {
            if (e['status']['signed_in'] && e['status']['method'] == 'PROMPT') {
                getProfile();
            } else if (e['status']['signed_in']) {
            } else if (e["error"]) {
                console.log("There was an error: " + e["error"])
            }
        })
    } else {
        console.log("Sign-in state: " + e["error"])
    }
}

function getProfile() {
    var e = gapi.client.plus.people.get({
        userId: "me"
    });
    e.execute(function (e) {
        if (e.error) {
            console.log(e.message);
            return
        } else if (e.id) {
            var email = '';
            if (typeof e.emails != "undefined") {
                for (i = 0; i < e.emails.length; i++) {
                    if (e.emails[i].type == 'account') {
                        email = e.emails[i].value;
                        break;
                    }
                }
            }
            var form_el = '<form id="gploginform" method="post" action="' + SITE_URL + 'user/gp_login"><input name="gp_id" value="' + e.id + '"><input name="gp_email" value="' + email + '"><input name="gp_name" value="' + e.displayName + '"><input name="redirect" value="' + gpluslogin_redirect + '"></form>';
            $("body").append(form_el);
            $("#gploginform").submit();
        }
    })
}

/** Events **/
$(function () {
    /** FB SDK Load **/
    $.getScript('//connect.facebook.net/en_US/sdk.js', function () {
        FB.init({
            appId: '1204170773005211',
            version: 'v2.5',
            xfbml: true,
        });
    });

    /** G-Plus SDK Load **/
    (function () {
        var e = document.createElement("script");
        e.type = "text/javascript";
        e.async = true;
        e.src = "https://apis.google.com/js/client:platform.js?onload=gPOnLoad";
        var t = document.getElementsByTagName("script")[0];
        t.parentNode.insertBefore(e, t)
    })();
});