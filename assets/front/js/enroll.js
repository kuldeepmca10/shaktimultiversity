$(document).ready(function () {
    /* Enrollment Date Selection Start */
    $("#sConfirmationSubmit").click(function () {
        if (!$('#agree').is(':checked')) {
            $("#sMessageDialogTitle").html('WARNING');
            $(".modal-dialog").addClass('width_500');
            $("#sMessageDialogContent").html('Please agree our terms and conditions');
            $("#sMessageDialog").modal('show');
            return false;
        } else if (!$('#agree_refund_policy').is(':checked')) {
            $("#sMessageDialogTitle").html('WARNING');
            $(".modal-dialog").addClass('width_500');
            $("#sMessageDialogContent").html('Please agree our refund policy');
            $("#sMessageDialog").modal('show');
            return false;
        }
    });
    $("#sCourseAddSubmit").click(function () {
        if ($('#note').val() == '') {
            $("#sMessageDialogTitle").html('WARNING');
            $(".modal-dialog").addClass('width_500');
            $("#sMessageDialogContent").html('Please enter note');
            $("#sMessageDialog").modal('show');
            return false;
        } else if (!$('#agree').is(':checked')) {
            $("#sMessageDialogTitle").html('WARNING');
            $(".modal-dialog").addClass('width_500');
            $("#sMessageDialogContent").html('Please agree our terms and conditions');
            $("#sMessageDialog").modal('show');
            return false;
        } else if (!$('#agree_refund_policy').is(':checked')) {
            $("#sMessageDialogTitle").html('WARNING');
            $(".modal-dialog").addClass('width_500');
            $("#sMessageDialogContent").html('Please agree our refund policy');
            $("#sMessageDialog").modal('show');
            return false;
        }
    });
    $(".enroll_it").click(function () {
        if (!$(this).attr('data-href')) {
            $("#sMessageDialogContent").html('No seat available');
            $("#sMessageDialog").modal('show');
        } else {
            var sURL = $(this).attr('data-href');
            window.location.href = sURL;
        }
    });
    $("#sEnrollNow").click(function () {
        $('#sEnrollNowModal').modal();
    });
    $("#sPrivactPolicy").click(function () {
        $("#sMessageDialogTitle").html('Privacy Policy');
        $(".modal-dialog").addClass('width_700');
        $("#sMessageDialogContent").html('Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.');
        $("#sMessageDialog").modal('show');
    });
    $("#sTermsAndCondition").click(function () {
        $("#sMessageDialogTitle").html('Terms & Conditions');
        $(".modal-dialog").addClass('width_700');
        $("#sMessageDialogContent").html('Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.');
        $("#sMessageDialog").modal('show');
    });
    /* Enrollment Date Selection End */

});
/* File Type Vefify Validation Start */
function validateFileType(id) {
    var fileName = document.getElementById(id).value;
    var idxDot = fileName.split(".");
    var extFile = idxDot[idxDot.length - 1].toLowerCase();
    if (extFile == "jpg" || extFile == "jpeg") {
        return true;
    } else {
        document.getElementById(id).value = '';
        alert("Only jpg/jpeg files are allowed!");
        return false;
    }
}
/* File Type Vefify Validation End */
