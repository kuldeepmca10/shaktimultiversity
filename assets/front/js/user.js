$(document).ready(function () {


    $('#sEnrollmentSessionDetail').click(function () {
        $("#sMessageDialogSessionDetail").modal('show');
    });

    $('#sUpdateProfileSubmit').click(function () {
        var sName = $('#name').val();
        var sEmailID = $('#email').val();
        var sCountryCode = $('#phone_code').val();
        var sMobileNo = $('#phone').val();
        if (!sName) {
            $("#sMessageDialogTitle").html('WARNING');
            $("#sMessageDialogContent").html('Please provide name');
            $("#sMessageDialog").modal('show');
        } else if (!sEmailID) {
            $("#sMessageDialogTitle").html('WARNING');
            $("#sMessageDialogContent").html('Please provide email');
            $("#sMessageDialog").modal('show');
        } else if (!sEmailID && !validateEmail(sEmailID)) {
            $("#sMessageDialogTitle").html('WARNING');
            $("#sMessageDialogContent").html('Please provide valid email');
            $("#sMessageDialog").modal('show');
        } else if (!sCountryCode || !validateEmail(sEmailID)) {
            $("#sMessageDialogTitle").html('WARNING');
            $("#sMessageDialogContent").html('Please provide country code');
            $("#sMessageDialog").modal('show');
        } else if (!sMobileNo || sMobileNo.length < 10 || !validateNumber(sMobileNo)) {
            $("#sMessageDialogTitle").html('WARNING');
            $("#sMessageDialogContent").html('Please provide valid phone number ');
            $("#sMessageDialog").modal('show');
        } else {
            $.ajax({
                type: "POST",
                url: "user/update-profile",
                data: $('#sUpdateForm').serialize(),
                cache: false,
                success: function (data) {
                    var data = jQuery.parseJSON(data);
                    if (data.status == true) {
                        $("#sMessageDialogIcon").html(data.icon);
                        $("#sMessageDialogTitle").html(data.title);
                        $("#sMessageDialogContent").html(data.message);
                        $("#name").val(data.data.name);
                        $("#email").val(data.data.email);
                        $("#phone_code").attr('selected', data.data.phone_code);
                        $("#phone").html(data.data.phone);
                        $("#sMessageDialog").modal('show');
                    } else {
                        $("#sMessageDialogIcon").html(data.icon);
                        $("#sMessageDialogTitle").html(data.title);
                        $("#sMessageDialogContent").html(data.message);
                        $("#sMessageDialog").modal('show');
                    }
                }
            });
        }
    });
    $('#sUpdatePasswordSubmit').click(function () {
        var sCurrentPassword = $('#sCurrentPassword').val();
        var sNewPassword = $('#sNewPassword').val();
        var sNewConfirmPassword = $('#sNewConfirmPassword').val();
        if (!sCurrentPassword) {
            $("#sMessageDialogTitle").html('WARNING');
            $("#sMessageDialogContent").html('Please provide current password');
            $("#sMessageDialog").modal('show');
        } else if (!sNewPassword) {
            $("#sMessageDialogTitle").html('WARNING');
            $("#sMessageDialogContent").html('Please provide new password');
            $("#sMessageDialog").modal('show');
        } else if (!sNewConfirmPassword) {
            $("#sMessageDialogTitle").html('WARNING');
            $("#sMessageDialogContent").html('Please provide confirm password');
            $("#sMessageDialog").modal('show');
        } else if ((sNewPassword && sNewConfirmPassword) && sNewPassword != sNewConfirmPassword) {
            $("#sMessageDialogTitle").html('WARNING');
            $("#sMessageDialogContent").html('New and Confirm password must be same');
            $("#sMessageDialog").modal('show');
        } else {
            $.ajax({
                type: "POST",
                url: "user/change-password",
                data: $('#sUpdatePassword').serialize(),
                cache: false,
                success: function (data) {
                    var data = jQuery.parseJSON(data);
                    if (data.status == true) {
                        $("#sMessageDialogIcon").html(data.icon);
                        $("#sMessageDialogTitle").html(data.title);
                        $("#sMessageDialogContent").html(data.message);
                        $('#sUpdatePassword')[0].reset();
                        $("#sMessageDialog").modal('show');
                    } else {
                        $("#sMessageDialogIcon").html(data.icon);
                        $("#sMessageDialogTitle").html(data.title);
                        $("#sMessageDialogContent").html(data.message);
                        $("#sMessageDialog").modal('show');
                    }
                }
            });
        }
    });
    $('#sendFeedbackSubmit').click(function () {


        var sFeedbackMessage = $('#sFeedbackMessage').val();
        if (!sFeedbackMessage) {
            $("#sMessageDialogTitle").html('WARNING');
            $("#sMessageDialogContent").html('Please enter your query');
            $("#sMessageDialog").modal('show');
        } else {
            $('#sLoaderRetrive').show();
            $.ajax({
                type: "POST",
                url: "user/enrollment-feedback/",
                data: $('#sFeedbackForm').serialize(),
                cache: false,
                success: function (data) {
                    var data = jQuery.parseJSON(data);
                    $('#sLoaderRetrive').hide();
                    $('#sFeedbackMessage').val('');
                    if (data.status == true) {
                        $("#sFeedbackDialog").modal('hide');
                        $("#sMessageDialogIcon").html(data.icon);
                        $("#sMessageDialogTitle").html(data.title);
                        $("#sMessageDialogContent").html(data.message);
                        $("#sMessageDialog").modal('show');
                    } else {
                        $("#sMessageDialogIcon").html(data.icon);
                        $("#sMessageDialogTitle").html(data.title);
                        $("#sMessageDialogContent").html(data.message);
                        $("#sMessageDialog").modal('show');
                    }
                }
            });
        }

    });
}
);
/* File Type Vefify Validation Start */
function sendFeedback(id) {
    if (confirm('Are you sure you want to contact to admin') == true) {
        $("#sEnrollmentID").val(id);
        $("#sFeedbackDialog").modal('show');
    }
}


/* Unsubscribe From Course Start */

function confirmBox(id) {
    if (confirm('Are you sure you want to unsubscribe from this course') == true) {
        $.ajax({
            url: "user/unsubscribe-enrollment/" + id,
            cache: false,
            success: function (data) {
                var data = jQuery.parseJSON(data);
                if (data.status == true) {
                    $("#sMessageDialogIcon").html(data.icon);
                    $("#sMessageDialogTitle").html(data.title);
                    $("#sMessageDialogContent").html(data.message);
                    $("#unsub-" + id).addClass('red');
                    $("#sMessageDialog").modal('show');
                } else {
                    $("#sMessageDialogIcon").html(data.icon);
                    $("#sMessageDialogTitle").html(data.title);
                    $("#sMessageDialogContent").html(data.message);
                    $("#sMessageDialog").modal('show');
                }
            }
        });
    }
}

/* File Type Vefify Validation Start */

function validateFileType(id) {
    var fileName = document.getElementById(id).value;
    var idxDot = fileName.split(".");
    var extFile = idxDot[idxDot.length - 1].toLowerCase();
    if (extFile == "jpg" || extFile == "jpeg") {
        return true;
    } else {
        document.getElementById(id).value = '';
        alert("Only jpg/jpeg files are allowed!");
        return false;
    }
}
/* File Type Vefify Validation End */

/* Email ID Validation Start */
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
/* Email ID Validation End */


/* Mobile Number Validation Start */
function validateNumber(Number) {
    var re = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/;
    return re.test(Number);
}
/* Mobile Number Validation End */


$(document).ready(function () {
    $("#sCompleteProfile").validate({
        rules: {
            alternative_email: {
                required: true,
                email: true
            },
            alternative_phone: {
                required: true,
//                number: true,
                minlength: 8,
                maxlength: 15,
            },
            skype_id: {
                required: true,
            },
//            hangout_id: {
//                required: true,
//            },
            other_id: {
                required: true,
            },
            profile_photo: {
                required: true,
            },
//            id_proof: {
//                required: true,
//            }
        },
        messages: {
            alternative_email: {
                required: "Please enter alternative email id",
                email: "Please enter valid alternative email id",
            },
            alternative_phone: {
                required: "Please enter alternative number",
//                required: "Please enter valid alternative number",
                minlength: "Number length should be between 8 to 15",
                maxlength: "Number length should be between 8 to 15",
            },
            skype_id: {
                required: "Please enter alternative skype id",
            },
//            hangout_id: {
//                required: "Please enter alternative hangout id",
//            },
            other_id: {
                required: "Please enter alternative other id",
            },
            profile_photo: {
                required: "Please upload profile photo",
            },
//            id_proof: {
//                required: "Please upload id proof",
//            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    $("#sRegisterForm").validate({
        rules: {
            name: "required",
            date: "required",
            month: "required",
            year: "required",
            address: "required",
            city: "required",
            state: "required",
            country: "required",
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 8,
                maxlength: 15,
            },
            contact_no: {
                required: true,
                number: true,
                minlength: 8,
                maxlength: 15,
            },
            gender: "required"
        },
        messages: {
            name: "Please enter name",
            dob: "Please select dob date",
            month: "Please select dob month",
            year: "Please select dob year",
            address: "Please enter address",
            city: "Please enter city",
            state: "Please enter state",
            country: "Please select country",
            email: {
                required: "Please enter email id",
                email: "Please enter valid email id"
            },
            password: {
                required: "Please enter password",
                minlength: "Password length should be 8 to 15",
                maxlength: "Password length should be 8 to 15",
            },
            contact_no: {
                required: "Please enter contact number",
                number: "Please enter valid contact number",
                maxlength: "Please enter valid contact number",
                minlength: "Please enter valid contact number",
            },
            gender: "Please select gender"
        },
        submitHandler: function (form) {
            $('#sLoaderRetrive').show();
            $('#sLoaderRetriveVerify').show();
            $('#sRegisterButton').attr('disabled', true);
            $('#sVerifyButton').attr('disabled', true);
            $.ajax({
                type: "POST",
                url: "register",
                data: $('#sRegisterForm').serialize(),
                cache: false,
                success: function (data) {
                    var data = jQuery.parseJSON(data);
                    $('#sLoaderRetrive').hide();
                    $('#sLoaderRetriveVerify').hide();
                    $('#sRegisterButton').attr('disabled', false);
                    $('#sVerifyButton').attr('disabled', false);
                    if (data.status == true) {

                        if (data.type == 'otp') {
                            $("#sMessageDialogIcon").html(data.icon);
                            $("#sMessageDialogTitle").html(data.title);
                            $("#sMessageDialogContent").html(data.message);
                            $("#sMessageDialog").modal('show');
                            $('#sRegisterationSection').hide();
                            $('#sOtpSection').show();
                        } else if (data.type == 'register') {
                            $('#sRegisterForm')[0].reset();
                            window.location.href = 'login';
                        }

                    } else {
                        $("#sMessageDialogIcon").html(data.icon);
                        $("#sMessageDialogTitle").html(data.title);
                        $("#sMessageDialogContent").html(data.message);
                        $("#sMessageDialog").modal('show');
                    }
                }
            });
//            form.submit();
        }
    });
    $("#sUpdateProfile").validate({
        rules: {
            name: "required",
            date: "required",
            month: "required",
            year: "required",
            address: "required",
            city: "required",
            state: "required",
            country: "required",
            email: {
                required: true,
                email: true
            },
            contact_no: {
                required: true,
                number: true,
                minlength: 8,
                maxlength: 15,
            },
            gender: "required",
            alternative_email: {
                required: true,
                email: true
            },
            alternative_phone: {
                required: true,
//                number: true,
                minlength: 8,
                maxlength: 15,
            },
            skype_id: {
                required: true,
            },
//            hangout_id: {
//                required: true,
//            },
            other_id: {
                required: true,
            }
        },
        messages: {
            name: "Please enter name",
            dob: "Please select dob date",
            month: "Please select dob month",
            year: "Please select dob year",
            address: "Please enter address",
            city: "Please enter city",
            state: "Please enter state",
            country: "Please select country",
            email: {
                required: "Please enter email id",
                email: "Please enter valid email id"
            },
            contact_no: {
                required: "Please enter contact number",
                number: "Please enter valid contact number",
                maxlength: "Please enter valid contact number",
                minlength: "Please enter valid contact number",
            },
            gender: "Please select gender",
            alternative_email: {
                required: "Please enter alternative email id",
                email: "Please enter valid alternative email id",
            },
            alternative_phone: {
                required: "Please enter alternative number",
//                required: "Please enter valid alternative number",
                minlength: "Number length should be between 8 to 15",
                maxlength: "Number length should be between 8 to 15",
            },
            skype_id: {
                required: "Please enter alternative skype id",
            },
            hangout_id: {
                required: "Please enter alternative hangout id",
            },
            other_id: {
                required: "Please enter alternative other id",
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
});
/* File Type Vefify Validation Start */

var _validFileExtensions = [".jpg", ".jpeg"];
function ValidateSingleInput(oInput) {
    if (oInput.type == "file") {
        var sFileName = oInput.value;
        if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }

            if (!blnValid) {
                $("#sMessageDialogTitle").html('WARNING');
                $("#sMessageDialogContent").html("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                $("#sMessageDialog").modal('show');
                oInput.value = "";
                return false;
            }
        }
    }
    return true;
}

var _validFileExtensions = [".jpg", ".jpeg", ".pdf"];
function ValidateSingleInputWithPdf(oInput) {
    if (oInput.type == "file") {
        var sFileName = oInput.value;
        if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }

            if (!blnValid) {
                $("#sMessageDialogTitle").html('WARNING');
                $("#sMessageDialogContent").html("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                $("#sMessageDialog").modal('show');
                oInput.value = "";
                return false;
            }
        }
    }
    return true;
}
/* File Type Vefify Validation End */

/* Update State List Of Selected Country Start */
$("#country").change(function () {
    var id = $('#country').val();
    var ar = id.split('_');
    if (ar[1] != '') {
        $('#country_code').val('+' + ar[1]);
    } else {
        $('#country_code').val('');
    }
    $.get("state/" + ar[0], function (data, status) {
        $("#state").html(data);
    });
});
/* Update State List Of Selected Country End */

setTimeout(function () {
    $(".session-message-dtl").slideUp(300);
}, 3000);

