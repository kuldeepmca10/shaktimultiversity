/** Enrollment Controller **/
app.controller("Enrollment", function ($scope, $http, $location, $state, $stateParams, Auth) {
    $scope.ajaxRes = false;
    $scope.data = {status: '1'};
    $scope.status_enrollments = [{value: '', name: 'Select Status'}, {value: 'pending for approval', name: 'Pending For Approval'}, {value: 'approved', name: 'Approved'}, {value: 'rejected', name: 'Rejected'}];
    $scope.payment_gateway = [{value: '', name: 'Select Type'}, {value: 'PayU', name: 'PayU'}, {value: 'paypal', name: 'paypal'}, {value: '', name: 'None'}];

    $scope.list_enrollments = function () {
        show_ajax_loader();
        $http({url: ADM_URL + 'student/course_enrollment', params: $location.search()}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.result = res.enrollmentlist.result;
                    $scope.page = res.enrollmentlist.page;
                    $scope.cats = res.cats;

                    $scope.ajaxRes = true;
                    hide_ajax_loader();
                }
        );
    }
    $scope.export_enrollment = function (init) {
        var cat_id = $scope.data.cat_id;
        var k = $scope.data.k;
        window.location.href = ADM_URL + 'student/export_enrollment?k=' + k + '&cat_id=' + cat_id;
    }
    $scope.search_enrollments = function (e, p) {
        e.preventDefault();
        if (p) {
            $scope.data.p = p;
        } else {
            $scope.data.p = '';
        }
        var d = $scope.remove_empty($scope.data);
        delete d['status'];
        $location.search(d);
        $scope.list_enrollments();
    }

    $scope.course_enrollment = function (userid, username) {
        $scope.user_id = userid;
        $scope.data.userid = userid;
        $scope.username = username;
        show_ajax_loader();
        $http({url: ADM_URL + 'student/course_enrollment/' + userid}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.enrollmentlist = res.enrollmentlist.result;
                    $scope.enrollmentpage = res.enrollmentlist.page;
                    $scope.status_enrollments = [{value: '', name: 'Select Status'}, {value: 'pending for approval', name: 'Pending For Approval'}, {value: 'approved', name: 'Approved'}, {value: 'rejected', name: 'Rejected'}];
                    $("#enrollmentListModal").modal();
                }
        );
    }

    $scope.course_enrollment_detail = function (id, userid) {
        $scope.user_id = userid;
        $scope.data.userid = userid;
        show_ajax_loader();
        $http({url: ADM_URL + 'student/course_enrollment_detail/' + id + '/' + userid}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.enrollmentDetail = res.enrollmentlist.result[0];
                    $("#enrollmentDetailModal").modal();
                }
        );
    }


    $scope.update_enrollment_change = function (id, username, data) {
        var statusValue = $('#enroll_status_' + id).val();
        $scope.sEnrollStatus = data.approval_status;
        $scope.sEnrolComment = data.comment;
        $scope.iEnrolID = id;
        if (statusValue != "") {
            if (confirm("Are you sure you want to change your status") == true) {
                $("#updateEnrollmentModal").modal();
                $("#updateEnrollmentModal").attr('style', 'z-index:11000px;display:block');
            }
        } else {
            $('#enroll_status_' + id).val('');
        }

    }

    $scope.update_enrollment_status = function () {
        var statusValue = $scope.sEnrollStatus;
        var id = $scope.iEnrolID;

        var comment = $scope.sEnrolComment;

        if (statusValue == "" || statusValue == undefined) {
            $scope.commentError = "Please select status";
        } else if (comment == "" || comment == undefined) {
            $scope.commentError = "Please provide comment";
        } else {
            $scope.commentError = "";
            show_ajax_loader();
            $http({url: ADM_URL + 'student/update_enrollment_status', method: 'POST', data: "id=" + id + "&status=" + statusValue + "&comment=" + comment}).success(
                    function (res) {
                        hide_ajax_loader();
                        res = Auth.check_res(res);
                        if (res.success == 'T') {
                            show_alert_msg(res.msg);
                            $scope.sEnrollStatus = '';
                            $scope.iEnrolID = '';
                            $scope.sEnrolComment = '';
                            $("#updateEnrollmentModal").modal('hide');
                            $scope.course_enrollment($scope.data.userid, $scope.data.username);
                            $scope.list_enrollments();

                        } else {
                            show_form_errors(res.msg);
                        }
                    }
            );
        }


    }

    /** Inits **/
    $scope.init = function () {
        set_tab_box();
        $scope.status_lookups = $scope.status_lookup();
        $scope.country_phonecodes = $scope.country_phonecode();

        $scope.status_lookups = $scope.status_lookup();
        show_ajax_loader();
        $http({url: ADM_URL + 'user/user_detail/' + $stateParams.id}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.roles = res.roles;
                    if (typeof res.dtl != "undefined") {
                        if (!res.dtl.role_id) {
                            $state.go('notFound');
                        }
                        $scope.data = res.dtl;
                    }
                    hide_ajax_loader();
                }
        );
        $scope.list_enrollments();
        $scope.data = $location.search();
    }
    $scope.init();
});

//EOF