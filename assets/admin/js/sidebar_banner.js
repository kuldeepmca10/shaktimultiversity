angular.module('app').controller("SidebarBanner", function ($scope, $http, $state, $location, Auth) {
    $scope.ajaxRes = false;
    $scope.sform_data = {};
    $scope.dtl = {status: '1', cat_id: ''};
    $scope.types = [{id: 'link', title: 'Link'}, {id: 'no_link', title: 'No Link'}];
    $scope.list_galleries = function (init) {
        show_ajax_loader();
        $http({url: ADM_URL + 'pro/sidebar_banner/' + init, params: $location.search()}).success(
                function (res) {
                    res = Auth.check_res(res);

                    $scope.result = res.result;
                    $scope.page = res.page;
                    if (init == 'T') {
                        $scope.cats = res.cats;
                    }

                    $scope.ajaxRes = true;
                    hide_ajax_loader();
                }
        );
    }

    $scope.search_gallery = function (e, p) {
        e.preventDefault();
        if (p) {
            $scope.sform_data.p = p;
        } else {
            $scope.sform_data.p = '';
        }
        $location.search($scope.remove_empty($scope.sform_data));
        $scope.list_galleries();
    }

    $scope.add_gallery = function () {
        $scope.dtl = {status: '1', cat_id: ''};
        hide_form_errors($("#bannerForm"));
        $("[name='type'][value='I']").prop('checked', true);

        $(".type_V").hide();
        $(".type_I").show();

        $("#bannerFormModal").modal();
    }
    $scope.save_banner = function (e) {
        e.preventDefault();
        var frm = $("#bannerForm");
        hide_form_errors(frm);

        var formData = new FormData(frm[0]);
        $scope.course_saving = true;
        $http({url: ADM_URL + 'pro/save_banner', method: 'POST', data: formData, headers: {'Content-Type': undefined}}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.course_saving = false;
                    if (res.success == 'T') {
                        var msg = $scope.dtl.id ? 'Gallery updated successfully.' : 'Gallery added successfully.';
                        show_alert_msg(msg);
                        $scope.list_galleries();
                        $("#bannerFormModal").modal('hide');
                    } else {
                        show_form_errors(res.errors, frm);
                        show_alert_msg("Error!", 'E');
                    }
                }
        );
    }


    $scope.add_gallery_images = function () {
        $scope.dtl = {status: '1', gallery_id: $scope.main_gallery_id};
        hide_form_errors($("#AddImagesForm"));
        $("[name='type'][value='I']").prop('checked', true);
        $("#image").val('');
        $("#AddImagesFormModal").modal();
    }


    $scope.save_banner_images = function (e) {
        e.preventDefault();
        var frm = $("#AddImagesForm");
        hide_form_errors(frm);

        var formData = new FormData(frm[0]);
        $scope.course_saving = true;
        $http({url: ADM_URL + 'pro/save_banner_images', method: 'POST', data: formData, headers: {'Content-Type': undefined}}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.course_saving = false;
                    if (res.success == 'T') {
                        var msg = $scope.dtl.id ? 'Gallery Image updated successfully.' : 'Gallery Image added successfully.';
                        show_alert_msg(msg);
                        $scope.imageList = res.imageList;
                        $("#AddImagesFormModal").modal('hide');
                    } else {
                        show_form_errors(res.errors, frm);
                        show_alert_msg("Error!", 'E');
                    }
                }
        );
    }

    $scope.view_gallery_images = function (gallery_id) {
        hide_form_errors($("#bannerForm"));
        show_ajax_loader();
        $http({url: ADM_URL + 'pro/gallery_images_list/' + gallery_id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.main_gallery_id = gallery_id;
                    $scope.imageList = res;
                    $("#GalleryListModal").modal();
                }
        );
    }

    $scope.edit_gallery = function (id) {

        hide_form_errors($("#bannerForm"));
        show_ajax_loader();
        $http({url: ADM_URL + 'pro/sidebar_banner_dtl/' + id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.dtl = res;
                    $("#banner_image").val('');
                    $("#bannerFormModal").modal();
                }
        );
    }

    $scope.edit_gallery_image = function (id) {
        hide_form_errors($("#AddImagesForm"));
        show_ajax_loader();
        $http({url: ADM_URL + 'pro/gallery_images_dtl/' + id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.dtl = res;
                    $("#AddImagesFormModal").modal();
                }
        );
    }

    $scope.delete_gallery_image = function (id, gallery_id, image) {
        if (!confirm("Are you sure to delete?")) {
            return;
        }

        show_ajax_loader('Deleting');
        $http({url: ADM_URL + 'pro/delete_gallery_image', method: 'POST', data: $.param({id: id, image: image, gallery_id: gallery_id})}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        $scope.imageList = res.imageList;
                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }

    $scope.delete_gallery = function (id, image) {
        if (!confirm("Are you sure to delete?")) {
            return;
        }

        show_ajax_loader('Deleting');
        $http({url: ADM_URL + 'pro/delete_gallery', method: 'POST', data: $.param({id: id, image: image})}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        $scope.list_galleries();
                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }

    /** Inits **/
    $scope.init = function () {
        set_numeric_input();
        $scope.status_lookups = $scope.status_lookup();

        $scope.list_galleries('T');
        $scope.sform_data = $location.search();
        set_datepicker($("#publish_date"));

        $("[name='type']").change(function () {
            var v = $(this).val();
            $(".type_I,.type_V").hide();
            $(".type_" + v).show();
        });
    }
    $scope.init();
});

/** Non Angular **/

//EOF