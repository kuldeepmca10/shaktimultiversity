angular.module('app').controller("Newsletter", function($scope, $http, $state, $location, Auth) {

    $scope.ajaxRes = false;
    $scope.sform_data = {};
    $scope.list_newsletters = function(init) {
        show_ajax_loader();
        $http({url: ADM_URL + 'user/newsletter/' + init, params: $location.search()}).success(
                function(res) {
                    res = Auth.check_res(res);

                    $scope.result = res.result;
                    $scope.page = res.page;
                    $scope.ajaxRes = true;
                    hide_ajax_loader();
                }
        );
    }
    $scope.export_newsletter = function(init) {
        window.location.href = ADM_URL + 'user/export_newsletter/' + init;
    }

    $scope.search_newsletter = function(e, p) {
        e.preventDefault();
        if (p) {
            $scope.sform_data.p = p;
        } else {
            $scope.sform_data.p = '';
        }
        $location.search($scope.remove_empty($scope.sform_data));
        $scope.list_newsletters();
    }

    $scope.save_newsletter = function(e) {
        e.preventDefault();
        var frm = $("#newsletterForm");
        hide_form_errors(frm);

        var formData = new FormData(frm[0]);
        $scope.course_saving = true;
        $http({url: ADM_URL + 'user/save_newsletter', method: 'POST', data: formData, headers: {'Content-Type': undefined}}).success(
                function(res) {
                    res = Auth.check_res(res);
                    $scope.course_saving = false;
                    if (res.success == 'T') {
                        var msg = $scope.dtl.id ? 'Newsletter subscriber updated successfully.' : 'Newsletter subscriber  added successfully.';
                        show_alert_msg(msg);
                        $scope.list_newsletters();
                        $("#newsletterFormModal").modal('hide');
                    } else {
                        show_form_errors(res.errors, frm);
                        show_alert_msg("Error!", 'E');
                    }
                }
        );
    }

    $scope.edit_newsletter = function(id) {
        hide_form_errors($("#newsletterForm"));
        $("#newsletterForm").find("input[type='file']").val('');
        show_ajax_loader();
        $http({url: ADM_URL + 'user/newsletter_detail/' + id}).success(
                function(res) {
                    hide_ajax_loader();
                    console.log(res);
                    res = Auth.check_res(res);
                    $scope.dtl = res;
                   $("#newsletterFormModal").modal();
                }
        );
    }

    $scope.delete_newsletter = function(id, image) {
        if (!confirm("Are you sure to delete?")) {
            return;
        }

        show_ajax_loader('Deleting');
        $http({url: ADM_URL + 'user/delete_newsletter', method: 'POST', data: $.param({id: id})}).success(
                function(res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        $scope.list_newsletters();
                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }


    /** Inits **/
    $scope.init = function() {
        set_numeric_input();
        $scope.status_lookups = $scope.status_lookup();
        //tinymce_insert_file_init();

        set_datepicker($("#start_date"));

        $scope.weekdays = [{k: 'Monday', v: 'Mon'}, {k: 'Tuesday', v: 'Tue'}, {k: 'Wednesday', v: 'Wed'}, {k: 'Thursday', v: 'Thu'}, {k: 'Friday', v: 'Fri'}, {k: 'Saturday', v: 'Sat'}, {k: 'Sunday', v: 'Sun'}];

        $scope.list_newsletters('T');
        $scope.sform_data = $location.search();
    }
    $scope.init();
});

//EOF