function set_active_nav(cur_url){
	$(".leftbar ul > li > a").removeClass('act');
	
	$(".leftbar ul > li > a").each(function(){
		var href=$(this).attr('ui-sref');
		var href1=$(this).attr('href');
		var child_links=[];
		if($(this).attr('childlinks')){
			child_links=$(this).attr('childlinks').split(',');
		}
		if(href){
			child_links.push(href);
		}
		if(href1){
			child_links.push(href1);
		}
		for(i=0; i<child_links.length; i++){
			if(cur_url.indexOf(child_links[i])!=-1){
				$(this).addClass('act');
			}
		}
	});
}


/** Events **/
$(function () {
    /** Initialization **/
	set_active_nav(location.href);
	
    set_datepicker();
	set_page_links();
	set_numeric_input();
	set_tab_box();
	set_confirm_link();
});

/** In case of multiple bootstrap modal zindex issue **/
$(document).on('show.bs.modal', '.modal', function () {
    var zIndex = 1040 + (10 * $('.modal:visible').length);
    $(this).css('z-index', zIndex);
    setTimeout(function() {
        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
    }, 0);
});

//EOF