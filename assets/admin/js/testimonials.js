angular.module('app').controller("Testimonial", function($scope, $http, $state, $location, Auth){
	$scope.ajaxRes=false;
	$scope.sform_data={};
	$scope.list_testimonials=function(init){
		show_ajax_loader();
		$http({url: ADM_URL+'master/testimonials/'+init, params:$location.search()}).success(
			function(res){
				res=Auth.check_res(res);
				
				$scope.result=res.result;
				$scope.page=res.page;
				if(init=='T'){
					$scope.cats=res.cats;
				}
				
				$scope.ajaxRes=true;
				hide_ajax_loader();
			}
		);
	}
	
	$scope.search_testimonial=function(e, p){
		e.preventDefault();
		if(p){
			$scope.sform_data.p=p;
		}else{
			$scope.sform_data.p='';
		}
		$location.search($scope.remove_empty($scope.sform_data));
		$scope.list_testimonials();
	}
	
	$scope.add_testimonial=function(){
		$scope.dtl={status:'1'};
		hide_form_errors($("#testiform"));
		$("#testiform").find("input[type='file']").val('');
		$("#testiFormModal").modal();
	}
	
	$scope.save_testimonial=function(e){
		e.preventDefault();
		var frm=$("#testiform");
		hide_form_errors(frm);
		
		var formData=new FormData(frm[0]);
		$scope.t_saving=true;
		$http({url:ADM_URL+'master/save_testimonial', method:'POST', data:formData, headers:{'Content-Type': undefined}}).success(
			function(res) {
				res=Auth.check_res(res);
				$scope.t_saving=false;
				if(res.success=='T'){
					var msg=$scope.dtl.id?'Testimonial updated successfully.':'Testimonial added successfully.';
					show_alert_msg(msg);
					$scope.list_testimonials();
					$("#testiFormModal").modal('hide');
				}else{
					show_form_errors(res.errors, frm);
					show_alert_msg("Error!", 'E');
				}
			}
		);
	}
	
	$scope.edit_testimonial=function(id){
		hide_form_errors($("#testiform"));
		$("#testiform").find("input[type='file']").val('');
		show_ajax_loader();
		$http({url: ADM_URL+'master/testimonial_dtl/'+id}).success(
			function(res){
				hide_ajax_loader();
				res=Auth.check_res(res);
				$scope.dtl=res;
				$("#testiFormModal").modal();
			}
		);
	}
	
	$scope.delete_testimonial=function(id, image){
		if(!confirm("Are you sure to delete?")){
			return;
		}
		
		show_ajax_loader('Deleting');
		$http({url: ADM_URL+'master/delete_testimonial', method:'POST', data:$.param({id:id, image:image})}).success(
			function(res){
				hide_ajax_loader();
				res=Auth.check_res(res);
				if(res.success=='T'){
					show_alert_msg(res.msg);
					$scope.list_testimonials();
				}else{
					show_alert_msg(res.msg, 'E');
				}
			}
		);
	}
	
	/** Inits **/
	$scope.init=function(){
		set_numeric_input();
		$scope.status_lookups=$scope.status_lookup();
		set_datepicker($("#publish_date"));
		
		$scope.list_testimonials('T');
		$scope.sform_data=$location.search();
	}
	$scope.init();
});

//EOF