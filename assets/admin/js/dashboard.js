/** Dashboard Controller **/
angular.module('app').controller("Dashboard", function($scope, $http, Auth){
	/** Init **/
	$scope.init=function(){
		show_ajax_loader();
		$http({url: ADM_URL+'dashboard'}).success(
			function(res){
				hide_ajax_loader();
				res=Auth.check_res(res);
			}
		);
	}
	$scope.init();
});

//EOF