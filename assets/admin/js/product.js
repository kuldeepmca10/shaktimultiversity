angular.module('app').controller("Product", function ($scope, $http, $state, $location, Auth) {
    $scope.ajaxRes = false;
    $scope.sform_data = {};
    $scope.list_products = function (init) {
        show_ajax_loader();
        $http({url: ADM_URL + 'pro/products/' + init, params: $location.search()}).success(
                function (res) {
                    res = Auth.check_res(res);

                    $scope.result = res.result;
                    $scope.page = res.page;
                    if (init == 'T') {
                        $scope.cats = res.cats;
                    }

                    $scope.ajaxRes = true;
                    hide_ajax_loader();
                }
        );
    }

    $scope.search_product = function (e, p) {
        e.preventDefault();
        if (p) {
            $scope.sform_data.p = p;
        } else {
            $scope.sform_data.p = '';
        }
        $location.search($scope.remove_empty($scope.sform_data));
        $scope.list_products();
    }

    $scope.add_product = function () {
        $scope.dtl = {price_type: 'free', show_on_home: '0', status: '1'};
        tinymce.EditorManager.get('pdesc').setContent('');
        hide_form_errors($("#proform"));
        $("#proform").find("input[type='file']").val('');
        $("#proFormModal").modal();
    }

    $scope.save_product = function (e) {
        e.preventDefault();
        var frm = $("#proform");
        hide_form_errors(frm);
        tinyMCE.triggerSave(false, true);

        var formData = new FormData(frm[0]);
        $scope.pro_saving = true;
        $http({url: ADM_URL + 'pro/save_product', method: 'POST', data: formData, headers: {'Content-Type': undefined}}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.pro_saving = false;
                    if (res.success == 'T') {
                        var msg = $scope.dtl.id ? 'Product updated successfully.' : 'Product added successfully.';
                        show_alert_msg(msg);
                        $scope.list_products();
                        $("#proFormModal").modal('hide');
                    } else {
                        show_form_errors(res.errors, frm);
                        show_alert_msg("Error!", 'E');
                    }
                }
        );
    }

    $scope.edit_product = function (id) {
        hide_form_errors($("#proform"));
        $("#proform").find("input[type='file']").val('');
        show_ajax_loader();
        $http({url: ADM_URL + 'pro/product_dtl/' + id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.dtl = res;
                    tinymce.EditorManager.get('pdesc').setContent(res.description);
                    $("#proFormModal").modal();
                }
        );
    }

    $scope.delete_product = function (id) {
        if (!confirm("Are you sure to delete?")) {
            return;
        }

        show_ajax_loader('Deleting');
        $http({url: ADM_URL + 'pro/delete_product', method: 'POST', data: $.param({id: id})}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        $scope.list_products();
                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }

    /** Images **/
    $scope.product_images = function (pro_id, pro_title) {
        $scope.pro_id = pro_id;
        $scope.pro_title = pro_title;
        show_ajax_loader();
        $http({url: ADM_URL + 'pro/product_images/' + pro_id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.images = res;
                    $("#imageModal").modal();
                }
        );
    }

    $scope.add_image = function () {
        $scope.sdtl = {};
        hide_form_errors($("#imageform"));
        $("#imageform").find("input[type='file']").val('');
        $("#imageFormModal").modal();
    }

    $scope.save_image = function (e) {
        e.preventDefault();
        var frm = $("#imageform");
        hide_form_errors(frm);

        var formData = new FormData(frm[0]);
        $scope.image_saving = true;
        $http({url: ADM_URL + 'pro/save_product_image', method: 'POST', data: formData, headers: {'Content-Type': undefined}}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.image_saving = false;
                    if (res.success == 'T') {
                        var msg = $scope.sdtl.id ? 'Image updated successfully.' : 'Image added successfully.';
                        show_alert_msg(msg);
                        $scope.images = res.images;
                        $("#imageFormModal").modal('hide');
                    } else {
                        show_form_errors(res.errors, frm);
                    }
                }
        );
    }

    $scope.edit_image = function (id) {
        hide_form_errors($("#imageform"));
        $("#imageform").find("input[type='file']").val('');
        show_ajax_loader();
        $http({url: ADM_URL + 'pro/product_image_dtl/' + id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.sdtl = res;
                    $("#imageFormModal").modal();
                }
        );
    }

    $scope.delete_image = function (id, image, pro_id) {
        show_ajax_loader('Deleting');
        $http({url: ADM_URL + 'pro/delete_product_image', method: 'POST', data: $.param({id: id, image: image, pro_id: pro_id})}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        $scope.images = res.images;
                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }

    /** Inits **/
    $scope.init = function () {
        set_numeric_input();
        $scope.status_lookups = $scope.status_lookup();
        set_tinymce("#pdesc", 200);

        $scope.list_products('T');
        $scope.sform_data = $location.search();
    }
    $scope.init();
});

//EOF