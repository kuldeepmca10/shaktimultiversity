angular.module('app').controller("BookOrderRequest", function ($scope, $http, $state, $location, Auth) {

    $scope.ajaxRes = false;
    $scope.sform_data = {};
    $scope.list_book_request = function (init) {
        show_ajax_loader();
        $http({url: ADM_URL + 'user/book_request/' + init, params: $location.search()}).success(
                function (res) {
                    res = Auth.check_res(res);

                    $scope.result = res.result;
                    $scope.page = res.page;
                    $scope.ajaxRes = true;
                    hide_ajax_loader();
                }
        );
    }
    $scope.export_service = function (init) {
        window.location.href = ADM_URL + 'user/export_service/' + init;
    }

    $scope.search_service = function (e, p) {
        e.preventDefault();
        if (p) {
            $scope.sform_data.p = p;
        } else {
            $scope.sform_data.p = '';
        }
        $location.search($scope.remove_empty($scope.sform_data));
        $scope.list_services();
    }

    $scope.save_service = function (e) {
        e.preventDefault();
        var frm = $("#serviceForm");
        hide_form_errors(frm);

        var formData = new FormData(frm[0]);
        $scope.course_saving = true;
        $http({url: ADM_URL + 'user/save_service', method: 'POST', data: formData, headers: {'Content-Type': undefined}}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.course_saving = false;
                    if (res.success == 'T') {
                        var msg = $scope.dtl.id ? 'Newsletter subscriber updated successfully.' : 'Newsletter subscriber  added successfully.';
                        show_alert_msg(msg);
                        $scope.list_services();
                        $("#serviceFormModal").modal('hide');
                    } else {
                        show_form_errors(res.errors, frm);
                        show_alert_msg("Error!", 'E');
                    }
                }
        );
    }

    $scope.edit_service = function (id) {
        hide_form_errors($("#serviceForm"));
        $("#serviceForm").find("input[type='file']").val('');
        show_ajax_loader();
        $http({url: ADM_URL + 'user/service_detail/' + id}).success(
                function (res) {
                    hide_ajax_loader();
                    console.log(res);
                    res = Auth.check_res(res);
                    $scope.dtl = res;
                    $("#serviceFormModal").modal();
                }
        );
    }

    $scope.delete_service = function (id, image) {
        if (!confirm("Are you sure to delete?")) {
            return;
        }

        show_ajax_loader('Deleting');
        $http({url: ADM_URL + 'user/delete_service', method: 'POST', data: $.param({id: id})}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        $scope.list_services();
                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }


    /** Inits **/
    $scope.init = function () {
        set_numeric_input();
        $scope.status_lookups = $scope.status_lookup();
        //tinymce_insert_file_init();

        set_datepicker($("#start_date"));

        $scope.weekdays = [{k: 'Monday', v: 'Mon'}, {k: 'Tuesday', v: 'Tue'}, {k: 'Wednesday', v: 'Wed'}, {k: 'Thursday', v: 'Thu'}, {k: 'Friday', v: 'Fri'}, {k: 'Saturday', v: 'Sat'}, {k: 'Sunday', v: 'Sun'}];

        $scope.list_book_request('T');
        $scope.sform_data = $location.search();
    }
    $scope.init();
});

//EOF