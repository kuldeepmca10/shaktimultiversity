app.controller("App", function($scope, $rootScope, $http, $state, Auth){
	$scope.login=function(e){
		e.preventDefault();
		var frm=$("#loginform");
		$scope.login_submit=true;
		$scope.formerr=false;
		frm.find(".smsg").hide();
		hide_form_errors(frm);
		$http({url: ADM_URL+'auth/login', method: 'POST', data: frm.serialize()}).success(
			function(res) {
				$scope.login_submit=false;
				if(res.success=='T'){
					Auth.setUser(res);
					Auth.redirectLogged();
				}else{
					if(res.msg){
						$scope.formerr=true;
						$scope.errmsg=res.msg;
					}
					show_form_errors(res.errors, frm);
				}
			}
		);
	}
	
	/** Inits **/
	Auth.redirectLogged();
});

/** Leftbar Ctrl **/
app.controller("Leftbar", function($scope, $http){
});