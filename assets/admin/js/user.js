/** Profile Controller **/
app.controller("Profile", function ($scope, $http, Auth) {
    /** Profile Update **/
    $scope.update_profile = function (e) {
        e.preventDefault();
        var frm = $("#profileform");
        $scope.profile_submit = true;
        hide_form_errors(frm);
        $http({url: ADM_URL + 'user/update_profile', method: 'POST', data: frm.serialize()}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.profile_submit = false;
                    if (res.success == 'T') {
                        $scope.user_id = res.id;
                        show_alert_msg("Profile updated successfully");
                    } else {
                        show_form_errors(res.errors, frm);
                    }
                }
        );
    }

    /** Change Password **/
    $scope.change_pass = function (e) {
        e.preventDefault();
        var frm = $("#passform");
        $scope.pass_submit = true;

        hide_form_errors(frm);
        $http({url: ADM_URL + 'user/change_pass', method: 'POST', data: frm.serialize()}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.pass_submit = false;
                    if (res.success == 'T') {
                        show_alert_msg("Password changed successfully");
                        frm[0].reset();
                    } else {
                        show_form_errors(res.errors, frm);
                    }
                }
        );
    }

    /** Inits **/
    $scope.init = function () {
        set_tab_box();
        $(".leftbar ul > li > a").removeClass('act');

        show_ajax_loader();
        $http({url: ADM_URL + 'user/profile'}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.profile = res;
                    hide_ajax_loader();
                }
        );
    }

    $scope.init();
});


/** Role Controller **/
app.controller("RoleList", function ($scope, $http, $location, Auth) {
    $scope.ajaxRes = false;
    $scope.sform_data = {};
    $scope.list_roles = function () {
        show_ajax_loader();

        $http({url: ADM_URL + 'user/roles', params: $location.search()}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.roles = res;
                    $scope.ajaxRes = true;

                    hide_ajax_loader();
                }
        );
    }

    $scope.search_roles = function (e) {
        e.preventDefault();
        $location.search($scope.remove_empty($scope.sform_data));
        $scope.list_roles();
    }

    /** Inits **/
    $scope.init = function () {
        $scope.list_roles();
        $scope.sform_data = $location.search();
    }
    $scope.init();
});

app.controller("RoleAdd", function ($scope, $http, $state, $stateParams, Auth) {
    $scope.data = {status: '1'};
    $scope.save_role = function (e) {
        e.preventDefault();
        var frm = $("#roleform");

        hide_form_errors(frm);
        $scope.role_submit = true;
        $http({url: ADM_URL + 'user/save_role', method: 'POST', data: $.param($scope.data)}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.role_submit = false;
                    if (res.success == 'T') {
                        show_alert_msg("Role " + ($scope.data.id ? 'Updated' : 'added') + " successfully");
                        $state.go("roles");
                    } else {
                        show_form_errors(res.errors, frm);
                    }
                }
        );
    }

    /** Inits **/
    $scope.init = function () {
        if ($state.current.name == 'edit_role' && !$stateParams.id) {
            $state.go('notFound');
        }

        $scope.status_lookups = $scope.status_lookup();
        show_ajax_loader();
        $http({url: ADM_URL + 'user/role_detail/' + $stateParams.id}).success(
                function (res) {
                    res = Auth.check_res(res);
                    if (typeof res.dtl != "undefined") {
                        if (!res.dtl.id) {
                            $state.go('notFound');
                        }
                        $scope.data = res.dtl;
                    }

                    hide_ajax_loader();
                }
        );
    }
    $scope.init();
});

app.controller("RoleAccess", function ($scope, $http, $state, $stateParams, Auth) {
    $scope.data = {};

    $scope.is_access = function (access_id, per) {
        var s = false;
        angular.forEach($scope.data.permissions, function (v, k) {
            if (v.access_id == access_id) {
                if (v[per] == 'Y') {
                    s = true;
                    return false;
                }
            }
        });

        return s;
    }

    $scope.save_permissions = function (e) {
        e.preventDefault();
        var frm = $("#roleperform");
        $scope.per_submit = true;
        $http({url: ADM_URL + 'user/save_permissions', method: 'POST', data: frm.serialize()}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.per_submit = false;
                    if (res.success == 'T') {
                        show_alert_msg("Role permissions saved successfully");
                    } else {
                        show_alert_msg("Error! Could not save. Try again.", "E");
                    }
                }
        );
    }

    /** Inits **/
    $scope.init = function () {
        if (!$stateParams.id) {
            $state.go('notFound');
        }

        show_ajax_loader();
        $http({url: ADM_URL + 'user/role_access_detail/' + $stateParams.id}).success(
                function (res) {
                    res = Auth.check_res(res);
                    if (typeof res.role_dtl != "undefined") {
                        if (!res.role_dtl.id) {
                            $state.go('notFound');
                        }
                        $scope.data = res;
                    }

                    hide_ajax_loader();
                }
        );
    }

    $scope.init();
});


/** User Controller **/
app.controller("User", function ($scope, $http, $location, $state, $stateParams, Auth) {
    $scope.ajaxRes = false;
    $scope.data = {status: '1'};

    $scope.list_users = function () {
        show_ajax_loader();
        $http({url: ADM_URL + 'user/users', params: $location.search()}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.result = res.result;
                    $scope.page = res.page;
                    $scope.roles = res.roles;
                    $scope.ajaxRes = true;
                    hide_ajax_loader();
                }
        );
    }

    $scope.search_users = function (e, p) {
        e.preventDefault();
        if (p) {
            $scope.data.p = p;
        } else {
            $scope.data.p = '';
        }
        var d = $scope.remove_empty($scope.data);
        delete d['status'];
        $location.search(d);
        $scope.list_users();
    }

    $scope.save_user = function (e) {
        e.preventDefault();
        var frm = $("#userform");

        hide_form_errors(frm);
        $scope.user_submit = true;
        $http({url: ADM_URL + 'user/save_user', method: 'POST', data: $.param($scope.data)}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.user_submit = false;
                    if (res.success == 'T') {
                        show_alert_msg("User " + ($scope.data.id ? 'Updated' : 'added') + " successfully");
                        $state.go("users");
                    } else {
                        show_form_errors(res.errors, frm);
                    }
                }
        );
    }

    /** Change Password **/
    $scope.change_pass = function (e) {
        e.preventDefault();
        var frm = $("#passform");
        $scope.pass_submit = true;

        hide_form_errors(frm);
        $http({url: ADM_URL + 'user/change_pass', method: 'POST', data: frm.serialize()}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.pass_submit = false;
                    if (res.success == 'T') {
                        show_alert_msg("Password changed successfully");
                        frm[0].reset();
                    } else {
                        show_form_errors(res.errors, frm);
                    }
                }
        );
    }

    /** Inits **/
    $scope.init = function () {
        set_tab_box();

        if ($state.current.name == 'edit_user' && !$stateParams.id) {
            $state.go('notFound');
        }

        if ($state.current.name == 'users') {
            $scope.list_users();
            $scope.data = $location.search();
        } else {
            $scope.status_lookups = $scope.status_lookup();
            show_ajax_loader();
            $http({url: ADM_URL + 'user/user_detail/' + $stateParams.id}).success(
                    function (res) {
                        res = Auth.check_res(res);
                        $scope.roles = res.roles;
                        if (typeof res.dtl != "undefined") {
                            if (!res.dtl.role_id) {
                                $state.go('notFound');
                            }
                            $scope.data = res.dtl;
                        }
                        hide_ajax_loader();
                    }
            );
        }
    }
    $scope.init();
});

//EOF