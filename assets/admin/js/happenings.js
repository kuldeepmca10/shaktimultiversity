angular.module('app').controller("Happenings", function ($scope, $http, $state, $location, Auth) {
    $scope.ajaxRes = false;
    $scope.sform_data = {};
    $scope.list_happenings = function () {
        show_ajax_loader();
        $http({url: ADM_URL + 'pro/happenings', params: $location.search()}).success(
                function (res) {
                    res = Auth.check_res(res);

                    $scope.result = res.result;
                    $scope.cats = res.cats;
                    $scope.page = res.page;

                    $scope.ajaxRes = true;
                    hide_ajax_loader();
                }
        );
    }

    $scope.search_happenings = function (e, p) {
        e.preventDefault();
        if (p) {
            $scope.sform_data.p = p;
        } else {
            $scope.sform_data.p = '';
        }
        $location.search($scope.remove_empty($scope.sform_data));
        $scope.list_happenings();
    }

    $scope.add_happenings = function () {
        $scope.dtl = {show_on_home: '0', status: '1'};
        tinymce.EditorManager.get('ndesc').setContent('');
        hide_form_errors($("#happeningsform"));
        $("#happeningsform").find("input[type='file']").val('');
        $("#happeningsFormModal").modal();
    }

    $scope.save_happenings = function (e) {
        e.preventDefault();
        var frm = $("#happeningsform");
        hide_form_errors(frm);
        tinyMCE.triggerSave(false, true);

        var formData = new FormData(frm[0]);
        $scope.happenings_saving = true;
        $http({url: ADM_URL + 'pro/save_happenings', method: 'POST', data: formData, headers: {'Content-Type': undefined}}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.happenings_saving = false;
                    if (res.success == 'T') {
                        var msg = $scope.dtl.id ? 'Happenings updated successfully.' : 'Happenings added successfully.';
                        show_alert_msg(msg);
                        $scope.list_happenings();
                        $("#happeningsFormModal").modal('hide');
                    } else {
                        show_form_errors(res.errors, frm);
                        show_alert_msg("Error!", 'E');
                    }
                }
        );
    }

    $scope.edit_happenings = function (id) {
        hide_form_errors($("#happeningsform"));
        $("#happeningsform").find("input[type='file']").val('');
        show_ajax_loader();
        $http({url: ADM_URL + 'pro/happenings_dtl/' + id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.dtl = res;
                    tinymce.EditorManager.get('ndesc').setContent(res.description);
                    $("#happeningsFormModal").modal();
                }
        );
    }

    $scope.delete_happenings = function (id, image) {
        if (!confirm("Are you sure to delete?")) {
            return;
        }

        show_ajax_loader('Deleting');
        $http({url: ADM_URL + 'pro/delete_happenings', method: 'POST', data: $.param({id: id, image: image})}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        $scope.list_happenings();
                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }

    /** Inits **/
    $scope.init = function () {
        set_numeric_input();
        $scope.status_lookups = $scope.status_lookup();
        set_tinymce("#ndesc", 200);
        set_datepicker($("#publish_date"));

        $scope.list_happenings();
        $scope.sform_data = $location.search();
    }
    $scope.init();
});

//EOF