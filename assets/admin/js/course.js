angular.module('app').controller("Course", function ($scope, $http, $state, $location, Auth) {
    $scope.lession = '';
    $scope.lession.type = 'online';
    $scope.ajaxRes = false;
    $scope.sform_data = {};
    $scope.list_courses = function (init) {
        show_ajax_loader();
        $http({url: ADM_URL + 'pro/courses/' + init, params: $location.search()}).success(
                function (res) {
                    res = Auth.check_res(res);

                    $scope.result = res.result;
                    $scope.page = res.page;
                    if (init == 'T') {
                        $scope.cats = res.cats;
                        $scope.teachers = res.teachers;
                    }
                    $scope.ajaxRes = true;
                    hide_ajax_loader();
                }
        );
    }
    /** \ **/
    $scope.editor = function (id) {
//        if (id == 'online' || id == 'offline') {
//            $('#descriptionlesson').show();
//            $('#studylesson').hide();
//            $('#videolesson').hide();
//        } else if (id == 'video') {
//            $('#videolesson').show();
//            $('#studylesson').hide();
//            $('#descriptionlesson').hide();
//        } else if (id == 'study') {
//            $('#studylesson').show();
//            $('#videolesson').hide();
//            $('#descriptionlesson').hide();
//        }
    }

    $scope.search_course = function (e, p) {
        e.preventDefault();
        if (p) {
            $scope.sform_data.p = p;
        } else {
            $scope.sform_data.p = '';
        }
        $location.search($scope.remove_empty($scope.sform_data));
        $scope.list_courses();
    }

    $scope.add_course = function () {
        $scope.dtl = {type: 'Online', teacher_id: '0', show_on_home: '0', featured: '0', price_type: 'free', status: '1'};

        tinymce.EditorManager.get('cdesc').setContent('');
        tinymce.EditorManager.get('ccari').setContent('');
        tinymce.EditorManager.get('howto').setContent('');
        tinymce.EditorManager.get('accom').setContent('');
        tinymce.EditorManager.get('ec').setContent('');
        tinymce.EditorManager.get('terms').setContent('');
        tinymce.EditorManager.get('benefit').setContent('');
        tinymce.EditorManager.get('faq').setContent('');

        hide_form_errors($("#courseform"));
        $("#courseform").find("input[type='file']").val('');
        $("#courseFormModal").modal();
    }

    $scope.save_course = function (e) {
        e.preventDefault();
        var frm = $("#courseform");
        hide_form_errors(frm);
        tinyMCE.triggerSave(false, true);

        var formData = new FormData(frm[0]);
        $scope.course_saving = true;
        $http({url: ADM_URL + 'pro/save_course', method: 'POST', data: formData, headers: {'Content-Type': undefined}}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.course_saving = false;
                    if (res.success == 'T') {
                        var msg = $scope.dtl.id ? 'Course updated successfully.' : 'Course added successfully.';
                        show_alert_msg(msg);
                        $scope.list_courses();
                        $("#courseFormModal").modal('hide');
                    } else {
                        show_form_errors(res.errors, frm);
                        show_alert_msg("Error!", 'E');
                    }
                }
        );
    }

    $scope.edit_course = function (id) {
        hide_form_errors($("#courseform"));
        $("#courseform").find("input[type='file']").val('');
        show_ajax_loader();
        $http({url: ADM_URL + 'pro/course_dtl/' + id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.dtl = res;

                    tinymce.EditorManager.get('cdesc').setContent(res.description);
                    tinymce.EditorManager.get('ccari').setContent(res.caricullaum);
                    tinymce.EditorManager.get('howto').setContent(res.how_to_apply);
                    tinymce.EditorManager.get('accom').setContent(res.accommodation_dtl);
                    tinymce.EditorManager.get('ec').setContent(res.eligibility_creteria);
                    tinymce.EditorManager.get('terms').setContent(res.terms);
                    tinymce.EditorManager.get('benefit').setContent(res.benefit);
                    tinymce.EditorManager.get('faq').setContent(res.faq);

                    $("#courseFormModal").modal();
                }
        );
    }

    $scope.delete_course = function (id, image) {
        if (!confirm("Are you sure to delete?")) {
            return;
        }

        show_ajax_loader('Deleting');
        $http({url: ADM_URL + 'pro/delete_course', method: 'POST', data: $.param({id: id, image: image})}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        $scope.list_courses();
                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }

    $scope.delete_batch = function (id, course_id) {
        if (!confirm("Are you sure to delete?")) {
            return;
        }

        show_ajax_loader('Deleting');
        $http({url: ADM_URL + 'pro/delete_batch', method: 'POST', data: $.param({id: id})}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        $scope.course_batches(course_id, '');
                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }

    $scope.delete_session = function (id, batch_id) {
        if (!confirm("Are you sure to delete?")) {
            return;
        }

        show_ajax_loader('Deleting');
        $http({url: ADM_URL + 'pro/delete_session', method: 'POST', data: $.param({id: id, batch_id: batch_id})}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        $scope.sessionList = res.sessions;
                        $("#batch_session_" + batch_id).html('(' + res.sessions.length + ')');

                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }

    $scope.course_batches = function (course_id, coursename) {
        $scope.course_id = course_id;
        $scope.coursename = coursename;
        show_ajax_loader();
        $http({url: ADM_URL + 'pro/course_batches/' + course_id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.batches = res.batches;
                    $("#batchModal").modal();
                }
        );
    }



    $scope.add_batch = function () {
        $scope.bdtl = {time_type: 'AM', batch_status: 'A'};
        hide_form_errors($("#batchform"));
        $("#batchform").find("input[type='checkbox']").prop('checked', false);
        $("#batchFormModal").modal();
    }

    $scope.edit_batch = function (id) {
        hide_form_errors($("#batchform"));
        $("#batchform").find("input[type='checkbox']").prop('checked', false);

        show_ajax_loader();
        $http({url: ADM_URL + 'pro/batch_dtl/' + id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.bdtl = res;

                    $.each($scope.bdtl.batch_days, function (k, v) {
                        $("#batchform").find("input[value='" + v + "']").prop('checked', true);
                    });

                    $("#batchFormModal").modal();
                }
        );
    }

    $scope.save_batch = function (e) {
        e.preventDefault();
        var frm = $("#batchform");
        hide_form_errors(frm);

        /** Validation **/
        /** **/

        var formData = new FormData(frm[0]);
        $scope.batch_saving = true;
        $http({url: ADM_URL + 'pro/save_batch', method: 'POST', data: formData, headers: {'Content-Type': undefined}}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.batch_saving = false;
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        $scope.batches = res.batches;
                        $("#batchFormModal").modal('hide');
                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }

    /** Lession **/
    $scope.course_lession = function (course_id, coursename) {
        $scope.course_id = course_id;
        $scope.coursename = coursename;
        show_ajax_loader();
        $http({url: ADM_URL + 'pro/course_lession/' + course_id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.lessonList = res.lession;
                    $("#lessionModel").modal();
                }
        );
    }

    $scope.add_lession = function () {
        set_tinymce("#ldescription", 200);
        tinymce.EditorManager.get('ldescription').setContent('');
        $scope.lession = {type: '', status: '', link: ''};
        hide_form_errors($("#lessionform"));
        $("#ldescriptionlession").hide();
        $("#descriptionlession").hide();
        $("#lessionFormModal").modal();
    }

    $scope.edit_lession = function (id) {
        set_tinymce("#ldescription", 200);
        hide_form_errors($("#lessionform"));
        $("#lessionform").find("input[type='checkbox']").prop('checked', false);

        show_ajax_loader();
        $http({url: ADM_URL + 'pro/lession_dtl/' + id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.lession = res;
                    tinymce.EditorManager.get('ldescription').setContent(res.description);
                    if ($scope.lession.type != 'recorded') {
                        $("#ldescriptionlession").show();
                        $("#descriptionlession").hide();
                    } else {
                        $("#ldescriptionlession").hide();
                        $("#descriptionlession").show();
                    }
                    $("#lessionFormModal").modal();
                }
        );
    }

    $scope.save_lession = function (e) {
        e.preventDefault();
        var frm = $("#lessionform");
        hide_form_errors(frm);

        /** Validation **/
        /** **/

        var formData = new FormData(frm[0]);
        $scope.lession_saving = true;
        $http({url: ADM_URL + 'pro/save_lession', method: 'POST', data: formData, headers: {'Content-Type': undefined}}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.lession_saving = false;
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        $scope.lessonList = res.lession;
                        $("#lessionFormModal").modal('hide');
                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }


    $scope.delete_lession = function (id, course_id) {
        if (!confirm("Are you sure to delete?")) {
            return;
        }

        show_ajax_loader('Deleting');
        $http({url: ADM_URL + 'pro/delete_lession', method: 'POST', data: $.param({id: id, course_id: course_id})}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        $scope.lessonList = res.lession;

                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }


    /** Slider **/
    $scope.slider = function (page_id, pagename) {
        $scope.page_id = page_id;
        $scope.pagename = pagename;
        show_ajax_loader();
        $http({url: ADM_URL + 'pro/slides/' + page_id + '/COURSE'}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.slides = res;
                    $("#sliderModal").modal();
                }
        );
    }

    $scope.add_slide = function () {
        $scope.sdtl = {};
        hide_form_errors($("#slideform"));
        $("#slideform").find("input[type='file']").val('');

        $("#slideFormModal").modal();
    }

    $scope.save_slide = function (e) {
        e.preventDefault();
        var frm = $("#slideform");
        hide_form_errors(frm);

        var formData = new FormData(frm[0]);
        $scope.slide_saving = true;
        $http({url: ADM_URL + 'pro/save_slide', method: 'POST', data: formData, headers: {'Content-Type': undefined}}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.slide_saving = false;
                    if (res.success == 'T') {
                        var msg = $scope.sdtl.id ? 'Slider image updated successfully.' : 'Slider image added successfully.';
                        show_alert_msg(msg);
                        $scope.slides = res.slides;
                        $("#slideFormModal").modal('hide');
                    } else {
                        show_form_errors(res.errors, frm);
                    }
                }
        );
    }

    $scope.edit_slide = function (id) {
        hide_form_errors($("#slideform"));
        $("#slideform").find("input[type='file']").val('');
        show_ajax_loader();
        $http({url: ADM_URL + 'pro/slide_dtl/' + id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.sdtl = res;
                    $("#slideFormModal").modal();
                }
        );
    }

    $scope.delete_slide = function (id, image, page_id) {
        if (!confirm("Are you sure to delete?")) {
            return;
        }

        show_ajax_loader('Deleting');
        $http({url: ADM_URL + 'pro/delete_slide', method: 'POST', data: $.param({id: id, image: image, page_id: page_id, type: 'COURSE'})}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        $scope.slides = res.slides;
                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }


    $scope.batch_session = function (batch_id, batch_name, course_id) {
        $scope.batch_id = batch_id;
        $scope.batch_name = batch_name;
        show_ajax_loader();
        $http({url: ADM_URL + 'pro/batch_sessions/' + batch_id + '/' + course_id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.sessionList = res.sessions;
                    $scope.lessonList = res.sessions;

                    $("#batchSessionModal").modal();
                }
        );
    }

    $scope.add_session = function () {
        show_ajax_loader();
        var course_id = $("#course_id").val();
        $http({url: ADM_URL + 'pro/course_lession/' + course_id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.lessonList = res.lession;

                    $scope.bdtl = {time_type: 'AM', batch_status: 'A'};
                    hide_form_errors($("#sessionForm"));
                    $("#sessionForm").find("input[type='checkbox']").prop('checked', false);
                    $("#sessionFormModal").modal();
                }
        );


    }

    $scope.edit_session = function (id, batch_id) {

        show_ajax_loader();

        var course_id = $('#course_id').val();
        hide_form_errors($("#sessionForm"));
        $http({url: ADM_URL + 'pro/course_lession/' + course_id}).success(
                function (res) {
                    $scope.lessonList = res.lession;
                    $("#sessionForm").find("input[type='checkbox']").prop('checked', false);
                }
        );

        $http({url: ADM_URL + 'pro/batch_session_dtl/' + id + '/' + batch_id + '/' + course_id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.bdtl = res;
                    $.each($scope.bdtl.selectedLession, function (k, v) {
                        $("#lesson_id_" + v).prop('checked', true);
                    });
                    $("#sessionFormModal").modal();
                }
        );
    }

    $scope.save_session = function (e) {
        e.preventDefault();
        var frm = $("#sessionForm");
        var batch_id = $("#batch_id").val();
        hide_form_errors(frm);

        var formData = new FormData(frm[0]);
        $scope.batch_saving = true;
        $http({url: ADM_URL + 'pro/save_session', method: 'POST', data: formData, headers: {'Content-Type': undefined}}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.batch_saving = false;
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        $scope.sessionList = res.sessions;
                        $("#batch_session_" + batch_id).html('(' + res.sessions.length + ')');
                        $("#sessionFormModal").modal('hide');
                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }



    /** Inits **/
    $scope.init = function () {
        set_numeric_input();
        $scope.status_lookups = $scope.status_lookup();
        set_tinymce("#cdesc", 200);
        set_tinymce("#ccari", 200);
        set_tinymce("#howto", 200);
        set_tinymce("#accom", 200);
        set_tinymce("#ec", 200);
        set_tinymce("#terms", 200);
        set_tinymce("#benefit", 200);
        set_tinymce("#faq", 200);
        set_tinymce(".ldescription", 200);

        //tinymce_insert_file_init();

        set_datepicker($("#start_date"));
        set_datepicker($("#end_date"));

        set_datepicker($("#session_start_date"));

        $scope.weekdays = [{k: 'Monday', v: 'Mon'}, {k: 'Tuesday', v: 'Tue'}, {k: 'Wednesday', v: 'Wed'}, {k: 'Thursday', v: 'Thu'}, {k: 'Friday', v: 'Fri'}, {k: 'Saturday', v: 'Sat'}, {k: 'Sunday', v: 'Sun'}];

        $scope.list_courses('T');
        $scope.sform_data = $location.search();
    }
    $scope.init();
});

//EOF