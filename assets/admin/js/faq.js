angular.module('app').controller("Faq", function ($scope, $http, $state, $location, Auth) {
    $scope.ajaxRes = false;
    $scope.sform_data = {};
    $scope.list_faq = function (init) {
        show_ajax_loader();
        $http({url: ADM_URL + 'master/faq/' + init, params: $location.search()}).success(
                function (res) {
                    res = Auth.check_res(res);

                    $scope.result = res.result;
                    $scope.page = res.page;
                    if (init == 'T') {
                        $scope.cats = res.cats;
                    }

                    $scope.ajaxRes = true;
                    hide_ajax_loader();
                }
        );
    }

    $scope.search_faq = function (e, p) {
        e.preventDefault();
        if (p) {
            $scope.sform_data.p = p;
        } else {
            $scope.sform_data.p = '';
        }
        $location.search($scope.remove_empty($scope.sform_data));
        $scope.list_faq();
    }

    $scope.add_faq = function () {
        $scope.dtl = {show_on_home: '0', status: '1'};
        hide_form_errors($("#faqform"));
        $("#faqform").find("input[type='file']").val('');
        $("#faqFormModal").modal();
    }

    $scope.save_faq = function (e) {
        e.preventDefault();
        var frm = $("#faqform");
        hide_form_errors(frm);
        tinyMCE.triggerSave(false, true);

        var formData = new FormData(frm[0]);
        $scope.f_saving = true;
        $http({url: ADM_URL + 'master/save_faq', method: 'POST', data: formData, headers: {'Content-Type': undefined}}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.f_saving = false;
                    if (res.success == 'T') {
                        var msg = $scope.dtl.id ? 'FAQ updated successfully.' : 'FAQ added successfully.';
                        show_alert_msg(msg);
                        $scope.list_faq();
                        $("#faqFormModal").modal('hide');
                    } else {
                        show_form_errors(res.errors, frm);
                    }
                }
        );
    }

    $scope.edit_faq = function (id) {
        hide_form_errors($("#faqform"));
        $("#faqform").find("input[type='file']").val('');
        show_ajax_loader();
        $http({url: ADM_URL + 'master/faq_dtl/' + id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.dtl = res;
                    $("#faqFormModal").modal();
                }
        );
    }

    $scope.delete_faq = function (id) {
        if (!confirm("Are you sure to delete?")) {
            return;
        }

        show_ajax_loader('Deleting');
        $http({url: ADM_URL + 'master/delete_faq', method: 'POST', data: $.param({id: id})}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        $scope.list_faq();
                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }

    /** Inits **/
    $scope.init = function () {
        set_numeric_input();
        $scope.status_lookups = $scope.status_lookup();
        $scope.list_faq('T');
        $scope.sform_data = $location.search();
    }
    $scope.init();
});

//EOF