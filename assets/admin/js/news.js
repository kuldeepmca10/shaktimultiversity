angular.module('app').controller("News", function($scope, $http, $state, $location, Auth){
	$scope.ajaxRes=false;
	$scope.sform_data={};
	$scope.list_news=function(){
		show_ajax_loader();
		$http({url: ADM_URL+'pro/news', params:$location.search()}).success(
			function(res){
				res=Auth.check_res(res);
				
				$scope.result=res.result;
				$scope.page=res.page;
				
				$scope.ajaxRes=true;
				hide_ajax_loader();
			}
		);
	}
	
	$scope.search_news=function(e, p){
		e.preventDefault();
		if(p){
			$scope.sform_data.p=p;
		}else{
			$scope.sform_data.p='';
		}
		$location.search($scope.remove_empty($scope.sform_data));
		$scope.list_news();
	}
	
	$scope.add_news=function(){
		$scope.dtl={show_on_home:'0', status:'1'};
		tinymce.EditorManager.get('ndesc').setContent('');
		hide_form_errors($("#newsform"));
		$("#newsform").find("input[type='file']").val('');
		$("#newsFormModal").modal();
	}
	
	$scope.save_news=function(e){
		e.preventDefault();
		var frm=$("#newsform");
		hide_form_errors(frm);
		tinyMCE.triggerSave(false, true);
		
		var formData=new FormData(frm[0]);
		$scope.news_saving=true;
		$http({url:ADM_URL+'pro/save_news', method:'POST', data:formData, headers:{'Content-Type': undefined}}).success(
			function(res) {
				res=Auth.check_res(res);
				$scope.news_saving=false;
				if(res.success=='T'){
					var msg=$scope.dtl.id?'News updated successfully.':'News added successfully.';
					show_alert_msg(msg);
					$scope.list_news();
					$("#newsFormModal").modal('hide');
				}else{
					show_form_errors(res.errors, frm);
					show_alert_msg("Error!", 'E');
				}
			}
		);
	}
	
	$scope.edit_news=function(id){
		hide_form_errors($("#newsform"));
		$("#newsform").find("input[type='file']").val('');
		show_ajax_loader();
		$http({url: ADM_URL+'pro/news_dtl/'+id}).success(
			function(res){
				hide_ajax_loader();
				res=Auth.check_res(res);
				$scope.dtl=res;
				tinymce.EditorManager.get('ndesc').setContent(res.description);
				$("#newsFormModal").modal();
			}
		);
	}
	
	$scope.delete_news=function(id, image){
		if(!confirm("Are you sure to delete?")){
			return;
		}
		
		show_ajax_loader('Deleting');
		$http({url: ADM_URL+'pro/delete_news', method:'POST', data:$.param({id:id, image:image})}).success(
			function(res){
				hide_ajax_loader();
				res=Auth.check_res(res);
				if(res.success=='T'){
					show_alert_msg(res.msg);
					$scope.list_news();
				}else{
					show_alert_msg(res.msg, 'E');
				}
			}
		);
	}
	
	/** Inits **/
	$scope.init=function(){
		set_numeric_input();
		$scope.status_lookups=$scope.status_lookup();
		set_tinymce("#ndesc", 200);
		set_datepicker($("#publish_date"));
		
		$scope.list_news();
		$scope.sform_data=$location.search();
	}
	$scope.init();
});

//EOF