/** Student Controller **/
app.controller("Student", function ($scope, $http, $location, $state, $stateParams, Auth) {
    $scope.ajaxRes = false;
    $scope.data = {status: '1'};
    $scope.membership_status = [{value: '', name: 'Select Membership Status'}, {value: '1', name: 'Member'}, {value: '0', name: 'Non Member'}];
    $scope.status_list = [{value: '', name: 'Select Status'}, {value: '1', name: 'Active'}, {value: '0', name: 'In Active'}];


    $scope.list_students = function () {
        show_ajax_loader();
        $http({url: ADM_URL + 'student/students', params: $location.search()}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.result = res.result;
                    $scope.page = res.page;

                    $scope.ajaxRes = true;
                    hide_ajax_loader();
                }
        );
    }


    $scope.list_enrollments = function () {
        show_ajax_loader();
        $http({url: ADM_URL + 'student/course_enrollment', params: $location.search()}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.enrollmentlist = res.enrollmentlist.result;
                    $scope.page = res.page;
                    $scope.ajaxRes = true;
                    hide_ajax_loader();
                }
        );
    }

    $scope.search_students = function (e, p) {
        e.preventDefault();
        if (p) {
            $scope.data.p = p;
        } else {
            $scope.data.p = '';
        }
        var d = $scope.remove_empty($scope.data);
//        delete d['status'];
        $location.search(d);
        $scope.list_students();
    }

    $scope.search_enrollments = function (e, p) {
        e.preventDefault();
        if (p) {
            $scope.data.p = p;
        } else {
            $scope.data.p = '';
        }
        var d = $scope.remove_empty($scope.data);
        delete d['status'];
        $location.search(d);
        $scope.course_enrollment(data.userid, data.username);
    }




    $scope.course_enrollment = function (userid, username) {
        $scope.user_id = userid;
        $scope.data.userid = userid;
        $scope.username = username;
        show_ajax_loader();
        $http({url: ADM_URL + 'student/course_enrollment/' + userid}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.enrollmentlist = res.enrollmentlist.result;
                    $scope.enrollmentpage = res.enrollmentlist.page;
                    $scope.status_enrollments = [{value: '', name: 'Select Status'}, {value: 'pending for approval', name: 'Pending For Approval'}, {value: 'approved', name: 'Approved'}, {value: 'rejected', name: 'Rejected'}];
                    $("#enrollmentListModal").modal();
                }
        );
    }

    $scope.course_enrollment_detail = function (id, userid) {
        $scope.user_id = userid;
        $scope.data.userid = userid;
        show_ajax_loader();
        $http({url: ADM_URL + 'student/course_enrollment_detail/' + id + '/' + userid}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.enrollmentDetail = res.enrollmentlist.result[0];
                    $("#enrollmentDetailModal").modal();
                }
        );
    }


    $scope.update_enrollment_change = function (id, username, data) {
        var statusValue = $('#enroll_status_' + id).val();
        $scope.sEnrollStatus = data.approval_status;
        $scope.sEnrolComment = data.comment;
        $scope.iEnrolID = id;
        if (statusValue != "") {
            if (confirm("Are you sure you want to change your status") == true) {
                $("#updateEnrollmentModal").modal();
                $("#updateEnrollmentModal").attr('style', 'z-index:11000px;display:block');
            }
        } else {
            $('#enroll_status_' + id).val('');
        }

    }

    $scope.update_enrollment_status = function () {
        var statusValue = $scope.sEnrollStatus;
        var id = $scope.iEnrolID;
        var comment = $scope.sEnrolComment;

        if (statusValue == "" || statusValue == undefined) {
            $scope.commentError = "Please select status";
        } else if (comment == "" || comment == undefined) {
            $scope.commentError = "Please provide comment";
        } else {
            $scope.commentError = "";
            show_ajax_loader();
            $http({url: ADM_URL + 'student/update_enrollment_status', method: 'POST', data: "id=" + id + "&status=" + statusValue + "&comment=" + comment}).success(
                    function (res) {
                        hide_ajax_loader();
                        res = Auth.check_res(res);
                        if (res.success == 'T') {
                            show_alert_msg(res.msg);
                            $scope.sEnrollStatus = '';
                            $scope.iEnrolID = '';
                            $scope.sEnrolComment = '';
                            $("#updateEnrollmentModal").modal('hide');
                            $scope.course_enrollment($scope.data.userid, $scope.data.username);

                        } else {
                            show_form_errors(res.msg);
                        }
                    }
            );
        }


    }

    /** Change Password **/
    $scope.change_pass = function (e) {
        e.preventDefault();
        var frm = $("#passform");
        $scope.pass_submit = true;

        hide_form_errors(frm);
        $http({url: ADM_URL + 'user/change_pass', method: 'POST', data: frm.serialize()}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.pass_submit = false;
                    if (res.success == 'T') {
                        show_alert_msg("Password changed successfully");
                        frm[0].reset();
                    } else {
                        show_form_errors(res.errors, frm);
                    }
                }
        );
    }


    $scope.add_student = function () {
        $scope.dtl = {status: '1', cat_id: ''};
        hide_form_errors($("#addStudentForm"));
        $("[name='type'][value='I']").prop('checked', true);

        $(".type_V").hide();
        $(".type_I").show();

        $("#addStudentModel").modal();
    }

    $scope.save_student = function (e) {
        e.preventDefault();
        var frm = $("#addStudentForm");
        hide_form_errors(frm);

        var formData = new FormData(frm[0]);
        $scope.course_saving = true;
        $http({url: ADM_URL + 'student/save_student', method: 'POST', data: formData, headers: {'Content-Type': undefined}}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.course_saving = false;
                    if (res.success == 'T') {
                        var msg = $scope.dtl.id ? 'Student updated successfully.' : 'Student added successfully.';
                        show_alert_msg(msg);
                        $scope.list_students();
                        $("#addStudentModel").modal('hide');
                    } else {
                        show_form_errors(res.errors, frm);
                        show_alert_msg("Error!", 'E');
                    }
                }
        );
    }

    $scope.edit_student = function (id) {

        hide_form_errors($("#addStudentForm"));
        show_ajax_loader();
        $http({url: ADM_URL + 'student/student_dtl/' + id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.dtl = res;
                    $("#addStudentModel").modal();
                }
        );
    }


    $scope.delete_student = function (id) {
        if (!confirm("Are you sure to delete?")) {
            return;
        }

        show_ajax_loader('Deleting');
        $http({url: ADM_URL + 'student/delete_student', method: 'POST', data: $.param({id: id})}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        $scope.list_students();
                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }
    $scope.update_status = function (id, status) {

        if (!confirm("Are you sure to uppdate status?")) {
            return;
        }

        show_ajax_loader('Updating...');
        $http({url: ADM_URL + 'student/update_student_status', method: 'POST', data: $.param({id: id, status: status})}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        $scope.list_students();
                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }
    $scope.update_membership = function (id, status) {

        if (!confirm("Are you sure to update lifetime membership status?")) {
            return;
        }

        show_ajax_loader('Updating...');
        $http({url: ADM_URL + 'student/update_membership_status', method: 'POST', data: $.param({id: id, status: status})}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        $scope.list_students();
                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }

    $scope.view_detail = function (id, userid) {
        $scope.user_id = userid;
        $scope.data.userid = userid;
        show_ajax_loader();
        $http({url: ADM_URL + 'student/student_dtl/' + id + '/' + userid}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.studentDetail = res;
                    $("#viewDetailModal").modal();
                }
        );
    }

    $scope.login_as_user = function (id) {
        $scope.id = id;
        show_ajax_loader();
        $http({url: ADM_URL + 'student/login_as_user/' + id}).success(
                function (res) {
                    hide_ajax_loader();
                    window.open(
                            res.redirect,
                            '_blank');
//                    res = Auth.check_res(res);
//                    window.location.href = res.redirect;
                }
        );
    }


    /** Inits **/
    $scope.init = function () {
        set_tab_box();
        $scope.status_lookups = $scope.status_lookup();
        $scope.country_phonecodes = $scope.country_phonecode();
        if ($state.current.name == 'edit_user' && !$stateParams.id) {
            $state.go('notFound');
        }

        if ($state.current.name == 'students') {
            $scope.list_students();
            $scope.data = $location.search();


        } else {
            $scope.status_lookups = $scope.status_lookup();
            show_ajax_loader();
            $http({url: ADM_URL + 'user/user_detail/' + $stateParams.id}).success(
                    function (res) {
                        res = Auth.check_res(res);
                        $scope.roles = res.roles;
                        if (typeof res.dtl != "undefined") {
                            if (!res.dtl.role_id) {
                                $state.go('notFound');
                            }
                            $scope.data = res.dtl;
                        }
                        hide_ajax_loader();
                    }
            );
        }
    }
    $scope.init();
});

//EOF
function chooseCountry(id) {
    var ar = id.split('_');
    if (ar[1] != '') {
        $('#country_code').val('+' + ar[1]);
    } else {
        $('#country_code').val('');
    }
}