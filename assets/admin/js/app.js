app.controller("App", function ($scope, $rootScope, $http, $state, Auth) {
    $rootScope.$on('$stateChangeStart', function (event, res) {
        show_ajax_loader();
        if (typeof res.pageTitle != "undefined") {
            $scope.PageTitle = res.pageTitle + ' | ' + $scope.SITE_NAME;
        } else {
            $scope.PageTitle = 'Welcome to: ' + $scope.SITE_NAME;
        }
        set_active_nav(res.url);
    });
});

/** Leftbar Ctrl **/
app.controller("Leftbar", function ($scope, $http) {
});