angular.module('app').controller("CmsWidget", function ($scope, $http, $state, $location, Auth) {
    $scope.ajaxRes = false;
    $scope.sform_data = {};
    $scope.dtl = {status: '1'};
    $scope.list_pages = function () {
        show_ajax_loader();
        $http({url: ADM_URL + 'pro/cms_widget', params: $location.search()}).success(
                function (res) {
                    res = Auth.check_res(res);

                    $scope.result = res.result;
                    $scope.page = res.page;
                    $scope.ajaxRes = true;
                    hide_ajax_loader();
                }
        );
    }

    $scope.search_page = function (e, p) {
        e.preventDefault();
        if (p) {
            $scope.sform_data.p = p;
        } else {
            $scope.sform_data.p = '';
        }
        $location.search($scope.remove_empty($scope.sform_data));
        $scope.list_pages();
    }

    $scope.add_page = function () {
        $scope.dtl = {status: '1'};

        tinymce.EditorManager.get('pdesc').setContent('');

        hide_form_errors($("#cmsform"));
        $("#cmsform").find("input[type='file']").val('');
        $("#cmsFormModal").modal();
    }

    $scope.save_page = function (e) {
        e.preventDefault();
        var frm = $("#cmsform");
        hide_form_errors(frm);
        tinyMCE.triggerSave(false, true);

        var formData = new FormData(frm[0]);
        $scope.page_saving = true;
        $http({url: ADM_URL + 'pro/save_cms_page', method: 'POST', data: formData, headers: {'Content-Type': undefined}}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.page_saving = false;
                    if (res.success == 'T') {
                        var msg = $scope.dtl.id ? 'Page updated successfully.' : 'Page added successfully.';
                        show_alert_msg(msg);
                        $scope.list_pages();
                        $("#cmsFormModal").modal('hide');
                    } else {
                        show_form_errors(res.errors, frm);
                        show_alert_msg("Error!", 'E');
                    }
                }
        );
    }

    $scope.edit_page = function (id) {
        hide_form_errors($("#cmsform"));
        $("#cmsform").find("input[type='file']").val('');
        show_ajax_loader();
        $http({url: ADM_URL + 'pro/cms_page_dtl/' + id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.dtl = res;

                    tinymce.EditorManager.get('pdesc').setContent(res.description);

                    $("#cmsFormModal").modal();
                }
        );
    }

    $scope.delete_page = function (id, image) {
        if (!confirm("Are you sure to delete?")) {
            return;
        }

        show_ajax_loader('Deleting');
        $http({url: ADM_URL + 'pro/delete_cms_page', method: 'POST', data: $.param({id: id, image: image})}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        $scope.list_pages();
                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }

    $scope.remove_featured_image = function (id, image) {
        show_ajax_loader('Deleting');
        $http({url: ADM_URL + 'pro/delete_cms_image', method: 'POST', data: $.param({id: id, image: image})}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    if (res.success == 'T') {
                        $scope.dtl.image = '';
                    }
                }
        );
    }

    /** Slider **/
    $scope.slider = function (page_id, pagename) {
        $scope.page_id = page_id;
        $scope.pagename = pagename;
        show_ajax_loader();
        $http({url: ADM_URL + 'pro/slides/' + page_id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.slides = res;
                    $("#sliderModal").modal();
                }
        );
    }

    $scope.add_slide = function () {
        $scope.sdtl = {};
        hide_form_errors($("#slideform"));
        $("#slideform").find("input[type='file']").val('');

        $("#slideFormModal").modal();
    }

    $scope.save_slide = function (e) {
        e.preventDefault();
        var frm = $("#slideform");
        hide_form_errors(frm);

        var formData = new FormData(frm[0]);
        $scope.slide_saving = true;
        $http({url: ADM_URL + 'pro/save_slide', method: 'POST', data: formData, headers: {'Content-Type': undefined}}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.slide_saving = false;
                    if (res.success == 'T') {
                        var msg = $scope.sdtl.id ? 'Slider image updated successfully.' : 'Slider image added successfully.';
                        show_alert_msg(msg);
                        $scope.slides = res.slides;
                        $("#slideFormModal").modal('hide');
                    } else {
                        show_form_errors(res.errors, frm);
                    }
                }
        );
    }

    $scope.edit_slide = function (id) {
        hide_form_errors($("#slideform"));
        $("#slideform").find("input[type='file']").val('');
        show_ajax_loader();
        $http({url: ADM_URL + 'pro/slide_dtl/' + id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.sdtl = res;
                    $("#slideFormModal").modal();
                }
        );
    }

    $scope.delete_slide = function (id, image, page_id) {
        if (!confirm("Are you sure to delete?")) {
            return;
        }

        show_ajax_loader('Deleting');
        $http({url: ADM_URL + 'pro/delete_slide', method: 'POST', data: $.param({id: id, image: image, page_id: page_id})}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        $scope.slides = res.slides;
                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }
    /** \ **/

    /** Inits **/
    $scope.init = function () {
        set_numeric_input();
        $scope.status_lookups = $scope.status_lookup();
        set_tinymce("#pdesc", 200);
        tinymce_insert_file_init();

        $scope.list_pages();
        $scope.sform_data = $location.search();
    }
    $scope.init();
});

//EOF