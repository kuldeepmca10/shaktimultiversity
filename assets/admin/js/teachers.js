angular.module('app').controller("Teacher", function ($scope, $http, $state, $location, Auth) {
    $scope.ajaxRes = false;
    $scope.sform_data = {};
    $scope.list_teachers = function (init) {
        show_ajax_loader();
        $http({url: ADM_URL + 'master/teachers/' + init, params: $location.search()}).success(
                function (res) {
                    res = Auth.check_res(res);

                    $scope.result = res.result;
                    $scope.page = res.page;
                    if (init == 'T') {
                        $scope.cats = res.cats;
                    }

                    $scope.ajaxRes = true;
                    hide_ajax_loader();
                }
        );
    }

    $scope.search_teacher = function (e, p) {
        e.preventDefault();
        if (p) {
            $scope.sform_data.p = p;
        } else {
            $scope.sform_data.p = '';
        }
        $location.search($scope.remove_empty($scope.sform_data));
        $scope.list_teachers();
    }

    $scope.add_teacher = function () {
        $scope.dtl = {show_on_home: '0', status: '1'};
        tinymce.EditorManager.get('about_lineage').setContent('');
        tinymce.EditorManager.get('contribution').setContent('');
        tinymce.EditorManager.get('social_responsibility').setContent('');
        tinymce.EditorManager.get('in_the_media').setContent('');
        hide_form_errors($("#teacherform"));
        $("#teacherform").find("input[type='file']").val('');
        $("#teacherFormModal").modal();
    }

    $scope.save_teacher = function (e) {
        e.preventDefault();
        var frm = $("#teacherform");
        hide_form_errors(frm);
        tinyMCE.triggerSave(false, true);

        var formData = new FormData(frm[0]);
        $scope.t_saving = true;
        $http({url: ADM_URL + 'master/save_teacher', method: 'POST', data: formData, headers: {'Content-Type': undefined}}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.t_saving = false;
                    if (res.success == 'T') {
                        var msg = $scope.dtl.id ? 'Teacher updated successfully.' : 'Teacher added successfully.';
                        show_alert_msg(msg);
                        $scope.list_teachers();
                        $("#teacherFormModal").modal('hide');
                    } else {
                        show_form_errors(res.errors, frm);
                    }
                }
        );
    }

    $scope.edit_teacher = function (id) {
        hide_form_errors($("#teacherform"));
        $("#teacherform").find("input[type='file']").val('');
        show_ajax_loader();
        $http({url: ADM_URL + 'master/teacher_dtl/' + id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.dtl = res;

                    tinymce.EditorManager.get('about_lineage').setContent('');
                    tinymce.EditorManager.get('contribution').setContent('');
                    tinymce.EditorManager.get('social_responsibility').setContent('');
                    tinymce.EditorManager.get('in_the_media').setContent('');
                    if (res.about_lineage != null) {
                        tinymce.EditorManager.get('about_lineage').setContent(res.about_lineage);
                    }
                    if (res.contribution != null) {
                        tinymce.EditorManager.get('contribution').setContent(res.contribution);
                    }
                    if (res.social_responsibility != null) {
                        tinymce.EditorManager.get('social_responsibility').setContent(res.social_responsibility);
                    }
                    if (res.in_the_media != null) {
                        tinymce.EditorManager.get('in_the_media').setContent(res.in_the_media);
                    }
                    $("#teacherFormModal").modal();
                }
        );
    }

    $scope.delete_teacher = function (id, image) {
        if (!confirm("Are you sure to delete?")) {
            return;
        }

        show_ajax_loader('Deleting');
        $http({url: ADM_URL + 'master/delete_teacher', method: 'POST', data: $.param({id: id, image: image})}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        $scope.list_teachers();
                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }

    /** Inits **/
    $scope.init = function () {
        set_numeric_input();
        $scope.status_lookups = $scope.status_lookup();

        set_tinymce("#about_lineage", 200);
        set_tinymce("#contribution", 200);
        set_tinymce("#social_responsibility", 200);
        set_tinymce("#in_the_media", 200);

        $scope.list_teachers('T');
        $scope.sform_data = $location.search();
    }
    $scope.init();
});

//EOF