app.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    var URL_PREF = '/admin/p/';
    $urlRouterProvider.otherwise(URL_PREF + 'notFound');
    $stateProvider
            .state('notFound', {
                url: URL_PREF + 'notFound',
                templateUrl: 'admin-views/notfound.html?v=' + VERSION,
                pageTitle: '404 Not Found'
            })
            .state('profile', {
                url: URL_PREF + 'profile',
                templateUrl: 'admin-views/user/profile.html?v=' + VERSION,
                controller: 'Profile',
                pageTitle: 'Profile'
            })
            .state('cats', {
                url: URL_PREF + 'cats',
                templateUrl: 'admin-views/master/cat.html?v=' + VERSION,
                pageTitle: 'Category Management',
                controller: 'Cat',
                resolve: {loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['assets/admin/js/cat.js?v=' + VERSION]);
                    }}
            })
            .state('teachers', {
                url: URL_PREF + 'teachers',
                templateUrl: 'admin-views/master/teachers.html?v=' + VERSION,
                controller: 'Teacher',
                pageTitle: 'Teacher Management',
                resolve: {loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['assets/admin/js/teachers.js?v=' + VERSION]);
                    }}
            })
            .state('courses', {
                url: URL_PREF + 'courses',
                templateUrl: 'admin-views/pro/course.html?v=' + VERSION,
                controller: 'Course',
                pageTitle: 'Course Management',
                resolve: {loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['assets/admin/js/course.js?v=' + VERSION]);
                    }}
            })
            .state('newsletter', {
                url: URL_PREF + 'newsletter',
                templateUrl: 'admin-views/user/newsletter.html?v=' + VERSION,
                controller: 'Newsletter',
                pageTitle: 'Newsletter Management',
                resolve: {loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['assets/admin/js/newsletter.js?v=' + VERSION]);
                    }}
            })
            .state('lifetime-service', {
                url: URL_PREF + 'lifetime-service',
                templateUrl: 'admin-views/user/lifetime_service.html?v=' + VERSION,
                controller: 'LifetimeService',
                pageTitle: 'Lifetime Service Management',
                resolve: {loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['assets/admin/js/lifetime.js?v=' + VERSION]);
                    }}
            })
            .state('book-order', {
                url: URL_PREF + 'book-order',
                templateUrl: 'admin-views/user/book_order.html?v=' + VERSION,
                controller: 'BookOrderRequest',
                pageTitle: 'Book Order Request Management',
                resolve: {loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['assets/admin/js/book_order.js?v=' + VERSION]);
                    }}
            })
            .state('products', {
                url: URL_PREF + 'products',
                templateUrl: 'admin-views/pro/product.html?v=' + VERSION,
                controller: 'Product',
                pageTitle: 'Product Management',
                resolve: {loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['assets/admin/js/product.js?v=' + VERSION]);
                    }}
            })
            .state('happenings', {
                url: URL_PREF + 'happenings',
                templateUrl: 'admin-views/pro/happenings.html?v=' + VERSION,
                controller: 'Happenings',
                pageTitle: 'Happenings Management',
                resolve: {loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['assets/admin/js/happenings.js?v=' + VERSION]);
                    }}
            })
            .state('cms-widget', {
                url: URL_PREF + 'cms-widget',
                templateUrl: 'admin-views/pro/cms_widget.html?v=' + VERSION,
                controller: 'CmsWidget',
                pageTitle: 'Cms Widget Management',
                resolve: {loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['assets/admin/js/cms_widget.js?v=' + VERSION]);
                    }}
            })
            .state('sidebar-banner', {
                url: URL_PREF + 'sidebar-banner',
                templateUrl: 'admin-views/pro/sidebar_banner.html?v=' + VERSION,
                controller: 'SidebarBanner',
                pageTitle: 'Sidebar Banner Management',
                resolve: {loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['assets/admin/js/sidebar_banner.js?v=' + VERSION]);
                    }}
            })
            .state('gallery', {
                url: URL_PREF + 'gallery',
                templateUrl: 'admin-views/pro/gallery.html?v=' + VERSION,
                controller: 'Gallery',
                pageTitle: 'Gallery Management',
                resolve: {loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['assets/admin/js/gallery.js?v=' + VERSION]);
                    }}
            })
            .state('cms', {
                url: URL_PREF + 'cms',
                templateUrl: 'admin-views/pro/cms.html?v=' + VERSION,
                controller: 'Cms',
                pageTitle: 'CMS Pages',
                resolve: {loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['assets/admin/js/cms.js?v=' + VERSION]);
                    }}
            })
            .state('testimonials', {
                url: URL_PREF + 'testimonials',
                templateUrl: 'admin-views/master/testimonials.html?v=' + VERSION,
                controller: 'Testimonial',
                pageTitle: 'Testimonials',
                resolve: {loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['assets/admin/js/testimonials.js?v=' + VERSION]);
                    }}
            })

            .state('shakticenters', {
                url: URL_PREF + 'shakticenters',
                templateUrl: 'admin-views/master/shakti_centers.html?v=' + VERSION,
                controller: 'Shakticenters',
                pageTitle: 'Shakti Centers',
                resolve: {loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['assets/admin/js/shakti_centers.js?v=' + VERSION]);
                    }}
            })

            .state('faq', {
                url: URL_PREF + 'faq',
                templateUrl: 'admin-views/master/faq.html?v=' + VERSION,
                controller: 'Faq',
                pageTitle: "FAQ's",
                resolve: {loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['assets/admin/js/faq.js?v=' + VERSION]);
                    }}
            })
            .state('menu', {
                url: URL_PREF + 'menu',
                templateUrl: 'admin-views/master/menu.html?v=' + VERSION,
                pageTitle: 'Menu and Quick Links',
                controller: 'Menu',
                resolve: {loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['assets/admin/js/menu.js?v=' + VERSION]);
                    }}
            })


            /** Role Management **/
            .state('roles', {
                url: URL_PREF + 'roles',
                templateUrl: 'admin-views/user/roles.html?v=' + VERSION,
                controller: 'RoleList',
                pageTitle: 'Role Management'
            })
            .state('add_role', {
                url: URL_PREF + 'add_role',
                templateUrl: 'admin-views/user/role_form.html?v=' + VERSION,
                controller: 'RoleAdd',
                pageTitle: 'Add New Role'
            })
            .state('edit_role', {
                url: URL_PREF + 'edit_role/:id',
                templateUrl: 'admin-views/user/role_form.html?v=' + VERSION,
                controller: 'RoleAdd',
                pageTitle: 'Edit Role'
            })
            .state('role_access', {
                url: URL_PREF + 'role_access/:id',
                templateUrl: 'admin-views/user/role_access.html?v=' + VERSION,
                controller: 'RoleAccess',
                pageTitle: 'Access Controls'
            })

            /** Website Students Management **/
            .state('students', {
                url: URL_PREF + 'students',
                templateUrl: 'admin-views/student/students.html?v=' + VERSION,
                controller: 'Student',
                pageTitle: 'Student Management',
                resolve: {loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['assets/admin/js/student.js?v=' + VERSION]);
                    }}
            })

            .state('enrollments', {
                url: URL_PREF + 'enrollments',
                templateUrl: 'admin-views/pro/enrollments.html?v=' + VERSION,
                controller: 'Enrollment',
                pageTitle: 'Course Enrollment Management',
                resolve: {loadPlugin: function ($ocLazyLoad) {
                        return $ocLazyLoad.load(['assets/admin/js/enrollments.js?v=' + VERSION]);
                    }}
            })

            .state('add_student', {
                url: URL_PREF + 'add_student/',
                templateUrl: 'admin-views/student/student_form.html?v=' + VERSION,
                controller: 'Student',
                pageTitle: 'Add New Student'
            })
            .state('edit_student', {
                url: URL_PREF + 'edit_student/:id',
                templateUrl: 'admin-views/students/student_form.html?v=' + VERSION,
                controller: 'Student',
                pageTitle: 'Edit Student'
            })

            /** User Management **/
            .state('users', {
                url: URL_PREF + 'users',
                templateUrl: 'admin-views/user/users.html?v=' + VERSION,
                controller: 'User',
                pageTitle: 'User Management'
            })
            .state('add_user', {
                url: URL_PREF + 'add_user',
                templateUrl: 'admin-views/user/user_form.html?v=' + VERSION,
                controller: 'User',
                pageTitle: 'Add New User'
            })
            .state('edit_user', {
                url: URL_PREF + 'edit_user/:id',
                templateUrl: 'admin-views/user/user_form.html?v=' + VERSION,
                controller: 'User',
                pageTitle: 'Edit User'
            })

    $locationProvider.html5Mode(true);
});

//EOF