/** Dashboard Controller **/
angular.module('app').controller("Cat", function ($scope, $http, Auth) {
    /** Add Cat **/
    $scope.add_cat = function (type) {
        $scope.catdtl = {};
        $scope.cattype = type;
        hide_form_errors($("#catform"));
        $("#catFormModal").modal();
    }

    $scope.edit_cat = function (id, type) {
        $scope.cattype = type;
        hide_form_errors($("#catform"));
        show_ajax_loader();
        $http({url: ADM_URL + 'master/cat_dtl/' + id}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.catdtl = res;
                    $("#catFormModal").modal();
                }
        );
    }

    $scope.delete_cat = function (id, type) {
        if (!confirm("Are you sure to delete?")) {
            return;
        }

        show_ajax_loader('Deleting');
        $http({url: ADM_URL + 'master/delete_cat', method: 'POST', data: $.param({id: id, type: type})}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        if (type == 'Course') {
                            $scope.ccats = res.cats;
                        } else if (type == 'Product') {
                            $scope.pcats = res.cats;
                        } else if (type == 'Teacher') {
                            $scope.tcats = res.cats;
                        } else if (type == 'Gallery') {
                            $scope.gcats = res.cats;
                        } else if (type == 'Happenings') {
                            $scope.hcats = res.cats;
                        } else if (type == 'FAQ') {
                            $scope.fcats = res.cats;
                        }
                    } else {
                        show_alert_msg(res.msg, 'E');
                    }
                }
        );
    }

    $scope.save_cat = function (e) {
        e.preventDefault();
        var frm = $("#catform");
        hide_form_errors(frm);
        var formData = new FormData(frm[0]);
        $scope.cat_saving = true;
        $http({url: ADM_URL + 'master/save_cat', method: 'POST', data: formData, headers: {'Content-Type': undefined}}).success(
                function (res) {
                    res = Auth.check_res(res);
                    $scope.cat_saving = false;
                    if (res.success == 'T') {
                        show_alert_msg(res.msg);
                        var type = res.type;
                        if (type == 'Course') {
                            $scope.ccats = res.cats;
                        } else if (type == 'Product') {
                            $scope.pcats = res.cats;
                        } else if (type == 'Teacher') {
                            $scope.tcats = res.cats;
                        } else if (type == 'Gallery') {
                            $scope.gcats = res.cats;
                        } else if (type == 'Happenings') {
                            $scope.hcats = res.cats;
                        } else if (type == 'FAQ') {
                            $scope.fcats = res.cats;
                        }
                        $("#catFormModal").modal('hide');
                    } else {
                        show_form_errors(res.errors, frm);
                    }
                }
        );
    }


    /** Init **/
    $scope.init = function () {
        set_numeric_input();
        $(".nav-tabs a").click(function (e) {
            e.preventDefault();
        });

        show_ajax_loader();
        $http({url: ADM_URL + 'master/cat_init'}).success(
                function (res) {
                    hide_ajax_loader();
                    res = Auth.check_res(res);
                    $scope.ccats = res.ccats;
                    $scope.pcats = res.pcats;
                    $scope.tcats = res.tcats;
                    $scope.gcats = res.gcats;
                    $scope.hcats = res.hcats;
                    $scope.fcats = res.fcats;
                }
        );
    }
    $scope.init();
});

//EOF