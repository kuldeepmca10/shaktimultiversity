/** Menu Controller **/
angular.module('app').controller("Menu", function($scope, $http, Auth){
	/** Add Menu **/
	$scope.add_menu=function(type){
		$scope.dtl={menu_type:'CMS_PAGE'};
		if(type!='Submenu'){
			$scope.parent_id=0;
			$scope.menuname='';
		}
		$scope.type=type;
		
		hide_form_errors($("#menuform"));
		$("#menuFormModal").modal();
	}
	
	$scope.edit_menu=function(id, type){
		if(type!='Submenu'){
			$scope.parent_id=0;
			$scope.menuname='';
		}
		$scope.type=type;
		hide_form_errors($("#menuform"));
		show_ajax_loader();
		$http({url: ADM_URL+'master/menu_dtl/'+id}).success(
			function(res){
				hide_ajax_loader();
				res=Auth.check_res(res);
				if(res.page_id==0){
					res.page_id='';
				}
				$scope.dtl=res;
				$("#menuFormModal").modal();
			}
		);
	}
	
	$scope.delete_menu=function(id, type, parent_id){
		if(!confirm("Are you sure to delete?")){
			return;
		}
		
		show_ajax_loader('Deleting');
		$http({url: ADM_URL+'master/delete_menu', method:'POST', data:$.param({id:id, type:type, parent_id:parent_id})}).success(
			function(res){
				hide_ajax_loader();
				res=Auth.check_res(res);
				if(res.success=='T'){
					show_alert_msg(res.msg);
					if(type=='Header'){
						$scope.hmenues=res.menues;
					}else if(type=='Footer'){
						$scope.fmenues=res.menues;
					}else if(type=='Submenu'){
						$scope.submenues=res.submenues;
					}
				}else{
					show_alert_msg(res.msg, 'E');
				}
			}
		);
	}
	
	$scope.save_menu=function(e){
		e.preventDefault();
		var frm=$("#menuform");
		hide_form_errors(frm);
		var formData=new FormData(frm[0]);
		
		/** Validation **/
		if(!$.trim($scope.dtl.title)){
			show_alert_msg("Enter menu title", "E");
			return;
		}
		
		switch($scope.dtl.menu_type){
			case 'CMS_PAGE':
				if(!$scope.dtl.page_id){
					show_alert_msg("Select CMS Page", "E");
					return;
				}
			break;
			
			case 'OTHER_PAGE':
				if(!$scope.dtl.other_page){
					show_alert_msg("Select Other Page", "E");
					return;
				}
			break;
			
			case 'LINK':
				if(!$scope.dtl.link){
					show_alert_msg("Enter link", "E");
					return;
				}
			break;
			
			case 'NOLINK':
			break;
		}
		/** \ **/
		
		$scope.m_saving=true;
		$http({url:ADM_URL+'master/save_menu', method:'POST', data:formData, headers:{'Content-Type': undefined}}).success(
			function(res) {
				res=Auth.check_res(res);
				$scope.m_saving=false;
				if(res.success=='T'){
					show_alert_msg(res.msg);
					var type=res.type;
					if(type=='Header'){
						$scope.hmenues=res.menues;
					}else if(type=='Footer'){
						$scope.fmenues=res.menues;
					}else if(type=='Submenu'){
						$scope.submenues=res.submenues;
					}
					$("#menuFormModal").modal('hide');
				}else{
					show_form_errors(res.errors, frm);
				}
			}
		);
	}
	
	$scope.sub_menues=function(parent_id, menuname){
		$scope.parent_id=parent_id;
		$scope.menuname=menuname;
		show_ajax_loader();
		$http({url: ADM_URL+'master/sub_menues/'+parent_id}).success(
			function(res){
				hide_ajax_loader();
				res=Auth.check_res(res);
				$scope.submenues=res.submenues;
				$("#submenuModal").modal();
			}
		);
	}
	
	$scope.check_menutype=function(){
		switch($scope.dtl.menu_type){
			case 'CMS_PAGE':
				$scope.dtl.other_page='';
				$scope.dtl.link='';
			break;
			
			case 'OTHER_PAGE':
				$scope.dtl.page_id='';
				$scope.dtl.link='';
			break;
			
			case 'LINK':
				$scope.dtl.other_page='';
				$scope.dtl.page_id='';
			break;
			
			case 'NOLINK':
				$scope.dtl.other_page='';
				$scope.dtl.page_id='';
				$scope.dtl.link='';
			break;
		}
	}
	
	/** Quick Links **/
	$scope.add_quicklink=function(type){
		$scope.dtl={};
		hide_form_errors($("#qlform"));
		$("#qlFormModal").modal();
	}
	
	$scope.save_quick_link=function(e){
		e.preventDefault();
		var frm=$("#qlform");
		hide_form_errors(frm);
		var formData=new FormData(frm[0]);
		
		$scope.ql_saving=true;
		$http({url:ADM_URL+'master/save_quick_link', method:'POST', data:formData, headers:{'Content-Type': undefined}}).success(
			function(res) {
				res=Auth.check_res(res);
				$scope.ql_saving=false;
				if(res.success=='T'){
					show_alert_msg(res.msg);
					$scope.quicklinks=res.quicklinks;
					$("#qlFormModal").modal('hide');
				}else{
					show_form_errors(res.errors, frm);
				}
			}
		);
	}
	
	$scope.edit_quick_link=function(id){
		hide_form_errors($("#menuform"));
		show_ajax_loader();
		$http({url: ADM_URL+'master/quick_link_dtl/'+id}).success(
			function(res){
				hide_ajax_loader();
				res=Auth.check_res(res);
				$scope.dtl=res;
				$("#qlFormModal").modal();
			}
		);
	}
	
	$scope.delete_quick_link=function(id){
		if(!confirm("Are you sure to delete?")){
			return;
		}
		
		show_ajax_loader('Deleting');
		$http({url: ADM_URL+'master/delete_quick_link', method:'POST', data:$.param({id:id})}).success(
			function(res){
				hide_ajax_loader();
				res=Auth.check_res(res);
				if(res.success=='T'){
					show_alert_msg(res.msg);
					$scope.quicklinks=res.quicklinks;
				}else{
					show_alert_msg(res.msg, 'E');
				}
			}
		);
	}
	
	
	/** Init **/
	$scope.init=function(){
		set_numeric_input();
		$(".nav-tabs a").click(function(e){
			e.preventDefault();
		});
		
		show_ajax_loader();
		$http({url: ADM_URL+'master/menu_init'}).success(
			function(res){
				hide_ajax_loader();
				res=Auth.check_res(res);
				$scope.hmenues=res.hmenues;
				$scope.fmenues=res.fmenues;
				$scope.cms_pages=res.cms_pages;
				$scope.other_pages=res.other_pages;
				$scope.link_open_type=res.link_open_type;
				
				$scope.quicklinks=res.quicklinks;
			}
		);
	}
	$scope.init();
});

//EOF