<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package     CodeIgniter
 * @author      Kuldeep Singh
 * @copyright           Copyright (c) 2015, Kuldeep Lab.
 * @license    
 * @link       
 * @since       Version 1.0
 * @filesource
 */
// ------------------------------------------------------------------------

/**
 * Kuldeep Lab core CodeIgniter class
 *
 * @package     CodeIgniter
 * @subpackage          Libraries
 * @category            Kuldeep Lab
 * @author      Kuldeep Singh
 * @link       
 */
class Payu {

    var $CI;

    public function __construct($params = array()) {
//        $this->CI = & get_instance();
//
//        $this->CI->load->helper('url');
//        $this->CI->config->item('base_url');
//        $this->CI->load->database();
    }

    public function makepayment($aData) {


        $action = '';

//        $aData = array();
//        $aData['transction_id'] = '99999999';
//        $aData['first_name'] = 'Kuldee';
//        $aData['email'] = 'Kuldeep.singh@kleward.com';
//        $aData['amount'] = '10';
//        $aData['productinfo'] = 'We';
//        $aData['phone'] = '838838383';

        $aPostedData = array(
            'key' => sPAYU_MERCHANT_KEY, 'hash' => '', 'txnid' => strtolower($aData['transction_id']), 'amount' => (int) $aData['amount'],
            'firstname' => $aData['first_name'], 'email' => $aData['email'], 'phone' => $aData['phone'], 'productinfo' => $aData['productinfo'],
            'surl' => sPAYU_SUCCESS_URL, 'furl' => sPAYU_FAILURE_URL, 'service_provider' => 'payu_paisa',
//            'lastname' => '', 'curl' => '', 'address1' => '', 'address2' => '',
//            'city' => '', 'state' => '', 'country' => '', 'zipcode' => '',
//            'udf1' => '', 'udf2' => '', 'udf3' => '', 'udf4' => '',
//            'udf5' => '', 'pg' => ''
        );


        if (!empty($aPostedData)) {
            //print_r($_POST);
            foreach ($aPostedData as $key => $value) {
                $aPostedData[$key] = $value;
            }
        }


        $formError = 0;

        if (empty($aPostedData['txnid'])) {
            // Generate random transaction id
            $txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
        } else {
            $txnid = $aPostedData['txnid'];
        }

        $sHASH_KEY = '';
// Hash Sequence

        $hashSequence = "key|txnid|amount|productinfo|firstname|email|udf1|udf2|udf3|udf4|udf5|udf6|udf7|udf8|udf9|udf10";
        if (empty($aPostedData['hash']) && sizeof($aPostedData) > 0) {
            if (empty($aPostedData['key']) || empty($aPostedData['txnid']) || empty($aPostedData['amount']) || empty($aPostedData['firstname']) || empty($aPostedData['email']) || empty($aPostedData['productinfo']) || empty($aPostedData['surl']) || empty($aPostedData['furl']) || empty($aPostedData['service_provider'])) {
                $formError = 1;
            } else {
                //$aPostedData['productinfo'] = json_encode(json_decode('[{"name":"tutionfee","description":"","value":"500","isRequired":"false"},{"name":"developmentfee","description":"monthly tution fee","value":"1500","isRequired":"false"}]'));
                $hashVarsSeq = explode('|', $hashSequence);
                $hash_string = '';
                foreach ($hashVarsSeq as $hash_var) {
                    $hash_string .= isset($aPostedData[$hash_var]) ? $aPostedData[$hash_var] : '';
                    $hash_string .= '|';
                }
                $hash_string .= sPAYU_SALT;

                $sHASH_KEY = strtolower(hash('sha512', $hash_string));
            }
        } elseif (!empty($aPostedData['hash'])) {
            $sHASH_KEY = $aPostedData['hash'];
        }

        if (isset($sHASH_KEY) && !empty($sHASH_KEY)) {
            ?>
            <form action="<?php echo sPAYU_PAYMENT_URL; ?>" method="post" name="payuForm" id="payuForm">
                <input type="hidden" name="key" value="<?php echo sPAYU_MERCHANT_KEY ?>" />
                <input type="hidden" name="hash" value="<?php echo $sHASH_KEY ?>"/>
                <input type="hidden" name="txnid" value="<?php echo $txnid ?>" />
                <input type="hidden" name="firstname" value="<?php echo $aPostedData['firstname'] ?>" />
                <input type="hidden" name="amount" value="<?php echo $aPostedData['amount'] ?>" />
                <input type="hidden" name="email" value="<?php echo $aPostedData['email'] ?>" />
                <input type="hidden" name="phone" value="<?php echo $aPostedData['phone'] ?>" />
                <input type="hidden" name="productinfo" value="<?php echo $aPostedData['productinfo'] ?>" />
                <input type="hidden" name="surl" value="<?php echo $aPostedData['surl'] ?>" />
                <input type="hidden" name="furl" value="<?php echo $aPostedData['furl'] ?>" />
                <input type="hidden" name="service_provider" value="payu_paisa" size="64" />
            </form>
            <script>
                document.getElementById('payuForm').submit();
            </script>
            <?php
            die;
        }
    }

    function check_txnid($tnxid) {
        global $link;
        return true;
        $valid_txnid = true;
        //get result set
        $sql = mysql_query("SELECT * FROM `payments` WHERE txnid = '$tnxid'", $link);
        if ($row = mysql_fetch_array($sql)) {
            $valid_txnid = false;
        }
        return $valid_txnid;
    }

    function check_price($price, $id) {
        $valid_price = false;
        //you could use the below to check whether the correct price has been paid for the product

        /*
          $sql = mysql_query("SELECT amount FROM `products` WHERE id = '$id'");
          if (mysql_num_rows($sql) != 0) {
          while ($row = mysql_fetch_array($sql)) {
          $num = (float)$row['amount'];
          if($num == $price){
          $valid_price = true;
          }
          }
          }
          return $valid_price;
         */
        return true;
    }

    function updatePayments($data) {
        global $link;

        if (is_array($data)) {
            $sql = mysql_query("INSERT INTO `payments` (txnid, payment_amount, payment_status, itemid, createdtime) VALUES (
				'" . $data['txn_id'] . "' ,
				'" . $data['payment_amount'] . "' ,
				'" . $data['payment_status'] . "' ,
				'" . $data['item_number'] . "' ,
				'" . date("Y-m-d H:i:s") . "'
				)", $link);
            return mysql_insert_id($link);
        }
    }

    public function getMetadata($pageid) {
        $sql = "SELECT * FROM pages WHERE pageid = ?";
        $results = $this->CI->db->query($sql, array($pageid));
        $pageMetadata = $results->row();
        $data['keywords'] = $pageMetadata->keywords;
        $data['description'] = $pageMetadata->description;
        $this->CI->load->view('templates/header', $data);
    }

}
