<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Form_validation extends CI_Form_validation {
	function get_errors(){
		return $this->_error_array;
	}
	
	function is_unique($str, $field) {
		$ci=&get_instance();
		list($table, $field, $ignore_cond)=explode('.', $field);
		
		$str=escape_str($str);
		$cond="$field='$str'";
		if($ignore_cond)
			$cond.=" AND $ignore_cond";
			
		$table=str_replace(":", ".", $table);
		return !$ci->dba->val($table, $cond, $field);
    }
}

//End of file