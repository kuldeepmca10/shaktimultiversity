<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package     CodeIgniter
 * @author      Kuldeep Singh
 * @copyright           Copyright (c) 2015, Kuldeep Lab.
 * @license    
 * @link       
 * @since       Version 1.0
 * @filesource
 */
// ------------------------------------------------------------------------

/**
 * Kuldeep Lab core CodeIgniter class
 *
 * @package     CodeIgniter
 * @subpackage          Libraries
 * @category            Kuldeep Lab
 * @author      Kuldeep Singh
 * @link       
 */
class Paypal {

    var $CI;

    public function __construct($params = array()) {
//        $this->CI = & get_instance();
//
//        $this->CI->load->helper('url');
//        $this->CI->config->item('base_url');
//        $this->CI->load->database();
    }

    public function makepayment($aData) {

// Check if paypal request or response
//        if (!isset($aData["txn_id"]) && !isset($aData["txn_type"])) {
        if (!isset($aData["txn_type"])) {
            $querystring = '';

            // Firstly Append paypal account to querystring
            $querystring .= "?business=" . urlencode(sPAYPAL_EMAIL) . "&";
//            $aData['amount'] = 0.01;
            // Append amount& currency (£) to quersytring so it cannot be edited in html
            //The item name and amount can be brought in dynamically by querying the $aData['item_number'] variable.
            $querystring .= "item_name=" . urlencode($aData['item_name']) . "&";
            $querystring .= "amount=" . urlencode($aData['amount']) . "&";

            //loop for posted values and append to querystring
            foreach ($aData as $key => $value) {
                $value = urlencode(stripslashes($value));
                $querystring .= "$key=$value&";
            }

            // Append paypal return addresses
            $querystring .= "return=" . urlencode(stripslashes(sPAYPAL_RETURN_URL)) . "&";
            $querystring .= "cancel_return=" . urlencode(stripslashes(sPAYPAL_CANCEL_URL)) . "&";
            $querystring .= "notify_url=" . urlencode(sPAYPAL_NOTIFY_URL);


            //  $querystring = "?business=kuldeep.mca10@gmail.com&item_name=Test Item&amount=1&cmd=_xclick&no_note=1&lc=UK¤cy_code=USD&bn=&first_name=Customer's First Name&last_name = Customer's Last Name&payer_email=&item_number=123456&return=http://localhost/shaktimultiversity/paypal1/payment-successful.html&cancel_return=http://localhost/shaktimultiversity/paypal1/payment-cancelled.html¬ify_url=http://localhost/shaktimultiversity/paypal1/payments.php";
            // Append querystring with custom field
            //$querystring .= "&custom=".USERID;
            // Redirect to paypal IPN
            header('location:' . sPAYPAL_PAYMENT_URL . $querystring);
            exit();
        } else {
            //Database Connection
//            $link = mysql_connect($host, $user, $pass);
//            mysql_select_db($db_name);
            // Response from Paypal
            // read the post from PayPal system and add 'cmd'
            $req = 'cmd=_notify-validate';
            foreach ($aData as $key => $value) {
                $value = urlencode(stripslashes($value));
                $value = preg_replace('/(.*[^%^0^D])(%0A)(.*)/i', '${1}%0D%0A${3}', $value); // IPN fix
                $req .= "&$key=$value";
            }

            // assign posted variables to local variables
            $data['item_name'] = $aData['item_name'];
            $data['item_number'] = $aData['item_number'];
            $data['payment_status'] = $aData['payment_status'];
            $data['payment_amount'] = $aData['mc_gross'];
            $data['payment_currency'] = $aData['mc_currency'];
            $data['txn_id'] = $aData['txn_id'];
            $data['receiver_email'] = $aData['receiver_email'];
            $data['payer_email'] = $aData['payer_email'];
            $data['custom'] = $aData['custom'];

            // post back to PayPal system to validate
            $header = "POST /cgi-bin/webscr HTTP/1.0\r\n";
            $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
            $header .= "Content-Length: " . strlen($req) . "\r\n\r\n";

            $fp = fsockopen('ssl://www.sandbox.paypal.com', 443, $errno, $errstr, 30);

            if (!$fp) {
                // HTTP ERROR
            } else {
                fputs($fp, $header . $req);
                while (!feof($fp)) {
                    $res = fgets($fp, 1024);
                    if (strcmp($res, "VERIFIED") == 0) {

                        // Used for debugging
                        // mail('user@domain.com', 'PAYPAL POST - VERIFIED RESPONSE', print_r($post, true));
                        // Validate payment (Check unique txnid & correct price)
                        $valid_txnid = check_txnid($data['txn_id']);
                        $valid_price = check_price($data['payment_amount'], $data['item_number']);
                        // PAYMENT VALIDATED & VERIFIED!
                        if ($valid_txnid && $valid_price) {

                            //  $orderid = updatePayments($data);

                            if ($orderid) {
                                // Payment has been made & successfully inserted into the Database
                            } else {
                                // Error inserting into DB
                                // E-mail admin or alert user
                                // mail('user@domain.com', 'PAYPAL POST - INSERT INTO DB WENT WRONG', print_r($data, true));
                            }
                        } else {
                            // Payment made but data has been changed
                            // E-mail admin or alert user
                        }
                    } else if (strcmp($res, "INVALID") == 0) {

                        // PAYMENT INVALID & INVESTIGATE MANUALY!
                        // E-mail admin or alert user
                        // Used for debugging
                        //@mail("user@domain.com", "PAYPAL DEBUGGING", "Invalid Response<br />data = <pre>".print_r($post, true)."</pre>");
                    }
                }
                fclose($fp);
            }
        }
    }

    function check_txnid($tnxid) {
        global $link;
        return true;
        $valid_txnid = true;
        //get result set
        $sql = mysql_query("SELECT * FROM `payments` WHERE txnid = '$tnxid'", $link);
        if ($row = mysql_fetch_array($sql)) {
            $valid_txnid = false;
        }
        return $valid_txnid;
    }

    function check_price($price, $id) {
        $valid_price = false;
        //you could use the below to check whether the correct price has been paid for the product

        /*
          $sql = mysql_query("SELECT amount FROM `products` WHERE id = '$id'");
          if (mysql_num_rows($sql) != 0) {
          while ($row = mysql_fetch_array($sql)) {
          $num = (float)$row['amount'];
          if($num == $price){
          $valid_price = true;
          }
          }
          }
          return $valid_price;
         */
        return true;
    }

    function updatePayments($data) {
        global $link;

        if (is_array($data)) {
            $sql = mysql_query("INSERT INTO `payments` (txnid, payment_amount, payment_status, itemid, createdtime) VALUES (
				'" . $data['txn_id'] . "' ,
				'" . $data['payment_amount'] . "' ,
				'" . $data['payment_status'] . "' ,
				'" . $data['item_number'] . "' ,
				'" . date("Y-m-d H:i:s") . "'
				)", $link);
            return mysql_insert_id($link);
        }
    }

    public function getMetadata($pageid) {
        $sql = "SELECT * FROM pages WHERE pageid = ?";
        $results = $this->CI->db->query($sql, array($pageid));
        $pageMetadata = $results->row();
        $data['keywords'] = $pageMetadata->keywords;
        $data['description'] = $pageMetadata->description;
        $this->CI->load->view('templates/header', $data);
    }

}
