<!--start courses  area -->
<div class="courses_area">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="title">
                    <h3 class="module-title">
                        Explore all the   <span> Courses</span>
                    </h3>
                </div>
            </div>
            <div class="col-md-2 text-right">
                <form name="sCurrencyForm" id="sCurrencyForm" action="" method="post">
                    <label>Currency</label>&nbsp;
                    <select name="currencyType" id="currencyType" onchange="this.form.submit()">
                        <option value="">Select</option>
                        <option value="INR" <?php echo (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'INR') ? 'selected = "selected"' : ''; ?>>INR</option>
                        <option value="USD" <?php echo (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'USD') ? 'selected = "selected"' : ''; ?>>USD</option>
                    </select>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="courses_owl curosel-style4">
                <?php foreach ($courses as $r): ?>
                    <!--start course single  item -->
                    <div class="col-md-12">
                        <div class="course_item">
                            <div class="courses_thumb">
                                <a href="<?php echo URL . 'course/' . $r['slug'] ?>"><img src="<?php echo UP_URL . 'course-sm/' . $r['image'] ?>" alt="" /></a>
                                <div class="courses_thumb_text">
                                    <?php if (isset($r['price_type']) && !empty($r['price_type']) && $r['price_type'] == 'free'): ?>
                                        Free    
                                    <?php else: ?>
                                        <?php if (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'INR') { ?>
                                            <?php if (isset($aUserData) && !empty(isset($aUserData)) && isset($aUserData['isLifetimeMember']) && $aUserData['isLifetimeMember'] == true && $r['price'] > 0): ?>
                                                <i class="fa fa-inr" aria-hidden="true"></i><span class="course_price"> <?php echo $r['mrp'] ?></span> |
                                                <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $r['price'] ?>
                                            <?php else: ?>
                                                <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $r['mrp'] ?>
                                            <?php endif; ?>
                                        <?php }else if (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'USD') { ?>
                                            <?php if (isset($aUserData) && isset($aUserData['isLifetimeMember']) && $aUserData['isLifetimeMember'] == true && $r['price_usd'] > 0): ?>
                                                <i class="fa fa-usd" aria-hidden="true"></i><span class="course_price"> <?php echo $r['mrp_usd'] ?></span> |
                                                <i class="fa fa-usd" aria-hidden="true"></i> <?php echo $r['price_usd'] ?>
                                            <?php else: ?>
                                                <i class="fa fa-usd" aria-hidden="true"></i> <?php echo $r['mrp_usd'] ?>
                                            <?php endif; ?>                                             
                                        <?php } ?>
                                    <?php endif; ?>						
                                </div>
                            </div>
                            <div class="courses_content">
                                <h2><a href="<?php echo URL . 'course/' . $r['slug'] ?>"><?php echo $r['title'] ?></a></h2>
                                <h5>Teacher: <strong><a href="<?php echo URL . 'master-detail/' . $r['teacher_slug']; ?>"><?php echo $r['teacher'] ?></a></strong></h5>

                                <h5>
                                    Duration:
                                    <strong class="h-link">
                                        <?php echo $r['duration_hr'] ? $r['duration_hr'] . ' Hours' : ''
                                        ?> <?php echo $r['duration_mn'] ? $r['duration_mn'] . ' Minutes' : '' ?>

                                    </strong>
                                </h5>
                                <h5>Batch Start Date: <strong class="h-link"><?php echo isset($r['batch_start_date']) && !empty($r['batch_start_date']) ? get_date($r['batch_start_date']) : 'N/A' ?>&nbsp;<span title="View More Batches"  class="view-more-batch" onclick="fnViewMoreBatch('<?php echo $r['id']; ?>', '<?php echo $r['slug']; ?>')" id="sViewMoreBatch">[+]</span></strong></h5>
                                <!--<div onclick="fnViewMoreBatch('<?php echo $r['id']; ?>', '<?php echo $r['slug']; ?>')" id="sViewMoreBatch">View More Batches</div>-->
                                <p><?php echo str_short($r['short_description'], 125) ?>

                                    <br/>
                                    <span class="text-left">
                                        <a href="<?php echo URL . 'course/' . $r['slug'] ?>" class="text_uppercase course_readmore">Read More..</a>
                                    </span>
                                </p>
                                <div class="text-center">
                                    <?php echo isset($r['batch_start_date']) && !empty($r['batch_start_date']) ? '<a href="' . URL . 'course/' . $r['slug'] . '" class="text_uppercase more_read text-right">Enroll Now</a>' : '<a href="' . URL . 'course/' . $r['slug'] . '" class="text_uppercase more_read_gray text-right">Enroll Now</a>'; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--End course single  item -->
                <?php endforeach; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <a href="courses" class="bmore">BROWSE ALL COURSES</a>
                </div>
            </div>
        </div>
        <br/>
    </div>
</div>	
<!--end courses  area -->
