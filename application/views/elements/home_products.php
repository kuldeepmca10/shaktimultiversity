<div class="priging_area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title">
                    <h3 class="module-title">
                        Our Latest  <span>Publications</span>
                    </h3>
                </div>
            </div>
        </div>			
        <div class="row">
            <?php foreach ($products as $r): $images = explode(",", $r['images']) ?>
                <!-- single item priging -->
                <div class="col-md-4 col-sm-6">
                    <div class="courses_thumb">
                        <div class="courses_thumb_text">
                            <?php if (isset($r['price_type']) && !empty($r['price_type']) && $r['price_type'] == 'free'): ?>
                                Free    
                            <?php else: ?>
                                <?php if (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'INR') { ?>
                                    <?php if (isset($aUserData) && !empty(isset($aUserData)) && isset($aUserData['isLifetimeMember']) && $aUserData['isLifetimeMember'] == true && $r['price'] > 0): ?>
                                        <i class="fa fa-inr" aria-hidden="true"></i><span class="course_price"> <?php echo $r['mrp'] ?></span> |
                                        <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $r['price'] ?>
                                    <?php else: ?>
                                        <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $r['mrp'] ?>
                                    <?php endif; ?>
                                <?php }else if (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'USD') { ?>
                                    <?php if (isset($aUserData) && isset($aUserData['isLifetimeMember']) && $aUserData['isLifetimeMember'] == true && $r['price_usd'] > 0): ?>
                                        <i class="fa fa-usd" aria-hidden="true"></i><span class="course_price"> <?php echo $r['mrp_usd'] ?></span> |
                                        <i class="fa fa-usd" aria-hidden="true"></i> <?php echo $r['price_usd'] ?>
                                    <?php else: ?>
                                        <i class="fa fa-usd" aria-hidden="true"></i> <?php echo $r['mrp_usd'] ?>
                                    <?php endif; ?>                                             
                                <?php } ?>
                            <?php endif; ?>						
                        </div>							
                    </div>
                    <div class="priging_item">
                        <div class="priging_thumb">						
                            <a href="<?php echo URL . 'shop/' . $r['slug'] ?>">                                
                                <img class="atvImg-layer" src="<?php echo UP_URL . 'pro-lg/' . $images[0] ?>"/>														
                            </a>
                        </div>
                        <div class="priging_content mobile-text-center">
                                <h2><?php echo $r['title'] ?></h2>	
                                <p><?php echo $r['writer'] ? 'by ' . $r['writer'] : '&nbsp;' ?></p>
                                <div class="mt10 text-center">
                                    <a href="<?php echo URL . 'shop/' . $r['slug'] ?>" class="text_uppercase more_read border_none text-right">Order Now</a>
                                </div>
                            </div>
                    </div>
                </div>
                <!-- end single item priging -->
            <?php endforeach; ?>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <a href="shop" class="bmore">BROWSE MORE</a>
                </div>
            </div>
        </div>			
    </div>
</div>	