<?php
$testimonials = footer_testimonials();
?>

<!-- testimonials -->		
<section class="testimonial">	
    <div class="container-fluid">

        <div class='row'>
            <div class='col-md-12' style="color:#282828">
                <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                    <!-- Bottom Carousel Indicators -->
                    <ol class="carousel-indicators">
                        <?php foreach ($testimonials as $i => $r): ?>
                            <li data-target="#quote-carousel" data-slide-to="<?php echo $i ?>" class="<?php echo $i == 0 ? 'active' : '' ?>"></li>
                        <?php endforeach; ?>
                    </ol>

                    <!-- Carousel Slides / Quotes -->
                    <div class="carousel-inner">
                        <?php foreach ($testimonials as $i => $r): ?>
                            <div class="item <?php echo $i == 0 ? 'active' : '' ?>">
                                <blockquote>
                                    <div class="row testimonial-text-footer" onclick="window.location.href = '<?php echo URL; ?>testimonials#testimonial-<?php echo $r['id']; ?>'">
                                        <?php
                                        $sClass = ' col-sm-12 ';
                                        if (file_exists(UP_PATH . 'testimonials/' . $r['image'])) {
                                            $sClass = ' col-sm-9 ';
                                            ?>
                                            <div class="col-sm-3 text-center">
                                                <img class="img-circle" src="<?php echo UP_URL . "testimonials/" . $r['image'] ?>" style="width:100px; height:100px;">
                                            </div>
                                        <?php } ?>
                                        <div class="text-decription <?php echo $sClass; ?> ">
                                            <p>
                                                <?php echo (isset($r['description']) && strlen($r['description']) > 220) ? substr($r['description'], 0, 220) . ' ...' : $r['description']; ?>
                                            </p>
                                            <small><?php echo $r['title'] ?></small>
											
                                        </div>
                                    </div>
                                </blockquote>
                            </div>
                        <?php endforeach; ?>
                    </div>

                    <!-- Carousel Buttons Next/Prev -->
                    <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                    <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
                </div>                          
            </div>
        </div>
    </div>
</section>	
<!-- testimonials -->
