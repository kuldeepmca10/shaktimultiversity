<!--<base href="<?php // echo ENVIRONMENT == 'development' ? '/shaktimultiversity/' : '/'                                ?>" />-->
<base href="<?php echo URL; ?>" />
<link rel="shortcut icon" type="image/x-icon" href="favicon.png">
<meta name="google" content="notranslate" />

<!-- Google Fonts
                ============================================ -->		
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href="https://allfont.net/allfont.css?fonts=montserrat-light" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<!-- style CSS
============================================ -->
<link rel="stylesheet" href="<?php echo URL; ?>dashboard/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo URL; ?>theme/front/css/bootstrap.min.css?v=<?php echo VERSION ?>">
<link rel="stylesheet" href="<?php echo URL; ?>theme/front/css/owl.carousel.css?v=<?php echo VERSION ?>">	
<link rel="stylesheet" href="<?php echo URL; ?>theme/front/css/owl.theme.css?v=<?php echo VERSION ?>">	
<link rel="stylesheet" href="<?php echo URL; ?>theme/front/css/owl.transitions.css?v=<?php echo VERSION ?>">
<link rel="stylesheet" href="<?php echo URL; ?>theme/front/css/font-awesome.min.css?v=<?php echo VERSION ?>">
<link rel="stylesheet" href="<?php echo URL; ?>theme/front/css/animate.css?v=<?php echo VERSION ?>">
<link rel="stylesheet" href="<?php echo URL; ?>theme/front/css/linearfont.css?v=<?php echo VERSION ?>">
<link rel="stylesheet" href="<?php echo URL; ?>theme/front/css/main.css?v=<?php echo VERSION ?>">
<!--<link rel="stylesheet" href="<?php echo URL; ?>theme/front/css/registration.css?v=<?php echo VERSION ?>">-->
<link rel="stylesheet" href="<?php echo URL; ?>theme/front/css/meanmenu.min.css?v=<?php echo VERSION ?>">
<link rel="stylesheet" href="<?php echo URL; ?>theme/front/css/mTab-style.css?v=<?php echo VERSION ?>">
<link rel="stylesheet" href="<?php echo URL; ?>theme/front/css/nivo-slider.css?v=<?php echo VERSION ?>">
<link rel="stylesheet" href="<?php echo URL; ?>theme/front/css/normalize.css?v=<?php echo VERSION ?>">
<link rel="stylesheet" href="<?php echo URL; ?>theme/front/css/thumbGrid.css?v=<?php echo VERSION ?>">

<?php if (isset($CSS) AND $CSS): ?>
    <?php foreach ($CSS as $c): ?>
        <link rel="stylesheet" href="<?php echo $c; ?><?php echo strpos($c, '?') === FALSE ? '?' : '&' ?>q=<?php echo VERSION; ?>" />
    <?php endforeach; ?>
<?php endif; ?>

<link rel="stylesheet" href="<?php echo URL; ?>theme/front/css/style.css?v=<?php echo VERSION ?>">
<link rel="stylesheet" href="<?php echo URL; ?>theme/front/css/responsive.css?v=<?php echo VERSION ?>">
<link rel="stylesheet" href="<?php echo URL; ?>theme/front/css/sat.css?v=<?php echo VERSION ?>">

<!-- jquery
============================================ -->		
<script src="<?php echo URL; ?>theme/front/js/vendor/jquery-1.11.3.min.js"></script>


<script type="text/javascript">
    var SITE_URL = '<?php echo URL; ?>';
</script>