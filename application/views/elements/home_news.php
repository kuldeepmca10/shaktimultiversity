<?php
	$first=$news[0];
	array_shift($news);
	$news=array_chunk($news, 2);
?>

<!--start news  area -->	
<div class="news_area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title">
                    <h3 class="module-title">
                        Our Latest  <span>News & Update</span>
                    </h3>
                </div>
            </div>
        </div>		
        <div class="row">
            <!--start single news  item -->	
            <div class="col-md-6 col-sm-12">
                <div class="single_news_item">
                    <div class="news_thumb">
                    <a href="news/<?php echo $first['slug']?>"><img src="<?php echo UP_URL."news-lg/".$first['image']?>" alt="" /></a>	
                    </div>
                    <div class="news_content">
                        <p class="date"><?php echo get_date($first['publish_date'])?></p>
                        <h2><a href="single-blog.html"><strong><?php echo $first['title']?></strong></a></h2>
                        <?php echo nl2br($first['short_description'])?>
                        <h2><strong>- <?php echo $first['source']?>,</strong></h2>
                        <p> <i><?php echo $first['source_subtitle']?></i></p>
                        <p><a href="news/<?php echo $first['slug']?>" class="">Read More...</a></p>
                    </div>
                </div>
            </div>
            <!--end single news  item -->
            
            <?php foreach($news as $n):?>
                <div class="col-md-3">
                    <div class="row">
                    	<?php foreach($n as $r):?>
                            <!--start single news  item -->	
                            <div class="col-md-12 col-sm-6">
                                <div class="single_news_item">
                                    <div class="news_thumb">
                                    <a href="news/<?php echo $r['slug']?>"><img src="<?php echo UP_URL."news-sm/".$r['image']?>" alt="" /></a>	
                                    </div>
                                    <div class="news_content">
                                        <p class="date"><?php echo get_date($r['publish_date'])?></p>
                                        <h2><a href="news/<?php echo $r['slug']?>"><?php echo $r['title']?></a></h2>
                                    </div>
                                </div>						
                            </div>
                            <!--end single news  item -->
                        <?php endforeach;?>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</div>
<!--end news  area -->