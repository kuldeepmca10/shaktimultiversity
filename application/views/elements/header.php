<?php
$menues = menues('Header');
$coursecats = cats('Course');
?>
<!--start header  area --> 
<div class="header_area">
    <div class="container">
        <div class="row">
            <!-- header  logo --> 
            <div class="col-md-4 col-sm-6 col-xs-5">
                <div class="logo"><a href="<?php echo URL ?>"><img src="theme/front/img/logo.png" alt="" /></a></div>
            </div>
            <!-- end header  logo --> 
            <div class="col-md-8 col-sm-6 col-xs-7 pull_right">
                <?php
                $aUserData = get_session(USR_SESSION_NAME);
                
                if (isset($aUserData['aUserData']) && !empty($aUserData['aUserData'])) {
                    $sWelcomeMessage = '<a href="user/dashboard">Welcome ' . $aUserData['aUserData']['name'] . '</a> | <a href="logout">Logout</a>';
                } else {
                    $sWelcomeMessage = '<a href="login">LOGIN</a> | <a href="register">REGISTER</a>';
                }
                ?>
                <div class="row">
                    <div class="form col-sm-12">
                        <div class="language header_right">
                            <?php if (isset($aUserData['aUserData']['isLifetimeMember']) && !empty($aUserData['aUserData']['isLifetimeMember']) && $aUserData['aUserData']['isLifetimeMember'] == 1) { ?>
                                <div class="blink">Privileged User</div>
                            <?php } ?>
                            <?php echo $sWelcomeMessage; ?>
                        </div>
                    </div>
                    <div class="language col-sm-12">
                        <div class="no-margin header_right"> 
                            <!--<small>&nbsp;</small>-->
<!--                            <small>
                                    <span class="icon-set"><i class="fa fa-phone"></i><span id="cart-item"></span></span> 
                                    <span class="icon-set"><i class="fa fa-phone"></i>&nbsp;<?php // echo sSITE_CONTACT_NO;             ?></span> 
                                <span class="icon-set"><i class="fa fa-shopping-cart"></i><a href="#">&nbsp;Checkout&nbsp;<span class="badge">5</span></a></span>
                            </small>-->
                        </div>			
                    </div>
                    <div class="phone_address col-sm-12">
                        <div class="header_right">
                            <small>
                                    <!--<span class="icon-set"><i class="fa fa-phone"></i><span id="cart-item"></span></span>--> 
                                    <!--<span class="icon-set"><i class="fa fa-phone"></i>&nbsp;<?php echo sSITE_CONTACT_NO; ?></span>--> 
                                <span class="icon-set"><i class="fa fa-envelope"></i>&nbsp;<?php echo sSITE_EMAIL_ADDRESS_PRIMARY; ?></span> 
                            </small>
                        </div>			
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end header  area --> 
<!--Start nav  area --> 

<script>
    jQuery("document").ready(function ($) {

        var nav = $('.nav-container');

        $(window).scroll(function () {
            if ($(this).scrollTop() > 70) {
                nav.addClass("f-nav");
            } else {
                nav.removeClass("f-nav");
            }

        });

    });

    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        //console.log(scroll);
        if (scroll >= 70) {
            //console.log('a');
            $(".nav-container-new").addClass("change");
        } else {
            //console.log('a');
            $(".nav-container-new").removeClass("change");
        }
    });
</script>	

<div class="nav_area nav-container">
    <div class="container">
        <div class="row">
            <!--nav item-->
            <div class="col-md-12 col-sm-12 col-xs-12">
                <!--  nav menu-->
                <nav class="menu">
                    <ul class="navid pull-left">
<!--                        <li><a href="<?php echo URL ?>">Home </a></li>
                        <li><a href="courses">Courses <i class="fa fa-angle-down"></i></a>
                            <ul>
                        <?php foreach ($coursecats as $c): ?>
                                                                                                                                                                        <li><a href="<?php echo URL . 'courses/' . $c['slug'] ?>"><?php echo $c['title'] ?></a></li>
                        <?php endforeach; ?>
                                <li><a href="<?php echo URL . 'free-courses' ?>">Free Courses</a></li>
                            </ul>							
                        </li>-->
                        <li class="nav-container-new"><a href="<?php echo URL ?>" style="padding:2px 0"><img src="theme/front/img/logo_white.png" width="110"> </a></li>
                        <?php foreach ($menues as $m): ?>
                            <li>
                                <a href="<?php echo $m['link'] ?>"><?php echo $m['title'] ?> <?php echo $m['submenues'] ? '<i class="fa fa-angle-down"></i>' : '' ?></a>
                                <?php if ($m['submenues']): ?>
                                    <ul>
                                        <?php
                                        foreach ($m['submenues'] as $sm):
                                            $sTarget = (isset($sm['link_open_type']) && !empty($sm['link_open_type']) && $sm['link_open_type'] != 'none') ? $sm['link_open_type'] : '';
                                            ?>
                                            <li><a href="<?php echo $sm['link'] ?>" target="<?php echo $sTarget; ?>"><?php echo $sm['title'] ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </nav>
                <!--end  nav menu-->	
                <div class="search pull-right">
                    <div class="search-box">
                        <input type="text" class="form_control" placeholder="search" />
                        <span class="search-open"><i class="fa fa-search search"></i><i class="fa fa-close hidden close"></i></span>
                    </div>
                </div>
            </div>
            <!--end nav item -->
        </div>	
    </div>

</div>
<!--end nav  area -->

<!--Start mobile menu  area -->
<div class="mobile_memu_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="mobile_memu">
                    <!--  nav menu-->
                    <nav>
                        <ul class="navid">
<!--                            <li><a href="<?php // echo URL      ?>">Home </a></li>
                            <li><a href="courses">Courses <i class="fa fa-angle-down"></i></a>
                                <ul>
                            <?php // foreach ($coursecats as $c): ?>
                                        <li><a href="<?php // echo URL . 'courses/' . $c['slug']      ?>"><?php echo $c['title'] ?></a></li>
                            <?php // endforeach; ?>
                                    <li><a href="<?php // echo URL . 'free-courses'      ?>">Free Courses</a></li>
                                </ul>							
                            </li>-->

                            <?php foreach ($menues as $m): ?>
                                <li>
                                    <a href="<?php echo $m['link'] ?>"><?php echo $m['title'] ?> <?php echo $m['submenues'] ? '<i class="fa fa-angle-down"></i>' : '' ?></a>
                                    <?php if ($m['submenues']): ?>
                                        <ul>
                                            <?php foreach ($m['submenues'] as $sm): ?>
                                                <li><a href="<?php echo $sm['link'] ?>"><?php echo $sm['title'] ?></a></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    <?php endif; ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </nav>
                    <!--end  nav menu-->
                </div>
            </div>
        </div>
    </div>
</div>
<!--end mobile menu  area --> 
