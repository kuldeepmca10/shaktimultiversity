<div class="teacher_area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title">
                    <h3 class="module-title">
                        Our fellow  <span>Masters</span>
                    </h3>
                </div>
            </div>
        </div>	
    </div>	
    <div class="container">
        <?php
        
        foreach ($teachers as $i => $r): ?>
            <!--start teacher single  item -->
            <div class="single_teacher_item <?php echo $i == 0 ? 'single_teacher_item_new' : '' ?>">
                <div class="teacher_thumb">
                    <img src="<?php echo UP_URL . 'teachers/' . $r['image'] ?>" alt="" />
                    <div class="thumb_text">
                        <h2><?php echo $r['title'] ?></h2>
                        <p><?php echo $r['cat'] ?></p>
                    </div>
                </div>
                <div class="teacher_content">
                    <h2><?php echo $r['title'] ?></h2>
                    <span><?php echo $r['cat'] ?></span>
                    <p><?php echo str_short($r['short_description'], 50) ?></p>
                    <p><a href="master-detail/<?php echo $r['slug'] ?>" class="">Read More...</a></p>
                    <div class="social_icons">
                        <?php if (isset($r['facebook_url']) && !empty($r['facebook_url'])) { ?>
                            <a href="<?php echo $r['facebook_url']; ?>"  class="fb" target="_blank"><i class="fa fa-facebook" ></i></a>
                        <?php } ?>
                        <?php if (isset($r['twitter_url']) && !empty($r['twitter_url'])) { ?>
                            <a href="<?php echo $r['twitter_url']; ?>"  class="tw" target="_blank"><i class="fa fa-twitter" ></i></a>
                        <?php } ?>
                        <?php if (isset($r['linkedin_url']) && !empty($r['linkedin_url'])) { ?>
                            <a href="<?php echo $r['linkedin_url']; ?>" class="lin" target="_blank"><i class="fa fa-linkedin" ></i></a>
                        <?php } ?>
                        <?php if (isset($r['google_plus_url']) && !empty($r['google_plus_url'])) { ?>
                            <a href="<?php echo $r['google_plus_url']; ?>"  class="go" target="_blank"><i class="fa fa-google-plus" ></i></a>
                            <?php } ?>
                    </div>
                </div>
            </div>
            <!--End teacher single  item -->
        <?php endforeach; ?>
    </div>
</div>