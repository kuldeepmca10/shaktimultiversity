<!-- modernizr JS
============================================ -->
<script src="theme/front/js/vendor/modernizr-2.8.3.min.js"></script>

<!-- bootstrap JS
============================================ -->		
<script src="theme/front/js/bootstrap.min.js"></script>
<!-- wow JS
============================================ -->		
<script src="theme/front/js/wow.min.js"></script>
<!-- Nivo Slider JS -->
<script src="theme/front/js/jquery.nivo.slider.pack.js"></script> 		
<!-- meanmenu JS
============================================ -->		
<script src="theme/front/js/jquery.meanmenu.min.js"></script>
<!-- owl.carousel JS
============================================ -->		
<script src="theme/front/js/owl.carousel.min.js"></script>
<!-- scrollUp JS
============================================ -->		
<script src="theme/front/js/jquery.scrollUp.min.js"></script>
<!-- Apple TV Effect -->
<script src="theme/front/js/atvImg-min.js"></script>
<!-- Add venobox js -->
<script src="theme/front/venobox/venobox.min.js"></script>
<!-- plugins JS
============================================ -->		
<script src="theme/front/js/plugins.js"></script>
<!-- main JS
============================================ -->		
<script src="theme/front/js/main.js"></script>


<script>
    $(document).ready(function () {
        $("#sInformationModal").modal('show');
    });
</script>
<!-- Form Modal -->
<div class="modal fade" id="sGETEnrollNowModal" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog width_800 modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Batch Information</h4>
            </div>

            <form id="courseBatchform">
                <div class="modal-footer">
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" role="dialog" id="sInformationModal"  aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog width_600 modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="font-size: 18px">WARNING!!</h4>
            </div>
            <form>
                <div class="modal-footer">
                    <div class="text-center"  style="font-size: 14px;font-weight: 500">
                        <p>This website is under construction and testing!!</p>
                        <p>Please do not make any purchases whatsoever as we will not be able to honour that.</p>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Sat -->

<!-- Form Modal -->
<div class="modal fade" id="sLoginModal" role="dialog" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog width_800 modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Login</h4>
            </div>

            <form id="courseform">
                <div class="modal-footer">
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Sat -->
<?php
if (isset($jquery_ui)) {
    add_jquery_ui();
}
?>
<script src="assets/common-js/util.js?v=<?php echo VERSION ?>"></script>
<script src="assets/front/js/site.js?v=<?php echo VERSION ?>"></script>

<!-- If any form validation error -->
<?php set_error_css($errors); ?>
<!-- / -->

<?php if (isset($JS) AND $JS): ?>
    <?php foreach ($JS as $j): ?>
        <script src="<?php echo $j; ?><?php echo strpos($j, '?') === FALSE ? '?' : '&' ?>q=<?php echo VERSION; ?>"></script>
    <?php endforeach; ?>
<?php endif; ?>