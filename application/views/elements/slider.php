<div class="container-fluid" style="padding-left:0;padding-right:0">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <?php foreach ($sliders as $i => $r): ?>
                <li data-target="#myCarousel" data-slide-to="<?php echo $i ?>" class="<?php echo $i == 0 ? 'active' : '' ?>"></li>
            <?php endforeach; ?>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <?php foreach ($sliders as $i => $r): ?>

                <div class="item <?php echo $i == 0 ? 'active' : '' ?>">
                    <?php if (isset($r['link']) && !empty($r['link'])) { ?>
                        <a href="<?php echo isset($r['link']) && !empty($r['link']) ? $r['link'] : 'javascript::void(0)' ?>" target="_blank" >
                        <?php } ?>
                        <img src="<?php echo UP_URL . "slider/" . $r['image'] ?>" alt="" style="width:100%;">
                        <!--                    <div class="carousel-caption">
                                                <h3><?php // echo $r['title']               ?></h3>
                                                <p><?php // echo $r['subtitle']               ?></p>
                                            </div>-->
                        <?php if (isset($r['link']) && !empty($r['link'])) { ?>
                        </a>
                    <?php } ?>
                </div>

            <?php endforeach; ?>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>