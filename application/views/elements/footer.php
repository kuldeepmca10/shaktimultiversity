<?php
$sControllerName = $this->uri->segment(1);
$menues = menues('Footer');

$aCountries = $this->common->countries();

if ($sControllerName == '' || (isset($sControllerName) && !empty($sControllerName) && strtolower($sControllerName) != 'user' && strtolower($sControllerName) != 'testimonials')) {
    $this->load->view("elements/quicklinks");
    $this->load->view("elements/testimonials");
}
?>

<!-- footer  area -->	
<div class="footer_area">
    <div class="container">
        <div class="row">
            <!-- widget area -->

            <?php foreach ($menues as $m): ?>
                <div class="col-md-2 col-sm-6">
                    <!--single widget item -->
                    <div class="single_widget">
                        <div class="widget_content">
                            <h2><?php echo $m['title'] ?></h2>
                            <ul>
                                <?php foreach ($m['submenues'] as $sm): ?>
                                    <li><a href="<?php echo $sm['link'] ?>"><?php echo $sm['title'] ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <!--single widget item -->
                </div>
            <?php endforeach; ?>

            <div class="col-md-4 col-sm-6">
                <!--single widget item -->

                <div class="single_widget">
                    <div class="widget_content">
                        <h2>Subscribe Newsletter</h2>
                        <p>Get latest Articles, Updates & News</p>
                        <div class="inputbox">
                            <form name="sSubscribeNewsletter" id="sSubscribeNewsletter" action="" method="post">
                                <input type="email" placeholder="your email here" id="subscriber_email" name="subscriber_email"/>
                                <button type="button" id="sSubscribeButton" class="read_more buttons">Subscribe</button> 
                                    <!--<i class="fa fa-graduation-cap"></i>-->
                            </form>
                        </div>
                    </div>
                </div>                                      
                <!--single widget item -->

            </div>				
            <div class="col-md-4 col-sm-6">
                <!--single widget item -->
                <div class="single_widget">
                    <div class="widget_content">
                        <h2>Get In Touch</h2>
                        <ul>
                            <li><a href="<?php echo sSITE_GOOGLE_MAP_ADDRESS; ?>" target="_blank"><strong>Registered Office &
                                        Correspondence Address:</strong><br/>
                                    <?php echo sSITE_ADDRESS; ?>
                                </a>
                                <a href="<?php echo sSITE_GOOGLE_MAP_ADDRESS; ?>" target="_blank">See Us on Google Map</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="single_widget">
                    <div class="widget_content">
                        <!--<p><strong>Call:</strong><?php echo sSITE_CONTACT_NO; ?></p>-->
                        <p><strong>Email:</strong>
                            <?php echo sSITE_EMAIL_ADDRESS_PRIMARY; ?>
                            <?php echo sSITE_EMAIL_ADDRESS_SECONDARY; ?>
                        </p>
                    </div>
                </div>
                <!--single widget item -->
            </div>

            <!-- end widget area -->
        </div>
    </div>
</div>	
<!-- end footer  area -->

<!-- footer bottom area -->
<div class="footer_bottom_area">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">

                <div class="footer_text">
                    <div class="col-sm-9 col-md-9 col-lg-9" style="float:left;padding-left: 0px !important">
                        <p>
                            Copyright © <?php echo date('Y'); ?> Shaktimultiversity. All Rights Reserved. 
                        </p>
                    </div>
                    <div class="col-sm-3 col-md-3 col-lg-3">	
                        <p>
                            <a href="<?php echo sSITE_FACEBOOK_LINK; ?>" target="_blank"><i class="fa fa-facebook-square font25" aria-hidden="true"></i></a>
                            <a href="<?php echo sSITE_GOOGLE_PLUS_LINK; ?>" target="_blank"><i class="fa fa-google-plus-square font25" aria-hidden="true"></i></a>
                            <a href="<?php echo sSITE_TWITTER_LINK; ?>" target="_blank"><i class="fa fa-twitter-square font25" aria-hidden="true"></i></a>
                            <!--<a href="<?php echo sLINKEDIN_PAGE_URL; ?>" target="_blank"><i class="fa fa-linkedin-square font25" aria-hidden="true"></i></a>-->
                            <a href="<?php echo sSITE_YOUTUBE_LINK; ?>" target="_blank"><i class="fa fa-youtube-square font25" aria-hidden="true"></i></a>
                        </p>
                    </div>		
                </div>
            </div>			
        </div>
    </div>	
</div>
<!-- Modal -->
<div id="sMessageDialog" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>
                    <div id="sMessageDialogIcon"><i class="fa fa-exclamation"></i></div>
                    <span id="sMessageDialogTitle"></span>
                </h4>
            </div>
            <div class="modal-body">
                <div id="sMessageDialogContent"></div>
                <button type="button" class="black-btn popup-ok" data-dismiss="modal" aria-label="Close">OK</button>
            </div>
        </div>
    </div>
</div>

<div id="sFeedbackDialog" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>
                    <div id="sFeedbackDialogIcon"><i class="fa fa-exclamation"></i></div>
                    <span id="sFeedbackDialogTitle">Send Feedback</span>
                </h4>
            </div>
            <div class="modal-body">
                <div id="sFeedbackDialogContent">
                    <form name="sFeedbackForm" id="sFeedbackForm" action="" method="post">
                        <div class="col-lg-12 form-group">
                            <div class="col-lg-12 text-left">
                                <label>Message<span class="required-hint">*</span></label>
                            </div>
                            <div class="col-lg-12">
                                <textarea name="sFeedbackMessage" id="sFeedbackMessage" cols="40" style="height:150px" required rows="5"></textarea>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="col-lg-6">
                                <input type="hidden" name="sEnrollmentID" id="sEnrollmentID" value="" />
                                <button type="button" class="btn btn-nerdy login_btn" id="sendFeedbackSubmit">SUBMIT
                                    <i id="sLoaderRetrive" class="fa fa-refresh fa-spin" style="display:none;color: white; margin-top: 3px; margin-left: 11px;"></i>
                                </button>
                            </div>
                            <div class="col-lg-6">&nbsp;</div>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>

                <!--<button type="button" class="black-btn popup-ok" data-dismiss="modal" aria-label="Close">OK</button>-->
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div id="sMessageHomeVideoDialog" class="modal fade" role="dialog">
    <div class="modal-dialog width_600">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" onclick="pauseAllVideos()">&times;</button>
                <!--                <h4>
                                    <div id="sMessageDialogIcon"><i class="fa fa-exclamation"></i></div>
                                    <span id="sMessageDialogTitle"></span>
                                </h4>-->
            </div>
            <div class="modal-body">
                <div><iframe width="100%" style="max-width: 540px" height="315" src="https://www.youtube.com/embed/hTDfgyjP5oU?feature=oembed&rel=0" frameborder="0" allowfullscreen></iframe></div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="sCourseEnqueryModal" class="modal fade" role="dialog" style="z-index: 9999">
    <div class="modal-dialog width_600">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>
                    <span id="sCourseEnquiryTitle">Batch Enquiry Form</span>
                </h4>
            </div>
            <div class="modal-body">
                <form  action="javascript::;" id="sCourseEnqueryForm" name="sCourseEnqueryForm" novalidate="true" autocomplete="off">
                    <div class="row">
                        <div class="col-lg-4"><label>Name</label></div>
                        <div class="col-lg-8"><input type="text" class="form-control" name="sEnquiryName" id="sEnquiryName" value="" ></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4"><label>Email</label></div>
                        <div class="col-lg-8"><input type="email" class="form-control" name="sEnquiryEmail" id="sEnquiryEmail" value="" ></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4"><label>Country</label></div>
                        <div class="col-lg-8"><input type="text" class="form-control" name="sEnquiryCountry" id="sEnquiryCountry" value="" ></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4"><label>Message</label></div>
                        <div class="col-lg-8">
                            <textarea rows="5" cols="50" name="sEnquiryMessage" class="form-control" id="sEnquiryMessage"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">&nbsp;</div>
                        <div class="col-lg-8 text-left">
                            <input type="hidden" class="form-control" name="sCourseName" id="sCourseName" value="" >
                            <input type="hidden" class="form-control" name="sCourseId" id="sCourseId" value="" >
                            <button type="submit" name="sCourseEnqueryButton" id="sCourseEnqueryButton" class="read_more buttonc">
                                Submit
                                <i id="sLoaderRetrive" class="fa fa-refresh fa-spin" style="display:none;color: white; margin-top: 3px; margin-left: 11px;"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="sServiceEnqueryModal" class="modal fade" role="dialog" style="z-index: 9999">
    <div class="modal-dialog width_600">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>
                    <span id="sServiceEnquiryTitle">Batch Enquiry Form</span>
                </h4>
            </div>
            <div class="modal-body">
                <form  action="javascript::;" id="sServiceEnqueryForm" name="sServiceEnqueryForm" novalidate="true" autocomplete="off">
                    <div class="row">
                        <div class="col-lg-4"><label>Name</label></div>
                        <div class="col-lg-8"><input type="text" class="form-control" name="sEnquiryName" id="sServiceEnquiryName" value="" ></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4"><label>Email</label></div>
                        <div class="col-lg-8"><input type="email" class="form-control" name="sEnquiryEmail" id="sServiceEnquiryEmail" value="" ></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4"><label>Country</label></div>
                        <div class="col-lg-8">
                            <select id="sEnquiryCountry" name="sEnquiryCountry" class="form-control" >
                                <option value=""> -- Select --</option>
                                <?php
                                if (isset($aCountries) && !empty($aCountries)) {
                                    foreach ($aCountries as $aCountry) {
                                        echo ' <option  value="' . ucwords(strtolower($aCountry['name'])) . '">' . ucwords(strtolower($aCountry['name'])) . '</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4"><label>Message</label></div>
                        <div class="col-lg-8">
                            <textarea rows="5" cols="50" name="sEnquiryMessage" class="form-control" id="sServicesEnquiryMessage"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">&nbsp;</div>
                        <div class="col-lg-8 text-left">
                            <input type="hidden" class="form-control" name="sCourseName" id="sServiceName" value="" >
                            <input type="hidden" class="form-control" name="sCourseId" id="sServiceId" value="" >
                            <button type="submit" name="sServiceEnqueryButton" id="sServiceEnqueryButton" class="read_more buttonc">
                                Submit
                                <i id="sLoaderRetrive" class="fa fa-refresh fa-spin" style="display:none;color: white; margin-top: 3px; margin-left: 11px;"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="sBookEnqueryModal" class="modal fade" role="dialog" style="z-index: 9999">
    <div class="modal-dialog width_600">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>
                    <span id="sBookEnquiryTitle">Batch Enquiry Form</span>
                </h4>
            </div>
            <div class="modal-body">
                <form  action="javascript::;" id="sBookEnqueryForm" name="sBookEnqueryForm" novalidate="true" autocomplete="off">
                    <div class="row">
                        <div class="col-lg-4"><label>Name</label></div>
                        <div class="col-lg-8"><input type="text" class="form-control" name="sBookEnquiryName" id="sBookEnquiryName" value="" ></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4"><label>Email</label></div>
                        <div class="col-lg-8"><input type="email" class="form-control" name="sBookEnquiryEmail" id="sBookEnquiryEmail" value="" ></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4"><label>Mobile</label></div>
                        <div class="col-lg-8"><input type="number" class="form-control" onkeypress="return isNumberKey(event)" maxlength="10" minlength="10"  name="sBookEnquiryMobile" id="sBookEnquiryMobile" value="" ></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4"><label>Quantity</label></div>
                        <div class="col-lg-8"><input type="number" class="form-control" onkeypress="return isNumberKey(event)" maxlength="3" minlength="1" name="sBookEnquiryQuantity" id="sBookEnquiryQuantity" value="" ></div>
                    </div>
                    <!--                    <div class="row">
                                            <div class="col-lg-4"><label>Country</label></div>
                                            <div class="col-lg-8">
                                                <select id="sEnquiryCountry" name="sEnquiryCountry" class="form-control" >
                                                    <option value=""> -- Select --</option>
                    <?php
//                                if (isset($aCountries) && !empty($aCountries)) {
//                                    foreach ($aCountries as $aCountry) {
//                                        echo ' <option  value="' . ucwords(strtolower($aCountry['name'])) . '">' . ucwords(strtolower($aCountry['name'])) . '</option>';
//                                    }
//                                }
                    ?>
                                                </select>
                                            </div>
                                        </div>-->
                    <div class="row">
                        <div class="col-lg-4"><label>Address</label></div>
                        <div class="col-lg-8">
                            <textarea rows="2" cols="10" name="sBookEnquiryAddress" class="form-control" id="sBookEnquiryAddress"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4"><label>Pin Code</label></div>
                        <div class="col-lg-8"><input type="text" class="form-control" name="sBookEnquiryPinCode" id="sBookEnquiryPinCode" value="" ></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4"><label>Message</label></div>
                        <div class="col-lg-8">
                            <textarea rows="5" cols="20" name="sBooksEnquiryMessage" class="form-control" id="sBooksEnquiryMessage"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">&nbsp;</div>
                        <div class="col-lg-8 text-left">
                            <input type="hidden" class="form-control" name="sBookName" id="sBookName" value="" >
                            <input type="hidden" class="form-control" name="sBookId" id="sBookId" value="" >
                            <button type="submit" name="sBookEnqueryButton" id="sBookEnqueryButton" class="read_more buttonc">
                                Submit
                                <i id="sLoaderRetrive" class="fa fa-refresh fa-spin" style="display:none;color: white; margin-top: 3px; margin-left: 11px;"></i>
                            </button>
                            Note:- We deliver in India only.
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script type="text/javascript">
                            function pauseAllVideos()
                            {
                                $("iframe").each(function () {
                                    var src = $(this).attr('src');
                                    $(this).attr('src', src);
                                });
                            }
                            function isNumberKey(evt)
                            {
                                var charCode = (evt.which) ? evt.which : event.keyCode
                                if (charCode > 31 && (charCode < 48 || charCode > 57))
                                    return false;

                                return true;
                            }
                            function sCourseEnquery(courseID, courseName) {
                                $('#sCourseEnqueryForm')[0].reset();
                                $("#sCourseEnquiryTitle").html('Batch Enquiry For ' + courseName);
                                $("#sCourseId").val(courseID);
                                $("#sCourseName").val(courseName);
                                $("#sGETEnrollNowModal").modal('hide');
                                $("#sCourseEnqueryModal").modal('show');
                            }
                            function fnBookEnquery(bookID, bookName) {
                                $('#sBookEnqueryForm')[0].reset();
                                $("#sBookEnquiryTitle").html('Enquiry For ' + bookName);
                                $("#sBookId").val(bookID);
                                $("#sBookName").val(bookName);
                                $("#sGETEnrollNowModal").modal('hide');
                                $("#sBookEnqueryModal").modal('show');
                            }
                            function fnServiceEnquery(courseID, courseName) {
                                $('#sServiceEnqueryForm')[0].reset();
                                $("#sServiceEnquiryTitle").html('Enquiry For ' + courseName);
                                $("#sServiceId").val(courseID);
                                $("#sServiceName").val(courseName);
                                $("#sGETEnrollNowModal").modal('hide');
                                $("#sServiceEnqueryModal").modal('show');
                            }
                            $(document).ready(function () {
                                $("#sMessageHomeVideo").click(function () {
                                    $("#sMessageHomeVideoDialog").modal('show');
                                });

                                $("#sMessageHomeVideo").click(function () {
                                    $("#sMessageHomeVideoDialog").modal('show');
                                });

                                $('#sCourseEnqueryButton').click(function () {
                                    var sEnquiryName = $('#sEnquiryName').val();
                                    var sEnquiryEmail = $('#sEnquiryEmail').val();
                                    var sEnquiryCoutry = $('#sEnquiryCoutry').val();
                                    var sEnquiryMessage = $('#sEnquiryMessage').val();
                                    if (sEnquiryName == '') {
                                        $("#sMessageDialogTitle").html('WARNING');
                                        $("#sMessageDialogContent").html('Please provide your name');
                                        $("#sMessageDialog").modal('show');
                                    } else if (sEnquiryEmail == '') {
                                        $("#sMessageDialogTitle").html('WARNING');
                                        $("#sMessageDialogContent").html('Please provide email');
                                        $("#sMessageDialog").modal('show');
                                    } else if (sEnquiryEmail != '' && !validateEmail(sEnquiryEmail)) {
                                        $("#sMessageDialogTitle").html('WARNING');
                                        $("#sMessageDialogContent").html('Please provide valid email');
                                        $("#sMessageDialog").modal('show');
                                    } else if (sEnquiryCoutry == '') {
                                        $("#sMessageDialogTitle").html('WARNING');
                                        $("#sMessageDialogContent").html('Please provide country');
                                        $("#sMessageDialog").modal('show');
                                    } else if (sEnquiryMessage == '') {
                                        $("#sMessageDialogTitle").html('WARNING');
                                        $("#sMessageDialogContent").html('Please provide message');
                                        $("#sMessageDialog").modal('show');
                                    } else {
                                        $('#sLoaderRetrive').show();
                                        $('#sCourseEnqueryButton').attr('disabled', true);
                                        $.ajax({
                                            type: "POST",
                                            url: "page/course_enquiry",
                                            data: $('#sCourseEnqueryForm').serialize(),
                                            cache: false,
                                            success: function (data) {
                                                var data = jQuery.parseJSON(data);
                                                $('#sLoaderRetrive').hide();
                                                $('#sCourseEnqueryButton').attr('disabled', false);
                                                $("#sMessageDialogTitle").html(data.title);
                                                $("#sMessageDialogIcon").html(data.icon);
                                                $("#sMessageDialogContent").html(data.message);
                                                $("#sMessageDialog").modal('show');
                                                $('#sCourseEnqueryForm')[0].reset();
                                                $("#sCourseEnqueryModal").modal('hide');
                                            }
                                        });
                                    }
                                });

                                $('#sServiceEnqueryButton').click(function () {
                                    var sEnquiryName = $('#sServiceEnquiryName').val();
                                    var sEnquiryEmail = $('#sServiceEnquiryEmail').val();
                                    var sEnquiryCoutry = $('#sServiceEnquiryCountry').val();
                                    var sEnquiryMessage = $('#sServicesEnquiryMessage').val();
                                    if (sEnquiryName == '') {
                                        $("#sMessageDialogTitle").html('WARNING');
                                        $("#sMessageDialogContent").html('Please provide your name');
                                        $("#sMessageDialog").modal('show');
                                    } else if (sEnquiryEmail == '') {
                                        $("#sMessageDialogTitle").html('WARNING');
                                        $("#sMessageDialogContent").html('Please provide email');
                                        $("#sMessageDialog").modal('show');
                                    } else if (sEnquiryEmail != '' && !validateEmail(sEnquiryEmail)) {
                                        $("#sMessageDialogTitle").html('WARNING');
                                        $("#sMessageDialogContent").html('Please provide valid email');
                                        $("#sMessageDialog").modal('show');
                                    } else if (sEnquiryCoutry == '') {
                                        $("#sMessageDialogTitle").html('WARNING');
                                        $("#sMessageDialogContent").html('Please provide country');
                                        $("#sMessageDialog").modal('show');
                                    } else if (sEnquiryMessage == '') {
                                        $("#sMessageDialogTitle").html('WARNING');
                                        $("#sMessageDialogContent").html('Please provide message');
                                        $("#sMessageDialog").modal('show');
                                    } else {
                                        $('#sLoaderRetrive').show();
                                        $('#sCourseEnqueryButton').attr('disabled', true);
                                        $.ajax({
                                            type: "POST",
                                            url: "page/service_enquiry",
                                            data: $('#sServiceEnqueryForm').serialize(),
                                            cache: false,
                                            success: function (data) {
                                                var data = jQuery.parseJSON(data);
                                                $('#sLoaderRetrive').hide();
                                                $('#sServiceEnqueryButton').attr('disabled', false);
                                                $("#sMessageDialogTitle").html(data.title);
                                                $("#sMessageDialogIcon").html(data.icon);
                                                $("#sMessageDialogContent").html(data.message);
                                                $("#sMessageDialog").modal('show');
                                                $('#sServiceEnqueryForm')[0].reset();
                                                $("#sServiceEnqueryModal").modal('hide');
                                            }
                                        });
                                    }
                                });
                                $('#sBookEnqueryButton').click(function () {
                                    var sEnquiryName = $('#sBookEnquiryName').val();
                                    var sEnquiryEmail = $('#sBookEnquiryEmail').val();
                                    var sBookEnquiryQuantity = $('#sBookEnquiryQuantity').val();
                                    var sEnquiryCoutry = $('#sBookEnquiryCountry').val();
                                    var sEnquiryAddress = $('#sBookEnquiryAddress').val();
                                    var sBookEnquiryPinCode = $('#sBookEnquiryPinCode').val();
                                    var sEnquiryMessage = $('#sBooksEnquiryMessage').val();
                                    if (sEnquiryName == '') {
                                        $("#sMessageDialogTitle").html('WARNING');
                                        $("#sMessageDialogContent").html('Please provide your name');
                                        $("#sMessageDialog").modal('show');
                                    } else if (sEnquiryEmail == '') {
                                        $("#sMessageDialogTitle").html('WARNING');
                                        $("#sMessageDialogContent").html('Please provide email');
                                        $("#sMessageDialog").modal('show');
                                    } else if (sEnquiryEmail != '' && !validateEmail(sEnquiryEmail)) {
                                        $("#sMessageDialogTitle").html('WARNING');
                                        $("#sMessageDialogContent").html('Please provide valid email');
                                        $("#sMessageDialog").modal('show');
                                    } else if (sBookEnquiryQuantity == '') {
                                        $("#sMessageDialogTitle").html('WARNING');
                                        $("#sMessageDialogContent").html('Please provide quantity');
                                        $("#sMessageDialog").modal('show');
                                    } else if (sEnquiryCoutry == '') {
                                        $("#sMessageDialogTitle").html('WARNING');
                                        $("#sMessageDialogContent").html('Please provide country');
                                        $("#sMessageDialog").modal('show');
                                    } else if (sEnquiryAddress == '') {
                                        $("#sMessageDialogTitle").html('WARNING');
                                        $("#sMessageDialogContent").html('Please enter address');
                                        $("#sMessageDialog").modal('show');
                                    } else if (sBookEnquiryPinCode == '') {
                                        $("#sMessageDialogTitle").html('WARNING');
                                        $("#sMessageDialogContent").html('Please enter pin code');
                                        $("#sMessageDialog").modal('show');
                                    } else if (sEnquiryMessage == '') {
                                        $("#sMessageDialogTitle").html('WARNING');
                                        $("#sMessageDialogContent").html('Please provide message');
                                        $("#sMessageDialog").modal('show');
                                    } else {
                                        $('#sLoaderRetrive').show();
                                        $('#sCourseEnqueryButton').attr('disabled', true);
                                        $.ajax({
                                            type: "POST",
                                            url: "page/book_enquiry",
                                            data: $('#sBookEnqueryForm').serialize(),
                                            cache: false,
                                            success: function (data) {
                                                var data = jQuery.parseJSON(data);
                                                $('#sLoaderRetrive').hide();
                                                $('#sBookEnqueryButton').attr('disabled', false);
                                                $("#sMessageDialogTitle").html(data.title);
                                                $("#sMessageDialogIcon").html(data.icon);
                                                $("#sMessageDialogContent").html(data.message);
                                                $("#sMessageDialog").modal('show');
                                                $('#sBookEnqueryForm')[0].reset();
                                                $("#sBookEnqueryModal").modal('hide');
                                            }
                                        });
                                    }
                                });

                                $('#sSubscribeButton').click(function () {
                                    var sEmailID = $('#subscriber_email').val();
                                    if (sEmailID == '') {
                                        $("#sMessageDialogTitle").html('WARNING');
                                        $("#sMessageDialogContent").html('Please provide email');
                                        $("#sMessageDialog").modal('show');
                                    } else if (sEmailID != '' && !validateEmail(sEmailID)) {
                                        $("#sMessageDialogTitle").html('WARNING');
                                        $("#sMessageDialogContent").html('Please provide valid email');
                                        $("#sMessageDialog").modal('show');
                                    } else {
                                        $.ajax({
                                            type: "POST",
                                            url: "auth/subscribe_newsletter",
                                            data: $('#sSubscribeNewsletter').serialize(),
                                            cache: false,
                                            success: function (data) {
                                                var data = jQuery.parseJSON(data);
                                                //                        if (data.status == true) {
                                                //                            $('#sSubscribeNewsletter')[0].reset();
                                                //                        }
                                                $("#sMessageDialogTitle").html(data.title);
                                                $("#sMessageDialogIcon").html(data.icon);
                                                $("#sMessageDialogContent").html(data.message);
                                                $("#sMessageDialog").modal('show');
                                                $('#sSubscribeNewsletter')[0].reset();

                                            }
                                        });
                                    }
                                });
                            });

                            function validateEmail(email) {
                                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                                return re.test(email);
                            }
</script>