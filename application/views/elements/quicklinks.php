<?php
$quick_links = quick_links();
?>

<section id="quick_links">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 quick_links_red text-center">
                <div class="quick_links">Quick Links</div>
            </div>
            <div class="col-md-9 quick_links_white">
                <ul>
                    <?php foreach ($quick_links as $r): ?>
                        <li><a href="<?php echo $r['link'] ?>"><i class="<?php echo $r['icon_class'] ?>" aria-hidden="true"></i> <?php echo $r['title'] ?></a></li>
                    <?php endforeach; ?>
                </ul>				
            </div>
        </div>
    </div>	
</section>