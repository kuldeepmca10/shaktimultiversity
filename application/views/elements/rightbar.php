<?php
$aHappenings = $this->common->selected_happening();
$aFirstSidebarBanner = sidebar_banner(2);
$aSecondSidebarBanner = sidebar_banner(1);
?>
<?php if (isset($aFirstSidebarBanner) && !empty($aFirstSidebarBanner)) { ?>
    <div class="met-box">
        <figure>      
            <?php
            if (isset($aFirstSidebarBanner['show_title']) && $aFirstSidebarBanner['show_title'] == 'yes') {
                echo ' <h3 class="met-box-title">' . $aFirstSidebarBanner['title'] . '</h3>';
            }
            $sURL = 'javascript::void(0)';
            if (isset($aFirstSidebarBanner['url']) && isset($aFirstSidebarBanner['type']) && $aFirstSidebarBanner['type'] == 'link') {
                $sURL = $aFirstSidebarBanner['url'];
            }
            ?>
            <a href="<?php echo $sURL; ?>">
                <img src="uploads/sidebar-lg/<?php echo $aFirstSidebarBanner['image']; ?>" alt="" />
            </a>
        </figure>						
    </div>
<?php } ?>
<?php if (isset($aHappenings) && !empty($aHappenings)) { ?>
    <div class="met-box">
        <h3 class="met-box-title">Announcements</h3>
        <?php foreach ($aHappenings as $i => $r): ?>
            <div class="row <?php echo $i > 0 ? 'mt25' : '' ?>">
                <div class="col-md-4 col-xs-4 thumbnail_img">
                    <a href="announcement-detail/<?php echo $r['slug'] ?>"><img src="<?php echo UP_URL . 'happenings-sm/' . $r['image'] ?>" alt="" /></a>
                </div>
                <div class="col-md-8 col-xs-8 news_title">
                    <p><a href="announcement-detail/<?php echo $r['slug'] ?>"><?php echo $r['title'] ?></a></p>
                    <p class="color_gray font12"><i class="fa fa-calendar-o" aria-hidden="true"></i> <?php echo date('l, d M Y', strtotime($r['publish_date'])) ?></p>
                </div>
                <div class="clearfix"></div>
            </div>
        <?php endforeach; ?>

        <div class="row">
            <div class="col-md-12"><h4><a href="happenings" class="text_uppercase more_read">All Events</a></h4></div>
        </div>									
    </div>
<?php } ?>
<?php if (isset($aSecondSidebarBanner) && !empty($aSecondSidebarBanner)) { ?>
    <div class="met-box">
        <?php
        if (isset($aSecondSidebarBanner['show_title']) && $aSecondSidebarBanner['show_title'] == 'yes') {
            echo ' <h3 class="met-box-title">' . $aSecondSidebarBanner['title'] . '</h3>';
        }
        ?>															
        <div class="priging_thumb">
            <?php
            $sURL = 'javascript::void(0)';
            if (isset($aSecondSidebarBanner['url']) && isset($aSecondSidebarBanner['type']) && $aSecondSidebarBanner['type'] == 'link') {
                $sURL = $aSecondSidebarBanner['url'];
            }
            ?>
            <a href="<?php echo $sURL; ?>">
                <img class="atvImg-layer" src="uploads/sidebar-lg/<?php echo $aSecondSidebarBanner['image']; ?>"/>
            </a>

        </div>
        <div class="tutor_content">
            <h4><a href="<?php echo $sURL; ?>" class="text_uppercase more_read">Explore Our Shop Now</a></h4>
        </div>
    </div>
<?php } ?>  