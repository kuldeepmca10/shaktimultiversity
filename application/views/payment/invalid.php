<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo URL ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li>Payment Fail</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->

<div class="about_area page text-center">
    <div class="container animated bounceInDown">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12"></div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="thank-you">
                        <img src="<?php echo URL; ?>theme/front/img/fail.png">
                        <div class="heading-text"><h2>Invalid Transaction</strong></h2>
                            <div class="dubble-border"></div>
                        </div>						

                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12"></div>
            </div>		
            <div class="row">
                <div class="col-md-12 col-sm-6">
                    <div class="about-lead-box">
                        <h3>Your Payment has been failed due to one of the following reasons:</h3>
                        <ul class="list_item">
                            <li>Your payment has been declined by your bank. Please contact your bank for any queries. If money has been deducted from your account, kindly inform us within 48 hrs and we will refund the same.</li>
                            <li>Your payment has been declined by your bank. You can try again now or contact your bank for any queries.</li>
                            <li>It may be causes due to Duplicate order id</li>
                            <li>3D secure verification of your card has failed. Please enter the correct 3D Secure verification code.</li>
                            <li>You presses cancel before logging in (WEB)</li>
                        </ul>
                    </div>
                    <div class="about_us">
                        <p>We strive to give the best possible answers to your queries and will get back to you soon.</p>					
                    </div>
                </div>

                <div class="col-md-12 col-sm-12">
                    <div class="about_bottom_text">
                        <p>For further query or suggestions, please feel free to contact us at</p>
                        <p style="color:#d83135; font-weight:bold"><?php echo sSITE_EMAIL_ADDRESS_PRIMARY; ?></p>
                        <!--<p style="color:#d83135; font-weight:bold">+91-7985 928 336 | info@shaktimultiversity.com</p>-->
                        <p></p>
                    </div>
                </div>				
            </div>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-12">
            <div class="social">
                <div class="col-md-2"></div>
                <div class="col-md-2"><a href="<?php echo sSITE_FACEBOOK_LINK; ?>" class="link facebook" target="_blank"><i class="fa fa-facebook-square"></i></a></div>
                <div class="col-md-2"><a href="<?php echo sSITE_TWITTER_LINK; ?>" class="link twitter" target="_blank"><i class="fa fa-twitter"></i></a></div>
                <div class="col-md-2"> <a href="<?php echo sSITE_GOOGLE_PLUS_LINK; ?>" class="link google-plus" target="_blank"><i class="fa fa-google-plus-square"></i></a></div>
                <div class="col-md-2"><a href="<?php echo sSITE_YOUTUBE_LINK; ?>" class="link youtube" target="_blank"><i class="fa fa-youtube-square"></i></a></div><div class="col-md-2"></div>
                <div class="col-md-2"></div>
            </div>
        </div>


    </div>		

</div>