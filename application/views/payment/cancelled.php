<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo URL ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li>Payment Cancelled</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->

<div class="about_area page text-center">
    <div class="container animated bounceInDown">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12"></div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="thank-you">
                        <img src="<?php echo URL; ?>theme/front/img/fail.png">
                        <div class="heading-text"><h2>Sorry... <strong>Payment Cancelled</strong></h2>
                            <div class="dubble-border"></div>
                        </div>						

                    </div>
                </div>
                <div class="col-md-3 col-sm-12 col-xs-12"></div>
            </div>		
            <div class="row">
                <div class="col-md-12 col-sm-6">
                    <div class="about-lead-box">
                        <h3>Oops! You cancelled the payment? What can we do for you?</h3>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="about_bottom_text">
                        <p>&nbsp;</p>
                        <p>For further query or suggestions, please feel free to contact us at</p>
                        <p style="color:#d83135; font-weight:bold"><?php echo sSITE_EMAIL_ADDRESS_PRIMARY; ?></p>
                        <!--<p style="color:#d83135; font-weight:bold">+91-7985 928 336 | info@shaktimultiversity.com</p>-->
                        <p></p>
                    </div>
                </div>				
            </div>
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-12">
            <div class="social">
                <div class="col-md-2"></div>
                <div class="col-md-2"><a href="<?php echo sSITE_FACEBOOK_LINK; ?>" class="link facebook" target="_blank"><i class="fa fa-facebook-square"></i></a></div>
                <div class="col-md-2"><a href="<?php echo sSITE_TWITTER_LINK; ?>" class="link twitter" target="_blank"><i class="fa fa-twitter"></i></a></div>
                <div class="col-md-2"> <a href="<?php echo sSITE_GOOGLE_PLUS_LINK; ?>" class="link google-plus" target="_blank"><i class="fa fa-google-plus-square"></i></a></div>
                <div class="col-md-2"><a href="<?php echo sSITE_YOUTUBE_LINK; ?>" class="link youtube" target="_blank"><i class="fa fa-youtube-square"></i></a></div><div class="col-md-2"></div>
                <div class="col-md-2"></div>
            </div>
        </div>


    </div>		

</div>