<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="index.html">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li>Thank You</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->		

<div class="page text-center">
    <div class="container animated bounceInDown">

        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12"></div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="thank-you">
                    <img src="<?php echo URL; ?>theme/front/img/success.jpg">
                    <div class="heading-text"><h2>Congratulations! <strong> <?php echo isset($result[0]['name']) ? ucwords(strtolower($result[0]['name'])) : 'N/A'; ?></strong></h2>
                        <div class="dubble-border"></div>
                    </div>					
                    <p>You have successfully applied for <strong><?php echo isset($result[0]['course_title']) ? ucwords(strtolower($result[0]['course_title'])) : 'N/A'; ?></strong> at <strong>Shakti Multiversity</strong>. Your payment has been done successfully</p>
                    <a href="#" target="_blank"><button class="btnn"><span class="glyphicon glyphicon-list-alt"></span> Download Receipt</button></a>
                    <p class="mt10 italic">Keep this payment receipt for any further communication. You will receive your course approval within 
                        24 hours on the registered E-mail ID and mobile Number.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12"></div>
            <div class="col-md-12 col-sm-12 col-xs-12 thanku-bg">
                <div class="background-yellow">
                    <h3>Your Registration ID is <strong><?php echo isset($result[0]['enrollment_number']) ? strtoupper($result[0]['enrollment_number']) : 'N/A'; ?></strong></h3>
                    <p>And the same already has been sent to your registered mobile number and E-mail ID.</p>
                    <a href="#" download><button class="btnn"><span class="glyphicon glyphicon-save-file red"></span> Download Brochure</button></a>

                </div>
            </div>
        </div>		
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="about_bottom_text">
                    <p>For further query or suggestions, please feel free to contact us at</p>
                    <p style="color:#d83135; font-weight:bold"><?php // echo sSITE_CONTACT_NO; ?><?php echo sSITE_EMAIL_ADDRESS_PRIMARY; ?></p>
                    <p></p>
                </div>
            </div>				
        </div>
        <div class="col-md-2"></div>
        <div class="col-md-12">
            <div class="social">
                <div class="col-md-2"></div>
                <div class="col-md-2"><a href="<?php echo sSITE_FACEBOOK_LINK; ?>" class="link facebook" target="_blank"><i class="fa fa-facebook-square"></i></a></div>
                <div class="col-md-2"><a href="<?php echo sSITE_TWITTER_LINK; ?>" class="link twitter" target="_blank"><i class="fa fa-twitter"></i></a></div>
                <div class="col-md-2"> <a href="<?php echo sSITE_GOOGLE_PLUS_LINK; ?>" class="link google-plus" target="_blank"><i class="fa fa-google-plus-square"></i></a></div>
                <div class="col-md-2"><a href="<?php echo sSITE_YOUTUBE_LINK; ?>" class="link youtube" target="_blank"><i class="fa fa-youtube-square"></i></a></div><div class="col-md-2"></div>
                <div class="col-md-2"></div>
            </div>
        </div>


    </div>		

</div>

<!--end about  area -->	