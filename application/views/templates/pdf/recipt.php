<div class="about_area page text-center">
    <div class="container animated bounceInDown">
        <div class="row">            
            <table width="100%" align="center" border="1" style="border:solid 1px #ccc">
                <tr>
                    <td colspan="4">
                        <table border="1">
                            <tr>
                                <td width="80%" colspan="2" align="center"><h1>Performa Invoice</h1></td>
                                <td width="20%" rowspan="3" align="right"><img src="<?php echo URL; ?>theme/front/img/logo.png"></td>
                            </tr>
                            <tr>
                                <td width="100%" colspan="2"  align="center">The Shaktimultiversity | <a href="<?php echo URL; ?>">www.shaktimultiversity.com</a></td>
                            </tr> 
                            <tr>
                                <td width="100%" colspan="2"  align="center"><b>Registered Office:</b> <?php echo sSITE_ADDRESS; ?> | Email: info@shaktimultiversity.com</td>
                            </tr>
                            <tr>
                                <td width="60%" colspan="2"></td>
                                <td width="10%" align="center">GSTIN</td>
                                <td width="20%" rowspan="3" align="center">Not Applicable</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">Customer Details</td>
                </tr>
                <tr>
                    <td width="20%">Name</td>
                    <td width="80%" colspan="3" align="left">Not Applicable</td>
                </tr>
                <tr>
                    <td width="20%">Address</td>
                    <td width="80%" colspan="3" align="left">Not Applicable</td>
                </tr>
                <tr>
                    <td width="20%">Customer GSTIN</td>
                    <td width="30%" align="left">Not Applicable</td>
                    <td width="20%">Invoice #</td>
                    <td width="30%" align="left">Date</td>
                </tr>
                <tr>
                    <td width="20%">Place Of Supply</td>
                    <td width="30%" align="left"><?php echo $sCountry; ?></td>
                    <td width="20%"><?php echo $sEnrollmentId; ?></td>
                    <td width="30%" align="left"><?php echo $sEnrollmentId; ?></td>
                </tr>
                <tr>
                    <td colspan="4">Product-wise Details:</td>
                </tr>
                <tr>
                    <td colspan="4">
                        <table border="1" align="center">
                            <tr>
                                <td align="center">Sr. No.</td>
                                <td align="center">Course Name & Description</td>
                                <td align="center">SAC Code<br/>999273</td>
                                <td align="center">Qty</td>
                                <td align="center">Price</td>
                                <td align="center">Discount</td>
                                <td align="center">Price After Discount</td>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td><?php echo $sCourseTitle; ?></td>
                                <td></td>
                                <td><?php echo $sCourseTitle; ?></td>
                                <td><?php echo $sCoursePrice; ?></td>
                                <td>0</td>
                                <td>0</td>
                            </tr>
                            <tr>
                                <td colspan="5"></td>
                                <td>Tax</td>
                                <td>0</td>
                            </tr>
                            <tr>
                                <td colspan="4"></td>
                                <td>Net Payable</td>
                                <td>Total</td>
                                <td>0</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="center">We thank you for choosing The Shakti Multiversity plateform for learning and growing with us!</td>
                </tr>
            </table>
        </div>
    </div>		

</div>