<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Thank You | Shakti Multiversity 2017</title>
    </head>

    <body>

        <table width="700" border="0" align="center" cellpadding="0" cellspacing="0" style="border:1px solid #999">
            <tbody><tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody><tr>
                                    <td width="2%">&nbsp;</td>
                                    <td width="96%">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td width="100%" style="height: 80px; text-align: left"><img src="<?php echo URL; ?>theme/front/img/logo.png" class="CToWUd"></td>
                                                   <!-- <td width="46%">&nbsp;
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="right">
                                                            <tbody><tr>
                                                                    <td width="24%"><img src="<?php echo URL; ?>theme/front/img/call-icon.png" width="71" height="80" class="CToWUd"></td>
                                                                    <td width="76%" style="font-family:Arial,Helvetica,sans-serif;font-size:16px;color:#08368a; padding:10px">Call us for any queries
                                                                        <br />
                                                                        <strong style="font-size:24px"> +91-7985 928 336</strong>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>-->
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="2%">&nbsp;</td>
                                </tr>
                            </tbody></table></td>
                </tr>

                <tr>
                    <td style="background:#ffd200;float:left;width:100%;height:5px"></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody><tr>
                                    <td>&nbsp;</td>
                                    <td style="background:#f1f2f4">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="background:#f1f2f4">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td width="2%">&nbsp;</td>
                                                    <td width="95%" style="font-family:Arial,Helvetica,sans-serif;font-size:13px">
                                                        <p style="font-family:Arial,Helvetica,sans-serif;font-size:18px;color:#333333">Dear <?php echo $name; ?>,</p>
                                                        <p>Greetings!!!</p>
                                                        <p>Thank you for contacting The Shakti Multiversity for <b>Lifetime Guidance Programme</b> under the mentorship of Ma Shakti & Acharya Agyaatadarshan Anand Nath. We are happy to see you recognizing the supreme benefits of staying under the guidance of your Masters. Still it would be great to enlist the benefits you shall gain upon enrolling for this Programme.</p>
                                                        <p style="font-family:Arial,Helvetica,sans-serif;font-size:18px;color:#333333">The Benefits & Opportunities</p>
                                                        <p>
                                                            1. You can choose to spend 21 days in a year the in the Master's premises and attend daily routine of Yoga, Meditation-Sadhana and Satsang* free of cost. The only expenditure you would probably incur (that too discounted for members) during your stay would be on food, laundry and local visits (if you choose to take up) and of course medication (if solicited).
                                                            <br/>
                                                            2. You can attend the ONLINE and OFFLINE courses at a discount 10%-30%.
                                                            <br/>
                                                            3. All the products whether purchased from our website or Ashram Sale Counter, will also be bear the special discounts for you.
                                                            <br/>
                                                            4. You will be getting the Free membership of the Periodicals/Journals of The Shakti Multiversity.
                                                            <br/>
                                                            5. In Ashram premises you shall have free access to the Library where assorted books on Tantra, Spirituality, Yoga and Life would be available.
                                                            <br/>
                                                            6. You shall be able to derive the pleasure of service by involving in the community Projects of the Masters in neighbouring villages.
                                                            <br/>
                                                            7. You shall get direct access to the Masters for any queries, questions or guidance relating to your current Sadhana.**
                                                            <br/>
                                                            Since you are in the process of making a life-changing decision we must give you first cut presentation of the selection-criterion and general rules. 
                                                        </p>
                                                        <p style="font-family:Arial,Helvetica,sans-serif;font-size:18px;color:#333333"> Eligibility & Brief on General Rules -</p>
                                                        <p>
                                                            1. Only Tattva Shakti Vigyaan Practitioners (Having at least Pratham Koti Deeksha with practice of minimum 1 year) shall be eligible to enroll in this programme. If you wish to bring in your partner/spouse he/she should also qualify as Tattva Shakti Vigyaan Practitioner (as stated above). No exception to rule shall ever be allowed.
                                                            <br/>
                                                            2. During your stay in the Master's premises you will have to abide by the Dress Code and other rules communicated to you communicated to you from time to time during your stay. 
                                                            <br/>
                                                            3. You are free to opt out of this programme at any point in time. No questions asked however No refund of fees is given.
                                                            <br/>
                                                            4. The decision of your selection in The Shakti Multiversity's Lifetime Guidance Programme is prerogative of Masters (Ma Shakti Devpriya & Acharya Agyaatadarshan). Also on behalf of The Shakti Multiversity they reserve the right to expel anyone from this programme at any point of time without giving any reason whosoever.
                                                            <br/>
                                                            Thank you once again for your query. Your request is in the queue. Please wait for the next mail from us. If your candidature is accepted by Masters you shall be receiving a detailed mail containing an application form and further instructions on how to proceed.
                                                            <br/>
                                                            In case you do not receive any response from us in next 30 days you are free to initiate a fresh query.
                                                            <br/><br/>
                                                            In the service of divinity in you,
                                                            <br/><br/>
                                                            <?php echo sMAIL_SIGNATURE; ?>
                                                            <!--                                                            <a href="http://www.shaktimultiversity.com" target="_blank">www.shaktimultiversity.com</a><br/>
                                                                                                                        <a href="http://www.sacredassociation.org" target="_blank">www.sacredassociation.org</a><br/>-->
                                                            <p style="font-family:Arial,Helvetica,sans-serif;font-size:12px">* Master's availability Calendar shall be published 6 months in Advance and The Shakti Multiversity shall try its best to adhere to the calendar to ensure Acharya ji's or Ma's availability on proposed dates. However changes in their availability schedule (Due to any unforeseen circumstances or emergencies) may be possible. In that case during your stay you shall be guided by a suitable Teacher as appointed by Masters..
                                                                <br/><br/>
                                                                **In general your mails/queries regarding your Current Sadhana or challenges therefore shall be directly answered by Masters however in certain circumstances representatives duly authorized by them can mediate the conversation.
                                                            </p> 
                                                    </td>
                                                    <td width="3%">&nbsp;</td>
                                                </tr>
<!--                                                <tr>
                                                    <td width="2%">&nbsp;</td>
                                                    <td><?php // echo sMAIL_SIGNATURE;  ?></td>
                                                    <td width="3%">&nbsp;</td>
                                                </tr>-->
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="2%">&nbsp;</td>
                                    <td width="96%" style="background:#f1f2f4">&nbsp;</td>
                                    <td width="2%">&nbsp;</td>
                                </tr>
                            </tbody></table></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td style="background:#ffd200;font-family:Arial,Helvetica,sans-serif;font-size:13px;color:#000; text-align:center; padding:10px" align="cente">For further details, kindly email us at&nbsp;<a href="mailto:info@shaktimultiversity.com" target="_blank">info@shaktimultiversity.com</a></td>
                </tr>
                <tr>
                    <td style="background:#ffd200;font-family:Arial,Helvetica,sans-serif;font-size:13px;color:#000" align="center">
                        <strong>Correspondence Address:</strong><br><?php echo sSITE_ADDRESS; ?></td>
                </tr>
                <tr>
                    <td style="background:#ffd200">&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <?php $this->load->view("templates/mail/mail_footer_layout"); ?>                
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </tbody></table>


    </body>
</html>
