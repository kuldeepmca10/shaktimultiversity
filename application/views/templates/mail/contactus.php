<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Thank You | Shakti Multiversity 2017</title>
    </head>

    <body>

        <table width="700" border="0" align="center" cellpadding="0" cellspacing="0" style="border:1px solid #999">
            <tbody><tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody><tr>
                                    <td width="2%">&nbsp;</td>
                                    <td width="96%">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td width="100%" style="height: 80px; text-align: left"><img src="<?php echo URL; ?>theme/front/img/mail/logo.png" class="CToWUd"></td>
                                                   <!-- <td width="46%">&nbsp;
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="right">
                                                            <tbody><tr>
                                                                    <td width="24%"><img src="<?php echo URL; ?>/theme/front/img/mail/call-icon.png" width="71" height="80" class="CToWUd"></td>
                                                                    <td width="76%" style="font-family:Arial,Helvetica,sans-serif;font-size:16px;color:#08368a; padding:10px">Call us for any queries
                                                                        <br />
                                                                        <strong style="font-size:24px"> +91-7985 928 336</strong>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>-->
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="2%">&nbsp;</td>
                                </tr>
                            </tbody></table></td>
                </tr>

                <tr>
                    <td style="background:#ffd200;float:left;width:100%;height:5px"></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody><tr>
                                    <td>&nbsp;</td>
                                    <td style="background:#f1f2f4">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="background:#f1f2f4"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody><tr>
                                                    <td width="2%">&nbsp;</td>
                                                    <td width="95%" style="font-family:Arial,Helvetica,sans-serif;font-size:13px">
                                                        <p style="font-family:Arial,Helvetica,sans-serif;font-size:18px;color:#333333">Dear Admin,</p>
                                                        <P> Please find the query details below:</P>
                                                        <P> Date & Time: <?php echo date('d M, Y h:i A'); ?></P>
                                                        <P> Name: <?php echo $name; ?></P>
                                                        <P> Email: <?php echo $email; ?></P>
                                                        <P> Message: <?php echo $message; ?></P>
                                                    </td>
                                                    <td width="3%">&nbsp;</td>
                                                </tr>
                                            </tbody></table></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="2%">&nbsp;</td>
                                    <td width="96%" style="background:#f1f2f4">&nbsp;</td>
                                    <td width="2%">&nbsp;</td>
                                </tr>
                            </tbody></table></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td style="background:#ffd200;font-family:Arial,Helvetica,sans-serif;font-size:13px;color:#000; text-align:center; padding:10px" align="cente">For further details, kindly email us at&nbsp;<a href="mailto:info@shaktimultiversity.com" target="_blank">info@shaktimultiversity.com</a></td>
                </tr>
                <tr>
                    <td style="background:#ffd200;font-family:Arial,Helvetica,sans-serif;font-size:13px;color:#000" align="center">
                        <strong>Correspondence Address:</strong><br><?php echo sSITE_ADDRESS; ?></td>
                </tr>
                <tr>
                    <td style="background:#ffd200">&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="text-align:center">
                        <a href="https://www.facebook.com/shardauniversity" target="_blank"><img src="<?php echo URL; ?>/theme/front/img/mail/facebook.png" width="40" height="41" alt="facebook" class="CToWUd"></a>
                        <a href="https://www.linkedin.com/" target="_blank"><img src="<?php echo URL; ?>/theme/front/img/mail/linkedin.png" width="40" height="41" alt="linkedin" class="CToWUd"></a>
                        <a href="https://www.pinterest.com/" target="_blank"><img src="<?php echo URL; ?>/theme/front/img/mail/pin.png" width="40" height="41" alt="Pinterest" class="CToWUd"></a> 
                        <a href="https://www.flickr.com/" target="_blank"><img src="<?php echo URL; ?>/theme/front/img/mail/flickr.png" width="40" height="41" alt="facebook" class="CToWUd"></a>
                        <a href="http://shaktimultiversity.com/blog/" target="_blank"><img src="<?php echo URL; ?>/theme/front/img/mail/blog.png" width="40" height="41" alt="Blog" class="CToWUd"></a>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </tbody></table>


    </body>
</html>
