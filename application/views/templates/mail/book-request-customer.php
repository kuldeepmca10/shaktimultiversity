<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Thank You | Shakti Multiversity 2017</title>
    </head>

    <body>

        <table width="700" border="0" align="center" cellpadding="0" cellspacing="0" style="border:1px solid #999">
            <tbody><tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody><tr>
                                    <td width="2%">&nbsp;</td>
                                    <td width="96%">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td width="100%" style="height: 80px; text-align: left"><img src="<?php echo URL; ?>theme/front/img/logo.png" class="CToWUd"></td>
                                                   <!-- <td width="46%">&nbsp;
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="right">
                                                            <tbody><tr>
                                                                    <td width="24%"><img src="<?php echo URL; ?>theme/front/img/call-icon.png" width="71" height="80" class="CToWUd"></td>
                                                                    <td width="76%" style="font-family:Arial,Helvetica,sans-serif;font-size:16px;color:#08368a; padding:10px">Call us for any queries
                                                                        <br />
                                                                        <strong style="font-size:24px"> +91-7985 928 336</strong>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>-->
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="2%">&nbsp;</td>
                                </tr>
                            </tbody></table></td>
                </tr>

                <tr>
                    <td style="background:#ffd200;float:left;width:100%;height:5px"></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody><tr>
                                    <td>&nbsp;</td>
                                    <td style="background:#f1f2f4">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="background:#f1f2f4">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td width="2%">&nbsp;</td>
                                                    <td width="95%" style="font-family:Arial,Helvetica,sans-serif;font-size:13px;line-height: 20px">
                                                        <p style="font-family:Arial,Helvetica,sans-serif;font-size:18px;color:#333333">Dear <?php echo $name; ?>,</p>
                                                        <p>Greetings!!!</p>
                                                        <p>Thank you for showing your interest in the book <?php echo $book_name; ?>.  WE ARE PROCESSING YOUR REQUEST. Please note as of now we do not deliver our books outside India. On select books we have delivery arrangement with Amazon so If are residing outside India then please check its availability on Amazon and make the purchase from there.</p>
                                                        <p>
                                                            1. If your address is clear and found eligible for delivery as per our list of locations we can serve we shall share a payment link with you within 72 Hrs.
                                                            <br/>
                                                            2. Upon receipt of that link please follow the link and complete the payment process.
                                                            <br/>
                                                            3. Once you make the payment on info@shaktimultiversity.com please email us the payment reference details with us.
                                                            <br/>
                                                            4. Post your payment confirmation we shall be shipping the book to you in 3-4 working day.
                                                            <br/>
                                                            5. Please READ OUR <a href="<?php echo URL . 'refund-policy.html'; ?>" target="_blank">REFUND POLICY</a> before making the payment.
                                                        </p>
                                                        <p>
                                                            <strong>NOTE -</strong> By making a choice to proceed for payment you show your agreement with The Shakti Multiversity's - Refund Policy.
                                                            <br/><br/>
                                                            <?php echo sMAIL_SIGNATURE; ?>
                                                    </td>
                                                    <td width="3%">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="2%">&nbsp;</td>
                                    <td width="96%" style="background:#f1f2f4">&nbsp;</td>
                                    <td width="2%">&nbsp;</td>
                                </tr>
                            </tbody></table></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <?php $this->load->view("templates/mail/mail_footer_layout"); ?>                
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </tbody></table>


    </body>
</html>
