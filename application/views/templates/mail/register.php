<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Thank You | Shakti Multiversity 2017</title>
    </head>

    <body>

        <table width="700" border="0" align="center" cellpadding="0" cellspacing="0" style="border:1px solid #999">
            <tbody><tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody><tr>
                                    <td width="2%">&nbsp;</td>
                                    <td width="96%">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td width="100%" style="height: 80px; text-align: left"><img src="<?php echo URL; ?>theme/front/img/logo.png" class="CToWUd"></td>
                                                   <!-- <td width="46%">&nbsp;
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="right">
                                                            <tbody><tr>
                                                                    <td width="24%"><img src="<?php echo URL; ?>theme/front/img/call-icon.png" width="71" height="80" class="CToWUd"></td>
                                                                    <td width="76%" style="font-family:Arial,Helvetica,sans-serif;font-size:16px;color:#08368a; padding:10px">Call us for any queries
                                                                        <br />
                                                                        <strong style="font-size:24px"> +91-7985 928 336</strong>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>-->
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="2%">&nbsp;</td>
                                </tr>
                            </tbody></table></td>
                </tr>

                <tr>
                    <td style="background:#ffd200;float:left;width:100%;height:5px"></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody><tr>
                                    <td>&nbsp;</td>
                                    <td style="background:#f1f2f4">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="background:#f1f2f4"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody><tr>
                                                    <td width="2%">&nbsp;</td>
                                                    <td width="95%" style="font-family:Arial,Helvetica,sans-serif;font-size:13px">
                                                        <p style="font-family:Arial,Helvetica,sans-serif;font-size:18px;color:#333333">Dear <?php echo $name; ?>,</p>

                                                        <P>Welcome to Shakti Multiversity.</P>
                                                        <P>Congratulations! Your account has been created.</P>
                                                        <P>Please click the following link to login into your account</P>
                                                        <P><a href="<?php echo URL; ?>login" target="_blank">https://www.shaktimultiversity.com/login</a></P>
                                                        <P>Below are your login details to access your account:</P>
                                                        <table style="font-family:tahoma,sans-serif;font-size:small;border-collapse:collapse;border-width:medium;border-style:none;border-color:-moz-use-text-color" cellspacing="0" cellpadding="0" border="1">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="width:150pt;border-width:1pt;border-style:solid;border-color:windowtext;padding:0in 5.4pt;" width="200" valign="middle">
                                                                        <p style="font-family:tahoma,sans-serif;font-size:small;text-align:center; margin:5px;" align="center"><strong>Email</strong></p>
                                                                    </td>
                                                                    <td style="width:279pt;border-width:1pt 1pt 1pt medium;border-style:solid solid solid none;border-color:windowtext windowtext windowtext -moz-use-text-color;padding:0in 5.4pt;" width="372" valign="middle">
                                                                        <p style="font-family:tahoma,sans-serif;font-size:small;text-align:center; margin:5px;" align="center"><?php echo $email; ?></p>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width:150pt;border-width:medium 1pt 1pt;border-style:none solid solid;border-color:-moz-use-text-color windowtext windowtext;padding:0in 5.4pt;" width="200" valign="middle">
                                                                        <p style="font-family:tahoma,sans-serif;font-size:small;text-align:center; margin:5px;" align="center"><strong>Password</strong></p>
                                                                    </td>
                                                                    <td style="width:279pt;border-width:medium 1pt 1pt medium;border-style:none solid solid none;border-color:-moz-use-text-color windowtext windowtext -moz-use-text-color;padding:0in 5.4pt;" width="372" valign="middle">
                                                                        <p style="font-family:tahoma,sans-serif;font-size:small;text-align:center; margin:5px;" align="center"><?php echo $password; ?></p>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td width="3%">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td width="2%">&nbsp;</td>
                                                    <td><?php echo sMAIL_SIGNATURE; ?></td>
                                                    <td width="3%">&nbsp;</td>
                                                </tr>
                                            </tbody></table></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="2%">&nbsp;</td>
                                    <td width="96%" style="background:#f1f2f4">&nbsp;</td>
                                    <td width="2%">&nbsp;</td>
                                </tr>
                            </tbody></table></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td style="background:#ffd200;font-family:Arial,Helvetica,sans-serif;font-size:13px;color:#000; text-align:center; padding:10px" align="cente">For further details, kindly email us at&nbsp;<a href="mailto:info@shaktimultiversity.com" target="_blank">info@shaktimultiversity.com</a></td>
                </tr>
                <tr>
                    <td style="background:#ffd200;font-family:Arial,Helvetica,sans-serif;font-size:13px;color:#000" align="center">
                        <strong>Correspondence Address:</strong><br><?php echo sSITE_ADDRESS; ?></td>
                </tr>
                <tr>
                    <td style="background:#ffd200">&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <?php $this->load->view("templates/mail/mail_footer_layout"); ?>                
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </tbody></table>


    </body>
</html>
