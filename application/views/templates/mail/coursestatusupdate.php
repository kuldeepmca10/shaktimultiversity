<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Thank You | Shakti Multiversity 2017</title>
    </head>

    <body>

        <table width="700" border="0" align="center" cellpadding="0" cellspacing="0" style="border:1px solid #999">
            <tbody><tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody><tr>
                                    <td width="2%">&nbsp;</td>
                                    <td width="96%">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td width="100%" style="height: 80px; text-align: left"><img src="<?php echo URL; ?>theme/front/img/logo.png" class="CToWUd"></td>
                                                   <!-- <td width="46%">&nbsp;
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" align="right">
                                                            <tbody><tr>
                                                                    <td width="24%"><img src="<?php echo URL; ?>/theme/front/img/call-icon.png" width="71" height="80" class="CToWUd"></td>
                                                                    <td width="76%" style="font-family:Arial,Helvetica,sans-serif;font-size:16px;color:#08368a; padding:10px">Call us for any queries
                                                                        <br />
                                                                        <strong style="font-size:24px"> +91-7985 928 336</strong>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>-->
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="2%">&nbsp;</td>
                                </tr>
                            </tbody></table></td>
                </tr>

                <tr>
                    <td style="background:#ffd200;float:left;width:100%;height:5px"></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody><tr>
                                    <td>&nbsp;</td>
                                    <td style="background:#f1f2f4">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="background:#f1f2f4">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <?php if ($status == 'approved') { ?>
                                                    <tr>
                                                        <td width="2%">&nbsp;</td>
                                                        <td width="95%" style="font-family:Arial,Helvetica,sans-serif;font-size:13px">
                                                            <p style="font-family:Arial,Helvetica,sans-serif;font-size:18px;color:#333333">Dear <?php echo ucwords(strtolower($name)); ?>,</p>
                                                            <P>Congratulations!</P>
                                                            <P style="font-weight:bold">Your enrollment (Number : <?php echo $enrollment_number; ?>) for course <?php echo $course_name; ?> is <?php echo $status; ?>.</P>
                                                            <P>We at The Shakti Multiversity cordially welcome you.</P>
                                                            <P>Your first session is scheduled on <?php echo date('d-M-Y', strtotime($aSessionDetail['date'])); ?> on <?php echo date('H:i', strtotime($aSessionDetail['time'])); ?> IST (IST = GMT+530)</P>
                                                            <P>The details of the sessions can also be found in ‘My Dashboard’ section. Please ensure you make yourself available to attend all the sessions on specified dates and time as missed sessions can not be rescheduled.</P>
                                                            <P>Please <b>DO NOT MISS</b> to read <b style="color:red">IMPORTANT NOTE</b> at the end of this mail.</P>
                                                        </td>
                                                        <td width="3%">&nbsp;</td>
                                                    </tr>
                                                <?php } else if ($status == 'rejected') { ?>
                                                    <tr>
                                                        <td width="2%">&nbsp;</td>
                                                        <td width="95%" style="font-family:Arial,Helvetica,sans-serif;font-size:13px">
                                                            <p style="font-family:Arial,Helvetica,sans-serif;font-size:18px;color:#333333">Dear <?php echo ucwords(strtolower($name)); ?>,</p>
                                                            <P>Regarding your application for <?php echo $course_name; ?>.</P>
                                                            <P>We regret to inform you that your request for enrollment has been rejected.</P>
                                                            <P>As per the criteria clearly stated in the 'Eligibility' section of 'Course Details page', you are NOT ELIGIBLE to attend this course.</P>
                                                            <P>AS PER POLICY WE DO NOT REFUND FEE ONCE PAID  – Please refer to our <a href="<?php echo URL . 'faq' ?>" target="_blank">refund policy</a></P>
                                                        </td>
                                                        <td width="3%">&nbsp;</td>
                                                    </tr>
                                                <?php } else { ?>
                                                    <tr>
                                                        <td width="2%">&nbsp;</td>
                                                        <td width="95%" style="font-family:Arial,Helvetica,sans-serif;font-size:13px">
                                                            <p style="font-family:Arial,Helvetica,sans-serif;font-size:18px;color:#333333">Dear <?php echo ucwords(strtolower($name)); ?>,</p>
                                                            <P>Your enrollment for course <?php echo $course_name; ?> is <?php echo $status; ?>.</P>
                                                            <?php if (isset($comment) && !empty($comment)) { ?>
                                                                <P><?php echo $comment; ?></P>
                                                            <?php } ?>
                                                        </td>
                                                        <td width="3%">&nbsp;</td>
                                                    </tr>
                                                <?php } ?>
                                                <tr>
                                                    <td width="2%">&nbsp;</td>
                                                    <td><?php echo sMAIL_SIGNATURE; ?></td>
                                                    <td width="3%">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td width="2%">&nbsp;</td>
                                                    <td><?php if (isset($comment) && !empty($comment)) { ?>
                                                            <p>IMPORTANT NOTE - <span style="color:red"><?php echo $comment; ?></span></P>
                                                        <?php } ?></td>
                                                    <td width="3%">&nbsp;</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="2%">&nbsp;</td>
                                    <td width="96%" style="background:#f1f2f4">&nbsp;</td>
                                    <td width="2%">&nbsp;</td>
                                </tr>
                            </tbody></table></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td style="background:#ffd200;font-family:Arial,Helvetica,sans-serif;font-size:13px;color:#000; text-align:center; padding:10px" align="cente">For further details, kindly email us at&nbsp;<a href="mailto:info@shaktimultiversity.com" target="_blank">info@shaktimultiversity.com</a></td>
                </tr>
                <tr>
                    <td style="background:#ffd200;font-family:Arial,Helvetica,sans-serif;font-size:13px;color:#000" align="center">
                        <strong>Correspondence Address:</strong><br><?php echo sSITE_ADDRESS; ?></td>
                </tr>
                <tr>
                    <td style="background:#ffd200">&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <?php $this->load->view("templates/mail/mail_footer_layout"); ?>                
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </tbody></table>


    </body>
</html>
