<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Thank You | Shakti Multiversity 2017</title>
    </head>

    <body>

        <table width="700" border="0" align="center" cellpadding="0" cellspacing="0" style="border:1px solid #999">
            <tbody><tr>
                    <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody><tr>
                                    <td width="2%">&nbsp;</td>
                                    <td width="96%">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td width="100%" style="height: 80px; text-align: left"><img src="<?php echo URL; ?>theme/front/img/logo.png" class="CToWUd"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="2%">&nbsp;</td>
                                </tr>
                            </tbody></table></td>
                </tr>

                <tr>
                    <td style="background:#ffd200;float:left;width:100%;height:5px"></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tbody><tr>
                                    <td>&nbsp;</td>
                                    <td style="background:#f1f2f4">&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="background:#f1f2f4">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tbody>
                                                <tr>
                                                    <td width="2%">&nbsp;</td>
                                                    <td width="95%" style="font-family:Arial,Helvetica,sans-serif;font-size:13px;line-height: 22px;">
                                                        <p style="font-family:Arial,Helvetica,sans-serif;font-size:18px;color:#333333">Dear <?php echo $name; ?>,</p>
                                                        <p>Welcome to The Shakti Multiversity Lifetime Guidance Programme!</p>
                                                        <p>We are pleased to inform that post the payment formalities duly completed by you recently <b>Your Membership Number <?php echo $membershipid; ?> has been APPROVED* for the 'Lifetime Guidance Programme'</b> by the Masters.</p>
                                                        <p>You are now eligible for all the benefits stated in The Shakti Multiversity's declaration such as :</p>
                                                        <p>
                                                            - 21 Days /Per year stay with Masters under their supervision & guidance.<br/>
                                                            - Special Discounts in all the courses, products and food.<br/>
                                                            - Direct access to Masters for your clarification of your queries.<br/>
                                                            - Access to the select rituals and Homas etc. with permission of masters.<br/>
                                                            - Participation in the Seva and Service projects of The Shakti Multiversity.<br/>
                                                            - You can avail all the inhouse Healing and Yoga modalities at special discounts.<br/> 
                                                            - Free access to Master's library & Shri Shakti Temple.<br/>
                                                        </p>
                                                        <p>Once again The Shakti Multiversity on behalf of its Masters Ma Shakti & Acharya Agyaatadarshan ji congratulate you on acquisition of these special privileges with the blessings of Ma Bhagwati.</p>
                                                        <p>Wishing you a blissful journey with Masters</p>
                                                        <p>In the service of divinity in you</p>

                                                        <?php echo sMAIL_SIGNATURE; ?>
                                                        <p style="font-family:Arial,Helvetica,sans-serif;font-size:12px">
                                                            *The approval and continuation is subjected to payment clearance and your conduct in accordance to the rules of stay in the Master's premises.
                                                        </p> 
                                                    </td>
                                                    <td width="3%">&nbsp;</td>
                                                </tr>
<!--                                                <tr>
                                                    <td width="2%">&nbsp;</td>
                                                    <td><?php // echo sMAIL_SIGNATURE;      ?></td>
                                                    <td width="3%">&nbsp;</td>
                                                </tr>-->
                                            </tbody>
                                        </table>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="2%">&nbsp;</td>
                                    <td width="96%" style="background:#f1f2f4">&nbsp;</td>
                                    <td width="2%">&nbsp;</td>
                                </tr>
                            </tbody></table></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td style="background:#ffd200;font-family:Arial,Helvetica,sans-serif;font-size:13px;color:#000; text-align:center; padding:10px" align="cente">For further details, kindly email us at&nbsp;<a href="mailto:info@shaktimultiversity.com" target="_blank">info@shaktimultiversity.com</a></td>
                </tr>
                <tr>
                    <td style="background:#ffd200;font-family:Arial,Helvetica,sans-serif;font-size:13px;color:#000" align="center">
                        <strong>Correspondence Address:</strong><br><?php echo sSITE_ADDRESS; ?></td>
                </tr>
                <tr>
                    <td style="background:#ffd200">&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <?php $this->load->view("templates/mail/mail_footer_layout"); ?>                
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </tbody></table>


    </body>
</html>
