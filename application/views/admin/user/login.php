
<div class="login-bx">
    <div class="row">
        <!-- header  logo --> 
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="home-logo"><a href="<?php echo URL ?>"><img src="theme/front/img/logo.png" alt="" /></a></div>
        </div>
    </div>
    <h1 class="text-center mb10">Welcome to <span><?php echo SITE_NAME; ?></span></h1>
    <?php
    echo get_flash();
    echo $msg;
    ?>
    <div class="box-grey pd10">
        <?php echo form_open(); ?>
        <div>
            <?php if (isset($passwordSent) && $passwordSent == 'sent') { ?>
                <div class="mb10">
                    <input type="text" name="password" id="password" class="form-control" placeholder="Password" />
                </div>
            <?php } else { ?>
                <div class="mb10 posRel">
                    <input type="text" name="username" id="username" class="form-control" value="<?php echo $dtl['username']; ?>" placeholder="Username" autocomplete="off" autofocus />
                </div>
            <?php } ?>
            <div>
                <button type="submit" class="btn btn-info btn-block">Login</button>
                <a href="javascript::void()" onclick="window.location.reload()" class="btn btn-danger btn-block">Reset</a>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>