<div class="footer">
	<div class="container text-center">
		&copy; <?php echo date('Y');?> - <a href="<?php echo URL;?>"><?php echo SITE_NAME;?>. All Rights Reserved.</a>
    </div>
</div>

<!-- Default Modal -->
<div class="modal fade" id="defaultModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
            
            <!--<div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success">Save</button>
            </div>-->
        </div>
    </div>
</div>

<!-- TinyMce Image Modal -->
<div class="modal fade" id="tinyMceImgModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Select Image</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>