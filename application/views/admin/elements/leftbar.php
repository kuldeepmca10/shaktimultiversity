<div class="logo">
    <a class="uc" href="<?php echo ADM_URL ?>dashboard"  target="_self"><span><?php echo SITE_NAME ?></span></a>
</div>

<ul>
    <li><a href="<?php echo ADM_URL ?>dashboard" target="_self"><i class="fa fa-th-large"></i>Dashboard</a></li>
    <li><a ui-sref="cats" ui-sref-opts="{reload: true}"><i class="fa fa-globe"></i>Manage Categories</a></li>
    <li><a ui-sref="teachers" ui-sref-opts="{reload: true}"><i class="fa fa-institution"></i>Manage Teachers</a></li>
    <li><a ui-sref="students" ui-sref-opts="{reload: true}"><i class="fa fa-globe"></i>Manage Students</a></li>
    <li><a ui-sref="courses" ui-sref-opts="{reload: true}"><i class="fa fa-globe"></i>Manage Courses</a></li>
    <li><a ui-sref="enrollments" ui-sref-opts="{reload: true}"><i class="fa fa-globe"></i>Manage Enrollment</a></li>
    <li><a ui-sref="products" ui-sref-opts="{reload: true}"><i class="fa fa-book"></i>Manage Products</a></li>
    <li><a ui-sref="happenings " ui-sref-opts="{reload: true}"><i class="fa fa-folder-open"></i>Manage Happenings</a></li>
    <li><a ui-sref="gallery" ui-sref-opts="{reload: true}"><i class="fa fa-image"></i>Manage Gallery</a></li>
    <li><a ui-sref="newsletter" ui-sref-opts="{reload: true}"><i class="fa fa-image"></i>Manage Newsletter</a></li>
    <li><a ui-sref="lifetime-service" ui-sref-opts="{reload: true}"><i class="fa fa-image"></i>Manage Lifetime Service</a></li>
    <li><a ui-sref="book-order" ui-sref-opts="{reload: true}"><i class="fa fa-image"></i>Manage Book Request</a></li>
    <li><a ui-sref="cms" ui-sref-opts="{reload: true}"><i class="fa fa-file"></i>CMS Pages</a></li>
    <!--<li><a ui-sref="cms-widget" ui-sref-opts="{reload: true}"><i class="fa fa-file"></i>CMS Widget</a></li>-->
    <li><a ui-sref="sidebar-banner" ui-sref-opts="{reload: true}"><i class="fa fa-file"></i>Sidebar Banner</a></li>
    <li><a ui-sref="testimonials" ui-sref-opts="{reload: true}"><i class="fa fa-globe"></i>Testimonials</a></li>
    <li><a ui-sref="shakticenters" ui-sref-opts="{reload: true}"><i class="fa fa-globe"></i>Shakti Centers</a></li>
    <li><a ui-sref="faq" ui-sref-opts="{reload: true}"><i class="fa fa-question"></i>FAQ's</a></li>
    <li><a ui-sref="menu" ui-sref-opts="{reload: true}"><i class="fa fa-list"></i>Menues & Quick Links</a></li>
    <!--<li><a ui-sref="roles" ui-sref-opts="{reload: true}"><i class="fa fa-circle"></i>Manage Roles</a></li>
    <li><a ui-sref="users" ui-sref-opts="{reload: true}"><i class="fa fa-user"></i>Manage System Users</a></li>-->
</ul>