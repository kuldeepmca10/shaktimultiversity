<script src="theme/plugins/jquery.min.js"></script>
<?php add_jquery_ui();?>

<!-- AngularJs -->
<script src="theme/plugins/angular/angular.min.js"></script>
<script src="theme/plugins/angular/angular-ui-router.min.js"></script>
<script src="theme/plugins/angular/angular-animate.min.js"></script>
<script src="theme/plugins/angular/ocLazyLoad.min.js"></script>

<script src="assets/common-js/angular.config.js?v=<?php echo VERSION?>"></script>
<script src="assets/common-js/angular.admin.auth.js"></script>
<?php if(!$page_view):?>
	<script src="assets/admin/js/url_route.js?v=<?php echo VERSION?>"></script>
    <script src="assets/admin/js/app.js?v=<?php echo VERSION?>"></script>
<?php endif;?>

<script src="assets/admin/js/user.js?v=<?php echo VERSION?>"></script>
<script src="assets/admin/js/student.js?v=<?php echo VERSION?>"></script>
<script src="assets/admin/js/enrollments.js?v=<?php echo VERSION?>"></script>
<!-- / -->

<script src="theme/plugins/fancybox/jquery.fancybox.pack.js"></script>
<script src="theme/plugins/tinymce/js/tinymce/tinymce.min.js"></script>
<script src="theme/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="theme/plugins/select2/js/select2.min.js"></script>

<script src="assets/common-js/util.js?v=<?php echo VERSION?>"></script>
<script src="assets/common-js/validation.js?v=<?php echo VERSION?>"></script>
<script src="assets/admin/js/site.js?v=<?php echo VERSION?>"></script>

<!-- JS Start -->
<?php if(isset($JS) AND $JS):?>
	<?php foreach($JS as $j):?>
		<script src="<?php echo $j;?><?php echo strpos($j, '?')===FALSE?'?':'&'?>q=<?php echo VERSION;?>"></script>
	<?php endforeach;?>
<?php endif;?>

<!-- If any form validation error -->
<?php set_error_css($errors);?>
<!-- / -->

<script>
$(window).load(function(){
	$(".pre-loader").fadeOut("slow");
});

/** **/
$(function(){
	var page_view='<?php if(isset($page_view) and $page_view){echo 'T';}?>';
	if(page_view=='T'){
		$("a[ui-sref]").each(function(){
			var ob=$(this);
			var s=ob.attr('ui-sref');
			ob.removeAttr('ui-sref');
			ob.removeAttr('ui-sref-opts');
			
			ob.attr({'target': '_self', 'href':ADM_URL+'p/'+s});
		});
	}
});
</script>