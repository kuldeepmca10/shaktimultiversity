<?php $logged_data=logged_data('admin');?>
<?php if($logged_data):?>
<header>
	<div class="wel-text uc">
        <i class="fa fa-user"></i>  <?php echo $logged_data['fname']?>,&nbsp;&nbsp; 
        <a ui-sref="profile" ui-sref-opts="{reload: true}">My Profile</a> |
        <a href="<?php echo ADM_URL."auth/logout"?>" target="_self">Logout</a>
    </div>
</header>
<?php endif;?>