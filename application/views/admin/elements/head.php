<!--<base href="<?php // echo ENVIRONMENT=='development'?'/projects/shakti/':'/'  ?>" />-->
<base href="<?php echo URL; ?>" />

<link rel="shortcut icon" type="image/x-icon" href="favicon.png">
<meta name="google" content="notranslate" />

<link rel="stylesheet" href="theme/plugins/bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" href="theme/plugins/font-awesome/css/font-awesome.min.css" />
<link rel="stylesheet" href="theme/plugins/select2/css/select2.min.css" />
<link rel="stylesheet" href="theme/plugins/select2/css/select2-bootstrap.min.css" />
<link rel="stylesheet" href="theme/plugins/fancybox/jquery.fancybox.css" />

<link rel="stylesheet" href="theme/common-css/common.css?v=<?php echo VERSION ?>" />
<link rel="stylesheet" href="theme/admin/css/style.css?v=<?php echo VERSION ?>" />
<link rel="stylesheet" href="theme/common-css/animate.css?v=<?php echo VERSION ?>" />

<?php if (isset($CSS) AND $CSS): ?>
    <?php foreach ($CSS as $c): ?>
        <link rel="stylesheet" href="<?php echo $c; ?><?php echo strpos($c, '?') === FALSE ? '?' : '&' ?>q=<?php echo VERSION; ?>" />
    <?php endforeach; ?>
<?php endif; ?>

<script type="text/javascript">
    var SITE_URL = '<?php echo URL; ?>';
    var ADM_URL = '<?php echo ADM_URL; ?>';
    var CURRENT_URL = '<?php echo CURRENT_URL; ?>';
    var VERSION = '<?php echo VERSION; ?>';
</script>