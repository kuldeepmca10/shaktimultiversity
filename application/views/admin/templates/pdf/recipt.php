<div class="about_area page text-center">
    <div class="container animated bounceInDown">
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12"></div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="thank-you">
                    <div class="heading-text">
                        <h2>Dear <strong><?php echo $sUsername; ?>,</strong></h2>
                        <div class="dubble-border"></div>
                    </div>				
                    <p>You have successfully applied for <strong><?php echo $sCourseTitle; ?></strong> at <strong>Shakti Multiversity</strong>. 
                        <?php if ($sCourseType == 'paid') { ?>Your payment has been done successfully<?php } ?></p>
                    <p class="mt10 italic bold">Your registration details are as mentioned below:</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-12 col-xs-12"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-2"></div>
                <div class="col-md-8 col-sm-12 col-xs-12">
                    <div class="heading-text"><h4>Registration Details</h4>
                        <div class="dubble-border"></div>
                    </div>		
                    <table class="table table-bordered text-left background-blue">
                        <thead>
                            <tr>
                                <th>Registration ID</th>
                                <td><?php echo $sEnrollmentId; ?></td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Email Id</th>
                                <td><?php echo $sEmailId; ?></td>
                            </tr>
                            <tr>
                                <th>Country</th>
                                <td><?php echo $sCountry; ?></td>
                            </tr>
<!--                            <tr>
                                <th>Contact Number</th>
                                <td><?php // echo sSITE_CONTACT_NO;  ?></td>
                            </tr>-->
                            <tr>
                                <th>Course</th>
                                <td><?php echo $sCourseTitle; ?></td>
                            </tr>
                            <tr>
                                <th>Catagory</th>
                                <td><?php echo $sCourseCategory; ?></td>
                            </tr>
                            <tr>
                                <th>Correspondence Address</th>
                                <td><?php echo sSITE_ADDRESS; ?></td>
                            </tr>						  
                        </tbody>
                    </table>
                    <?php if ($sCourseType == 'paid') { ?>
                        <div class="heading-text"><h4>Transaction Details</h4>
                            <div class="dubble-border"></div>
                        </div>	

                        <table class="table table-bordered text-left background-blue">
                            <thead>
                                <tr>
                                    <th>Transaction Amount</th>
                                    <td>INR 3200</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>Transaction Mode</th>
                                    <td>DC - Axis Bank</td>
                                </tr>
                                <tr>
                                    <th>Transaction Status</th>
                                    <td>Txn Tuccess</td>
                                </tr>
                                <tr>
                                    <th>Transaction ID</th>
                                    <td>7056395606966</td>
                                </tr>	
                                <tr>
                                    <th>Transaction Date</th>
                                    <td>26 July 2017</td>
                                </tr>			  
                            </tbody>
                        </table>
                    <?php }
                    ?> 
                    <p class="text-left font12 italic">Keep this  <?php if ($sCourseType == 'paid') { ?>payment <?php } ?>receipt for any further communication. You will receive your course approval within 
                        24 hours on the registered E-mail ID and mobile Number.</p>
                    <p class="text-left bold">Thanks, Shakti Multiversity</p>

                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
    </div>		

</div>