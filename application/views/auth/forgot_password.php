<script>
    $(document).ready(function () {
        $('#sForgotSubmit').click(function () {
            var sEmailID = $('#forgot_email').val();
            var sOtpSend = $('#sOtpSend').val();
            if (sEmailID == '') {
                $("#sMessageDialogTitle").html('WARNING');
                $("#sMessageDialogContent").html('Please provide email');
                $("#sMessageDialog").modal('show');
                return false;
            } else if (sEmailID != '' && !validateEmail(sEmailID)) {
                $("#sMessageDialogTitle").html('WARNING');
                $("#sMessageDialogContent").html('Please provide valid email');
                $("#sMessageDialog").modal('show');
                return false;
            } else {
                $('#sLoaderRetrive').show();
                $('#sForgotSubmit').attr('disabled', true);
                $.ajax({
                    type: "POST",
                    url: "forgot-password",
                    data: $('#sForgotForm').serialize(),
                    cache: false,
                    success: function (data) {
                        var data = jQuery.parseJSON(data);

                        $('#sLoaderRetrive').hide();
                        $('#sForgotSubmit').attr('disabled', false);

                        $("#sMessageDialogIcon").html(data.icon);
                        $("#sMessageDialogTitle").html(data.title);
                        $("#sMessageDialogContent").html(data.message);
                        $("#sMessageDialog").modal('show');
                        if (data.status == true) {
                            $('.emailclass').hide();
                            $('.otpclass').show();
                        }
                    }
                });
            }
        });

        $('#sSetPasswordSubmit').click(function () {
            var sEmailID = $('#forgot_email').val();
            if (sEmailID == '') {
                $("#sMessageDialogTitle").html('WARNING');
                $("#sMessageDialogContent").html('Please provide email');
                $("#sMessageDialog").modal('show');
                return false;
            } else if (sEmailID != '' && !validateEmail(sEmailID)) {
                $("#sMessageDialogTitle").html('WARNING');
                $("#sMessageDialogContent").html('Please provide valid email');
                $("#sMessageDialog").modal('show');
                return false;
            } else if ($('#forgot_otp').val() == '') {
                $("#sMessageDialogTitle").html('WARNING');
                $("#sMessageDialogContent").html('Please enter OTP');
                $("#sMessageDialog").modal('show');
                return false;
            } else if ($('#forgot_otp').val() != '' && $('#forgot_otp').val().length < 6) {
                $("#sMessageDialogTitle").html('WARNING');
                $("#sMessageDialogContent").html('Please enter 6 digit valid OTP');
                $("#sMessageDialog").modal('show');
                return false;
            } else if ($('#forgot_password').val() == '') {
                $("#sMessageDialogTitle").html('WARNING');
                $("#sMessageDialogContent").html('Please enter new password');
                $("#sMessageDialog").modal('show');
                return false;
            } else if ($('#forgot_password').val() != '' && $('#forgot_password').val().length < 6) {
                $("#sMessageDialogTitle").html('WARNING');
                $("#sMessageDialogContent").html('Please enter at least 6 character password');
                $("#sMessageDialog").modal('show');
                return false;
            } else {
                $('#sSetPassword').show();
                $('#sSetPasswordSubmit').attr('disabled', true);

                $.ajax({
                    type: "POST",
                    url: "forgot-password",
                    data: $('#sForgotForm').serialize(),
                    cache: false,
                    success: function (data) {
                        var data = jQuery.parseJSON(data);

                        $('#sSetPassword').hide();
                        $('#sSetPasswordSubmit').attr('disabled', false);

                        if (data.status == true) {
                            window.location.href = 'login';
//                            $('#sForgotForm')[0].reset();
//                            $('.otpclass').hide();
//                            $('.emailclass').show();
                        } else {
                            $("#sMessageDialogIcon").html(data.icon);
                            $("#sMessageDialogTitle").html(data.title);
                            $("#sMessageDialogContent").html(data.message);
                            $("#sMessageDialog").modal('show');
                        }
                    }
                });
            }
        });
    });
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
</script>
<!-- breadcrumb-area start -->

<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo URL ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li>Forgot Your Password</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->

<section id="banner">
    <div class="banner-inner img-responsive">
        <div class="container">
            <div class="row">
              <!-- <div class=" full-nerdy" ><img src="tutorexam/images/nerdy-face.png" class="login-imge-responsive" alt="img"> </div> -->
                <div class="col-md-12">
                    <?php
                    if (!empty($this->session->flashdata('item'))) {
                        $message = $this->session->flashdata('item');
                        ?>
                        <div class="<?php echo $message['class']; ?>"><?php echo $message['message']; ?></div>
                        <?php
                    }
                    ?>
                </div>
                <div class="col-md-3"></div>
                <div class="col-md-6 col-xs-12">
                    <div class="form-upper-login" style="position:relative;">
                        <div class="form-inner-login">
                            <div class="form-inner-containt">
                                <div id="forgot-error"  style="color: green;font-size: 15px; height:100px; text-align:center; margin-bottom:5px;display:none;margin-top:110px;"></div>
                                <div id="forgotform">
                                    <div class="forgot-form"> 
                                        <div class="titel-main"> 
                                            <h1>FORGOT PASSWORD</h1>
                                        </div>
                                        <p> Enter your registered email recover password</p>
                                        <div id="blk-Email" class="toHide" style="display:block">
                                            <form  id="sForgotForm" name="sForgotForm" method="post">
                                                <div class="rgstr-form">
                                                    <div class="form-group1">
                                                        <input type="text" name="forgot_email" id="forgot_email" required aria-required="true" value="">
                                                        <label for="input" class="control-label">Enter Your Email</label>
                                                        <span class="pull-right fm-chk"><i class="icon-profile-male" aria-hidden="true"></i></span> <i class="bar"></i>
                                                    </div>
                                                </div>
                                                <div class="rgstr-form otpclass" style="display:none">
                                                    <div class="form-group1">
                                                        <input type="text" name="forgot_otp" id="forgot_otp" minlength="6" maxlength="6" required aria-required="true" value="">
                                                        <label for="input" class="control-label">OTP</label>
                                                        <span class="pull-right fm-chk"><i class="icon-profile-male" aria-hidden="true"></i></span> <i class="bar"></i>
                                                    </div>
                                                </div>
                                                <div class="rgstr-form otpclass"  style="display:none">
                                                    <div class="form-group1">
                                                        <input type="text" name="forgot_password" id="forgot_password" required aria-required="true" value="">
                                                        <label for="input" class="control-label">Set Password</label>
                                                        <span class="pull-right fm-chk"><i class="icon-profile-male" aria-hidden="true"></i></span> <i class="bar"></i>
                                                    </div>
                                                </div>
                                                <div class="text-center emailclass">
                                                    <button type="button" class="btn btn-nerdy login_btn" id="sForgotSubmit" name="forget_login"> RETRIEVE PASSWORD
                                                        <i id="sLoaderRetrive" class="fa fa-refresh fa-spin" style="display:none;color: white; margin-top: 3px; margin-left: 11px;"></i>
                                                    </button>
                                                </div>
                                                <div class="text-center otpclass"  style="display:none">
                                                    <button type="button" class="btn btn-nerdy login_btn" id="sSetPasswordSubmit" name="forget_login"> SET PASSWORD <i id="loaderEmail" class="fa fa-refresh fa-spin" style="display:none;color: white; margin-top: 3px; margin-left: 11px;"></i>
                                                        <i id="sSetPassword" class="fa fa-refresh fa-spin" style="display:none;color: white; margin-top: 3px; margin-left: 11px;"></i>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <ul class="list-unstyled">
                                        <li class="fl" style="margin:0">
                                            <div class="form-group1">
                                                <div class=""> <span class="">Back to<a href="login" style="cursor: pointer; color:#d03134;"> Login</a></span> </div>
                                            </div>
                                        </li>
                                        <li class="fr form-group1" style="margin-top:10px;">New User? <a href="register" style="color:#d03134"> Register</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
    </div>
</section>