<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<script src="assets/front/js/user.js?<?php echo VERSION ?>"></script>
<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo URL ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li>Register</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->

<style>
    .error{
        font-size: 12px;
        font-weight: normal;
        color: red;
    }
    .width_700{
        width:700px !important;
    }
    .width_700 .modal-header{
        background-color: #999;
        border: solid 1px #999;
    }
    .width_700 .modal-header #sMessageDialogTitle{
        padding-left: 230px !important;
    }
    .width_700 .modal-body #sMessageDialogContent{
        text-align: justify;
        line-height: 22px;
    }
    .width_700 .black-btn{ 
        display: none
    }
    .form-control{
        font-size: 13px !important;
        font-weight: normal;
        color: #999 !important;
    }
    .control-label{
        text-align: left !important;
    }
    textarea{
        width: 100% !important
    }
</style>
<div class="container">
    <div class="row">
        <div class="form_box">
            <div class="form_inner">
                <form action="" method="post" id="sRegisterForm" name="sRegisterForm" novalidate="true">
                    <div id="sRegisterationSection">
                        <div class="section1">
                            <div class="labelclass text-center">
                                Registration Form
                            </div>
                        </div>
                        <div class="text-center error">
                            <?php
                            if ($this->session->flashdata('msg')) {
                                $aMessage = $this->session->flashdata('msg');
                                if ($aMessage['status'] == 'success') {
                                    unset($aUserData);
                                }
                                echo '<span class="' . $aMessage['status'] . '">' . $aMessage['message'] . '</span>';
                            }
                            ?>
                        </div>
                        <div class="part_a">
                            <div class="blok">
                                <ul>
                                    <li>
                                        <div class="labelclass">Full Name</div>
                                        <div class="textclass"><input type="text" name="name"  id="name" value="<?php echo (isset($aUserData['name']) && !empty($aUserData['name'])) ? $aUserData['name'] : '' ?>"/></div>
                                    </li>

                                    <li>
                                        <div class="labelclass">Email Address</div>
                                        <div class="textclass"><input type="email" name="email" id="email" value="<?php echo (isset($aUserData['email']) && !empty($aUserData['email'])) ? $aUserData['email'] : '' ?>" /></div>
                                    </li>

                                    <li>
                                        <div class="labelclass">Password</div>
                                        <div class="textclass"><input type="password" name="password" id="password"  minlength="8" maxlength="15" value="<?php echo (isset($aUserData['password']) && !empty($aUserData['password'])) ? $aUserData['password'] : '' ?>" /></div>
                                    </li>

                                </ul>
                            </div>
                            <div class="blok">
                                <ul>
                                    <li>
                                        <div class="labelclass">Country</div>
                                        <div class="textclass">
                                            <select id="country" name="country">
                                                <option value=""> -- Select --</option>
                                                <?php
                                                if (isset($aCountries) && !empty($aCountries)) {
                                                    foreach ($aCountries as $aCountry) {
                                                        $sSelected = (isset($aUserData['country']) && !empty($aUserData['country']) && $aUserData['country'] == $aCountry['id']) ? 'selected="selected"' : '';
                                                        echo ' <option  value="' . $aCountry['id'] . '_' . $aCountry['phonecode'] . '" ' . $sSelected . '>' . ucwords(strtolower($aCountry['name'])) . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="labelclass">State/Province</div>
                                        <div class="textclass">
                                            <select id="state" name="state">
                                                <option value=""> -- Select --</option>                                            
                                                <?php
                                                if (isset($aStateList) && !empty($aStateList)) {
                                                    foreach ($aStateList as $aState) {
                                                        $sSelected = (isset($aUserData['state']) && !empty($aUserData['state']) && $aUserData['state'] == $aState['id']) ? 'selected="selected"' : '';
                                                        echo ' <option value="' . $aState['id'] . '" ' . $sSelected . '>' . ucwords(strtolower($aState['name'])) . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>

                                    </li>
                                    <li>
                                        <div class="labelclass">City</div>
                                        <div class="textclass"><input type="text" name="city" id="city" value="<?php echo (isset($aUserData['city']) && !empty($aUserData['city'])) ? $aUserData['city'] : '' ?>" /></div>

                                    </li>
                                </ul>
                            </div>
                            <div class="blok">
                                <ul>
                                    <li>
                                        <div class="labelclass">Address</div>
                                        <div class="textclass"><textarea name="address" id="address"><?php echo (isset($aUserData['address']) && !empty($aUserData['address'])) ? $aUserData['address'] : '' ?></textarea></div>

                                    </li>
                                    <li>
                                        <div class="labelclass">Date of Birth</div>
                                        <div class="textclass" style="width:100%">
                                            <?php
                                            // set start and end year range
                                            $dateArray = range(1, 31);
                                            ?>
                                            <!-- displaying the dropdown list -->
                                            <select name="date"  style="width:32%;">
                                                <option value="">Select Date</option>
                                                <?php
                                                foreach ($dateArray as $date) {
                                                    // if you want to select a particular year
                                                    $selected = ($date == date('d')) ? 'selected' : '';
                                                    echo '<option ' . $selected . ' value="' . $date . '">' . $date . '</option>';
                                                }
                                                ?>
                                            </select> 
                                            <?php
                                            // set the month array
                                            $formattedMonthArray = array(
                                                "1" => "January", "2" => "February", "3" => "March", "4" => "April",
                                                "5" => "May", "6" => "June", "7" => "July", "8" => "August",
                                                "9" => "September", "10" => "October", "11" => "November", "12" => "December",
                                            );
                                            $monthArray = range(1, 12);
                                            ?>
                                            <!-- displaying the dropdown list -->
                                            <select name="month" style="width:32%;">
                                                <option value="">Select Month</option>
                                                <?php
                                                foreach ($monthArray as $month) {
                                                    // if you want to select a particular month
                                                    $selected = ($month == date('m')) ? 'selected' : '';
                                                    // if you want to add extra 0 before the month uncomment the line below
                                                    //$month = str_pad($month, 2, "0", STR_PAD_LEFT);
                                                    echo '<option ' . $selected . ' value="' . $month . '">' . $formattedMonthArray[$month] . '</option>';
                                                }
                                                ?>
                                            </select>

                                            <?php
                                            // set start and end year range
                                            $yearArray = range(date("Y", strtotime("-100 year")), date('Y'));
                                            ?>
                                            <!-- displaying the dropdown list -->
                                            <select name="year" style="width:32%;">
                                                <option value="">Select Year</option>
                                                <?php
                                                foreach ($yearArray as $year) {
                                                    // if you want to select a particular year
                                                    $selected = ($year == date('Y')) ? 'selected' : '';
                                                    echo '<option ' . $selected . ' value="' . $year . '">' . $year . '</option>';
                                                }
                                                ?>
                                            </select>

  <!--                                        <div class="calendar"><img src="theme/front/img/calendar.png" /></div>
                                          <input type="text" name="dob" id="dob" value="<?php // echo (isset($aUserData['dob']) && !empty($aUserData['dob'])) ? $aUserData['dob'] : ''                                                 ?>" />-->
                                        </div>

                                    </li>
                                    <li>
                                        <div class="labelclass">Gender</div>
                                        <div class="textclass">
                                            <select id="gender" name="gender">
                                                <option value=""> -- Select --</option>
                                                <option value="male" <?php echo (isset($aUserData['gender']) && !empty($aUserData['gender']) && $aUserData['gender'] == 'male') ? 'selected="selected"' : ''; ?>>Male</option>
                                                <option value="female" <?php echo (isset($aUserData['gender']) && !empty($aUserData['gender']) && $aUserData['gender'] == 'female') ? 'selected="selected"' : ''; ?>>Female</option>
                                                <option value="other" <?php echo (isset($aUserData['gender']) && !empty($aUserData['gender']) && $aUserData['gender'] == 'other') ? 'selected="selected"' : ''; ?>>Other</option>
                                            </select>
                                        </div>

                                    </li>
                                </ul>
                                <div class="blok">
                                    <ul>
                                        <li>
                                            <div class="labelclass">Mobile No</div>
                                            <div class="textclass" style="display:inline-block">
                                                <input type="text" readonly="true" name="country_code" minlength="2" style="width:20%" maxlength="5" id="country_code" value="<?php echo (isset($aUserData['country_code']) && !empty($aUserData['country_code'])) ? $aUserData['country_code'] : '' ?>" />
                                                <input type="text" name="contact_no" minlength="8" style="width:78%" maxlength="15" id="contact_no_1" value="<?php echo (isset($aUserData['contact_no']) && !empty($aUserData['contact_no'])) ? $aUserData['contact_no'] : '' ?>" />
                                            </div>
                                        </li>
                                        <li>
                                            <div class="labelclass">&nbsp;</div>
                                            <div class="textclass">
                                                <button type="submit" id="sRegisterButton" class="btn btn-nerdy login_btn" >SUBMIT
                                                    <i id="sLoaderRetrive" class="fa fa-refresh fa-spin" style="display:none;color: white; margin-top: 3px; margin-left: 11px;"></i>
                                                </button>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="labelclass">&nbsp;</div>
                                            <div class="textclass">
                                                <button type="reset" class="btn btn-nerdy login_btn" >RESET</button>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="blok">
                                    <div class="form-group1">
                                        <span class="">Back to<a href="login" style="cursor: pointer; color:#d03134;"> Login</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="sOtpSection" style="display: none">
                        <div class="section1">
                            <div class="labelclass text-center">
                                Verify your email
                            </div>
                        </div>
                        <div class="part_a">
                            <div class="blok">
                                <ul>
                                    <li>
                                        <div class="labelclass">Enter your OTP</div>
                                        <div class="textclass"><input type="text" name="email_otp" minlength="6" maxlength="6"  id="email_otp" value=""/></div>
                                    </li>
                                    <li>
                                        <div class="labelclass">&nbsp;</div>
                                        <div class="textclass">
                                            <button type="submit" class="btn btn-nerdy login_btn" id="sVerifyButton" >VERIFY 
                                                <i id="sLoaderRetriveVerify" class="fa fa-refresh fa-spin" style="display:none;color: white; margin-top: 3px; margin-left: 11px;"></i>
                                            </button>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<script type="text/javascript">
    $("#country").change(function () {
        var id = $('#country').val();
        var ar = id.split('_');
        if (ar[1] != '') {
            $('#country_code').val('+' + ar[1]);
        } else {
            $('#country_code').val('');
        }
        $.get("state/" + ar[0], function (data, status) {
            $("#state").html(data);
        });
    });
    function validateFileType(id) {
        var fileName = document.getElementById(id).value;
        var idxDot = fileName.split(".");
        var extFile = idxDot[idxDot.length - 1].toLowerCase();
        if (extFile == "jpg" || extFile == "jpeg") {
            return true;
        } else {
            document.getElementById(id).value = '';
            alert("Only jpg/jpeg files are allowed!");
            return false;
        }
    }
</script>