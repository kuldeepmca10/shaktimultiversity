<script>
    $(document).ready(function () {
        $('#sLoginForm').click(function () {
            var sEmailID = $('#login_email').val();
            var sPassword = $('#login_password').val();
            if (sEmailID == '') {
                $("#sMessageDialogTitle").html('WARNING');
                $("#sMessageDialogContent").html('Please provide email');
                $("#sMessageDialog").modal('show');
            } else if (sEmailID != '' && !validateEmail(sEmailID)) {
                $("#sMessageDialogTitle").html('WARNING');
                $("#sMessageDialogContent").html('Please provide valid email');
                $("#sMessageDialog").modal('show');
            } else if (sPassword == '') {
                $("#sMessageDialogTitle").html('WARNING');
                $("#sMessageDialogContent").html('Please provide password');
                $("#sMessageDialog").modal('show');
            } else {
                $.ajax({
                    type: "POST",
                    url: "login",
                    data: $('#loginFrom').serialize(),
                    cache: false,
                    success: function (data) {
                        var data = jQuery.parseJSON(data);
                        if (data.status == true) {
                            if (data.redirect != false) {
                                window.location.href = data.redirect;
                            } else {
                                window.location.reload();
                            }
                        } else {
                            $("#sMessageDialogTitle").html('ERROR');
                            $("#sMessageDialogContent").html(data.message);
                            $("#sMessageDialog").modal('show');
                        }
                    }
                });
            }
        });
    });

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
</script>
<!-- breadcrumb-area start -->

<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo URL ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li>Login</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->

<section id="banner">
    <div class="banner-inner img-responsive">
        <div class="container">
            <div class="row">
              <!-- <div class=" full-nerdy" ><img src="tutorexam/images/nerdy-face.png" class="login-imge-responsive" alt="img"> </div> -->
                <div class="col-md-12">
                    <?php
                    if (!empty($this->session->flashdata('item'))) {
                        $message = $this->session->flashdata('item');
                        ?>
                        <div class="<?php echo $message['class']; ?>"><?php echo $message['message']; ?></div>
                        <?php
                    }
                    ?>
                </div>
                <div class="col-md-3"></div>
                <div class="col-md-6 col-xs-12">
                    <div class="form-upper-login" style="position:relative;">

                        <div class="form-inner-login">
                            <div class="form-inner-containt">
                                <div id="loginform">
                                    <div class="titel-main">
                                        <h1>LOGIN HERE!</h1>
                                    </div>
                                    <form role="form" name="loginFrom" id="loginFrom" method="post" autocomplete="off" novalidate>
                                        <div id="form-error" style="position:absolute;
                                             top:-69px;
                                             z-index:9;
                                             left:0;
                                             width:100%;
                                             ">
                                        </div>
                                        <div class="tab-content">
                                            <div class="tab-pane active" role="tabpanel" id="step1">
                                                <div class="rgstr-form">
                                                    <div class="form-group1">
                                                        <div class="">
                                                            <input type="text" class="required email" name="login_email" id="login_email" required aria-required="true" value="">
                                                            <label for="input" class="control-label">Enter Your Email</label>
                                                            <span class="pull-right fm-chk"><i class="fa fa-user font22" aria-hidden="true"></i></span> <i class="bar"></i> </div>
                                                    </div>
                                                    <div class="form-group1">
                                                        <div class="">
                                                            <input type="password" class="required email" name="login_password" id="login_password" required aria-required="true" value="">
                                                            <label for="input" class="control-label">Enter Your Password</label>
                                                            <span class="pull-right fm-chk"><i class="fa fa-key font22" aria-hidden="true"></i></span> <i class="bar"></i> </div>
                                                    </div>
                                                </div>
                                                <div >
                                                    <div class="">
                                                        <input type="checkbox" name="remember" id="remember" class="css-checkbox-rem"  checked />
                                                        <label for="remember" class="css-label"> Remember Me </label>
                                                    </div>
                                                </div>
                                                <div class=" text-center " style="overflow:hidden;
                                                     ">
                                                    <button type="button" class="btn btn-nerdy login_btn" id="sLoginForm">LOGIN <i id="loader-login" class="fa fa-refresh fa-spin" style="display:none;
                                                                                                                                   color: white;
                                                                                                                                   margin-top: 3px;
                                                                                                                                   margin-left: 11px;
                                                                                                                                   "></i> </button>
                                                </div>
                                                <ul>
                                                    <li class="fl" style="margin:0">
                                                        <div class="form-group1">
                                                            <div class=""> <span class="">New User? <a href="register" style="color:#d03134"> Register</a></span> </div>
                                                        </div>
                                                    </li>
                                                    <li class = "fr form-group1" style = "margin-top:10px;"> <a href = "forgot-password" style = "cursor: pointer;" class = "turtle">Forgot Password?</a></li>
                                                </ul>

                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class = "col-md-3"></div>
            </div>
        </div>
    </div>
</section>                                 