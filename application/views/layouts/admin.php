<!DOCTYPE HTML>
<html ng-app="app" ng-controller="App">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <title ng-bind="PageTitle"><?php echo $page_title ? $page_title : SITE_NAME; ?></title>
        <?php $this->load->view("admin/elements/head"); ?>
        <!-- Preloader Css -->
        <style type="text/css">
            .pre-loader 	{position: fixed; left: 0px; top: 0px; width: 100%; height: 100%; z-index: 9999; background:#fff}
            .spinner 		{width:60px; height: 60px; margin: 250px auto; background-color: #29b6a6; border-radius: 100%; -webkit-animation: sk-scaleout 1.0s infinite ease-in-out; animation: sk-scaleout 1.0s infinite ease-in-out;}
            @-webkit-keyframes sk-scaleout 	{0% {-webkit-transform: scale(0) } 100% {-webkit-transform: scale(1.0); opacity: 0;}}
            @keyframes sk-scaleout			{0% {-webkit-transform: scale(0); transform: scale(0);} 100% { -webkit-transform: scale(1.0); transform: scale(1.0); opacity: 0;}}
        </style>
    </head>

    <body>
        <!-- Preloader -->
        <div class="pre-loader"> <div class="spinner"></div> </div>
        <!-- / -->

        <div class="wrapper">
            <?php $this->load->view("admin/elements/header"); ?>
            <div class="container box-container">
                <!-- Leftbar -->
                <div class="leftbar" ng-controller="Leftbar">
                    <?php $this->load->view("admin/elements/leftbar"); ?>
                </div>
                <!-- / -->

                <!-- Main Content View -->
                <div class="animate-view" ui-view>
                    <?php if ($page_view) {
                        $this->load->view($page_view);
                    } ?>
                </div>
                <!-- / -->
            </div>
        <?php $this->load->view("admin/elements/footer"); ?>
        </div>

<?php $this->load->view("admin/elements/foot"); ?>
    </body>
</html>