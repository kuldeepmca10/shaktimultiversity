<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title><?php echo $page_title?$page_title:SITE_NAME;?></title>
    <?php $this->load->view("admin/elements/head");?>
</head>

<body class="login" ng-app="app">
	<div class="wrapper">
    	<?php $this->load->view("admin/elements/header");?>
    	<div class="container">
            <?php $this->load->view($page_view);?>
        </div>
        <?php $this->load->view("admin/elements/footer");?>
    </div>
    
    <?php $this->load->view("admin/elements/foot");?>
</body>
</html>