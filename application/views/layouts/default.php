<!DOCTYPE HTML>
<html class="no-js" lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <title><?php echo $page_title ? $page_title : SITE_NAME; ?></title>

        <?php if (isset($seo_description)): ?>
            <meta name="description" content="<?php echo $seo_description; ?>">
        <?php endif; ?>
        <?php if (isset($seo_keywords)): ?>
            <meta name="keywords" content="<?php echo $seo_keywords; ?>">
        <?php endif; ?>

        <?php if (isset($og_title)): ?>
            <meta property="og:title" content="<?php echo $og_title; ?>"/>
        <?php endif; ?>
        <?php if (isset($og_image)): ?>
            <meta property="og:image" content="<?php echo $og_image; ?>"/>
        <?php endif; ?>
        <?php if (isset($og_description)): ?>
            <meta property="og:description" content="<?php echo $og_description; ?>"/>
        <?php endif; ?>

        <?php $this->load->view("elements/head"); ?>
    </head>

    <body>
        <!--[if lt IE 8]>
       <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
   <![endif]-->

        <?php $this->load->view("elements/header"); ?>
        <?php $this->load->view($page_view); ?>
        <?php $this->load->view("elements/footer"); ?>
        <?php $this->load->view("elements/foot"); ?>
    </body>
</html>