<style type="text/css">
.tinyimgbx				{width:106px; height:80px; float:left; margin:0 5px 5px 0; border:1px solid #ccc; cursor:pointer; position:relative}
.tinyimgbx:hover		{border:3px solid #FF8040}
.tinyimgbx:hover img	{border:none}
.tinyimgbx img			{max-width:100%; max-height:100%; left:0; right:0; top:0; bottom:0; margin:auto; position:absolute; z-index:100; border:2px solid #f9f9f9}
</style>

<div class="tinymce_images_bx">
	<div class="pdB10">
		<?php echo form_open_multipart("", array("id"=>"tinymcefileform"));?>
            <input type="file" name="file" id="tinymce_file_inpt" class="hide" accept="image/jpeg,image/png" />
        <?php echo form_close();?>
        <button class="btn btn-success btn-sm tinymce_btnup">
        	<i class="fa fa-plus"></i>&nbsp;Upload Image
        </button>
    </div>
    
	<div style="max-height:500px; overflow:auto; margin-bottom:10px">
		<?php if($result):?>
            <div>
                <?php foreach($result as $i=>$rs):?>
                    <div class="tinyimgbx">
                        <img src="<?php echo UP_URL."files/thumbs/".$rs['filename']?>" picurl="<?php echo UP_URL."files/images/".$rs['filename']?>" />
                    </div>
                <?php endforeach;?>
                <div class="clearfix"></div>
            </div>
        <?php else:?>
            <div class="not-found">No image found.</div>
        <?php endif;?>
    </div>
    
    <?php if($page['total_pages']>1):?>
    	<div class="mypaging tinymcepaging">Pages: <?php paging_links($page, URL."files/lists/", 'act');?></div>
    <?php endif;?>
</div>

<script>
$(".tinyimgbx").click(function(){
	var picurl=$(this).find('img').attr('picurl');
	var img='<img src="'+picurl+'">';
	tinymce.activeEditor.execCommand('mceInsertContent', false, img);
	
	//$(".fancybox-close").trigger('click');
	$("#tinyMceImgModal").modal('hide');
});

/** Paging **/
$(".tinymcepaging a").click(function(e){
	e.preventDefault();
	var href=$(this).attr('href');
	
	$.ajax({
		url: href,
		success: function(res){
			$(".tinymce_images_bx").parent().html(res);
		}
	});
});

/** Upload **/
$("#tinymce_file_inpt").change(function(){
	if(!check_image($(this)[0])){
		return;
	}
	
	$(".tinymce_btnup").prop('disabled', true).text('Uploading...');
	
	var formData = new FormData(document.forms['tinymcefileform']);
	ajaxFormData({
		url:'files/upload',
		data:formData,
		progress: function(per){
		},
		complete: function(res){
			$(".tinymce_images_bx").parent().html(res);
		}
	});
});

$(".tinymce_btnup").click(function(e){
	e.preventDefault();
	$("#tinymce_file_inpt").click();
});
</script>