<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<script src="assets/front/js/enroll.js?<?php echo VERSION ?>"></script>

<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo URL ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li>Online Registration</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->

<style>
    .error{
        font-size: 12px;
        font-weight: normal;
        color: red;
    }
    .form-control{
        font-size: 13px !important;
        font-weight: normal;
        color: #999 !important;
    }
    .control-label{
        text-align: left !important;
    }
</style>
<?php
$aUserData = get_session(USR_SESSION_NAME);
$sUserName = '';
$sUserEmail = '';
$sEmailFieldType = '';
if (isset($aUserData) && isset($aUserData['aUserData']['email']) && !empty($aUserData['aUserData']['name'])) {
    $sUserName = $aUserData['aUserData']['name'];
    $sUserEmail = $aUserData['aUserData']['email'];
    $aUserName = explode(' ', $sUserName);
    $sFirstName = isset($aUserName[0]) && !empty($aUserName[0]) ? $aUserName[0] : '';
    $sLastName = isset($aUserName[1]) && !empty($aUserName[1]) ? $aUserName[1] : '';
    $sEmailFieldType = ' disabled="true" readonly="readonly" ';
}
?>
<!-- breadcrumb-area end -->
<div class="about_area page">
    <h1 class="text-center font28">Online Registration For <?php echo isset($aCourseDetail['title']) && !empty($aCourseDetail['title']) ? ucwords($aCourseDetail['title']) : ''; ?></h1>
    <p class="text-center font12"><?php echo isset($aBatchDetail['title']) && !empty($aBatchDetail['title']) ? ucwords($aBatchDetail['title']) : ''; ?> (<?php echo isset($aBatchDetail['start_date']) && !empty($aBatchDetail['start_date']) ? get_date($aBatchDetail['start_date']) : ''; ?>)</p>
    <div class="container">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10 mt25" style="padding:35px 0">
                <form class="form-horizontal" enctype="multipart/form-data" action="" method="post" role="form" id="OnlineEnrollment">
                    <div class="form-group">
                        <label for="firstName" class="col-sm-3 control-label">First Name</label>
                        <div class="col-sm-6">
                            <input id="firstName" name="first_name" placeholder="First Name" value="<?php echo $sFirstName; ?>" class="form-control required" autofocus="" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="firstName" class="col-sm-3 control-label">Last Name</label>
                        <div class="col-sm-6">
                            <input id="lastName" name="last_name" placeholder="Last Name" value="<?php echo $sLastName; ?>" class="form-control required" autofocus="" type="text">
                        </div>
                    </div>
                    <!--                    <div class="form-group">
                                            <label for="email" class="col-sm-3 control-label">Age</label>
                                            <div class="col-sm-2">
                                                <input id="email" placeholder="Age" class="form-control required number" type="email">
                                            </div>
                                        </div>-->
                    <div class="form-group">
                        <label class="control-label col-sm-3">Gender</label>
                        <div class="col-sm-6">
                            <div class="row">
                                <div class="col-sm-4">
                                    <label class="radio-inline">
                                        <input id="femaleRadio" name="gender" value="Female" type="radio">Female
                                    </label>
                                </div>
                                <div class="col-sm-4">
                                    <label class="radio-inline">
                                        <input id="maleRadio"  name="gender" checked="checked" value="Male" type="radio">Male
                                    </label>
                                </div>
                                <div class="col-sm-4">
                                    <label class="radio-inline">
                                        <input id="uncknownRadio"  name="gender"  value="other" type="radio">Other
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>				
                    <div class="form-group">
                        <label for="email" class="col-sm-3 control-label">Date of Birth</label>
                        <div class="col-sm-6">
                            <input type="text" placeholder="MM/DD/YYYY" name="dob" id="datepicker" value="" class="form-control required">
                            <!--<input id="dob" name="dob"  placeholder="MM/DD/YY" class="form-control required hasCal" type="text">-->
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Photograph" class="col-sm-3 control-label">Photograph</label>
                        <div class="col-sm-6">
                            <input id="image" name="image" type="file" onchange="validateFileType(this.id)">
                            <span class="help-block">(optional in the beginning but MUST with ONLINE deeksha courses). (JPG Format Only)</span>
                        </div>
                    </div>				
                    <div class="form-group">
                        <label for="Nationality" class="col-sm-3 control-label">Nationality</label>
                        <div class="col-sm-3">
                            <select id="nationality" name="nationality" class="form-control required">
                                <option value="">--Select Nationality--</option>
                                <option value="indian">Indian</option>
                                <option value="other">Other</option>
                            </select>
                        </div></div>
                    <div class="form-group">
                        <label for="Proof of Identity/Nationality" class="col-sm-3 control-label">Proof of Identity/Nationality </label>
                        <div class="col-sm-3">
                            <input id="identity_proof" name="identity_proof" placeholder="Passport/Adhar/PAN etc" class="form-control required" type="text">
                        </div>

                        <div class="col-sm-2">
                            <input id="identity_proof_document" name="identity_proof_document" type="file" onchange="validateFileType(this.id)">
                            <span class="help-block">(JPG Format Only)</span>
                        </div>					
                    </div>

                    <div class="form-group">
                        <label for="Address" class="col-sm-3 control-label">Address</label>
                        <div class="col-sm-6">
                            <textarea id="address" name="address" placeholder="Address" class="form-control required"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="City" class="col-sm-3 control-label">City</label>
                        <div class="col-sm-3">
                            <input id="city" name="city" placeholder="City" class="form-control required" type="text">
                        </div>

                        <label for="State" class="col-sm-1 control-label">State</label>	
                        <div class="col-sm-2">
                            <input id="state" name="state" placeholder="State" class="form-control required" type="text">
                        </div>					
                    </div>
                    <div class="form-group">
                        <label for="Country" class="col-sm-3 control-label">Country</label>
                        <div class="col-sm-3">
                            <select id="country" name="country" class="form-control required">
                                <option value="">--Select Country--</option>
                                <option value="Bahamas">Bahamas</option>
                                <option value="Cambodia">Cambodia</option>
                                <option value="Denmark">Denmark</option>
                                <option value="Ecuador">Ecuador</option>
                                <option value="Fiji">Fiji</option>
                                <option value="Gabon">Gabon</option>
                                <option value="Haiti">Haiti</option>
                            </select>
                        </div>
                        <label for="Pin" class="col-sm-1 control-label">Pin</label>	
                        <div class="col-sm-2">
                            <input id="pincode" name="pincode" placeholder="Pin" maxlength="6" class="form-control required" type="text">
                        </div>					
                    </div>
                    <div class="form-group"></div>

                    <div class="form-group">
                        <label for="Email" class="col-sm-3 control-label">Email</label>
                        <div class="col-sm-6">
                            <input id="email" name="email" placeholder="Email" value="<?php echo $sUserEmail; ?>" <?php echo $sEmailFieldType; ?> class="form-control required" type="email">
                            <span class="help-block">This email can NOT be changed throughout your membership. Choose carefully.</span>
                        </div>
                    </div>					
                    <div class="form-group">
                        <label for="Contact Number" class="col-sm-3 control-label">Contact Number</label>
                        <div class="col-sm-6">
                            <input id="contact_no" name="contact_no" placeholder="Country Code + Number" class="form-control required number" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Skype ID" class="col-sm-3 control-label">Skype ID</label>
                        <div class="col-sm-6">
                            <input id="skype_id" name="skype_id" placeholder="Skype ID" class="form-control required" type="text">
                            <span class="help-block">(to join the LIVE course &amp; FOLLOW UP sessions)</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Google Hangout ID" class="col-sm-3 control-label">Google Hangout ID</label>
                        <div class="col-sm-6">
                            <input id="hangout_id" name="hangout_id" placeholder="Hangout ID" class="form-control required" type="text">
                            <span class="help-block">(to join the LIVE course sessions)</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Contact" class="col-sm-3 control-label">Personal Messenger ID</label>
                        <div class="col-sm-6">
                            <input id="messenger_id" name="messenger_id" placeholder="Messenger ID" class="form-control required" type="text">
                            <span class="help-block">WhatsApp No / Twitter Handle (To receive instant messages/ urgent notifications during/before programme sessions)</span>
                        </div>
                    </div>				
                    <div class="form-group">
                        <div class="col-sm-6 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary btn-block" style="background:#d03134;border:1px solid #d03134">Register</button>
                        </div>
                    </div>
                </form> <!-- /form -->
            </div>
            <div class="col-md-1"></div>
        </div> <!-- ./container -->
    </div>
</div>

<script type="text/javascript">
    function validateFileType(id) {
        var fileName = document.getElementById(id).value;
        var idxDot = fileName.split(".");
        var extFile = idxDot[idxDot.length - 1].toLowerCase();
        if (extFile == "jpg" || extFile == "jpeg") {
            return true;
        } else {
            document.getElementById(id).value = '';
            alert("Only jpg/jpeg files are allowed!");
            return false;
        }
    }
    $(document).ready(function () {
        alert('lplp');
        var date_input = $('input[name="dob"]'); //our date input has the name "date"
        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
            format: 'mm/dd/yyyy',
            container: container,
            todayHighlight: true,
            endDate: 'today',
            autoclose: true,
        })
    })
</script>