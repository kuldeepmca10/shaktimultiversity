<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo URL ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li>Free Courses</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->

<div class="single_courcse">
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-sm-9">
                <div class="single_cos_item">
                    <div class="courses_area" style="background-image:none">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="title">
                                    <h3 class="module-title">
                                        Explore all Our <span>Free Courses</span>
                                    </h3>
                                    <p class="lead">
                                        We believe Knowledge is your right!! 
                                    </p>						
                                </div>
                            </div>
                        </div>
                        <?php if ($result): ?>
                            <?php foreach ($result as $r):
                                ?>
                                <!--start course single  item -->
                                <div class="col-xs-12 col-md-4 col-sm-6 col-lg-4">
                                    <div class="course_item">
                                        <div class="courses_thumb">
                                            <a href="<?php echo URL . 'course/' . $r['slug'] ?>"><img src="<?php echo UP_URL . 'course-sm/' . $r['image'] ?>" alt="" /></a>
                                            <div class="courses_thumb_text">
                                                <a href="#">
                                                    <?php if (isset($r['price_type']) && !empty($r['price_type']) && $r['price_type'] == 'free'): ?>
                                                        Free    
                                                    <?php else: ?>
                                                        <?php if ($r['mrp'] > 0): ?>
                                                            <i class="fa fa-inr" aria-hidden="true"></i><span class="course_price"> <?php echo $r['mrp'] ?></span> |
                                                        <?php endif; ?>
                                                        <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $r['price'] ?>
                                                    <?php endif; ?>
                                                </a>							
                                            </div>							
                                        </div>
                                        <div class="courses_content">
                                            <h2><?php echo $r['title'] ?></h2>
                                            <h5>Teacher: <strong><a href="<?php echo URL . 'master-detail/' . $r['teacher_slug'] ?>"><?php echo $r['teacher'] ?></a></strong></h5>
                                            <h5>Duration: <strong><a href="#"><?php echo $r['duration_hr'] ? $r['duration_hr'] . ' Hours' : '' ?> <?php echo $r['duration_mn'] ? $r['duration_mn'] . ' Minutes' : '' ?></a></strong></h5>
                                            <h5>Batch Start Date: <strong><a href="#"><?php echo get_date($r['batch_start_date']) ?></a></strong></h5>
                                            <p><?php echo str_short($r['short_description'], 50) ?></p>
                                            <p><a href="<?php echo URL . 'course/' . $r['slug'] ?>" class="text_uppercase more_read">Read More ...</a></p>
                    <!--                                <a href="#" class="text_uppercase"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Add to Cart</a> | 
                                            <a href="#" class="text_uppercase"><i class="fa fa-heart" aria-hidden="true"></i> Wish List</a>-->
                                        </div>
                                    </div>
                                </div>
                                <!--End course single  item -->
                            <?php endforeach; ?>
                            <div class="clearfix"></div>
                            <div class="row" style="padding-bottom:10px;"> 
                                <div class="col-md-12"> 
                                    <div class="text-center">
                                        <a href="courses" class="bmore">EXPLORE ALL COURSES</a>
                                    </div> 
                                </div> 
                            </div>
                            <div class="clearfix"></div>

                            <!-- paginitaion-->
                            <?php
//                $category = isset($category) && !empty($category) ? $category : 'all-course';
                            $sPaginateURL = URL . 'free-courses/';
                            if (isset($page['total_pages']) && $page['total_pages'] > 1) {
                                $sPagoinationHtml = '<div class="col-md-12">'
                                        . '<div class="pagenition_bar">'
                                        . '<nav>'
                                        . '<ul class="pagination paginition_text">';
                                if ($page['cur_page'] > 1) {
                                    $sPagoinationHtml .= ' <li><a href="' . $sPaginateURL . ($page['cur_page'] - 1) . '">Previous</a></li>';
                                }
                                for ($i = 1; $i <= $page['total_pages']; $i++) {
                                    $sDisabled = (isset($page['cur_page']) && !empty($page['cur_page']) && $page['cur_page'] == $i) ? 'disabled' : '';
                                    $sPagoinationHtml .= ' <li class="' . $sDisabled . '"><a href="' . $sPaginateURL . $i . '">' . $i . '</a></li>';
                                }
                                if ($page['cur_page'] != $page['total_pages']) {
                                    $sPagoinationHtml .= ' <li><a href="' . $sPaginateURL . ($page['cur_page'] - 1) . '">Next</a></li>';
                                }
                                $sPagoinationHtml .= '</ul>'
                                        . '</nav>'
                                        . '</div>'
                                        . '</div>';
                                echo $sPagoinationHtml;
                            }
                            ?>
                            <!--end  paginitaion-->	
                            <?php
                        else:
                            echo 'No Free Course Declared Yet';
                        endif;
                        ?>

                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-3">
                <?php $this->load->view("elements/rightbar", array('pg' => 'course_list')); ?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<style>
    .courses_area.courses_padding{
        padding-top: 0px !important;
    }
</style>