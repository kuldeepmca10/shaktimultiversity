<!--Start about title  area --> 
<div class="about_area_s">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading_about">
                    <h1 class="page-title">
                        <?php echo $dtl['heading'] ?>
                    </h1>					
                </div>
            </div>
        </div>
    </div>
</div>
<!--Start about  area --> 

<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo URL ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li><?php echo $dtl['title'] ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->

<div class="about_area page">
    <div class="container">
        <div class="col-md-8 cms-content">
            <?php echo $dtl['description'] ?>
        </div>

        <div class="col-md-4 col-sm-6">
            <?php $this->load->view("elements/rightbar", array('pg' => 'cms')); ?>
        </div>
    </div>		
</div>