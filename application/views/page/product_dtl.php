<?php
//pr($dtl);
?>
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <form name="sCurrencyForm" id="sCurrencyForm" action="" method="post">
                        <ul>
                            <li><a href="<?php echo URL; ?>">Home</a><i class="fa fa-angle-right"></i></li>
                            <li><a href="<?php echo URL . 'shop' ?>">Books & Products </a> <i class="fa fa-angle-right"></i></li>							
                            <li><?php echo ucwords($dtl['title']); ?></li>
                            <?php if (isset($dtl['price_type']) && !empty($dtl['price_type']) && $dtl['price_type'] == 'paid') { ?>
                                <li style="float:right">
                                    <label>Currency</label>&nbsp;
                                    <select name="currencyType" id="currencyType" onchange="this.form.submit()">
                                        <option value="">Select</option>
                                        <option value="INR" <?php echo (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'INR') ? 'selected = "selected"' : ''; ?>>INR</option>
                                        <option value="USD" <?php echo (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'USD') ? 'selected = "selected"' : ''; ?>>USD</option>
                                    </select>
                                </li>
                            <?php } ?>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="single_courcse mt25">
    <div class="container">
        <div class="row">

            <div class="content-wrapper">	
                <div class="item-container">	
                    <div class="container">
                        <div class="col-md-5">
                            <?php
                            if (isset($dtl['images']) && !empty($dtl['images']) && count($dtl['images']) > 0) {
                                $sClass = count($dtl['images']) > 1 ? ' col-md-8 ' : ' col-md-12 ';
                                ?>
                                <div class="row">
                                    <div class="product col-xs-12 <?php echo $sClass; ?> service-image-left">
                                        <center>
                                            <?php if (file_exists(UP_PATH . 'pro-lg/' . $dtl['images'][0]['image'])) { ?>
                                                <img id="item-display" src="<?php echo UP_URL . 'pro-lg/' . $dtl['images'][0]['image']; ?>">
                                            <?php } ?>
                                        </center>
                                    </div>
                                    <?php
                                    if (count($dtl['images']) > 1) {
                                        echo '<div class="container service1-items col-sm-4 col-md-4 pull-left">
                                                <center>';
                                        for ($i = 1; $i < count($dtl['images']); $i++) {
                                            if (file_exists(UP_PATH . 'pro-lg/' . $dtl['images'][$i]['image'])) {
                                                echo '<a id="item-' . $i . '" class="service1-item" style="margin:0px 0px 3px 0px;">
                                                <img src="' . UP_URL . 'pro-lg/' . $dtl['images'][$i]['image'] . '" alt="">
                                            </a>';
                                            }
                                        }
                                        echo '</center>
                                        </div>';
                                    }
                                    ?>

                                </div>
                            <?php } ?>
                        </div>
                        <div class="col-md-7">
                            <div class="product-title"><?php echo ucwords($dtl['title']); ?></div>
                            <div class="product-desc course_details">
                                <?php echo $dtl['short_description']; ?>
                            </div>
                            <div class="product-rating">
                                <ul class="course_heading_ul">
                                    <li class="blank-stars"></li>
                                    <li><span>7854 Learners</span></li>
                                </ul>
                            </div>
                            <hr>
                            <div class="product-price">
                                <?php if (isset($dtl['price_type']) && !empty($dtl['price_type']) && $dtl['price_type'] == 'free'): ?>
                                    Free    
                                <?php else: ?>
                                    <?php if (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'INR') { ?>
                                        <?php if (isset($aUserData) && !empty(isset($aUserData)) && isset($aUserData['isLifetimeMember']) && $aUserData['isLifetimeMember'] == true && $dtl['price'] > 0): ?>
                                            <i class="fa fa-inr" aria-hidden="true"></i><span class="course_price"> <?php echo $dtl['mrp'] ?></span> |
                                            <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $dtl['price'] ?>
                                        <?php else: ?>
                                            <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $dtl['mrp'] ?>
                                        <?php endif; ?>
                                    <?php }else if (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'USD') { ?>
                                        <?php if (isset($aUserData) && isset($aUserData['isLifetimeMember']) && $aUserData['isLifetimeMember'] == true && $dtl['price_usd'] > 0): ?>
                                            <i class="fa fa-usd" aria-hidden="true"></i><span class="course_price"> <?php echo $dtl['mrp_usd'] ?></span> |
                                            <i class="fa fa-usd" aria-hidden="true"></i> <?php echo $dtl['price_usd'] ?>
                                        <?php else: ?>
                                            <i class="fa fa-usd" aria-hidden="true"></i> <?php echo $dtl['mrp_usd'] ?>
                                        <?php endif; ?>                                             
                                    <?php } ?>
                                <?php endif; ?>	
                            </div>
                            <div class="product-stock">Available</div>
                            <hr>
                            <div class="btn-group cart">
                                <!--<a href="#" class="text_uppercase more_read border_none text-right">Order Now</a>-->

                                <button type="button" class="btn btn-danger"  onclick="fnBookEnquery('<?php echo $dtl['id']; ?>', '<?php echo $dtl['title']; ?>')">
                                    Order Now
                                </button>
                            </div>
                            <!--                            <div class="btn-group wishlist">
                                                            <button type="button" class="btn btn-danger">
                                                                Add to wishlist 
                                                            </button>
                                                        </div>-->
                        </div>
                    </div> 
                </div>
                <div class="mt25">		
                    <div class="col-md-12 product-info">
                        <ul id="myTab" class="nav nav-tabs nav_tabs">
                            <li class="active"><a href="#service-one" data-toggle="tab">DESCRIPTION</a></li>
                            <!--<li><a href="#service-three" data-toggle="tab">REVIEWS</a></li>-->
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div class="tab-pane fade in active" id="service-one">
                                <section class="container product-info course_details">
                                    <p><?php echo $dtl['description']; ?></p>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </div>







        </div>

    </div>	
</div>