<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<script src="assets/front/js/enroll.js?<?php echo VERSION ?>"></script>
<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo URL ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li><a href="<?php echo URL ?>course/<?php echo isset($aCourseDetail['slug']) && !empty($aCourseDetail['slug']) ? $aCourseDetail['slug'] : ''; ?>"><?php echo isset($aCourseDetail['title']) && !empty($aCourseDetail['title']) ? ucwords($aCourseDetail['title']) : ''; ?> </a> <i class="fa fa-angle-right"></i></li>							
                        <li>Enroll Now</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->
<?php
$aUserData = get_session(USR_SESSION_NAME);
$sLoginUser = false;

if (isset($aUserData) && !empty($aUserData)) {
    $sAdminLogin = isset($aUserData['sAdminLogin']) && !empty($aUserData['sAdminLogin']) ? $aUserData['sAdminLogin'] : false;
    $sLoginUser = true;
}
?>
<div class="container">
    <div class="row">
        <div class="form_box">
            <div class="card enrollment-registeration">
                <form action="" method="post" id="sEnrollmentConfirmation" name="sEnrollmentConfirmation">
                    <div class="part_a part_b">
                        <h3>Course Details<span class="reg-intro">(As per your selection/interest)</span> </h3>
                        <div class="blok">
                            <div class="col-sm-3 col-xs-12"><label class="labelclass">Course Type</label></div>
                            <div class="col-sm-3 col-xs-12"><label class="textclass"><?php echo isset($aCourseDetail['type']) && !empty($aCourseDetail['type']) ? ucwords($aCourseDetail['type']) : ''; ?></label></div>
                            <div class="col-sm-3 col-xs-12"><label class="labelclass">Course Category</label></div>
                            <div class="col-sm-3 col-xs-12"><label class="textclass"><?php echo isset($aCategoryDetail['title']) && !empty($aCategoryDetail['title']) ? ucwords($aCategoryDetail['title']) : ''; ?></label></div>
                        </div>

                        <div class="blok">
                            <div class="col-sm-3 col-xs-12"><label class="labelclass">Course Name</label></div>
                            <div class="col-sm-3 col-xs-12"><label class="textclass"><?php echo isset($aCourseDetail['title']) && !empty($aCourseDetail['title']) ? ucwords($aCourseDetail['title']) : ''; ?></label></div>
                            <div class="col-sm-3 col-xs-12"><label class="labelclass">Your Batch</label></div>
                            <div class="col-sm-3 col-xs-12"><label class="textclass"><?php echo isset($aBatchDetail['start_date']) && !empty($aBatchDetail['start_date']) ? get_date($aBatchDetail['start_date']) : ''; ?> - <?php echo isset($aBatchDetail['batch_time']) && !empty($aBatchDetail['batch_time']) ? date('h:i A', strtotime($aBatchDetail['batch_time'])) : ''; ?></label></div>
                        </div>

                        <div class="blok">
                            <div class="col-sm-3 col-xs-12"><label class="labelclass">Course Fee</label></div>
                            <div class="col-sm-3 col-xs-12"><label class="textclass">
                                    <?php
                                    if (isset($aCourseDetail['price_type']) && !empty($aCourseDetail['price_type']) && $aCourseDetail['price_type'] == 'free'):
                                        ?>
                                        Free    
                                        <?php
                                    else:
                                        if (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'INR') {
                                            if (isset($aUserData) && !empty(isset($aLoginUserData)) && isset($aLoginUserData['isLifetimeMember']) && $aLoginUserData['isLifetimeMember'] == true && $aCourseDetail['price'] > 0):
                                                ?>
                                                <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $aCourseDetail['price'] ?>
                                            <?php else: ?>
                                                <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $aCourseDetail['mrp'] ?>
                                            <?php
                                            endif;
                                        }else if (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'USD') {
                                            ?>
                                            <?php if (isset($aLoginUserData) && isset($aLoginUserData['isLifetimeMember']) && $aLoginUserData['isLifetimeMember'] == true && $aCourseDetail['price_usd'] > 0): ?>
                                                <i class="fa fa-usd" aria-hidden="true"></i> <?php echo $aCourseDetail['price_usd'] ?>
                                            <?php else: ?>
                                                <i class="fa fa-usd" aria-hidden="true"></i> <?php echo $aCourseDetail['mrp_usd'] ?>
                                            <?php
                                            endif;
                                        }
                                    endif;
                                    ?></label>
                            </div>

                            <?php if ($sAdminLogin == true) { ?>
                                <div class="col-sm-3 col-xs-12"><label class="labelclass">Payable Amount</label></div>
                                <div class="col-sm-3 col-xs-12"><label class="textclass">
                                        <?php
                                        if (isset($aCourseDetail['price_type']) && !empty($aCourseDetail['price_type']) && $aCourseDetail['price_type'] == 'free'):
                                            ?>
                                            Free    
                                            <?php
                                        else:
                                            if (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'INR') {
                                                ?>
                                                <i class="fa fa-inr" aria-hidden="true"></i> 0
                                                <?php
                                            } else if (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'USD') {
                                                ?>
                                                <i class="fa fa-usd" aria-hidden="true"></i> 0
                                                <?php
                                            }
                                        endif;
                                        ?></label>
                                </div>
                            <?php } ?>
                        </div>
                        <?php if ($sAdminLogin == true) { ?>
                            <div class="blok">
                                <div class="col-sm-3 col-xs-12"><label class="labelclass">Note</label></div>
                                <div class="col-sm-9 col-xs-12">
                                    <input type="text" name="note" rows="3" id="note" class="form-control" placeholder="Please add note here" />
                                </div>
                            </div>
                        <?php } ?>
                        <div class="checkbox">
                            <input name="agree" id="agree" type="checkbox" value="" />I agree to the 
                            <a class="various1 terms-policy" href="<?php echo URL; ?>terms-and-conditions.html" target="_blank">Terms & Conditions</a> and 
                            <a class="various1 terms-policy" href="<?php echo URL; ?>privacy-policy.html" target="_blank">Privacy Policy</a> of Shakti Multiversity
                        </div>
                        <div class="checkbox">
                            <input name="agree_refund_policy" id="agree_refund_policy" type="checkbox" value="" />I agree to the 
                            <a class="various1 terms-policy" href="<?php echo URL; ?>refund-policy.html" target="_blank">Refund Policy</a> of Shakti Multiversity
                        </div>
                        <div class="submitclass">
                            <?php if (isset($aCourseDetail['price_type']) && !empty($aCourseDetail['price_type']) && $aCourseDetail['price_type'] == 'free') { ?>
                                <input type="submit" id="sConfirmationSubmit" value="Claim Your Free Course Here" class="course_details_enroll"/> 
                                <?php
                            } else {
                                if ($sAdminLogin == false) {
                                    ?>
                                    <input type="submit" id="sConfirmationSubmit" value="Proceed To Payment" class="course_details_enroll" />
                                <?php } else { ?>
                                    <input type="submit" id="sCourseAddSubmit" value="Add Course" class="course_details_enroll" />
                                    <?php
                                }
                            }
                            ?>
                            <div class="text-btm">If you want to make changes in your selection, <a href="<?php echo URL ?>course/<?php echo isset($aCourseDetail['slug']) && !empty($aCourseDetail['slug']) ? $aCourseDetail['slug'] : ''; ?>">Click Here</a></div>
                        </div>
                    </div>

            </div>
            </form>
        </div>
    </div>
</div>
</div>
<!-- Modal -->
<script type="text/javascript">
    $("#country").change(function () {
        var id = $('#country').val();
        $.get("state/" + id, function (data, status) {
            $("#state").html(data);
        });
    });
    function validateFileType(id) {
        var fileName = document.getElementById(id).value;
        var idxDot = fileName.split(".");
        var extFile = idxDot[idxDot.length - 1].toLowerCase();
        if (extFile == "jpg" || extFile == "jpeg") {
            return true;
        } else {
            document.getElementById(id).value = '';
            alert("Only jpg/jpeg files are allowed!");
            return false;
        }
    }
</script>