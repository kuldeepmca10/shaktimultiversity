
<!--Start slider  area --> 
<div class="slider_area">
    <div class="container-fluid">
        <div class="row">
            <!-- Tab panes -->
            <div class="tab-content" style="position:relative">
                <?php
                $iCount = 0;
                foreach ($course['result'] as $r) {
                    $sActiveClass = '';
                    if ($iCount == 0) {
                        $sActiveClass = 'active';
                    }
                    $iCount++;
                    ?>
                    <!--<div style="position:absolute;top:200px;left:10px;z-index:99999;font-size:30px;"><a href="#<?php echo $r['id']; ?>" data-toggle="tab" style="color:#fff"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i></a></div>	
                    <div style="position:absolute;top:200px;right:10px;z-index:99999;font-size:30px;"><a href="#<?php echo $r['id']; ?>" data-toggle="tab" style="color:#fff"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></div>	-->

                    <div class="tab-pane <?php echo $sActiveClass; ?>" id="<?php echo $r['id']; ?>">
                        <div class="col-md-7 col-sm-7 col-xs-12 pd0">
                            <div class="slide_thumb">
                                <img src="<?php echo UP_URL . 'course-sm/' . $r['image'] ?>" alt="" />
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12 pd0">
                            <div class="slide_text">
                                <h2 class="slide_title"><?php echo $r['title'] ?></h2>
                                <p class="slide_ptext"><?php echo str_short($r['short_description'], 50) ?></p>
                                <a href="<?php echo URL . 'course/' . $r['slug'] ?>" class="slide_readmore">Explore Course</a>
                            </div>
                        </div>
                    </div>	
                <?php } ?>

            </div>

        </div>
    </div>
</div>
<!--end slider  area -->
<!-- tab nav slide item-->

<!-- tab nav slide item-->

<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo URL; ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li>Master</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->	

<!--start courses  area -->
<div class="single_courcse">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8">
                <div class="single_cos_item">
                    <div class="courses_area" style="background-image:none">
                        <div class="row">
                            <div class="col-md-12 col-xs-12 text-center">
                            	<img src="<?php echo UP_URL . 'teachers/' . $aMasterDetail['image'] ?>" class="course_details_img course_details_img2">
                            </div>
                            <div class="col-md-12 col-xs-12 text-center">
                                <h1 class="mt25"><?php echo $aMasterDetail['title'] ?></h1>
                                <p class="gray"><?php echo $aMasterDetail['short_description'] ?></p>
                                <ul class="course_heading_ul">
                                    <?php if (isset($aMasterDetail['facebook_url']) && !empty($aMasterDetail['facebook_url'])) { ?>
                                        <li><a href="<?php echo $aMasterDetail['facebook_url']; ?>" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <?php } ?>
                                    <?php if (isset($aMasterDetail['twitter_url']) && !empty($aMasterDetail['twitter_url'])) { ?>
                                        <li><a href="<?php echo $aMasterDetail['twitter_url']; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <?php } ?>
                                    <?php if (isset($aMasterDetail['linkedin_url']) && !empty($aMasterDetail['linkedin_url'])) { ?>
                                        <li><a href="<?php echo $aMasterDetail['linkedin_url']; ?>"  target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                                    <?php } ?>
                                    <?php if (isset($aMasterDetail['google_plus_url']) && !empty($aMasterDetail['google_plus_url'])) { ?>
                                        <li><a href="<?php echo $aMasterDetail['google_plus_url']; ?>" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                    <?php } ?>
<!--<li><a href="#"><i class="fa fa-envelope" aria-hidden="true"></i> shivashakti@shaktimultiversity.com</a></li>-->
                                </ul>
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <div class="course_details_point">
                                            <div class="col-md-7">
                                                <ul class="border-right">
                                                    <?php if (isset($aMasterDetail['workshop_seminar']) && !empty($aMasterDetail['workshop_seminar'])) { ?>
                                                        <li><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $aMasterDetail['workshop_seminar'] ?></li>
                                                    <?php } ?>
                                                    <?php if (isset($aMasterDetail['sessions']) && !empty($aMasterDetail['sessions'])) { ?>
                                                        <li><i class="fa fa-desktop" aria-hidden="true"></i> <?php echo $aMasterDetail['sessions'] ?></li>
                                                    <?php } ?>
                                                </ul>									
                                            </div>
                                            <div class="col-md-5">
                                                <ul>
                                                    <?php if (isset($aMasterDetail['experience']) && !empty($aMasterDetail['experience'])) { ?>
                                                        <li><i class="fa fa-file-o" aria-hidden="true"></i> <?php echo $aMasterDetail['experience'] ?></li>
                                                    <?php } ?>
                                                    <?php if (isset($aMasterDetail['avaliablity']) && !empty($aMasterDetail['avaliablity'])) { ?>
                                                        <li><i class="fa fa-unlock-alt" aria-hidden="true"></i> <?php echo $aMasterDetail['avaliablity'] ?></li>
                                                    <?php } ?>
                                                </ul>									
                                            </div>		
                                            <div class="clear"></div>									
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <!-- Modal -->



                        <div class="row">
                            <!--start course single  item -->
                            <div class="col-md-12 course_details mt25 b_title_mdetails">
                                <!-- Latest compiled and minified Bootstrap CSS -->

                                <?php if (isset($aMasterDetail['about_lineage']) && !empty($aMasterDetail['about_lineage'])) { ?>
                                    <div class="b_title"><h3>About</h3></div>
                                    <?php echo $aMasterDetail['about_lineage'] ?>
                                <?php } ?>

                                <?php if (isset($aMasterDetail['contribution']) && !empty($aMasterDetail['contribution'])) { ?>
                                    <div class="b_title"><h3>Masters’ Contribution</h3></div>
                                    <?php echo $aMasterDetail['contribution'] ?>
                                <?php } ?>

                                <?php if (isset($aMasterDetail['social_responsibility']) && !empty($aMasterDetail['social_responsibility'])) { ?>
                                    <div class="b_title"><h3>Social Responsibility</h3></div>
                                    <?php echo $aMasterDetail['social_responsibility'] ?>
                                <?php } ?>

                                <?php if (isset($aMasterDetail['in_the_media']) && !empty($aMasterDetail['in_the_media'])) { ?>
                                    <div class="b_title"><h3>In The Media</h3></div>
                                    <?php echo $aMasterDetail['in_the_media'] ?>
                                <?php } ?>
                                <!-- end container -->

                            </div>			

                        </div>

                    </div>						

                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <?php $this->load->view("elements/rightbar", array('pg' => 'master_dtl')); ?>
            </div>
        </div>

    </div>	
</div>
<!--end courses  area -->