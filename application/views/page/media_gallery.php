
<!--Start about title  area --> 
<div class="about_area_s gallery_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading_about">
                    <h1 class="page-title">
                        Media Gallery
                    </h1>					
                </div>
            </div>
        </div>
    </div>
</div>
<!--Start about  area --> 


<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo URL ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li>Media Gallery</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->		


<!--Start about  area --> 
<div class="about_area">


    <div class="container">	
        <div class="row">
            <div class="col-md-12 col-xs-12  photo_gallery"><h1>Media Gallery</h1></div>
        </div>	
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <div class="container">
                    <!--image gellary  item --> 
                    <div class="col-md-6 col-sm-12">
                        <div class="single_gellary_item">			
                            <div class="gellary_thumb">
                                <p class="img-height">
                                    <a href="<?php echo URL . 'image-gallery'; ?>">      
                                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJuMVCMv4ejZQMzbIancX46uLtKwjy5C8plWCJOMI8oK1a9coC" alt="" class="width100" />
                                    </a>
                                </p>	
                                <div class="gallery_text">
                                    <h2>Image Gallery</h2>	
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--video gellary  item --> 
                    <div class="col-md-6 col-sm-12">
                        <div class="single_gellary_item">			
                            <div class="gellary_thumb">
                                <p class="img-height"><a href="<?php echo URL . 'video-gallery' ?>">
                                        <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJuMVCMv4ejZQMzbIancX46uLtKwjy5C8plWCJOMI8oK1a9coC" alt="" class="width100" />
                                    </a></p>	
                                <div class="gallery_text">
                                    <h2> Video Gallery                                     
                                    </h2>							
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- sidebar space -->
            <div class="col-md-4 col-sm-6">

            </div>
        </div>
    </div>
</div>
<!--end about  area -->	
<script>
    function loadGallery(val) {
        window.location.href = "<?php echo URL ?>image-gallery/" + val;
    }
</script>