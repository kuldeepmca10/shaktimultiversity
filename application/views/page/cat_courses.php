<!-- breadcrumb-area start -->
<?php
$sCategoryName = isset($category_name) ? ucwords(strtolower($category_name)) : 'All Courses';
if (isset($sCategoryName) && $sCategoryName == 'All Courses') {
    $sMainClass = 'col-md-12 col-sm-12';
    $sItemClass = 'col-md-3 col-sm-6 col-lg-3';
} else {
    $sMainClass = 'col-md-9 col-sm-6';
    $sItemClass = 'col-md-4 col-sm-6 col-lg-4';
}
$sMainClass = 'col-md-9 col-sm-6';
?>
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <form name="sCurrencyForm" id="sCurrencyForm" action="" method="post">
                        <ul>
                            <li><a href="<?php echo URL ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                            <li><?php echo $sCategoryName = isset($category_name) ? ucwords(strtolower($category_name)) : 'All Courses'; ?></li>
                            <li style="float:right">
                                <label>Currency</label>&nbsp;
                                <select name="currencyType" id="currencyType" onchange="this.form.submit()">
                                    <option value="">Select</option>
                                    <option value="INR" <?php echo (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'INR') ? 'selected = "selected"' : ''; ?>>INR</option>
                                    <option value="USD" <?php echo (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'USD') ? 'selected = "selected"' : ''; ?>>USD</option>
                                </select>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->

<div class="single_courcse">
    <div class="container">
        <div class="row">
            <div class="<?php echo $sMainClass; ?>">
                <div class="single_cos_item">
                    <div class="courses_area" style="background-image:none">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="title">
                                    <h3 class="module-title">Explore all the <span>Courses</span></h3>
                                    <!--<h3 class="module-title"><span><?php echo isset($category) ? ucwords(strtolower($category)) : 'N/A'; ?></span></h3>-->
                                </div>
                            </div>
                            <div class="col-md-6 photo_gallery">
                                <label>Select a Category</label>
                                <?php
                                if (isset($cats) && !empty($cats)) {
                                    $sSelect = '<select name="category_name" id="category_name" class="selectpicker"  style="width:55%;">';
                                    $sSelect .= ' <option value="">All Categories</option>';
                                    foreach ($cats as $iKey => $sValue) {
                                        $sSelected = '';
                                        if (isset($aPostedData['category_name']) && !empty($aPostedData['category_name']) && $aPostedData['category_name'] == $iKey) {
                                            $sSelected = 'Selected="selected"';
                                        }
                                        $sSelect .= ' <option value="' . $iKey . '" ' . $sSelected . '>' . $sValue . '</option>';
                                    }
                                    $sSelected = '';
                                    if (isset($aPostedData['category_name']) && !empty($aPostedData['category_name']) && $aPostedData['category_name'] == 'free-course') {
                                        $sSelected = 'Selected="selected"';
                                    }
                                    $sSelect .= ' <option value="free-course" ' . $sSelected . '>Free Course</option>';

                                    $sSelect .= '</select>';
                                }
                                echo $sSelect;
                                ?>
                            </div>
                        </div>
                        <?php if ($result): ?>
                            <div class="row">
                                <?php foreach ($result as $r): ?>
                                    <!--start course single  item -->
                                    <div class="<?php echo $sItemClass; ?>">
                                        <div class="course_item">
                                            <div class="courses_thumb">
                                                <a href="<?php echo URL . 'course/' . $r['slug'] ?>"><img src="<?php echo UP_URL . 'course-sm/' . $r['image'] ?>" alt="" /></a>
                                                <div class="courses_thumb_text">
                                                    <?php if (isset($r['price_type']) && !empty($r['price_type']) && $r['price_type'] == 'free'): ?>
                                                        Free    
                                                    <?php else: ?>
                                                        <?php if (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'INR') { ?>
                                                            <?php if ($r['price'] > 0): ?>
                                                                <?php if ($r['mrp'] > 0): ?>
                                                                    <i class="fa fa-inr" aria-hidden="true"></i><span class="course_price"> <?php echo $r['mrp'] ?></span> |
                                                                <?php endif; ?>
                                                                <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $r['price'] ?>
                                                            <?php endif; ?>
                                                        <?php }else if (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'USD') { ?>
                                                            <?php if ($r['price_usd'] > 0): ?>
                                                                <?php if ($r['mrp_usd'] > 0): ?>
                                                                    <i class="fa fa-usd" aria-hidden="true"></i><span class="course_price"> <?php echo $r['mrp_usd'] ?></span> |
                                                                <?php endif; ?>
                                                                <i class="fa fa-usd" aria-hidden="true"></i> <?php echo $r['price_usd'] ?>
                                                            <?php endif; ?>                                                       
                                                        <?php } ?>
                                                    <?php endif; ?>						
                                                </div>							
                                            </div>
                                            <div class="courses_content">
                                                <h2><?php echo $r['title'] ?></h2>
                                                <h5>Teacher: <strong><a href="<?php echo URL . 'master-detail/' . $r['teacher_slug'] ?>"><?php echo $r['teacher'] ?></a></strong></h5>
                                                <h5>Duration: <strong><a href="#"><?php echo $r['duration_hr'] ? $r['duration_hr'] . ' Hours' : '' ?> <?php echo $r['duration_mn'] ? $r['duration_mn'] . ' Minutes' : '' ?></a></strong></h5>
                                                <h5>Batch Start Date: <strong><a href="#"><?php echo get_date($r['batch_start_date']) ?></a></strong></h5>
                                                <div class="view-more-batch" onclick="fnViewMoreBatch('<?php echo $r['id']; ?>', '<?php echo $r['slug']; ?>')" id="sViewMoreBatch">View More Batch</div>
                                                <p><?php echo str_short($r['short_description'], 50) ?></p>
                                                <div class="text-center">
                                                    <a href="<?php echo URL . 'course/' . $r['slug'] ?>" class="text_uppercase more_read">Read More ...</a>                                    
                                                    <a href="<?php echo URL . 'course/' . $r['slug'] ?>" class="text_uppercase more_read text-right">Enroll Now</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--End course single  item -->
                                <?php endforeach; ?>
                                <div class="clearfix"></div> 
                                <div class="row" style="padding-bottom:10px;"> 
                                    <div class="col-md-12"> 
                                        <div class="text-center">
                                            <a href="courses" class="bmore">EXPLORE ALL COURSES</a>
                                        </div> 
                                    </div> 
                                </div>
                                <div class="clearfix"></div>    <!-- paginitaion-->
                                <?php
                                $category = isset($category) && !empty($category) ? $category : 'all-course';
                                $sPaginateURL = URL . 'courses/' . $category . '/' . $sCurrencyType . '/';
                                if (isset($page['total_pages']) && $page['total_pages'] > 1) {
                                    $sPagoinationHtml = '<div class="col-md-12">'
                                            . '<div class="pagenition_bar">'
                                            . '<nav>'
                                            . '<ul class="pagination paginition_text">';
                                    if ($page['cur_page'] > 1) {
                                        $sPagoinationHtml .= ' <li><a href="' . $sPaginateURL . ($page['cur_page'] - 1) . '">Previous</a></li>';
                                    }
                                    for ($i = 1; $i <= $page['total_pages']; $i++) {
                                        $sDisabled = (isset($page['cur_page']) && !empty($page['cur_page']) && $page['cur_page'] == $i) ? 'disabled' : '';
                                        $sPagoinationHtml .= ' <li class="' . $sDisabled . '"><a href="' . $sPaginateURL . $i . '">' . $i . '</a></li>';
                                    }
                                    if ($page['cur_page'] != $page['total_pages']) {
                                        $sPagoinationHtml .= ' <li><a href="' . $sPaginateURL . ($page['cur_page'] - 1) . '">Next</a></li>';
                                    }
                                    $sPagoinationHtml .= '</ul>'
                                            . '</nav>'
                                            . '</div>'
                                            . '</div>';
                                    echo $sPagoinationHtml;
                                }
                                ?>
                                <!--end  paginitaion-->	
                            </div>
                        <?php else: ?>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-lg-12 min_height_200 text-center">
                                    <?php echo sMSG_101; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php if (isset($sCategoryName) && $sCategoryName != 'All Courses') { ?>
                <!-- Latest Updates -->
                <div class="col-md-3 col-sm-4">
                    <?php $this->load->view("elements/rightbar", array('pg' => 'course_list')); ?>
                </div>
                <div class="clearfix"></div>
            <?php } ?>
        </div>
    </div>
</div>
<!-- Modal -->