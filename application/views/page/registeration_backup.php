<script src="assets/front/js/enroll.js?<?php echo VERSION ?>"></script>
<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo URL ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li>Enroll Now</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->

<style>
    .error{
        font-size: 12px;
        font-weight: normal;
        color: red;
    }
    .width_700{
        width:700px !important;
    }
    .width_700 .modal-header{
        background-color: #999;
        border: solid 1px #999;
    }
    .width_700 .modal-header #sMessageDialogTitle{
        padding-left: 230px !important;
    }
    .width_700 .modal-body #sMessageDialogContent{
        text-align: justify;
        line-height: 22px;
    }
    .width_700 .black-btn{ 
        display: none
    }
    .form-control{
        font-size: 13px !important;
        font-weight: normal;
        color: #999 !important;
    }
    .control-label{
        text-align: left !important;
    }
    textarea{
        width: 100% !important
    }
</style>
<?php
$aUserData = get_session(USR_SESSION_NAME);

$sLoginUser = false;
if (isset($aUserData) && !empty($aUserData)) {
    $sLoginUser = true;
}
?>

<div class="container">
    <div class="row">
        <div class="form_box">
            <div class="form_inner">
                <form action="" method="post" id="sEnrollmentForm" name="sEnrollmentForm">

                    <?php if (isset($sLoginUser) && $sLoginUser == true) { ?>
                        <div class="section1">
                            <div class="labelclass">Registration Form</div>
                        </div>
                        <div class="part_a">
                            <h3>Basis Details</h3>
                            <div class="blok">
                                <ul>
                                    <li>
                                        <div class="labelclass">Full Name</div>
                                        <div class="textclass"><input type="text" name="name"  id="name" value="<?php echo $sUserName; ?>"/></div>

                                    </li>
                                    <li>
                                        <div class="labelclass">Email Address</div>
                                        <div class="textclass"><input type="email" name="email" id="email" value="<?php echo $sUserEmail; ?>" <?php echo $sEmailFieldType; ?> /></div>

                                    </li>
                                    <li>
                                        <div class="labelclass">Mobile No</div>
                                        <div class="textclass"><input type="text" name="contact_no" minlength="10" maxlength="10"  id="contact_no" /></div>

                                    </li>
                                </ul>
                            </div>
                            <div class="blok">
                                <ul>
                                    <li>
                                        <div class="labelclass">Country</div>
                                        <div class="textclass">
                                            <select id="country" name="country">
                                                <option value=""> -- Select --</option>
                                                <?php
                                                if (isset($aCountries) && !empty($aCountries)) {
                                                    foreach ($aCountries as $aCountry) {
                                                        echo ' <option value="' . $aCountry['id'] . '">' . ucwords(strtolower($aCountry['name'])) . '</option>';
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>

                                    </li>
                                    <li>
                                        <div class="labelclass">State/Province</div>
                                        <div class="textclass">
                                            <select id="state" name="state">
                                                <option value=""> -- Select --</option>
                                            </select>
                                        </div>

                                    </li>
                                    <li>
                                        <div class="labelclass">City</div>
                                        <div class="textclass"><input type="text" name="city" id="city" /></div>

                                    </li>
                                </ul>
                            </div>
                            <div class="blok">
                                <ul>
                                    <li>
                                        <div class="labelclass">Address</div>
                                        <div class="textclass"><textarea name="address" id="address"></textarea></div>

                                    </li>
                                    <li>
                                        <div class="labelclass">Date of Birth</div>
                                        <div class="textclass"><div class="calendar"><img src="theme/front/img/calendar.png" /></div><input type="text" name="dob" id="dob" /></div>

                                    </li>
                                    <li>
                                        <div class="labelclass">Gender</div>
                                        <div class="textclass">
                                            <select id="gender" name="gender">
                                                <option value=""> -- Select --</option>
                                                <option value="male">Male</option>
                                                <option value="female">Female</option>
                                                <option value="other">Other</option>
                                            </select>
                                        </div>

                                    </li>
                                </ul>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="part_a part_b">
                        <h3>Course Details<span class="reg-intro">(As per your selection/interest)</span> </h3>
                        <div class="blok">
                            <ul>
                                <li>
                                    <div class="labelclass">Course Type</div>
                                    <div class="textclass">
                                        <div class="display-info">
                                            <?php echo isset($aCourseDetail['type']) && !empty($aCourseDetail['type']) ? ucwords($aCourseDetail['type']) : ''; ?>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="labelclass">Course Category</div>
                                    <div class="textclass">
                                        <div class="display-info">
                                            <?php echo isset($aCategoryDetail['title']) && !empty($aCategoryDetail['title']) ? ucwords($aCategoryDetail['title']) : ''; ?>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="labelclass">Course Name</div>
                                    <div class="textclass">
                                        <div class="display-info">
                                            <?php echo isset($aCourseDetail['title']) && !empty($aCourseDetail['title']) ? ucwords($aCourseDetail['title']) : ''; ?>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="labelclass">Your Batch</div>
                                    <div class="textclass">
                                        <div class="display-info">
                                            <?php echo isset($aBatchDetail['batch_days']) && !empty($aBatchDetail['batch_days']) ? ucwords($aBatchDetail['batch_days']) : ''; ?></br> <?php echo isset($aBatchDetail['start_date']) && !empty($aBatchDetail['start_date']) ? get_date($aBatchDetail['start_date']) : ''; ?> - <?php echo isset($aBatchDetail['batch_time']) && !empty($aBatchDetail['batch_time']) ? date('h:i A', strtotime($aBatchDetail['batch_time'])) : ''; ?>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="labelclass">Course Fee</div>
                                    <div class="textclass">
                                        <div class="display-info">
                                            <?php if (isset($aCourseDetail['price_type']) && !empty($aCourseDetail['price_type']) && $aCourseDetail['price_type'] == 'free') { ?>
                                                Free    
                                            <?php } else if (isset($aCourseDetail['price_type']) && !empty($aCourseDetail['price_type']) && $aCourseDetail['price_type'] == 'paid') { ?>
                                                <?php if (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'INR') { ?>
                                                    <?php if ($aCourseDetail['price'] > 0): ?>
                                                        <?php if ($aCourseDetail['mrp'] > 0): ?>
                                                            <i class="fa fa-inr" aria-hidden="true"></i><span class="course_price"> <?php echo $aCourseDetail['mrp'] ?></span> |
                                                        <?php endif; ?>
                                                        <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $aCourseDetail['price'] ?>
                                                    <?php endif; ?>
                                                <?php }else if (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'USD') { ?>
                                                    <?php if ($aCourseDetail['price_usd'] > 0): ?>
                                                        <?php if ($aCourseDetail['mrp_usd'] > 0): ?>
                                                            <i class="fa fa-usd" aria-hidden="true"></i><span class="course_price"> <?php echo $aCourseDetail['mrp_usd'] ?></span> |
                                                        <?php endif; ?>
                                                        <i class="fa fa-usd" aria-hidden="true"></i> <?php echo $aCourseDetail['price_usd'] ?>
                                                    <?php endif; ?>                                                       
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </li>

                            </ul>

                        </div>
                        <div class="checkbox">
                            <input name="agree" id="agree" type="checkbox" value="" />I agree to the 
                            <a class="various1 terms-policy" id="sTermsAndCondition">Terms & Conditions</a> and 
                            <a class="various1 terms-policy" id="sPrivactPolicy">Privacy Policy</a> of Shakti Multiversity
                        </div>
                    </div>
                    <div class="submitclass"><input type="submit" value="Submit" /></div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<script type="text/javascript">
    $("#country").change(function () {
        var id = $('#country').val();
        $.get("state/" + id, function (data, status) {
            $("#state").html(data);
        });
    });
    function validateFileType(id) {
        var fileName = document.getElementById(id).value;
        var idxDot = fileName.split(".");
        var extFile = idxDot[idxDot.length - 1].toLowerCase();
        if (extFile == "jpg" || extFile == "jpeg") {
            return true;
        } else {
            document.getElementById(id).value = '';
            alert("Only jpg/jpeg files are allowed!");
            return false;
        }
    }
</script>