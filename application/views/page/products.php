<!--Start about title  area --> 
<div class="about_area_s about_area_shop">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading_about">
                    <h1 class="page-title">
                        Shop
                    </h1>					
                </div>
            </div>
        </div>
    </div>
</div>
<!--Start about  area --> 	

<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <form name="sCurrencyForm" id="sCurrencyForm" action="" method="post">
                        <ul>
                            <li><a href="<?php echo URL ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                            <li>Shop</li>
                            <li style="float:right">
                                <label>Currency</label>&nbsp;
                                <select name="currencyType" id="currencyType" onchange="this.form.submit()">
                                    <option value="">Select</option>
                                    <option value="INR" <?php echo (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'INR') ? 'selected = "selected"' : ''; ?>>INR</option>
                                    <option value="USD" <?php echo (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'USD') ? 'selected = "selected"' : ''; ?>>USD</option>
                                </select>
                            </li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->


<div class="priging_area priging_area_shop">
    <div class="container">
        <?php if ($result): ?>
            <div class="row">
                <?php foreach ($result as $r): $images = explode(",", $r['images']) ?>
                    <div class="col-xs-12 col-sm-6 col-lg-3">
                        <div class="courses_thumb">
                            <div class="courses_thumb_text">
                                <?php if (isset($r['price_type']) && !empty($r['price_type']) && $r['price_type'] == 'free'): ?>
                                    Free    
                                <?php else: ?>
                                    <?php if (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'INR') { ?>
                                        <?php if (isset($aUserData) && !empty(isset($aUserData)) && isset($aUserData['isLifetimeMember']) && $aUserData['isLifetimeMember'] == true && $r['price'] > 0): ?>
                                            <i class="fa fa-inr" aria-hidden="true"></i><span class="course_price"> <?php echo $r['mrp'] ?></span> |
                                            <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $r['price'] ?>
                                        <?php else: ?>
                                            <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $r['mrp'] ?>
                                        <?php endif; ?>
                                    <?php }else if (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'USD') { ?>
                                        <?php if (isset($aUserData) && isset($aUserData['isLifetimeMember']) && $aUserData['isLifetimeMember'] == true && $r['price_usd'] > 0): ?>
                                            <i class="fa fa-usd" aria-hidden="true"></i><span class="course_price"> <?php echo $r['mrp_usd'] ?></span> |
                                            <i class="fa fa-usd" aria-hidden="true"></i> <?php echo $r['price_usd'] ?>
                                        <?php else: ?>
                                            <i class="fa fa-usd" aria-hidden="true"></i> <?php echo $r['mrp_usd'] ?>
                                        <?php endif; ?>                                             
                                    <?php } ?>
                                <?php endif; ?>						
                            </div>							
                        </div>
                        <div class="priging_item">

                            <div class="priging_thumb">
                                <a href="<?php echo URL . 'shop/' . $r['slug'] ?>">
                                    <img class="atvImg-layer" src="<?php echo UP_URL . 'pro-lg/' . $images[0] ?>"/>											
                                </a>
                            </div>
                            <div class="priging_content mobile-text-center">
                                <h2><?php echo $r['title'] ?></h2>	
                                <p><?php echo $r['writer'] ? 'by ' . $r['writer'] : '&nbsp;' ?></p>
                                <div class="mt10 text-center">
                                    <a href="<?php echo URL . 'shop/' . $r['slug'] ?>" class="text_uppercase more_read border_none text-right">Order Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                <div class="clearfix"></div>
            </div>
        <?php endif; ?>
    </div>
</div>