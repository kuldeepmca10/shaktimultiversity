<!--Start about title  area --> 
<div class="about_area_s happening_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading_about">
                    <h1 class="page-title">
                        Happenings
                    </h1>					
                </div>
            </div>
        </div>
    </div>
</div>
<!--Start about  area --> 


<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo URL ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li>Happenings</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->	

<!--start courses  area -->
<div class="faq_area">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-xs-12  photo_happening"><h1>Happenings</h1></div>
            <div class="col-md-4 col-xs-12  text-center pull-right">
                <div style="background: #fff200;padding:10px 10px;margin-bottom: 20px;">
                    <?php
                    if (isset($cats) && !empty($cats)) {
                        $sSelect = '<select class="selectpicker" onchange="loadHappenings(this.value)" style="width:100%;">';
                        $sSelect .=' <option value="">All Event</option>';
                        foreach ($cats as $iKey => $sValue) {
                            $sSelected = '';
                            if (isset($category_slug) && !empty($category_slug) && $category_slug == $iKey) {
                                $sSelected = 'Selected="selected"';
                            }
                            $sSelect .=' <option value="' . $iKey . '" ' . $sSelected . '>' . $sValue . '</option>';
                        }
                        $sSelect.='</select>';
                    }
                    echo $sSelect;
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- single new_bulletin item -->
            <?php
            if (isset($result) && !empty($result)) {
                foreach ($result as $aHappeningsDetail) {
                    $sClass = ($iCount % 2) == 0 ? 'border:1px solid #e7ecf1; padding:5px; background:#F9F9F9' : 'border:1px solid #e8ecf1; padding:5px; background:#F1F1F1';
                    ?>
                    <div class="col-sm-12 margin-bottom" style="<?php echo $sClass; ?>">
                       					
                            <div class="col-lg-3 col-md-3 col-sm-3">
                                <div class="new_bulletin">
                                    <a href="<?php echo URL . "happening-detail/" . $aHappeningsDetail['slug']; ?>" class="course_details_img">
                                        <?php $sImagePath = isset($aHappeningsDetail['image']) && !empty($aHappeningsDetail['image']) ? UP_URL . "happenings-sm/" . $aHappeningsDetail['image'] : ''; ?>
                                        <img src="<?php echo $sImagePath; ?>" alt="" />
                                    </a>
                                </div>

                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9">
                                <div class="news_content news_buletin_pra">
                                    <h2>
                                        <a href="<?php echo URL . "happening-detail/" . $aHappeningsDetail['slug']; ?>">
                                            <?php echo isset($aHappeningsDetail) && !empty($aHappeningsDetail) ? $aHappeningsDetail['title'] : ''; ?>
                                        </a>
                                    </h2>
                                    <p class="date">
                                        <span><i class="fa fa-calendar"></i>&nbsp;<?php echo get_date($aHappeningsDetail['publish_date']); ?></span>
                                        <span><i class="fa fa-eye"></i> Source: <?php echo $aHappeningsDetail['source']; ?></span>
                                    </p>
                                    <p> <?php echo isset($aHappeningsDetail) && !empty($aHappeningsDetail) ? $aHappeningsDetail['short_description'] : ''; ?></p>
                                    <a href="<?php echo URL . "happening-detail/" . $aHappeningsDetail['slug']; ?>">Read More </a>

                                </div>				
                            </div>				
                        
                    </div>
                    <?php
                }
            } else {
                echo '<div class="col-md-12 col-sm-12 text-center">No Happenings Found</div>';
            }
            ?>
            <!-- end single new_bulletin item -->
            <!-- paginitaion-->
<!--            <div class="col-md-12">
                <div class="pagenition_bar">
                    <nav>
                        <ul class="pagination paginition_text mg_bottom60">
                            <li class="disabled"><a href="">1</a></li>
                            <li><a href="">2</a></li>
                            <li><a href="">3</a></li>
                            <li><a href="">4</a></li>
                            <li><a href="">5</a></li>
                            <li><a href="">6</a></li>
                            <li><a href="">Next</a></li>
                        </ul>
                    </nav>					
                </div>					
            </div>-->
            <!--end  paginitaion-->

        </div>
    </div>
</div>	
<!--end courses  area -->	

<script>
    function loadHappenings(val) {
        window.location.href = "<?php echo URL ?>happenings/" + val;
    }
</script>