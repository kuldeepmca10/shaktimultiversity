<?php
if ($result) {
    $testis = array_chunk($result, 3);
}
?>

<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo URL; ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li><?php echo ucwords(str_replace('-', ' ', $category_slug)); ?> Testimonials </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->		

<div class="about_area page">

    <div class="container">

        <div class="row">
            <div class="col-md-8 col-xs-12  photo_gallery"><h1><?php echo ucwords(str_replace('-', ' ', $category_slug)); ?> Testimonials </h1></div>
            <div class="col-md-4 col-xs-12  text-center pull-right">
                <div style="background: #f6f6ef;padding:10px 10px;margin-bottom: 20px;">
                    <?php
                    if (isset($cats) && !empty($cats)) {
                        $sSelect = '<select class="selectpicker" onchange="loadTestimonials(this.value)" style="width:100%;">';
                        $sSelect .= ' <option value="">All Courses</option>';
                        foreach ($cats as $iKey => $sValue) {
                            $sSelected = '';
                            if (isset($category_slug) && !empty($category_slug) && $category_slug == $iKey) {
                                $sSelected = 'Selected="selected"';
                            }
                            $sSelect .= ' <option value="' . $iKey . '" ' . $sSelected . '>' . $sValue . '</option>';
                        }
                        $sSelect .= '</select>';
                    }
                    echo $sSelect;
                    ?>
                </div>
            </div>
        </div>	

        <div class="row">
            <div class="container">
                <div class="row">
                    <?php if ($result): ?>
                        <?php foreach ($testis as $testimonials): ?>
                            <?php foreach ($testimonials as $i => $r): ?>
                                <div class="col-md-4" id="testimonial-<?php echo $r['id']; ?>">
                                    <div class="<?php echo $i == 0 ? 'testimonial-bg' : ($i == 1 ? 'testimonial-bg testimonial-bg1' : 'testimonial-bg') ?>">
                                        <div id="">
                                            <div class="testimonial_new">
                                                <?php
                                                $sClass = '';
                                                if (strlen($r['description']) > 205) {
                                                    $sClass = 'description_scroll';
                                                }
                                                ?>

                                                <p class="date"><?php echo get_date($r['publish_date']); ?></p>
                                                <p class="description <?php echo $sClass; ?> ">
                                                    <?php echo $r['description'] ?>
                                                </p>

                                                <?php if (file_exists(UP_PATH . 'testimonials/' . $r['image'])) { ?>
                                                    <div class="pic">
                                                        <img src="<?php echo UP_URL . "testimonials/" . $r['image'] ?>" alt="">
                                                    </div>
                                                    <?php
                                                } else {
                                                    echo '<br>';
                                                }
                                                ?>
                                                <h3 class="title"><?php echo $r['title'] ?></h3>
                                                <div class="post"><?php echo isset($r['subtitle']) && !empty($r['subtitle']) ? $r['subtitle'] : '&nbsp;' ?></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>        
                            <?php endforeach; ?>
                        <?php endforeach; ?>

                        <?php
                        $category = isset($category_slug) && !empty($category_slug) ? $category_slug : 'all-course';
                        $sPaginateURL = URL . 'testimonials/' . $category . '/';
                        if (isset($page['total_pages']) && $page['total_pages'] > 1) {
                            $sPagoinationHtml = '<div class="col-md-12">'
                                    . '<div class="pagenition_bar">'
                                    . '<nav>'
                                    . '<ul class="pagination paginition_text">';
                            if ($page['cur_page'] > 1) {
                                $sPagoinationHtml .= ' <li><a href="' . $sPaginateURL . ($page['cur_page'] - 1) . '">Previous</a></li>';
                            }
                            for ($i = 1; $i <= $page['total_pages']; $i++) {
                                $sDisabled = (isset($page['cur_page']) && !empty($page['cur_page']) && $page['cur_page'] == $i) ? 'disabled' : '';
                                $sPagoinationHtml .= ' <li class="' . $sDisabled . '"><a href="' . $sPaginateURL . $i . '">' . $i . '</a></li>';
                            }
                            if ($page['cur_page'] != $page['total_pages']) {
                                $sPagoinationHtml .= ' <li><a href="' . $sPaginateURL . ($page['cur_page'] + 1) . '">Next</a></li>';
                            }
                            $sPagoinationHtml .= '</ul>'
                                    . '</nav>'
                                    . '</div>'
                                    . '</div>';
                            echo $sPagoinationHtml;
                        }
                        ?>
                        <!--end  paginitaion-->	
                    <?php else: ?>
                        <div class="col-md-4">
                            No Testimonial Found
                        </div>   
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end about  area -->	
<script>
    function loadTestimonials(val) {
        window.location.href = "<?php echo URL ?>testimonials/" + val;
    }
</script>