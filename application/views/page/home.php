<!-- Slider -->
<?php $this->load->view("elements/slider"); ?>
<!-- / -->

<!--Start about  area --> 
<div class="about_area">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="content-inner">
                    <h3 class="module-title">
                        Welcome to  <span> The Shakti Multiversity</span>
                    </h3>
                    <div class="content-desc text-justify">
                        <div class="">
                            <p class="lead">Offering courses in Yoga, Tantra, Spirituality and rare Ancient Sciences to the world under the guidance of Enlightened Masters!</p>
                        </div>

                        <div class="about_texts">
                            <p>Shakti Multiversity is the only resource available in India that offers the opportunity to learn Yoga, Tantra, Ritualistic processes and many other ancient occult sciences in its purest and authentic form. This initiative by enlightened Tantra Masters Ma Shakti Devpriya and Acharya Agyaatadarshan Anand Nath aims at making the applied spiritual knowledge available to all the seekers worldwide.</p>
                            <p><a href="<?php echo URL; ?>/about-us.html">read more...</a></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="video-wrapper pull-right">
                    <a class="venobox_custom mfp-iframe vidwrap"><iframe src="https://www.youtube.com/embed/lqI86Y0STWI?feature=oembed&rel=0" allowfullscreen="" height="360" frameborder="0" width="100%"></iframe></a>
                </div>				
            </div>
        </div>
    </div>
</div>
<!--end about  area -->

<!--start service  area -->
<div class="service_area">
    <div class="container">
        <div class="row">
            <?php
            $aUSPBanner = sidebar_banner_list(array('section' => 'usp'));

            if (isset($aUSPBanner) && !empty($aUSPBanner['result'])) {
                foreach ($aUSPBanner['result'] as $aUSPBanner) {
                    ?>
                    <div class="col-md-3 col-sm-6 col-lg-3">
                        <div class="service_item">
                            <span class="lnr">
                                <img width="100" height="100" src="uploads/sidebar-lg/<?php echo $aUSPBanner['image'] ?>">
                            </span>
                            <h2><?php echo $aUSPBanner['title'] ?></h2>
                            <p><?php echo $aUSPBanner['description'] ?></p>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>

    </div>
</div>	
<!--End service  area -->
<?php // echo isset($block['description']) && !empty($block['description']) ? $block['description'] : '';  ?>

<!-- Courses -->
<?php $this->load->view("elements/home_courses"); ?>
<!-- / -->

<!--start offer  area -->
<div class="offer_area">
    <div class="container">	
        <div class="row">
            <div class="col-md-6 col-sm-6 col-lg-6">
                <div class="title">
                    <h3 class="offer-title">
                        Do the things You like the most
                        <span>GET STARTED TODAY</span>
                    </h3>
                </div>				
                <div class="offer_item">
                    <div class="offer_content">							
                        <p>Join us in creating a better world through participating in our charitable social initiatives for the poorest of the poor..</p>
                        <a href="http://www.sacredassociation.org/contact-us/" target="_blank" class="readmore">Support</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-6">
                <div class="media-box">
                    See! What We can do Together
                    <a class="video-button text-uppercase cursor_pointer" id="sMessageHomeVideo">
                        <i class="fa fa-play-circle-o"></i>
                        <span>Watch Video</span>						
                    </a>																		
                </div>
            </div>				
        </div>
    </div>
</div>	
<!--end offer  area -->



<!-- News -->
<?php $this->load->view("elements/home_happenings"); ?>
<!-- / -->

<!-- Teachers -->
<?php $this->load->view("elements/home_teachers"); ?>
<!-- / -->

<!-- Products -->
<?php $this->load->view("elements/home_products"); ?>
<!-- / -->