<div class="about_area_s">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading_about">
                    <h1 class="page-title">
                        Academic Calender
                    </h1>					
                </div>
            </div>
        </div>
    </div>
</div>

<!-- breadcrumb-area start -->

<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo URL ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li><?php echo isset($category_name) ? ucwords(strtolower($category_name)) : 'Academic Calender'; ?></li>
<!--                        <li style="float:right">
                            <label>Currency</label>&nbsp;
                            <select name="currencyType" id="currencyType">
                                <option value="">Select</option>
                                <option value="INR" <?php echo (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'INR') ? 'selected = "selected"' : ''; ?>>INR</option>
                                <option value="USD" <?php echo (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'USD') ? 'selected = "selected"' : ''; ?>>USD</option>
                            </select>
                        </li>-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->
<div class="about_area page">
    <div class="container">
        <div class="col-md-8">
            <?php if ($result) { ?>
                <div class="row">
                    <?php
                    
                    foreach ($result as $r) {
                        ?>
                        <div class="row margin-bottom">						
                            <div class="news_content news_buletin_pra">
                                <h1><a href="<?php echo URL . 'course/' . $r['slug'] ?>"><?php echo $r['title'] ?></a></h1>
                                <p class="date">
                                    <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Master"><i class="fa fa-user"></i>&nbsp;<?php
                                        echo $r['teacher']
                                        ?></span>
                                    <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Category"><i class="fa fa-folder"></i><a href="<?php echo URL . 'course/' . $r['cat'] ?>">&nbsp;<?php echo $r['cat'] ?></a></span>
                                    <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Batch Start Date"><i class="fa fa-calendar"></i>&nbsp;<?php echo get_date($r['batch_start_date']) ?></span>
                                    <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Duation <?php echo isset($r['duration_hr']) && !empty($r['duration_hr']) ? $r['duration_hr'] . ' Hours' : ''; ?> <?php echo isset($r['duration_mn']) && !empty($r['duration_mn']) ? $r['duration_mn'] . ' Min' : ''; ?>"><i class="fa fa-clock-o"></i> Duration: <?php echo isset($r['duration_hr']) && !empty($r['duration_hr']) ? $r['duration_hr'] . ' Hours' : ''; ?> <?php echo isset($r['duration_mn']) && !empty($r['duration_mn']) ? $r['duration_mn'] . ' Min' : ''; ?> </span>
                                </p>							
                                <p><?php echo $r['short_description']; ?></p>
                                <a href="<?php echo URL . 'course/' . $r['slug'] ?>" class="text_uppercase more_read mg0">Read More...</a>
                            </div>				
                        </div>

                    <?php }
                    ?>
                </div>
                <?php
            }
            ?>





            <!-- end single blog item -->
            <!-- paginitaion-->
            <div class="row">
                <div class="col-md-12">
                    <?php
                    $category = isset($category) && !empty($category) ? $category : 'all-course';
                    $sPaginateURL = URL . 'courses/' . $category . '/' . $sCurrencyType . '/';
                    if (isset($page['total_pages']) && $page['total_pages'] > 1) {
                        $sPagoinationHtml = '<div class="col-md-12">'
                                . '<div class="pagenition_bar">'
                                . '<nav>'
                                . '<ul class="pagination paginition_text">';
                        if ($page['cur_page'] > 1) {
                            $sPagoinationHtml .= ' <li><a href="' . $sPaginateURL . ($page['cur_page'] - 1) . '">Previous</a></li>';
                        }
                        for ($i = 1; $i <= $page['total_pages']; $i++) {
                            $sDisabled = (isset($page['cur_page']) && !empty($page['cur_page']) && $page['cur_page'] == $i) ? 'disabled' : '';
                            $sPagoinationHtml .= ' <li class="' . $sDisabled . '"><a href="' . $sPaginateURL . $i . '">' . $i . '</a></li>';
                        }
                        if ($page['cur_page'] != $page['total_pages']) {
                            $sPagoinationHtml .= ' <li><a href="' . $sPaginateURL . ($page['cur_page'] - 1) . '">Next</a></li>';
                        }
                        $sPagoinationHtml .= '</ul>'
                                . '</nav>'
                                . '</div>'
                                . '</div>';
                        echo $sPagoinationHtml;
                    }
                    ?>					
                </div>
            </div>
            <!--end  paginitaion-->						

            <!--  blog Right -->


        </div>

        <div class="col-md-4 col-sm-6">
            <?php $this->load->view("elements/rightbar", array('pg' => 'course_list')); ?>
        </div>
    </div>		

</div>
<script>
    $(document).ready(function () {
        $("#currencyType").change(function () {
            window.location.href = '<?php echo URL . 'courses/' . $category ?>/' + $(this).val();
        });
    });
</script>