<style>

    #thumbGrid{
        width: 980px;
        margin: auto;

    }

    label{
        color: white;
    }


    #customize {
        display: inline-block;
        margin-top: -30px;
        position: absolute;
        color: white
    }


    #customize input, #customize select {
        font-size: 17px;
        margin: 3px;
        padding: 4px;
        border: 1px solid rgba(255, 255, 255, 0.5);
        border-radius: 0;
        color:#fff;
        background-color: rgba(0, 0, 0, 0.2)
    }

    #customize select {
        margin: 0;
        background-image: url(css/thumbgrid-font/font/downArrow.svg);
        background-repeat: no-repeat;
        background-position: right center;
        padding-right: 25px;
        outline: none;
        overflow: hidden;
        text-indent: 0.01px;
        text-overflow: '';
        -webkit-appearance: none;
        -moz-appearance: none;
        -ms-appearance: none;
        -o-appearance: none;
        appearance: none;
        vertical-align: middle;

    }

    #customize label {
        font-size: 16px;
        margin: 3px 0;
        padding: 3px 0;
        width: 150px;
        display: inline-block;
    }
</style>
<!--happening-->		
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="<?php echo URL; ?>theme/front/js/jquery.mb.browser.min.js"></script>
<script src="<?php echo URL; ?>theme/front/js/jquery.mb.CSSAnimate.min.js"></script>
<script src="<?php echo URL; ?>theme/front/js/jquery.mb.thumbGrid.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo URL; ?>theme/front/css/thumbGrid.css"/>
<!--happening-->

<!--Start about title  area --> 
<div class="about_area_s happening_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading_about">
                    <h1 class="page-title">
                        Announcement Detail
                    </h1>					
                </div>
            </div>
        </div>
    </div>
</div>
<!--Start about  area --> 


<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo URL; ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li><a href="<?php echo URL; ?>announcement">Announcements</a> <i class="fa fa-angle-right"></i></li>							
                        <li>Announcement Detail</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->		


<div class="about_area page">
    <div class="container">
        <div class="col-md-8 no-padding-mobile">
            <div class="single_cos_item"> 
                <div class="courses_area" style="background-image:none"> 
                    <div class="row"> 
                        <?php if (isset($aAnnouncementDetail)) { ?>
                            <div class="col-md-12 col-xs-12">
                                <h1><?php echo $aAnnouncementDetail['title']; ?></h1>
                                <p class="date">
                                    <span class="col-xs-12 no-padding-mobile">
                                        <i class="fa fa-calendar-o" aria-hidden="true"></i>&nbsp;<?php echo date('D, d M Y', strtotime($aAnnouncementDetail['publish_date'])) ?>&nbsp;&nbsp;&nbsp;
                                    </span>
                                    <span class="col-xs-12 no-padding-mobile">
                                        <i class="fa fa-eye"></i>&nbsp;Source:&nbsp;<?php echo $aAnnouncementDetail['source']; ?>
                                    </span>
                                </p>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <?php $sImagePath = isset($aAnnouncementDetail['image']) && !empty($aAnnouncementDetail['image']) ? UP_URL . "happenings-sm/" . $aAnnouncementDetail['image'] : ''; ?>
                                <img src="<?php echo $sImagePath; ?>" alt=""  class="course_details_img" style="width:100%">
                            </div> 
                            <div class="col-md-12 col-xs-12" style="padding-top:10px;">
                                <?php echo $aAnnouncementDetail['description']; ?>
                            </div> 
                        <?php } ?>
                        <div class="clearfix"></div>
                    </div> <!-- Modal -->
                </div> 
            </div>

        </div>
        <div class="col-md-4 col-sm-6 no-padding-mobile">
            <?php $this->load->view("elements/rightbar", array('pg' => 'cms')); ?>
        </div>
    </div>		
</div>
<!--Start about  area --> 