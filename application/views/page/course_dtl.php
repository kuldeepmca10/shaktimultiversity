<!-- Slider -->
<div class="slider_area">
    <div class="container-fluid">
        <div class="row">
            <div class="tab-content">
                <?php foreach ($sliders as $r): ?>
                    <!--single tab slide item-->
                    <div class="tab-pane active" id="a<?php echo $r['id'] ?>">
                        <div class="col-md-7 col-sm-7 col-xs-12 pd0">
                            <div class="slide_thumb">
                                <a href="<?php echo isset($r['link']) && !empty($r['link']) && $r['link'] != '#' ? $r['link'] : 'javascript::;'; ?>"><img src="<?php echo UP_URL . 'slider/' . $r['image'] ?>" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12 pd0">
                            <div class="slide_text">
                                <h2 class="slide_title"><?php echo $r['title'] ?></a></h2>
                                <p class="slide_ptext"><?php echo $r['subtitle'] ?></a></p>
                            </div>
                        </div>
                    </div>		
                    <!--end single tab slide item-->
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
<!--/ -->


<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="breadcrumb">
                    <form name="sCurrencyForm" id="sCurrencyForm" action="" method="post">
                        <ul>
                            <li><a href="<?php echo URL ?>">Home</a> <i class="fa fa-angle-right"></i></li>
                            <li><a href="<?php echo URL . 'courses' ?>">Courses</a> <i class="fa fa-angle-right"></i></li>							
                            <li><?php echo $dtl['title'] ?></li>
                            <?php if (isset($dtl['price_type']) && !empty($dtl['price_type']) && $dtl['price_type'] == 'paid') { ?>
                                <li style="float:right">
                                    <label>Currency</label>&nbsp;
                                    <select name="currencyType" id="currencyType" onchange="this.form.submit()">
                                        <option value="">Select</option>
                                        <option value="INR" <?php echo (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'INR') ? 'selected = "selected"' : ''; ?>>INR</option>
                                        <option value="USD" <?php echo (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'USD') ? 'selected = "selected"' : ''; ?>>USD</option>
                                    </select>
                                </li>
                            <?php } ?>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->

<div class="single_courcse">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6 col-xs-12">
                <div class="single_cos_item">
                    <div class="courses_area" style="background-image:none">
                        <div class="row">
                            <div class="col-md-5 col-xs-12"><img src="<?php echo UP_URL . 'course-lg/' . $dtl['image'] ?>" class="course_details_img"></div>
                            <div class="col-md-7 col-xs-12">
                                <h1><?php echo $dtl['title'] ?></h1>
                                <p><?php echo $dtl['short_description'] ?></p>
                                <ul class="course_heading_ul">
                                    <li class="blank-stars"></li>
                                    <li class="col-xs-12 no_padding"><span style="padding-left:2px;"><?php echo $dtl['no_of_learners'] ?> Learners</span></li>
                                    <!--<li><a href="#"><i class="fa fa-heart-o" aria-hidden="true"></i> Add to wishlist</a></li>-->
                                </ul>
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <div class="course_details_point">
                                            <div class="col-md-7">
                                                <ul class="border-right">
                                                    <li><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $dtl['duration_hr'] ? $dtl['duration_hr'] . ' Hrs' : '' ?> <?php echo $dtl['duration_mn'] ? $dtl['duration_mn'] . ' Min.' : '' ?> Course Duration</li>
                                                    <li><i class="fa fa-desktop" aria-hidden="true"></i> <?php echo $dtl['batches'][0]['duration_hr'] ? $dtl['batches'][0]['duration_hr'] . ' Hrs' : '' ?> <?php echo $dtl['batches'][0]['duration_mn'] ? $dtl['batches'][0]['duration_mn'] . ' Min.' : '' ?> Sessions</li>
                                                </ul>									
                                            </div>
                                            <div class="col-md-5">
                                                <ul>
                                                    <li><i class="fa fa-file-o" aria-hidden="true"></i> <?php echo $dtl['batches'][0]['sessions']; ?> Sessions</li>
                                                    <li><i class="fa fa-unlock-alt" aria-hidden="true"></i> 24/7 Learning Access</li>
                                                </ul>									
                                            </div>		
                                            <div class="clear"></div>									
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="course_details_price">
                                <div class="col-md-12 text-right">
                                    <div class="col-md-3 col-sm-6 col-xs-12 mobile-text-center mobile-margin-10 no_padding">
                                        <a href="javascript::void(0)">
                                            <?php if (isset($dtl['price_type']) && !empty($dtl['price_type']) && $dtl['price_type'] == 'free'): ?>
                                                Free    
                                            <?php else: ?>
                                                <?php if (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'INR') { ?>
                                                    <?php if (isset($aUserData) && !empty(isset($aUserData)) && isset($aUserData['isLifetimeMember']) && $aUserData['isLifetimeMember'] == true && $dtl['price'] > 0): ?>
                                                        <i class="fa fa-inr" aria-hidden="true"></i><span class="course_price"> <?php echo $dtl['mrp'] ?></span> |
                                                        <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $dtl['price'] ?>
                                                    <?php else: ?>
                                                        <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $dtl['mrp'] ?>
                                                    <?php endif; ?>
                                                <?php }else if (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'USD') { ?>
                                                    <?php if (isset($aUserData) && isset($aUserData['isLifetimeMember']) && $aUserData['isLifetimeMember'] == true && $dtl['price_usd'] > 0): ?>
                                                        <i class="fa fa-usd" aria-hidden="true"></i><span class="course_price"> <?php echo $dtl['mrp_usd'] ?></span> |
                                                        <i class="fa fa-usd" aria-hidden="true"></i> <?php echo $dtl['price_usd'] ?>
                                                    <?php else: ?>
                                                        <i class="fa fa-usd" aria-hidden="true"></i> <?php echo $dtl['mrp_usd'] ?>
                                                    <?php endif; ?>                                             
                                                <?php } ?>
                                            <?php endif; ?>
                                        </a>
                                    </div>
                                    <div class="col-md-3 col-sm-6 col-xs-12 mobile-text-center no_padding mobile-margin-10">
                                        <form name="sCurrencyForm" id="sCurrencyForm" action="" method="post">
                                            <label>Currency</label>
                                            <select name="currencyType" id="currencyType" onchange="this.form.submit()">
                                                <option value="">Select</option>
                                                <option value="INR" <?php echo (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'INR') ? 'selected = "selected"' : ''; ?>>INR</option>
                                                <option value="USD" <?php echo (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'USD') ? 'selected = "selected"' : ''; ?>>USD</option>
                                            </select>
                                        </form>
                                    </div>
                                    <div class="col-md-2 col-sm-12 col-xs-12 mobile-text-center mobile-margin-10" style="padding:0px; margin: 0px;">  
                                        <a href="javascript::;"  onclick="fnViewMoreBatch('<?php echo $dtl['id']; ?>', '<?php echo $dtl['slug']; ?>')" class="course_details_enroll">Enroll Now</a>
                                    </div> 
                                    <div class="col-md-4 col-sm-12 col-xs-12 mobile-text-center mobile-margin-20" style="padding:0px; margin: 0px;">  
                                        <a href="courses" class="bmore">Browse Other Courses</a>
                                    </div>
                                </div>
                                <div class="no-margin">
                                    <div class="col-md-6 hidden-xs"></div>

                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>


                        <div class="row">

                            <!--start course single  item -->
                            <div class="col-md-12 course_details">
                                <?php if ($dtl['batches']): ?>
                                    <div>
                                        <div class="b_title"><h3> Batch Information</h3></div>
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Starting From</th>
                                                    <th>Availability</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($dtl['batches'] as $r): ?> 
                                                    <tr>
                                                        <td><a href="#"><?php echo get_date($r['start_date']) ?></a></td>
                                                        <td><a href="#"><?php echo batch_status_name($r['batch_status']) ?></a></td>
                                                    </tr>  
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                <?php endif; ?>

                                <?php if ($dtl['description']): ?>
                                    <div class="b_title mt25"><h3> Course Description</h3></div>
                                    <?php echo $dtl['description'] ?>
                                <?php endif; ?>

                                <?php if ($dtl['caricullaum']): ?>
                                    <div class="b_title"><h3> Course Curriculum</h3></div>
                                    <?php echo $dtl['caricullaum'] ?>
                                <?php endif; ?>

                                <?php if ($dtl['eligibility_creteria']): ?>
                                    <div class="b_title"><h3> Eligibility Criteria</h3></div>
                                    <?php echo $dtl['eligibility_creteria'] ?>
                                <?php endif; ?>

                                <?php if ($dtl['benefit']): ?>
                                    <div class="b_title"><h3> Course Benefits</h3></div>
                                    <?php echo $dtl['benefit'] ?>
                                <?php endif; ?>

                                <?php if ($dtl['accommodation_dtl']): ?>
                                    <div class="b_title"><h3>Accommodation</h3></div>
                                    <?php echo $dtl['accommodation_dtl'] ?>
                                <?php endif; ?>

                                <?php if ($dtl['faq']): ?>
                                    <div class="b_title"><h3> FAQ's</h3></div>
                                    <?php echo $dtl['faq'] ?>
                                <?php endif; ?>

                                <?php if ($dtl['terms']): ?>
                                    <div class="b_title"><h3> Terms &amp; Conditions</h3></div>
                                    <?php echo $dtl['terms'] ?>
                                <?php endif; ?>

                                <div class="b_title"><h3> How To Apply</h3></div>
                                <div id="how-to-apply_new">
                                    <div class="col-md-12 how-to-apply wow bounceInUp animated animated" style="visibility: visible; animation-name: bounceInUp;">
                                        <div class="col-md-1 col-xs-12 text-center">
                                            <i class="glyphicon glyphicon-list-alt circle_new circle1 white"></i>
                                        </div>
                                        <div class="col-md-3 col-xs-12 pd15">
                                            Select Course & fill in The Registration Form
                                        </div>
                                        <div class="col-md-1 col-xs-12 text-center">
                                            <i class="glyphicon glyphicon-credit-card circle_new circle2 white"></i>
                                        </div>
                                        <div class="col-md-3 col-xs-12 pd15">
                                            Confirm Batch details and proceed to payment
                                        </div>
                                        <div class="col-md-1 col-xs-12 text-center">
                                            <i class="glyphicon glyphicon-time circle_new circle3 white"></i>
                                        </div>
                                        <div class="col-md-3 col-xs-12 pd15">
                                            Get course confirmation within 24 hours				
                                        </div>
                                    </div>

                                    <?php echo $dtl['how_to_apply'] ?>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="text-center"> 
                                            <a href="javascript::;" onclick="fnViewMoreBatch('<?php echo $dtl['id']; ?>', '<?php echo $dtl['slug']; ?>')" class="col-xs-12 course_details_enroll mobile-margin-10">Enroll Now</a>
                                            <a href="courses" class="col-xs-12 mobile-margin-10 bmore">BROWSE OTHER COURSES</a>
                                        </div>
                                    </div>
                                    
                                </div>
                                <!-- end container -->
                            </div>			

                        </div>
                    </div>						
                </div>
            </div>

            <!-- Latest Updates -->
            <div class="col-md-4 col-sm-6 col-xs-12">
                <?php $this->load->view("elements/rightbar", array('pg' => 'course_dtl')); ?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<script src="assets/front/js/enroll.js?<?php echo VERSION ?>"></script>
<style>
    .width_800{
        /*width: 800px !important;*/
    }
    .modal-title {
        margin: 15px 0px !important;
        line-height: 1.42857143;
    }
</style>