<div class="about_area_s">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading_about">
                    <h1 class="page-title">
                        Academic Calendar
                        <!--<span class="small">On The Learning</span>-->
                    </h1>					
                </div>
            </div>
        </div>
    </div>
</div>

<!-- breadcrumb-area start -->

<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
           <div class="breadcrumb">
				<div class="col-md-9">
                    <ul>
                        <li><a href="<?php echo URL ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li><?php echo isset($category_name) ? 'Academic Calendar - ' . ucwords(strtolower($category_name)) : 'Academic Calendar'; ?></li>
                    </ul>
                </div>
                <div class="col-md-3">
					<label>Currency</label>&nbsp;
					<select name="currencyType" id="currencyType">
						<option value="">Select</option>
						<option value="INR" <?php echo (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'INR') ? 'selected = "selected"' : ''; ?>>INR</option>
						<option value="USD" <?php echo (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'USD') ? 'selected = "selected"' : ''; ?>>USD</option>
					</select>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- breadcrumb-area end -->
<div class="about_area page page">
    <div class="container">
        <div class="col-md-8 col-xs-12 col-sm-8">
            <!-- Target elements -->
            <div class="arlo">
                <div class="arlo-page-title arlo-font-primary">
                    <h2>Academic Calendar</h2>
                </div>
                <div class="search_filter"> 
                    <div class="row"> 
                        <form name="searchForm" id="searchForm" action="" method="GET">
                            <div class="col-md-4 col-xs-12 text-left"> 
                                <label>Batch</label>
								<div class="iconinput-container">
									<input type="text" name="batch_date" id="batch_date" value="<?php echo isset($aPostedData['batch_date']) && !empty($aPostedData['batch_date']) ? $aPostedData['batch_date'] : '' ?>" style="width:100%;" />
<!--<input type="text" name="keyword" id="keyword" placeholder="Keyword..." value="<?php echo isset($aPostedData['keyword']) && !empty($aPostedData['keyword']) ? $aPostedData['keyword'] : '' ?>" style="width:60%;" />-->
								</div>
                            </div>
                            <div class="col-md-5 col-xs-12 photo_gallery">	 
                                <label>Category</label>
                                <?php
                                if (isset($cats) && !empty($cats)) {
                                    $sSelect = '<select name="category_name" id="category_name" class="selectpicker"  style="width:100%;">';
                                    $sSelect .=' <option value="">All Category</option>';
                                    foreach ($cats as $iKey => $sValue) {
                                        $sSelected = '';
                                        if (isset($aPostedData['category_name']) && !empty($aPostedData['category_name']) && $aPostedData['category_name'] == $iKey) {
                                            $sSelected = 'Selected="selected"';
                                        }
                                        $sSelect .=' <option value="' . $iKey . '" ' . $sSelected . '>' . $sValue . '</option>';
                                    }
                                    $sSelected = '';
                                    if (isset($aPostedData['category_name']) && !empty($aPostedData['category_name']) && $aPostedData['category_name'] == 'free-course') {
                                        $sSelected = 'Selected="selected"';
                                    }
                                    $sSelect .=' <option value="free-course" ' . $sSelected . '>Free Course</option>';

                                    $sSelect.='</select>';
                                }
                                echo $sSelect;
                                ?>
                            </div>

                            <div class="col-md-3 text-left mrg17"> 
                                <input type="submit" name="submit" id="submit" class="more_read" value="Search" />
                                <input type="reset" name="reset" id="reset"  class="more_read" onclick="window.location.href = '<?php echo URL ?>academic-calendar'" value="Reset" />
                            </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div id="upcoming-events-list1">                     
                    <?php if ($result) { ?>
                        <ul class="arlo-event-list">
                            <?php
                            foreach ($result as $r) {
                                ?>   
                                <li id="view7" class="mrgin25" style="list-style-type:none;">
                                  <div class="row">
                                        <div class="col-md-3 right_zero">
										   <div class="left">
												<div class="date">
													<?php echo get_date($r['batch_date']); ?>
												</div>
											<!--                                        <div class="time">
																						1:00 PM - 1:45 PM
																					</div>-->
												<div class="location">
													<?php echo strtoupper($r['type']); ?>
												</div>
											</div>
                                    </div>

                                    <div class="col-md-5 right_left_zero">
                                        <div class="middle">
                                        <h4><?php echo ucwords(strtolower($r['title'])); ?></h4>
                                        <div class="presenters">
                                            <span class="presenterlabel">Category: </span>
                                            <span class="presenter">
                                                <a href="<?php echo URL . 'course/' . $r['cat'] ?>" class="arlo-text-color-link"><?php echo ucwords(strtolower($r['cat'])); ?></a>
                                            </span>
                                        </div>
                                        <div class="presenters">
                                            <span class="presenterlabel">Master: </span>
                                            <span class="presenter">
                                                <a href="#" class="arlo-text-color-link"><?php echo ucwords(strtolower($r['teacher'])); ?></a>
                                            </span>
                                        </div>

                                   		</div>
                                    </div>

                                    <div class="col-md-4 left_zero">
                                       <div class="right">
                                        <div class="duration">
                                            <span class="content"><strong>Duration: </strong>
                                                <?php echo isset($r['duration_hr']) && !empty($r['duration_hr']) ? $r['duration_hr'] . ' hours' : '0 hours'; ?> <?php echo isset($r['duration_mn']) && !empty($r['duration_mn']) ? $r['duration_mn'] . ' minutes' : '0 minute'; ?>
                                            </span>
                                        </div>
                                        <div class="offers">
                                            <div class="arlo-offer-container ">
                                                <span class="arlo-offer-amount">
                                                    <?php if (isset($r['price_type']) && !empty($r['price_type']) && $r['price_type'] == 'free'): ?>
                                                        <strong>Course Fee:</strong> Free
                                                    <?php else: ?>
                                                        <?php if (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'INR') { ?>
                                                            <strong>Course Fee:</strong>                                                           
                                                            <?php if ($r['price'] > 0): ?>
                                                                <?php if ($r['mrp'] > 0): ?>
                                                                    <i class="fa fa-inr" aria-hidden="true"></i><span class="course_price"> <?php echo $r['mrp'] ?></span> |
                                                                <?php endif; ?>
                                                                <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $r['price'] ?>
                                                            <?php endif; ?>
                                                        <?php }else if (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'USD') { ?>
                                                            <strong>Course Fee:</strong>                                                           
                                                            <?php if ($r['price_usd'] > 0): ?>
                                                                <?php if ($r['mrp_usd'] > 0): ?>
                                                                    <i class="fa fa-usd" aria-hidden="true"></i><span class="course_price"> <?php echo $r['mrp_usd'] ?></span> |
                                                                <?php endif; ?>
                                                                <i class="fa fa-usd" aria-hidden="true"></i> <?php echo $r['price_usd'] ?>
                                                            <?php endif; ?>                                                       
                                                        <?php } ?>
                                                    <?php endif; ?>                                                   
                                                </span>
                                            </div>
                                        </div>

                                        <div class="registration">
                                            <a class="arlo-event-register" href="<?php echo URL . 'registeration/' . $r['slug'] . '/' . $r['batch_code'] ?>">Enroll Now</a>
                                            or <a href="<?php echo URL . 'course/' . $r['slug'] ?>" target="_blank">read more</a>

                                        </div>
                                    </div>
								</div>
                         	</div>
                                </li>
                            <?php }
                            ?>
                        </ul>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <!-- paginitaion-->
            <div class="row">
                <?php
                $category = isset($category) && !empty($category) ? $category : 'academic-calendar';
                $sPaginateURL = URL . $category . '/';
                if (isset($page['total_pages']) && $page['total_pages'] > 1) {
                    $sPagoinationHtml = '<div class="col-md-12">'
                            . '<div class="pagenition_bar">'
                            . '<nav>'
                            . '<ul class="pagination paginition_text">';
                    if ($page['cur_page'] > 1) {
                        $sPagoinationHtml .= ' <li><a href="' . $sPaginateURL . ($page['cur_page'] - 1) . '">Previous</a></li>';
                    }
                    for ($i = 1; $i <= $page['total_pages']; $i++) {
                        $sDisabled = (isset($page['cur_page']) && !empty($page['cur_page']) && $page['cur_page'] == $i) ? 'disabled' : '';
                        $sPagoinationHtml .= ' <li class="' . $sDisabled . '"><a href="' . $sPaginateURL . $i . '">' . $i . '</a></li>';
                    }
                    if ($page['cur_page'] != $page['total_pages']) {
                        $sPagoinationHtml .= ' <li><a href="' . $sPaginateURL . ($page['cur_page'] + 1) . '">Next</a></li>';
                    }
                    $sPagoinationHtml .= '</ul>'
                            . '</nav>'
                            . '</div>'
                            . '</div>';
                    echo $sPagoinationHtml;
                }
                ?>
            </div>
            <!--end  paginitaion-->						

            <!--  blog Right -->
        </div>
        <div class="col-md-4 col-xs-12 col-sm-4">
            <?php $this->load->view("elements/rightbar", array('pg' => 'course_list')); ?>
        </div>
    </div>		

</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.16.0/jquery.validate.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<script src="assets/front/js/enroll.js?<?php echo VERSION ?>"></script>
<script>
                                    $(document).ready(function () {
                                        /* Load Date Picker Start */
                                        var date_input = $('input[name="batch_date"]'); //our date input has the name "date"
                                        var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                                        date_input.datepicker({
                                            minViewMode: 1,
                                            format: 'M yyyy',
                                            container: container,
                                            todayHighlight: true,
                                            startDate: 'today',
                                            autoclose: true,
                                        });

                                        /* Load Date Picker End */
                                        $("#currencyType").change(function () {
                                            window.location.href = '<?php echo URL . 'courses/' . $category ?>/' + $(this).val();
                                        });
                                    });
</script>
<?php
//pr($result);
//pr($cat);
?>