
<!--Start about title  area --> 
<div class="about_area_s gallery_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading_about">
                    <h1 class="page-title">
                        Video Gallery
                    </h1>					
                </div>
            </div>
        </div>
    </div>
</div>
<!--Start about  area --> 


<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo URL ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li><a href="<?php echo URL ?>media-gallery">Media Gallery</a> <i class="fa fa-angle-right"></i></li>							
                        <li>Video Gallery</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->		


<!--Start about  area --> 
<div class="about_area">

    <div class="container">	
        <div class="row">
            <div class="col-md-8 photo_gallery"><h1>Video Gallery</h1></div>
            <div class="col-md-4 text-center pull-right">
                <div style="background: #fff200;padding:10px 10px;margin-bottom: 20px;">
                    <?php
                    if (isset($cats) && !empty($cats)) {
                        $sSelect = '<select class="selectpicker" onchange="loadGallery(this.value)" style="width:100%;">';
                        $sSelect .= ' <option value="">All Event</option>';
                        foreach ($cats as $iKey => $sValue) {
                            $sSelected = '';
                            if (isset($category_slug) && !empty($category_slug) && $category_slug == $iKey) {
                                $sSelected = 'Selected="selected"';
                            }
                            $sSelect .= ' <option value="' . $iKey . '" ' . $sSelected . '>' . $sValue . '</option>';
                        }
                        $sSelect .= '</select>';
                    }
                    echo $sSelect;
                    ?>
                </div>
            </div>
        </div>	

        <div class="row">
            <?php
            if (isset($result) && !empty($result)) {
                foreach ($result as $aGalleryData) {
                    if (isset($aGalleryData) && !empty($aGalleryData)) {
                        $aVideoData = explode('watch?v=', $aGalleryData['video_url']);
                        $sVideoData = explode('&', $aVideoData[1]);
                        $sVideoId = isset($sVideoData[0]) && !empty($sVideoData[0]) ? $sVideoData[0] : '';
                        ?>
                        <div class="col-md-3 col-sm-3">
                            <article>
                                <figure style="position:relative">
                                    <a href="javascript::void(0)" class="play-video"  data-title="<?php echo isset($aGalleryData) && !empty($aGalleryData) ? $aGalleryData['title'] : ''; ?>" data-src="<?php echo isset($sVideoId) && !empty($sVideoId) ? $sVideoId : ''; ?>">
                                        <?php
                                        echo '<img src="http://img.youtube.com/vi/' . $sVideoId . '/mqdefault.jpg" width="100%" />';
                                        ?>
                                        <figcaption class="video_gallery_figcaption2"><i class="fa fa-play" aria-hidden="true"></i></figcaption>
                                </figure></a>
                                <div class="video_gallery">
                                    <p><?php echo isset($aGalleryData) && !empty($aGalleryData) ? $aGalleryData['title'] : ''; ?></p>
                                    <!--<p><span>538 views</span> <time><?php // echo get_date($aGalleryData['created'], FALSE, 'Y-m-d');          ?></time> </p>-->
                                </div>
                            </article>								
                        </div>			                    
                        <?php
                    }
                }
            } else {
                ?>
                <div class="col-md-12 col-sm-12 text-center">
                    No Video Uploaded							
                </div>
                <?php
            }
            ?>

        </div>		

    </div>
</div>
</div>
<!--end about  area -->	
<script>
    function loadGallery(val) {
        window.location.href = "<?php echo URL ?>video-gallery/" + val;
    }
</script>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span id="sVideoTitle"></span>
                <button type="button" class="close" id="stopvideo" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p><iframe src="" id="video" allowfullscreen="" height="400" frameborder="0" width="100%"></iframe></p>
            </div>
        </div>

    </div>
</div>


<script>
    $(document).ready(function () {
        $('.play-video').on('click', function (ev) {
            var videoURL = $(this).attr('data-src');
            $('#sVideoTitle').html($(this).attr('data-title'));
//            $("#video")[0].src = videoURL;
            $("#video")[0].src = "https://www.youtube.com/embed/" + videoURL + "?feature=oembed&autoplay=1";
            $("#myModal").modal('show');
            ev.preventDefault();
        });

        $('#stopvideo').on('click', function (ev) {
            $("#video")[0].src = "";
            $("#myModal").modal('hide');
            ev.preventDefault();
        });
    });
</script>	