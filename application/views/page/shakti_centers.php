<!--Start about title  area --> 
<div class="about_area_s">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading_about">
                    <h1 class="page-title">
                        Shakti Centers
                    </h1>					
                </div>
            </div>
        </div>
    </div>
</div>
<!--Start about  area --> 
<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo URL; ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li>Shakti Centers</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->	

<!--start courses  area -->
<div class="single_courcse">
    <div class="container">
        <div class="row">
            <?php
            $i = 0;
            $j = 0;
            if ($result && count($result) > 0) {
                foreach ($result as $aData) {
                    
                    $i++;
                    $j++;
                    ?>
                    <div class="col-md-4 col-sm-4">
                        <div class="single_cos_item">
                            <div class="courses_area" style="background-image:none;">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12">
                                        <div class="shakti_centres_box<?php echo $i; ?>">
                                            <div class="shakti_centres_heading text-center">
                                                <h2><?php echo isset($aData['title']) && !empty($aData['title']) ? $aData['title'] : ''; ?></h2>
                                                <h3><?php echo isset($aData['subtitle']) && !empty($aData['subtitle']) ? $aData['subtitle'] : ''; ?></h3>
                                                <p><?php echo isset($aData['location']) && !empty($aData['location']) ? $aData['location'] : ''; ?></p>
                                            </div>
                                            <?php echo isset($aData['image']) && !empty($aData['image']) ? '<img src="' . UP_URL . 'shakticenters/' . $aData['image'] . '" class="">' : ''; ?>
                                            <div class="mt25 text-justify shakti_centres_text1">
                                                <?php echo isset($aData['description']) && !empty($aData['description']) ? $aData['description'] : ''; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>						
                        </div>
                    </div>
                    <?php
                    if ($i == 2) {
                        $i = 0;
                    }
                    if ($j == 3) {
                        $j = 0;
                        echo '<div class="clearfix"></div>';
                    }
                }
            }
            ?>
            <div class="clearfix"></div>
        </div>
    </div>	
</div>
<!--end courses  area -->