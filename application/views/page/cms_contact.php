<!--Start about title  area --> 
<div class="about_area_s">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading_about">
                    <h1 class="page-title">
                        <?php echo $dtl['heading'] ?>
                    </h1>					
                </div>
            </div>
        </div>
    </div>
</div>
<!--Start about  area --> 

<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo URL ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li><?php echo $dtl['title'] ?></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->

<!-- breadcrumb-area end -->	
<div class="contact_area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title text-center">
                    <p class="lead"><strong>We are always here to hear from you</strong></p>
                </div>
            </div>
        </div>		
        <div class="row">
            <!-- start  blog left -->
            <div class="col-md-4 col-sm-4">
                <div class="contact-address">	
                    <div class="media">
                        <div class="media-left">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Correspondence Address:</h4>
                            <p>
                                <a href="<?php echo sSITE_GOOGLE_MAP_ADDRESS; ?>" target="_blank" style="color:#373737;">
                                    <span class="contact-emailto">
                                        D-607, West Vinod Nagar<br/>
                                        Delhi 110092, India							
                                    </span>
                                </a>
                            </p>
                        </div>
                    </div>					
                    <!--                    <div class="media">
                                            <div class="media-left">
                                                <i class="fa fa-phone"></i>
                                            </div>
                                            <div class="media-body">
                                                <h4 class="media-heading">Phone</h4>
                                                <p>
                                                    <span class="contact-emailto"><?php // echo sSITE_CONTACT_NO;            ?></span>
                                                </p>
                                            </div>
                                        </div>-->
                    <div class="media">
                        <div class="media-left">
                            <i class="fa fa-envelope"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Email</h4>
                            <p>
                                <span class="contact-emailto"><?php echo sSITE_EMAIL_ADDRESS_PRIMARY; ?></span>
                                <span class="contact-emailto"><?php echo sSITE_EMAIL_ADDRESS_SECONDARY; ?></span>
                            </p>
                        </div>
                    </div>					
                </div>				
            </div>
            <div class="col-md-8 col-sm-8">
                <div class="contact_us">
                    <form action="javascript:void(0)" method="post" name="sContactUsForm" id="sContactUsForm">
                        <div class="form-group">



                            <div>
                                <div class="i_box">
                                    <label class="" for="Name">Name <em>*</em></label>							
                                    <input type="text" name="name" id="name" class="required" placeholder="Enter Name" />								
                                </div>						
                            </div>
                        </div>
                        <div class="form-group">



                            <div>
                                <div class="i_box">	
                                    <label class="" for="Email">Email <em>*</em></label>							
                                    <input type="email" name="email" id="email" class="required email" placeholder="Enter Email ID" />							
                                </div>						
                            </div>
                        </div>
                        <div class="form-group">

                            <div>
                                <label class="" for="mes">Message <em>*</em></label>
                                <div class="i_box">									
                                    <textarea name="message" id="message" cols="30" rows="20"  class="required" placeholder="Enter Message"></textarea>							
                                </div>						
                            </div>
                        </div>
                        <div class="form-group">
                            <div>
                                <p class=""><button type="submit" name="sContactUsButton" id="sContactUsButton" class="read_more buttonc">submit</button></p>
                            </div>							
                        </div>
                    </form>
                </div>
            </div>			
        </div>
    </div>
</div>	
<!--end courses  area -->

<div class="map_area">
    <div class="container-fulid">
        <div class="map">
            <!-- Start contact-map -->
            <div class="">
                <div>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3502.088222380283!2d77.30188431468011!3d28.627117882419963!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390ce5aca16007fd%3A0xc8d97432678c871a!2sShaktimultiversity!5e0!3m2!1sen!2sin!4v1528137130494" width="100%" height="550" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
            <!-- End contact-map -->
        </div>							
    </div>	
</div>

<!-- Google Map js -->
<script src="https://maps.googleapis.com/maps/api/js"></script>
<script>
    function initialize() {
        var mapOptions = {
            zoom: 15,
            scrollwheel: false,
            center: new google.maps.LatLng(40.663293, -73.956351)
        };

        var map = new google.maps.Map(document.getElementById('googleMap'),
                mapOptions);


        var marker = new google.maps.Marker({
            position: map.getCenter(),
            animation: google.maps.Animation.BOUNCE,
            icon: 'theme/front/img/map-marker.png',
            map: map
        });

    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>

<script>
    $(document).ready(function () {
        $("#sMessageHomeVideo").click(function () {
            $("#sMessageHomeVideoDialog").modal('show');
        });

        $('#sContactUsButton').click(function () {
            var sName = $('#name').val();
            var sEmailID = $('#email').val();
            var sMessage = $('#message').val();
            if (sName == '') {
                $("#sMessageDialogTitle").html('WARNING');
                $("#sMessageDialogContent").html('Please provide name');
                $("#sMessageDialog").modal('show');
            } else if (sEmailID == '') {
                $("#sMessageDialogTitle").html('WARNING');
                $("#sMessageDialogContent").html('Please provide email');
                $("#sMessageDialog").modal('show');
            } else if (sEmailID != '' && !validateEmail(sEmailID)) {
                $("#sMessageDialogTitle").html('WARNING');
                $("#sMessageDialogContent").html('Please provide valid email');
                $("#sMessageDialog").modal('show');
            } else if (sMessage == '') {
                $("#sMessageDialogTitle").html('WARNING');
                $("#sMessageDialogContent").html('Please provide your message');
                $("#sMessageDialog").modal('show');
            } else {
                $.ajax({
                    type: "POST",
                    url: "page/contactus",
                    data: $('#sContactUsForm').serialize(),
                    cache: false,
                    success: function (data) {
                        var data = jQuery.parseJSON(data);
//                        if (data.status == true) {
//                            $('#sSubscribeNewsletter')[0].reset();
//                        }
                        $("#sMessageDialogTitle").html(data.title);
                        $("#sMessageDialogIcon").html(data.icon);
                        $("#sMessageDialogContent").html(data.message);
                        $("#sMessageDialog").modal('show');
                        $('#sContactUsForm')[0].reset();

                    }
                });
            }
        });
    });

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
</script>