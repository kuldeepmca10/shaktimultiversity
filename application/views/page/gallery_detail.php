<style>

    #thumbGrid{
        width: 980px;
        margin: auto;

    }

    label{
        color: white;
    }


    #customize {
        display: inline-block;
        margin-top: -30px;
        position: absolute;
        color: white
    }


    #customize input, #customize select {
        font-size: 17px;
        margin: 3px;
        padding: 4px;
        border: 1px solid rgba(255, 255, 255, 0.5);
        border-radius: 0;
        color:#fff;
        background-color: rgba(0, 0, 0, 0.2)
    }

    #customize select {
        margin: 0;
        background-image: url(css/thumbgrid-font/font/downArrow.svg);
        background-repeat: no-repeat;
        background-position: right center;
        padding-right: 25px;
        outline: none;
        overflow: hidden;
        text-indent: 0.01px;
        text-overflow: '';
        -webkit-appearance: none;
        -moz-appearance: none;
        -ms-appearance: none;
        -o-appearance: none;
        appearance: none;
        vertical-align: middle;

    }

    #customize label {
        font-size: 16px;
        margin: 3px 0;
        padding: 3px 0;
        width: 150px;
        display: inline-block;
    }
</style>
<!--gallery-->		
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="<?php echo URL; ?>theme/front/js/jquery.mb.browser.min.js"></script>
<script src="<?php echo URL; ?>theme/front/js/jquery.mb.CSSAnimate.min.js"></script>
<script src="<?php echo URL; ?>theme/front/js/jquery.mb.thumbGrid.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo URL; ?>theme/front/css/thumbGrid.css"/>
<!--gallery-->

<!--Start about title  area --> 
<div class="about_area_s gallery_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading_about">
                    <h1 class="page-title">
                        Event Gallery
                    </h1>					
                </div>
            </div>
        </div>
    </div>
</div>
<!--Start about  area --> 


<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo URL; ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li><a href="<?php echo URL; ?>image-gallery">Gallery</a> <i class="fa fa-angle-right"></i></li>							
                        <li>Event Gallery</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->		


<!--Start about  area --> 
<div class="about_area">	
    <div class="container">
        <?php if (isset($gallery)) { ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="title">
                        <h3 class="module-title about_titlea text-center">
                            <span><?php echo isset($gallery['title']) && !empty($gallery['title']) ? $gallery['title'] : '' ?></span> 
                            <?php echo isset($gallery['created']) && !empty($gallery['created']) ? date('jS F Y', strtotime($gallery['created'])) : '' ?>
                        </h3>
                    </div>
                </div>
            </div>	

            <div class="row">
                

                <div class="col-md-12">
                    <div id="thumbGrid" data-thumbgrid="true" data-effect="scaleIn" data-delay="60" data-timing="800" data-pagination="12" data-galleryeffectnext="scaleIn"data-galleryeffectprev="scaleOut" class="img100">
                       
                        <?php
                        if (isset($gallery_images) && !empty($gallery_images)) {
                            foreach ($gallery_images as $aGalleryData) {
                                $sSMImagePath = isset($aGalleryData['image']) && !empty($aGalleryData['image']) ? UP_URL . "gal-sm/" . $aGalleryData['image'] : '';
                                $sLGImagePath = isset($aGalleryData['image']) && !empty($aGalleryData['image']) ? UP_URL . "gal-sm/" . $aGalleryData['image'] : '';
                                $sTitle = isset($aGalleryData['title']) && !empty($aGalleryData['title']) ? $aGalleryData['title'] : '';

                                echo '<img src="' . $sSMImagePath . '" alt="' . $sTitle . '" data-highres="' . $sLGImagePath . '" data-caption="' . $sTitle . '"/>';
                            }
                        }
                        ?>
                       
                    </div>

                </div>


            </div>
        <?php } else {
            ?>
            <div class="row">
                <div class="col-md-12 text-center">No Gallery Uploaded</div>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<!--end about  area -->	
<script>

    jQuery(function() {
        jQuery("[data-thumbgrid]").thumbGrid();

        //    customize
        jQuery("#effect").on("change", function() {
            var x = $(this).val();
            jQuery("#thumbGrid").data("effect", x);
        });

        jQuery("#delay").on("change", function() {
            var x = parseFloat($(this).val());
            jQuery("#thumbGrid").data("delay", x);
        });

        jQuery("#timing").on("change", function() {
            var x = parseFloat($(this).val());
            jQuery("#thumbGrid").data("timing", x);
        });
    });

</script> 