<div class="faq_area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="title">
                    <h3 class="module-title">
                        <span>FAQs</span>
                    </h3>						
                </div>
            </div>
        </div>
        
        <?php foreach($cats as $cat):?>
            <div class="row">
            	<?php foreach($cat as $c):?>
                    <div class="col-lg-6 col-md-6 col-sm-6">						
                        <div class="panel-group" id="accordion">
                            <div class="faqHeader"><i class="<?php echo $c['icon_class']?>" aria-hidden="true"></i> <?php echo $c['title']?></div>
                            <?php foreach($c['faq'] as $i=>$r):?>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#acco<?php echo $r['id']?>"><?php echo $r['title']?></a>
                                        </h4>
                                    </div>
                                    <div id="acco<?php echo $r['id']?>" class="panel-collapse collapse <?php echo $i==0?'in':''?>">
                                        <div class="panel-body">
                                            <?php echo nl2br($r['description'])?>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                <?php endforeach;?>		
            </div>
        <?php endforeach;?>
    </div>
</div>