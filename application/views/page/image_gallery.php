
<!--Start about title  area --> 
<div class="about_area_s gallery_bg">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading_about">
                    <h1 class="page-title">
                        Gallery
                    </h1>					
                </div>
            </div>
        </div>
    </div>
</div>
<!--Start about  area --> 


<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo URL ?>">Home</a> <i class="fa fa-angle-right"></i></li>							
                        <li><a href="<?php echo URL ?>media-gallery">Media Gallery</a> <i class="fa fa-angle-right"></i></li>							
                        <li>Photo Gallery</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->		


<!--Start about  area --> 
<div class="about_area">


    <div class="container">	
        <div class="row">
            <div class="col-md-8 col-xs-12  photo_gallery"><h1>Photo Gallery</h1></div>
            <div class="col-md-4 col-xs-12 text-center pull-right">
                <div style="background: #fff200;padding:10px 10px;margin-bottom: 20px;">
                    <?php
                    if (isset($cats) && !empty($cats)) {
                        $sSelect = '<select class="selectpicker" onchange="loadGallery(this.value)" style="width:100%;">';
                        $sSelect .= ' <option value="">All Event</option>';
                        foreach ($cats as $iKey => $sValue) {
                            $sSelected = '';
                            if (isset($category_slug) && !empty($category_slug) && $category_slug == $iKey) {
                                $sSelected = 'Selected="selected"';
                            }
                            $sSelect .= ' <option value="' . $iKey . '" ' . $sSelected . '>' . $sValue . '</option>';
                        }
                        $sSelect .= '</select>';
                    }
                    echo $sSelect;
                    ?>
                </div>
            </div>
        </div>	


        <div class="row">
            <?php
            if (isset($result) && !empty($result)) {
                foreach ($result as $aGalleryData) {
                    ?>
                    <!--single gellary  item --> 
                    <div class="col-md-3 col-sm-4">
                        <div class="single_gellary_item">			
                            <div class="gellary_thumb">
                                <p class="img-height"><a href="<?php echo URL . "gallery-detail/" . $aGalleryData['slug']; ?>">
                                        <?php $sImagePath = isset($aGalleryData['image']) && !empty($aGalleryData['image']) ? UP_URL . "gal-sm/" . $aGalleryData['image'] : ''; ?>
                                        <img src="<?php echo $sImagePath; ?>" alt="" class="width100" />
                                    </a></p>	
                                <div class="gallery_text">
                                    <h2>
                                        <?php echo isset($aGalleryData) && !empty($aGalleryData) ? $aGalleryData['title'] : ''; ?>
                                    </h2>							
                                    <div class="gallery_icon text-center">
                                        <i class="fa fa-calendar" aria-hidden="true"></i> <?php echo get_date($aGalleryData['created']); ?>&nbsp;&nbsp;|&nbsp;&nbsp; 
                                        <a href="<?php echo URL . "gallery-detail/" . $aGalleryData['slug']; ?>"><i class="fa fa-eye" aria-hidden="true"></i> View Gallery</a>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
            } else {
                echo '<div class="col-md-12 col-sm-12 text-center">No Image Uploaded</div>';
            }
            ?>
        </div>
    </div>
</div>
<!--end about  area -->	
<script>
    function loadGallery(val) {
        window.location.href = "<?php echo URL ?>image-gallery/" + val;
    }
</script>