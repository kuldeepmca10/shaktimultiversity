<!-- Slider -->
<div class="slider_area">
    <div class="container-fluid">
        <div class="row">
            <div class="tab-content">
                <?php foreach ($sliders as $r): ?>
                    <!--single tab slide item-->
                    <div class="tab-pane active" id="a<?php echo $r['id'] ?>">
                        <div class="col-md-7 col-sm-7 col-xs-12 pd0">
                            <div class="slide_thumb">
                                <a href="<?php echo isset($r['link']) && !empty($r['link']) && $r['link'] != '#' ? $r['link'] : 'javascript::;'; ?>"><img src="<?php echo UP_URL . 'slider/' . $r['image'] ?>" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12 pd0">
                            <div class="slide_text">
                                <h2 class="slide_title"><?php echo $r['title'] ?></a></h2>
                                <p class="slide_ptext"><?php echo $r['subtitle'] ?></a></p>
                            </div>
                        </div>
                    </div>		
                    <!--end single tab slide item-->
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
<!--/ -->


<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <form name="sCurrencyForm" id="sCurrencyForm" action="" method="post">
                        <ul>
                            <li><a href="<?php echo URL ?>">Home</a> <i class="fa fa-angle-right"></i></li>
                            <li><a href="<?php echo URL . 'courses' ?>">Courses</a> <i class="fa fa-angle-right"></i></li>							
                            <li><?php echo $dtl['title'] ?></li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->

<div class="single_courcse">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-6">
                <div class="single_cos_item">
                    <div class="courses_area" style="background-image:none">
                        <div class="row">
                            <div class="col-md-5 col-xs-12"><img src="<?php echo UP_URL . 'course-lg/' . $dtl['image'] ?>" class="course_details_img"></div>
                            <div class="col-md-7 col-xs-12">
                                <h1><?php echo $dtl['title'] ?></h1>
                                <p><?php echo $dtl['short_description'] ?></p><div class="row">
                                    <div class="course_details_price  mobile-margin-10">
                                        <div class="col-md-12 text-center">
                                            <div class="col-md-6 col-xs-12">
                                                <a href="javascript::;"  onclick="fnServiceEnquery('<?php echo $dtl['id']; ?>', '<?php echo $dtl['slug']; ?>')" class="course_details_enroll">Request Now</a>
                                            </div>
                                            <div class="col-md-6 col-xs-12  mobile-margin-10">
                                                <a href="courses" class="course_details_enroll">Browse Courses</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="row">

                        <!--start course single  item -->
                        <div class="col-md-12 course_details">
                            <?php if ($dtl['description']): ?>
                                <div class="b_title mt25"><h3> Program Description</h3></div>
                                <?php echo $dtl['description'] ?>
                            <?php endif; ?>

                            <?php if ($dtl['caricullaum']): ?>
                                <div class="b_title"><h3> Program Curriculum</h3></div>
                                <?php echo $dtl['caricullaum'] ?>
                            <?php endif; ?>

                            <?php if ($dtl['eligibility_creteria']): ?>
                                <div class="b_title"><h3> Eligibility Criteria</h3></div>
                                <?php echo $dtl['eligibility_creteria'] ?>
                            <?php endif; ?>

                            <?php if ($dtl['benefit']): ?>
                                <div class="b_title"><h3> Program Benefits</h3></div>
                                <?php echo $dtl['benefit'] ?>
                            <?php endif; ?>

                            <?php if ($dtl['faq']): ?>
                                <div class="b_title"><h3> FAQ's</h3></div>
                                <?php echo $dtl['faq'] ?>
                            <?php endif; ?>

                            <?php if ($dtl['terms']): ?>
                                <div class="b_title"><h3> Terms &amp; Conditions</h3></div>
                                <?php echo $dtl['terms'] ?>
                            <?php endif; ?>

                            <div class="b_title"><h3> How To Apply</h3></div>
                            <div id="how-to-apply_new">
                                <div class="col-md-12 how-to-apply wow bounceInUp animated animated" style="visibility: visible; animation-name: bounceInUp;">
                                    <div class="col-md-1 col-xs-12 text-center">
                                        <i class="glyphicon glyphicon-list-alt circle_new circle1 white"></i>
                                    </div>
                                    <div class="col-md-3 col-xs-12 pd15">
                                        Select Course & fill in The Registration Form
                                    </div>
                                    <div class="col-md-1 col-xs-12 text-center">
                                        <i class="glyphicon glyphicon-credit-card circle_new circle2 white"></i>
                                    </div>
                                    <div class="col-md-3 col-xs-12 pd15">
                                        Confirm Batch details and proceed to payment
                                    </div>
                                    <div class="col-md-1 col-xs-12 text-center">
                                        <i class="glyphicon glyphicon-time circle_new circle3 white"></i>
                                    </div>
                                    <div class="col-md-3 col-xs-12 pd15">
                                        Get course confirmation within 24 hours				
                                    </div>
                                </div>

                                <?php echo $dtl['how_to_apply'] ?>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="text-center">
                                        <a href="courses" class="bmore">BROWSE COURSES</a>
                                    </div>
                                </div>
                            </div>
                            <!-- end container -->
                        </div>			

                    </div>
                </div>						
            </div>
        </div>

        <!-- Latest Updates -->
        <div class="col-md-4 col-sm-6">
            <?php $this->load->view("elements/rightbar", array('pg' => 'course_dtl')); ?>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
</div>

<script src="assets/front/js/enroll.js?<?php echo VERSION ?>"></script>
<style>
    .width_800{
        /*width: 800px !important;*/
    }
    .modal-title {
        margin: 15px 0px !important;
        line-height: 1.42857143;
    }
</style>