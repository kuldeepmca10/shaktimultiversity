<h2><strong>Returns &amp; Refund</strong></h2>
<p><b>WE DO NOT REFUND COURSE FEE ONCE PAID.</b></p>
<ol type="1">
<li>Courses once purchased can not be asked to be dropped or changed.</li>
<li>Please make sure to ascertain your eligible for the course you intend to purchase.</li>
<li>We recommend you go through the 'Eligibility' section in the Course Detail page.</li>
<li>In case it is not clear to you or you still have any doubts about your eligibility for a particular course please feel free to write to us at<span>&nbsp;</span><a href="mailto:info@shaktimultiversity.com" target="_blank" rel="noopener noreferrer">info@shaktimultiversity.com</a><span>&nbsp;</span>or <a href="mailto:shakti.multiversity@gmail.com" target="_blank" rel="noopener noreferrer">shakti.multiversity@gmail.com</a></li>
</ol>
<p></p>
<p><b>UNDER ANY CIRCUMSTANCES THE SHAKTI MULTIVERSITY DOES NOT ENTERTAIN ANY REFUND REQUEST FOR THE COURSE FEE ONCE PAID.</b><br /><b></b></p>
<p><strong>About Books Replacement policy</strong></p>
<p><strong>NO RETURN OR REFUND IS GIVEN FOR PURCHASED BOOKS.</strong></p>
<p>Except under the following circumstances where we send you replacements.</p>
<ol type="1">
<li>The wrong book was shipped to you.</li>
<li>The book you receive is misprinted or genuinely has been damaged in the transit. In this case a mail should reach to us<span>&nbsp;</span><span class="aBn" data-term="goog_442686845" tabindex="0"><span class="aQJ">within 24 hours</span></span><span>&nbsp;</span>of delivery with a photograph of the book capturing misprinting/damage.</li>
<li>You detect a damaged package right at the time of delivery and return it immediately.</li>
</ol>
<p><b>About Products REFUND &amp; Replacement Policy</b><br /><b></b></p>
<p>The shakti multiversity will provide replacements in the following circumstances.</p>
<p>Genuine defect or damage in the product received &amp; reported within 24 hours of delivery with photograph.</p>
<p>Package damaged in transit (reported within 24 hrs with photograph)</p>
<p>In case wrong item has been shipped to you. (Reported within 24 hours)</p>
<p>In case of wrong size (in case of clothes/robes etc.) dispatched to you. (To be reported within 24 hrs)</p>
<p><u>ONLY in case replacement product is not available, we shall issue refund of your payment.</u></p>
<p>However, no replacement will be given in the following cases:</p>
<p>In case of minor design and color variations. With handmade/handloom products there will always be some workmanship variations between the products.</p>
<p>Products returned in a used or damaged condition. We treat the product flawless unless reported&nbsp;<span class="aBn" data-term="goog_442686848" tabindex="0"><span class="aQJ">within 24 hours</span></span>&nbsp;with picture of damage.</p>
<p>If you change your mind after purchasing or upon receipt of the product.</p>
<p>If the product is returned without a written (email) confirmation from The Shakti Multiversity.</p>
<p>If the replacement request doesn't reach us with 24 hours either through email as mentioned below</p>
<p><strong>What to do in case the package is damaged?</strong><br /><strong></strong></p>
<p>Incase the package appears damaged, crushed, wet or leaked from the outside please do not accept the package from the delivery person. It is probably damage-in-transit and should be not accepted from the courier company. PLEASE DO NOT ACCEPT DAMAGED PACKAGE FROM COURIER.</p>
<p>However, if you discover the damage once you open the parcel, please report the damage to us within 24 hours of the delivery of the product through email (<a href="mailto:info@shaktimultiversity.com" target="_blank" rel="noopener noreferrer">info@shaktimultiversity.com</a>) describing the damage along with the picture as mentioned below.</p>
<p><strong>What is the replacement process?</strong><br /><strong></strong></p>
<p><strong>Step 1</strong>: All eligible replacement requests need to be notified to us by email to <a href="mailto:info@shaktimultiversity.com" target="_blank" rel="noopener noreferrer">info@shaktimultiversity.com</a> within 48 hours of the delivery of the product. Please mention the following information clearly in the email:</p>
<ol type="1">
<li>Shakti Multiversity Order Number</li>
<li>Your email ID and phone number registered with us</li>
<li>Name of the damaged product</li>
<li>Description of the issue</li>
<li>Clear photo of the package and the damaged products</li>
</ol>
<p><strong>Step 2:</strong>&nbsp;We will review your replacement request and confirm the same on email</p>
<p><strong>Step 3:</strong>&nbsp;After getting our email confirmation, please ensure the damaged product reaches back to us within 7 days of confirmation.</p>
<p><strong>Step 4:</strong>&nbsp;After the receipt of your parcel, we will verify the products and authorize it for replacement within 3-4 days.</p>
<p><strong>Step 5:</strong>&nbsp;Once approved, we will ship replacement product. You can expect&nbsp; the delivery within 4-10 days from dispatch.</p>
<p>However, in case the replacement product is not available in stock, we will issue the refund to the same account used for placing order. This process will take 7-10 working days.</p>
<p><strong>Do you arrange for reverse pickups?</strong></p>
<p>We do not arrange reverse pickups. You can choose a courier of your choice to send the parcel back to us. The courier cost of return parcel is to be borne by you.</p>
<p><strong>Where do I mail the returns?</strong></p>
<p>The Shakti Multiversity (Shop Division) D-607, Ground Floor, West Vinod Nagar, Delhi - 100092</p>
<p>Mention your order number and contact number&nbsp;</p>
<p>The Shakti Multiversity will not be able to fulfill Replacement requests for items that go missing in reverse transit.</p>
<p><strong>When will I receive a replacement?</strong></p>
<p>Replacement product will be shipped within 4-7 days of the receipt of the original item, subject to the following conditions:</p>
<ol type="1">
<li >The product(s) should be unused, unsoiled and unwashed.</li>
<li >The packaging and products (clothes) should have the unremoved original tags in place.</li>
</ol>
<p>Any returned item received by us that does not meet the above mentioned conditions would not be accepted. No replacement or Refund will be provided for such products.</p>
<p><strong>Can I choose a different product in exchange?</strong></p>
<p>No. You can not choose a different product in exchange. The same product and size is allowed.</p>
<p><strong>How much time does it take to issue refund?</strong></p>
<p>The refund is given only in case the replacement is not available. The refund process will take 7-10 working days from the receipt of the returned goods.</p>
<p><strong>Can I modify or cancel my order?</strong></p>
<p>You can not modify or cancel an order once placed successfully on the website.</p>