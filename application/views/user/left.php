<?php
$sMethodName = $this->uri->segment(2);
if (isset($aUserData['profile_photo']) && !empty($aUserData['profile_photo'])) {
    $sUserImage = "uploads/user/" . $aUserData['profile_photo'];
} else {
    $sUserImage = "theme/front/img/tim_80x80.png";
}
?>
<div class="sidebar" data-color="purple" data-image="theme/front/img/sidebar-1.jpg">
    <div class="user-panel">  
        <?php if (isset($aUserData['isLifetimeMember']) && !empty($aUserData['isLifetimeMember']) && $aUserData['isLifetimeMember'] == 1) { ?>
            <div class="text-center" style="color:red;cursor: pointer"><img src="theme/front/img/member.jpeg" title="Lifetime Member" alt="" /></div>
        <?php } ?>
        <div class="image">
            <!--<img src="theme/front/img/tim_80x80.png" class="img-circle" alt="User Image">-->
            <img width="100" src="<?php echo $sUserImage; ?>" class="img-circle" alt="User Image">
        </div>
        <div class="info">
            <p><?php echo $aUserData['name']; ?></p>
            <h5><?php echo $aUserData['email']; ?></h5>
            <!--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>-->
        </div>
    </div>

    <div class="sidebar-wrapper">
        <ul class="nav">
            <li <?php echo (isset($sMethodName) && ($sMethodName == 'dashboard' || $sMethodName == 'course-information')) ? 'class="active"' : ''; ?>>
                <a href="<?php echo URL ?>user/dashboard">
                    <i class="material-icons">content_paste</i>
                    <p>My Dashboard</p>
                </a>
            </li>
            <li <?php echo (isset($sMethodName) && $sMethodName == 'update-profile') ? 'class="active"' : ''; ?> >
                <a href="<?php echo URL ?>user/update-profile">
                    <i class="material-icons">dashboard</i>
                    <p>My Profile</p>
                </a>
            </li>
            <li <?php echo (isset($sMethodName) && ($sMethodName == 'course-enrollment' || $sMethodName == 'enrollment-detail')) ? 'class="active"' : ''; ?>>
                <a href="<?php echo URL ?>user/course-enrollment">
                    <i class="material-icons">person</i>
                    <p>My Courses</p>
                </a>
            </li>
            <li <?php echo (isset($sMethodName) && ($sMethodName == 'mark-your-attendance' )) ? 'class="active"' : ''; ?>>
                <a href="<?php echo URL ?>user/mark-your-attendance">
                    <i class="material-icons">how_to_reg</i>
                    <p>Mark Your Attendance</p>
                </a>
            </li>
            <!--            <li>
                            <a href="javascript::;">
                                <i class="material-icons">content_paste</i>
                                <p>My Orders</p>
                            </a>
                        </li>-->
            <li  <?php echo (isset($sMethodName) && $sMethodName == 'change-password') ? 'class="active"' : ''; ?>>
                <a href="<?php echo URL ?>user/change-password">
                    <i class="material-icons">library_books</i>
                    <p>Change Password</p>
                </a>
            </li>

            <li  <?php echo (isset($sMethodName) && $sMethodName == 'logout') ? 'class="active"' : ''; ?>>
                <a href="<?php echo URL ?>user/logout">
                    <i class="material-icons">transfer_within_a_station</i>
                    <p>Logout</p>
                </a>
            </li>

        </ul>
    </div>

</div>