<script>
    $(document).ready(function () {
        $('#sEnrollmentList').DataTable();
    });
</script>
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="assets/front/js/user.js?<?php echo VERSION ?>"></script>
<div class="dashboard">
    <div class="wrapper">
        <?php $this->load->view("user/left"); ?>
        <div class="main-panel ps-container ps-theme-default ps-active-y">

            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div>
            </nav>            

            <div class="content margin-top-20">

                <div class="session-message">
                    <?php
                    if (!empty($this->session->flashdata('item'))) {
                        $message = $this->session->flashdata('item');
                        ?>
                        <div class="session-message-dtl <?php echo $message['class']; ?>"><?php echo $message['message']; ?></div>
                        <?php
                    }
                    ?>
                </div>               

                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="card card-nav-tabs">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">Dashboard</h4>
                                </div>
                                <div class="card-content">
                                    <div class="tab-content">
                                        <div class="tab-pane active table-responsive" id="profile">
                                            <table class=" table-striped table-bordered table-hover table-condensed" id="sEnrollmentList">
                                                <thead class="text-primary">
                                                    <tr>
                                                        <th>S No.</th>
                                                        <th>Course Name</th>
                                                        <th>Enrollment No</th>
                                                        <th>Batch</th>
                                                        <th>Batch Start date</th>
                                                        <th>Enrollment Date</th>
                                                        <th>Payment Status</th>
                                                        <th>Status</th>                                                        
                                                        <th>Action</th>
                                                    </tr>
                                                </thead> 
                                                <?php
                                                $i = 0;

                                                if (isset($aEnrollmentList) && !empty($aEnrollmentList)) {
                                                    echo ' <tbody>';
                                                    foreach ($aEnrollmentList as $aEnrollment) {
                                                        $i++;
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $i; ?></td>
                                                            <td><?php echo $aEnrollment['courseTitle']; ?></td>
                                                            <td><?php echo $aEnrollment['enrollment_number']; ?></td>
                                                            <td><?php echo $aEnrollment['title']; ?></td>
                                                            <td>
                                                                <?php
                                                                if (isset($aEnrollment['batch_date']) && !empty($aEnrollment['batch_date'])) {
                                                                    echo isset($aEnrollment['batch_date']) && !empty($aEnrollment['batch_date']) ? get_date($aEnrollment['batch_date'], false, 'd M Y') : 'N/A';
//                                                                    echo isset($aEnrollment['batch_time']) && !empty($aEnrollment['batch_time']) ? get_date($aEnrollment['batch_time'], false, 'h:i A') : 'N/A';
                                                                } else {
                                                                    echo 'N/A';
                                                                }
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                echo isset($aEnrollment['created']) && !empty($aEnrollment['created']) ? get_date($aEnrollment['created'], false, 'd M Y') : 'N/A';
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                echo isset($aEnrollment['payment_status']) && !empty($aEnrollment['payment_status']) ? ucwords(strtolower($aEnrollment['payment_status'])) : 'N/A';
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php
                                                                echo isset($aEnrollment['approval_status']) && !empty($aEnrollment['approval_status']) ? ucwords(strtolower($aEnrollment['approval_status'])) : 'N/A';
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <a href="<?php echo URL . 'user/course-information/' . $aEnrollment['id'] ?>" title="View Detail"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                                &nbsp; <a onclick="sendFeedback('<?php echo $aEnrollment['id']; ?>')" title="Feedback/Contact"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
        <!--                                                                <i  title="Unsubscribed" id="unsub-<?php echo $aEnrollment['id']; ?>"  class="fa fa-ban red" aria-hidden="true"></i>
                                                                &nbsp;<a href="javascript::void(0)" onclick="confirmBox('<?php echo $aEnrollment['id']; ?>')" title="Subscribed"><i id="unsub-<?php echo $aEnrollment['id']; ?>"  class="fa fa-ban" aria-hidden="true"></i></a>
                                                                &nbsp;<i title="Pending For Approval" id="unsub-<?php echo $aEnrollment['id']; ?>"  class="fa fa-ban gray" aria-hidden="true"></i>-->
                                                            </td>        
        <!--<td><button type="button" class="btn btn-success">Order Placed</button></td>-->
                                                        </tr>
                                                        <?php
                                                    }
                                                    echo '</tbody>';
                                                }
                                                ?>
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="messages">
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input name="optionsCheckboxes" checked="" type="checkbox"><span class="checkbox-material"><span class="check"></span></span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit
                                                        </td>
                                                        <td class="td-actions text-right">
                                                            <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-simple btn-xs">
                                                                <i class="material-icons">edit</i>
                                                            </button>
                                                            <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                                <i class="material-icons">close</i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input name="optionsCheckboxes" type="checkbox"><span class="checkbox-material"><span class="check"></span></span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>Sign contract for "What are conference organizers afraid of?"</td>
                                                        <td class="td-actions text-right">
                                                            <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-simple btn-xs">
                                                                <i class="material-icons">edit</i>
                                                            </button>
                                                            <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                                <i class="material-icons">close</i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane" id="settings">
                                            <table class="table">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input name="optionsCheckboxes" type="checkbox"><span class="checkbox-material"><span class="check"></span></span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>Lines From Great Russian Literature? Or E-mails From My Boss?</td>
                                                        <td class="td-actions text-right">
                                                            <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-simple btn-xs">
                                                                <i class="material-icons">edit</i>
                                                            </button>
                                                            <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                                <i class="material-icons">close</i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input name="optionsCheckboxes" checked="" type="checkbox"><span class="checkbox-material"><span class="check"></span></span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>Flooded: One year later, assessing what was lost and what was found when a ravaging rain swept through metro Detroit
                                                        </td>
                                                        <td class="td-actions text-right">
                                                            <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-simple btn-xs">
                                                                <i class="material-icons">edit</i>
                                                            </button>
                                                            <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                                <i class="material-icons">close</i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="checkbox">
                                                                <label>
                                                                    <input name="optionsCheckboxes" type="checkbox"><span class="checkbox-material"><span class="check"></span></span>
                                                                </label>
                                                            </div>
                                                        </td>
                                                        <td>Sign contract for "What are conference organizers afraid of?"</td>
                                                        <td class="td-actions text-right">
                                                            <button type="button" rel="tooltip" title="Edit Task" class="btn btn-primary btn-simple btn-xs">
                                                                <i class="material-icons">edit</i>
                                                            </button>
                                                            <button type="button" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-xs">
                                                                <i class="material-icons">close</i>
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="assets/front/js/material.min.js" type="text/javascript"></script>
<script src="assets/front/js/perfect-scrollbar.jquery.min.js"></script>
<script src="assets/front/js/material-dashboard.js?v=1.2.0"></script>
<script src="assets/front/js/user.js?<?php echo VERSION ?>"></script>
