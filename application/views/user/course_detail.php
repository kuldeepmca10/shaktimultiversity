<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul>
                        <li>
                            <a href="<?php echo URL ?>">Home</a><i class="fa fa-angle-right"></i>
                        </li>							
                        <li>Course Enrollment</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->		

<div class="about_area page text-center">
    <div class="container animated">
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12 text-left dashboard-left-panel">
                <?php $this->load->view("user/left"); ?>
            </div>
            <div class="col-md-9 col-sm-12 col-xs-12  text-left dashboard-right-panel">
                <div class="dashboard-page-title">Course Detail</div>
                <div class="table-responsive">
                    <table id="sEnrollmentList" border="0" class="table table-striped table-bordered" >
                        <tbody>
                            <?php
                            $i = 0;

                            echo '<pre>';print_r($aEnrollmentList);echo '</pre>';die;
                            
                            if (isset($aEnrollmentList) && !empty($aEnrollmentList)) {
                                foreach ($aEnrollmentList as $aEnrollment) {
                                    $i++;
                                    ?>
                                    <tr>
                                        <td><?php echo $i; ?></td>
                                        <td><?php echo $aEnrollment['courseTitle']; ?></td>
                                        <td><?php echo $aEnrollment['enrollment_number']; ?></td>
                                        <td>
                                            <?php
                                            if (isset($aEnrollment['batch_date']) && !empty($aEnrollment['batch_date'])) {
                                                echo isset($aEnrollment['batch_date']) && !empty($aEnrollment['batch_date']) ? get_date($aEnrollment['batch_date'], false, 'd M Y') : 'N/A';
                                                echo '&nbsp;';
                                                echo isset($aEnrollment['batch_time']) && !empty($aEnrollment['batch_time']) ? get_date($aEnrollment['batch_time'], false, 'h:i A') : 'N/A';
                                            } else {
                                                echo 'N/A';
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo isset($aEnrollment['created']) && !empty($aEnrollment['created']) ? get_date($aEnrollment['created'], false, 'd M Y') : 'N/A';
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo isset($aEnrollment['payment_status']) && !empty($aEnrollment['payment_status']) ? ucwords(strtolower($aEnrollment['payment_status'])) : 'N/A';
                                            ?>
                                        </td>
                                        <td>
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>		
    </div>		

</div>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function () {
        $('#sEnrollmentList').DataTable();
    });
</script>
<style>
    .table-responsive{
        overflow-x:hidden;
    }
    .dashboard-page-title{
        font-weight: bold;
        font-size: 16px;
        text-align: center;
        padding-bottom: 10px;
    }
    .table thead td{
        font-weight: bold;  
    }
</style>
<!--end about  area -->	