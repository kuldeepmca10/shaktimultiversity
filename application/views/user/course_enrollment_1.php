<div class="dashboard">
    <div class="wrapper">
        <div class="sidebar" data-color="purple" data-image="assets/img/sidebar-1.jpg">
            <?php $this->load->view("user/left"); ?>
            <div class="sidebar-background" style="background-image: url(assets/img/sidebar-1.jpg) "></div>

        </div>
        <div class="main-panel ps-container ps-theme-default ps-active-y" data-ps-id="88a86589-6850-4604-ff31-4d575aabe6d5">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">My Courses</a>
                    </div>

                </div>
            </nav>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-header card-chart">
                                    <div class="ct-chart"><img src="img/1.jpg"></div>
                                </div>
                                <div class="card-content">
                                    <h4 class="title">Power of Five Elements</h4>
                                    <p class="category">
                                        <span class="text-success"><i class="fa fa-long-arrow-up"></i> Teacher: </span> Ma Shakti Devpriya
                                    </p>
                                    <p class="category">
                                        <span class="text-success"><i class="fa fa-long-arrow-up"></i> Duration: </span> 6 Hours 
                                    </p>
                                    <p class="category">
                                        <span class="text-success"><i class="fa fa-long-arrow-up"></i> Batch Start Date: </span> 01 Jul 2017 
                                    </p>									
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons">access_time</i> Every individual has a dominant Element in one’s..
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-header card-chart" data-background-color="orange">
                                    <div class="ct-chart"><img src="img/2.jpg"></div>
                                </div>
                                <div class="card-content">
                                    <h4 class="title">SWAR YOG - BASIC</h4>
                                    <p class="category">
                                        <span class="text-success"><i class="fa fa-long-arrow-up"></i> Teacher: </span> Ma Shakti Devpriya
                                    </p>
                                    <p class="category">
                                        <span class="text-success"><i class="fa fa-long-arrow-up"></i> Duration: </span> 6 Hours 
                                    </p>
                                    <p class="category">
                                        <span class="text-success"><i class="fa fa-long-arrow-up"></i> Batch Start Date: </span> 17 Oct 2017
                                    </p>									
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons">access_time</i> Swar Yog (Basic) is a beginner course that gives y...
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-header card-chart" data-background-color="red">
                                    <div class="ct-chart"><img src="img/3.jpg"></div>
                                </div>
                                <div class="card-content">
                                    <h4 class="title">SWAR YOG - BASIC</h4>
                                    <p class="category">
                                        <span class="text-success"><i class="fa fa-long-arrow-up"></i> Teacher: </span> Ma Shakti Devpriya
                                    </p>
                                    <p class="category">
                                        <span class="text-success"><i class="fa fa-long-arrow-up"></i> Duration: </span> 6 Hours 
                                    </p>
                                    <p class="category">
                                        <span class="text-success"><i class="fa fa-long-arrow-up"></i> Batch Start Date: </span> 01 Jul 2017 
                                    </p>									
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons">access_time</i> Every individual has a dominant Element in one’s..
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-header card-chart" data-background-color="red">
                                    <div class="ct-chart"><img src="img/4.jpg"></div>
                                </div>
                                <div class="card-content">
                                    <h4 class="title">Power of Five Elements</h4>
                                    <p class="category">
                                        <span class="text-success"><i class="fa fa-long-arrow-up"></i> Teacher: </span> Ma Shakti Devpriya
                                    </p>
                                    <p class="category">
                                        <span class="text-success"><i class="fa fa-long-arrow-up"></i> Duration: </span> 6 Hours 
                                    </p>
                                    <p class="category">
                                        <span class="text-success"><i class="fa fa-long-arrow-up"></i> Batch Start Date: </span> 01 Jul 2017 
                                    </p>									
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons">access_time</i> Every individual has a dominant Element in one’s..
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-header card-chart" data-background-color="red">
                                    <div class="ct-chart"><img src="img/2.jpg"></div>
                                </div>
                                <div class="card-content">
                                    <h4 class="title">Power of Five Elements</h4>
                                    <p class="category">
                                        <span class="text-success"><i class="fa fa-long-arrow-up"></i> Teacher: </span> Ma Shakti Devpriya
                                    </p>
                                    <p class="category">
                                        <span class="text-success"><i class="fa fa-long-arrow-up"></i> Duration: </span> 6 Hours 
                                    </p>
                                    <p class="category">
                                        <span class="text-success"><i class="fa fa-long-arrow-up"></i> Batch Start Date: </span> 01 Jul 2017 
                                    </p>									
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons">access_time</i> Every individual has a dominant Element in one’s..
                                    </div>
                                </div>
                            </div>
                        </div>						
                    </div>
                </div>
            </div>


        </div>
    </div>
    <!-- breadcrumb-area start -->
    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="breadcrumb">
                        <ul>
                            <li>
                                <a href="<?php echo URL ?>">Home</a><i class="fa fa-angle-right"></i>
                            </li>							
                            <li>Course Enrollment</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb-area end -->		

    <div class="about_area page text-center">
        <div class="container animated">
            <div class="row">
                <div class="col-md-3 col-sm-12 col-xs-12 text-left dashboard-left-panel">
                    <?php $this->load->view("user/left"); ?>
                </div>
                <div class="col-md-9 col-sm-12 col-xs-12  text-left dashboard-right-panel">
                    <div class="page-title-dashboard">
                        <div class="col-md-12 col-sm-12 col-xs-12 text-center"><h1>Course Enrollment</h1></div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="table-responsive">
                        <table id="sEnrollmentList" border="0" class="table table-striped table-bordered" >
                            <thead>
                                <tr>
                                    <td>S.No</td>
                                    <td>Course</td>
                                    <td>Enroll Number</td>
                                    <td>Class Date & Time</td>
                                    <td>Enroll Date</td>
                                    <td>Payment Status</td>
                                    <td>Attendance</td>
                                    <td>Action</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;

                                if (isset($aEnrollmentList) && !empty($aEnrollmentList)) {
                                    foreach ($aEnrollmentList as $aEnrollment) {
                                        $i++;
                                        ?>
                                        <tr>
                                            <td><?php echo $i; ?></td>
                                            <td><?php echo $aEnrollment['courseTitle']; ?></td>
                                            <td><?php echo $aEnrollment['enrollment_number']; ?></td>
                                            <td>
                                                <?php
                                                if (isset($aEnrollment['batch_date']) && !empty($aEnrollment['batch_date'])) {
                                                    echo isset($aEnrollment['batch_date']) && !empty($aEnrollment['batch_date']) ? get_date($aEnrollment['batch_date'], false, 'd M Y') : 'N/A';
                                                    echo isset($aEnrollment['batch_time']) && !empty($aEnrollment['batch_time']) ? get_date($aEnrollment['batch_time'], false, 'h:i A') : 'N/A';
                                                } else {
                                                    echo 'N/A';
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo isset($aEnrollment['created']) && !empty($aEnrollment['created']) ? get_date($aEnrollment['created'], false, 'd M Y') : 'N/A';
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo isset($aEnrollment['payment_status']) && !empty($aEnrollment['payment_status']) ? ucwords(strtolower($aEnrollment['payment_status'])) : 'N/A';
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                echo isset($aEnrollment['payment_status']) && !empty($aEnrollment['payment_status']) ? ucwords(strtolower($aEnrollment['payment_status'])) : 'N/A';
                                                ?>
                                            </td>
                                            <td class="text-center">
                                                <a href="<?php echo URL . 'user/enrollment-detail/' . $aEnrollment['id'] ?>" title="View Detail"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                &nbsp; <a onclick="sendFeedback('<?php echo $aEnrollment['id']; ?>')" title="Feedback/Contact"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                                                <?php if (isset($aEnrollment['status']) && !empty($aEnrollment['status']) && $aEnrollment['status'] == '2') { ?>
                                                    &nbsp;<i  title="Unsubscribed" id="unsub-<?php echo $aEnrollment['id']; ?>"  class="fa fa-ban red" aria-hidden="true"></i>
                                                <?php } else if (isset($aEnrollment['status']) && !empty($aEnrollment['status']) && $aEnrollment['status'] == '1') { ?>
                                                    &nbsp;<a href="javascript::void(0)" onclick="confirmBox('<?php echo $aEnrollment['id']; ?>')" title="Subscribed"><i id="unsub-<?php echo $aEnrollment['id']; ?>"  class="fa fa-ban" aria-hidden="true"></i></a>
                                                <?php } else { ?>
                                                    &nbsp;<i title="Pending For Approval" id="unsub-<?php echo $aEnrollment['id']; ?>"  class="fa fa-ban gray" aria-hidden="true"></i>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    <tr>
                                    ?>
                                        <td colspan="8" class="text-center">No Enrollment Yet</td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>		
        </div>		

    </div>


    <!-- Modal -->
    <div id="sFeedbackDialog" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4>
                        <div id="sFeedbackDialogIcon"><i class="fa fa-exclamation"></i></div>
                        <span id="sFeedbackDialogTitle">Send Feedback</span>
                    </h4>
                </div>
                <div class="modal-body">
                    <div id="sFeedbackDialogContent">
                        <form name="sFeedbackForm" id="sFeedbackForm" action="" method="post">
                            <div class="col-lg-12 form-group">
                                <div class="col-lg-12 text-left">
                                    <label>Message<span class="required-hint">*</span></label>
                                </div>
                                <div class="col-lg-12">
                                    <textarea name="sFeedbackMessage" id="sFeedbackMessage" class="form-control" required rows="5"></textarea>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <div class="col-lg-4">&nbsp;</div>
                                <div class="col-lg-6">
                                    <input type="hidden" name="sEnrollmentID" id="sEnrollmentID" value="" />
                                    <button type="button" class="btn btn-nerdy login_btn" id="sendFeedbackSubmit">SUBMIT</button>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>

                    <!--<button type="button" class="black-btn popup-ok" data-dismiss="modal" aria-label="Close">OK</button>-->
                </div>
            </div>
        </div>
    </div>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="assets/front/js/user.js?<?php echo VERSION ?>"></script>
    <style>
        .table-responsive{
            overflow-x:hidden;
        }
        .dashboard-page-title{
            font-weight: bold;
            font-size: 16px;
            text-align: center;
            padding-bottom: 10px;
        }
        .table thead td{
            font-weight: bold;  
        }
        .gray{
            color:#ccc !important;
        }
    </style>
    <!--end about  area -->	