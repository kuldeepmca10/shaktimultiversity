
<div class="about_area page text-center">
    <div class="container animated">
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12 text-left dashboard-left-panel">
                <?php $this->load->view("user/left"); ?>
            </div>
            <div class="col-md-9 col-sm-12 col-xs-12  text-left dashboard-right-panel">

                <?php
                if ($this->session->flashdata('item')) {
                    echo $message = $this->session->flashdata('item');
                }
                ?>
                <div class="page-title-dashboard">
                    <div class="col-md-12 col-sm-12 col-xs-12 text-center"><h1>Update Profile</h1></div>
                    <div class="clearfix"></div>
                </div>
                <div class="dashboard-update-profile">
                    <form class="form-horizontal" method="post" role="form" name="sUpdateForm" id="sUpdateForm" enctype="multipart/form-data"  autocomplete="off" novalidate>
                        <div id="form-error" style="position:absolute; top:-69px; z-index:9; left:0; width:100%; "></div>
                        <div class="col-lg-10 form-group">
                            <div class="col-lg-3">
                                <label>Enter Your Name <span class="required-hint">*</span></label>
                            </div>
                            <div class="col-lg-9">
                                <input type="text" id="name" tabindex="1" name="name" class="form-control" required value="<?php echo isset($aUserData['name']) && !empty($aUserData['name']) ? $aUserData['name'] : ''; ?>" />
                            </div>
                        </div>
                        <div class="col-lg-10 form-group">
                            <div class="col-lg-3">
                                <label>Enter Your Email <span class="required-hint">*</span></label>
                            </div>
                            <div class="col-lg-9">
                                <input type="email" id="email" tabindex="1" name="email" class="form-control" required value="<?php echo isset($aUserData['email']) && !empty($aUserData['email']) ? $aUserData['email'] : ''; ?>" />
                            </div>
                        </div>
                        <div class="col-lg-10 form-group">
                            <div class="col-lg-3">
                                <label>Country<span class="required-hint">*</span></label>
                            </div>
                            <div class="col-lg-9">
                                <?php
                                if (isset($aCuntryListWithPhoneCode) && !empty($aCuntryListWithPhoneCode)) {
                                    $sPhoneCodeSelectedBox = '<select name="phone_code"  class="form-control" id="phone_code" >';
                                    foreach ($aCuntryListWithPhoneCode as $aPhoneCodes) {
                                        $sSelected = '';
                                        if ($aPhoneCodes['phonecode'] == $aUserData['phone_code']) {
                                            $sSelected = 'selected="selected"';
                                        }
                                        $sPhoneCodeSelectedBox .='<option value="' . $aPhoneCodes['phonecode'] . '" ' . $sSelected . '>' . $aPhoneCodes['nicename'] . '</option>';
                                    }
                                    $sPhoneCodeSelectedBox .='</select>';
                                    echo $sPhoneCodeSelectedBox;
                                }
                                ?>                                
                            </div>
                        </div>
                        <div class="col-lg-10 form-group">
                            <div class="col-lg-3">
                                <label>Enter Phone No.<span class="required-hint">*</span></label>
                            </div>
                            <div class="col-lg-9">
                                <input type="tel" name="phone" id="phone"  value="<?php echo isset($aUserData['phone']) && !empty($aUserData['phone']) ? $aUserData['phone'] : ''; ?>"  class="form-control" tabindex="4" required maxlength="10">                              
                            </div>
                        </div>

                        <div class="col-lg-10">
                            <div class="col-lg-3">&nbsp;</div>
                            <div class="col-lg-6">
                                <button type="button" class="btn btn-nerdy login_btn" id="sRegisterSubmit">SUBMIT</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>		
    </div>		

</div>

<!--end about  area -->	


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="assets/front/js/user.js?<?php echo VERSION ?>"></script>