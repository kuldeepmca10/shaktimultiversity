<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>My Profile - Shakti Multiversity &#8211; Spreading Tantric Spirituality</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- favicon
        ============================================ -->		
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">

        <!-- Google Fonts
        ============================================ -->		
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link href="http://allfont.net/allfont.css?fonts=montserrat-light" rel="stylesheet" type="text/css" />

        <!-- style CSS
        ============================================ -->
        <link rel="stylesheet" href="style.css">

        <!-- modernizr JS
        ============================================ -->		
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>

        <!--dashboard-css-->
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>		
        <!-- Bootstrap core CSS     -->
        <link href="dashboard/css/bootstrap.min.css" rel="stylesheet" />
        <!--  Material Dashboard CSS    -->
        <link href="dashboard/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />		
        <!--dashboard-css-->

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <!--start header  area --> 
        <div class="header_area">
            <div class="container">
                <div class="row">
                    <!-- header  logo --> 
                    <div class="col-md-4 col-sm-3 col-xs-12">
                        <div class="logo"><a href="index.html"><img src="img/logo.png" alt="" /></a></div>
                    </div>
                    <!-- end header  logo --> 
                    <div class="col-md-8 col-sm-9 col-xs-12">
                        <div>
                            <div class="form pull-right">
                                <div class="language">
                                    <a href="login.html">LOGIN</a> | <a href="registration.html">REGISTER</a>
                                </div>
                            </div>

                        </div>
                        <div class="phone_address pull-right clear">
                            <p class="no-margin">
                                <small>
                                    <span class="icon-set"><i class="fa fa-phone"></i> +91-7985 928 336</span> 
                                    <span class="icon-set"><i class="fa fa-envelope"></i> info@shaktimultiversity.com</span> 
                                </small>
                            </p>				
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end header  area --> 
        <!--Start nav  area --> 
        <div class="nav_area">
            <div class="container">
                <div class="row">
                    <!--nav item-->
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <!--  nav menu-->
                        <nav class="menu">
                            <ul class="navid pull-left">
                                <li><a href="index.html">Home </a></li>
                                <li><a href="about-us.html">About Us</a></li>							
                                <li><a href="courses.html">Courses <i class="fa fa-angle-down"></i></a>
                                    <ul>
                                        <li><a href="courses.html">Tantra</a></li>
                                        <li><a href="courses.html">Yoga</a></li>
                                        <li><a href="courses.html">Healing</a></li>
                                        <li><a href="courses.html">Ancient Wisdom</a></li>
                                        <li><a href="free-courses.html">Free Courses</a></li>
                                    </ul>							
                                </li>
                                <li><a href="courses-item-1.html">Ashram <i class="fa fa-angle-down"></i></a>
                                    <ul>
                                        <li><a href="shakti-centers.html">Shakti Centers</a></li>
                                        <li><a href="lodge-of-masters.html">Lodge of Masters</a></li>
                                    </ul>							
                                </li>							
                                <li><a href="#">Happenings <i class="fa fa-angle-down"></i></a>
                                    <ul>
                                        <li><a href="event-calendar">Event Calendar</a></li>
                                        <li><a href="notice-board">Notice Board</a></li>
                                        <li><a href="gallery.html">Image Gallery</a></li>
                                        <li><a href="testimonails.html">Testimonails</a></li>
                                        <li><a href="faq.html">FAQ's</a></li>									
                                    </ul>							
                                </li>						
                                <li><a href="shop.html">Book & Products</a></li>
                                <li><a href="blog.html">Blog</a></li>							
                                <li><a href="contact.html">Contact</a></li>
                            </ul>
                        </nav>
                        <!--end  nav menu-->	
                        <div class="search pull-right">
                            <div class="search-box">
                                <input type="text" class="form_control" placeholder="search" />
                                <span class="search-open"><i class="fa fa-search search"></i><i class="fa fa-close hidden close"></i></span>
                            </div>
                        </div>
                    </div>
                    <!--end nav item -->
                </div>	
            </div>

        </div>
        <!--end nav  area -->
        <!--Start mobile menu  area -->
        <div class="mobile_memu_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div class="mobile_memu">
                            <!--  nav menu-->
                            <nav>
                                <ul class="navid">
                                    <li><a href="index.html">Home </a></li>
                                    <li><a href="about-us.html">About Us</a></li>							
                                    <li><a href="courses.html">Courses <i class="fa fa-angle-down"></i></a>
                                        <ul>
                                            <li><a href="courses.html">Tantra</a></li>
                                            <li><a href="courses.html">Yoga</a></li>
                                            <li><a href="courses.html">Healing</a></li>
                                            <li><a href="courses.html">Ancient Wisdom</a></li>
                                            <li><a href="free-courses.html">Free Courses</a></li>
                                        </ul>							
                                    </li>
                                    <li><a href="courses-item-1.html">Ashram <i class="fa fa-angle-down"></i></a>
                                        <ul>
                                            <li><a href="shakti-centers.html">Shakti Centers</a></li>
                                            <li><a href="lodge-of-masters.html">Lodge of Masters</a></li>
                                        </ul>							
                                    </li>							
                                    <li><a href="#">Happenings <i class="fa fa-angle-down"></i></a>
                                        <ul>
                                            <li><a href="event-calendar">Event Calendar</a></li>
                                            <li><a href="notice-board">Notice Board</a></li>
                                            <li><a href="gallery.html">Image Gallery</a></li>
                                            <li><a href="testimonails.html">Testimonails</a></li>
                                            <li><a href="faq.html">FAQ's</a></li>									
                                        </ul>							
                                    </li>						
                                    <li><a href="shop.html">Book & Products</a></li>
                                    <li><a href="blog.html">Blog</a></li>							
                                    <li><a href="contact.html">Contact</a></li>
                                </ul>
                            </nav>
                            <!--end  nav menu-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end mobile menu  area -->	

        <!--body-->
        <div class="dashboard">
            <div class="wrapper">
                <div class="sidebar" data-color="purple" data-image="img/sidebar-1.jpg">
                    <!--
                Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"
        
                Tip 2: you can also add an image using data-image tag
                    -->



                    <div class="user-panel">
                        <div class="image">
                            <img src="img/tim_80x80.png" class="img-circle" alt="User Image">
                        </div>
                        <div class="info">
                            <p>Alexander Pierce</p>
                            <h5>alexanderpierce@gmail.com</h5>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>

                    <div class="sidebar-wrapper">
                        <ul class="nav">
                            <li>
                                <a href="./my-dashboard.html">
                                    <i class="material-icons">content_paste</i>
                                    <p>My Dashboard</p>
                                </a>
                            </li>
                            <li class="active">
                                <a href="./my-profile.html">
                                    <i class="material-icons">dashboard</i>
                                    <p>My Profile</p>
                                </a>
                            </li>
                            <li>
                                <a href="./my-courses.html">
                                    <i class="material-icons">person</i>
                                    <p>My Courses</p>
                                </a>
                            </li>
                            <li>
                                <a href="./my-orders.html">
                                    <i class="material-icons">content_paste</i>
                                    <p>My Orders</p>
                                </a>
                            </li>
                            <li>
                                <a href="./change-password.html">
                                    <i class="material-icons">library_books</i>
                                    <p>Change Password</p>
                                </a>
                            </li>
                            <li>
                                <a href="./notifications.html">
                                    <i class="material-icons text-gray">notifications</i>
                                    <p>Notifications</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="main-panel">
                    <nav class="navbar navbar-transparent navbar-absolute">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="#"> Profile </a>
                            </div>
                            <div class="collapse navbar-collapse">
                                <ul class="nav navbar-nav navbar-right">
                                    <li>
                                        <!--<a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="material-icons">dashboard</i>
                                            <p class="hidden-lg hidden-md">Dashboard</p>
                                        </a>-->
                                    </li>
                                    <li class="dropdown">
                                        <a href="./notifications.html" class="dropdown-toggle">
                                            <i class="material-icons">notifications</i>
                                            <span class="notification">5</span>
                                            <p class="hidden-lg hidden-md">Notifications</p>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="./change-password.html" class="dropdown-toggle">
                                            <i class="material-icons">person</i>
                                            <p class="hidden-lg hidden-md">Change Password</p>
                                        </a>
                                    </li>
                                </ul>
                                <form class="navbar-form navbar-right" role="search">
                                    <div class="form-group  is-empty">
                                        <input type="text" class="form-control" placeholder="Search">
                                        <span class="material-input"></span>
                                    </div>
                                    <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                        <i class="material-icons">search</i>
                                        <div class="ripple-container"></div>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </nav>
                    <div class="content">
                        <div class="container-fluid">
                            <div class="row">

                                <div class="col-md-8">
                                    <div class="card">
                                        <div class="card-header" data-background-color="purple">
                                            <h4 class="title">Complete Profile</h4>
                                            <p class="category">Complete your profile</p>
                                        </div>
                                        <div class="card-content">
                                            <form>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Alternate Email ID</label>
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Alternate Phone Number</label>
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>											
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Skype ID</label>
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Hangout ID</label>
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Other ID</label>
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group label-floating">
                                                            <label class="">Upload ID Proof</label>
                                                            <input type="file" id="myFile" name="Upload ID Proof" value="Upload ID Proof" style="opacity: 100;top: 30px;">
                                                        </div>
                                                    </div>	
                                                </div>
                                                <!--<div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>About Me</label>
                                                            <div class="form-group label-floating">
                                                                <label class="control-label"> Message</label>
                                                                <textarea class="form-control" rows="5"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>-->
                                                <button type="submit" class="btn btn-primary pull-right">Update Profile</button>
                                                <div class="clearfix"></div>
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-12">
                                    <div class="card">
                                        <div class="card-header" data-background-color="orange">
                                            <h4 class="title">User Information</h4>
                                            <p class="category">New user on 15th September, 2016</p>
                                        </div>
                                        <div class="card-content table-responsive">
                                            <table class="table table-hover">
                                                <thead class="text-warning">
                                                <th></th>
                                                <th></th>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <th>ID</th>
                                                        <td>SM0001</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Name</th>
                                                        <td>Alexander Pierce</td>												
                                                    </tr>
                                                    <tr>
                                                        <th>Email</th>
                                                        <td>alexanderpierce@gmail.com</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Phone</th>
                                                        <td>9877665544</td>
                                                    </tr>	
                                                    <tr>
                                                        <th>Address</th>
                                                        <td>House # 10, Block # 110, Mayur Vihar - III</td>
                                                    </tr>	
                                                    <tr>
                                                        <th>State</th>
                                                        <td>--</td>
                                                    </tr>											
                                                    <tr>
                                                        <th>Country</th>
                                                        <td>India</td>
                                                    </tr>											
                                                    <tr>
                                                        <th>Gender</th>
                                                        <td>Male</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Birthday</th>
                                                        <td>--</td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>						

                            </div>


                        </div>
                    </div>

                </div>
            </div>
        </div>	
        <!--body-->

        <!-- Quick Links area -->	
        <section id="quick_links" style="margin-top: 0">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3 quick_links_red text-center">
                        <div class="quick_links">Quick Links</div>
                    </div>
                    <div class="col-md-9 quick_links_white">
                        <ul>
                            <li><a href="/"><i class="fa fa-check-square" aria-hidden="true"></i> Register</a></li>
                            <li><a href="course"><i class="fa fa-book" aria-hidden="true"></i> Free Courses</a></li>
                            <li><a href="event-calendar"><i class="fa fa-calendar" aria-hidden="true"></i> Event Calendar</a></li>
                            <li><a href="faq"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Shop</a></li>
                            <li><a href="login"><i class="fa fa-hand-o-right" aria-hidden="true"></i> Blog</a></li>
                        </ul>				
                    </div>
                </div>
            </div>	
        </section>
        <!-- End Quick Links area -->
        <!-- testimonials -->		
        <section class="testimonial">	
            <div class="container-fluid">

                <div class='row'>
                    <div class='col-md-12' style="color:#282828">
                        <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                            <!-- Bottom Carousel Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
                                <li data-target="#quote-carousel" data-slide-to="1"></li>
                                <li data-target="#quote-carousel" data-slide-to="2"></li>
                            </ol>

                            <!-- Carousel Slides / Quotes -->
                            <div class="carousel-inner">

                                <!-- Quote 1 -->
                                <div class="item active">
                                    <blockquote>
                                        <div class="row">
                                            <div class="col-sm-3 text-center">
                                                <img class="img-circle" src="http://www.reactiongifs.com/r/overbite.gif" style="width: 100px;height:100px;">
                                                <!--<img class="img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/kolage/128.jpg" style="width: 100px;height:100px;">-->
                                            </div>
                                            <div class="col-sm-9">
                                                <p>Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit!</p>
                                                <small>Someone famous</small>
                                            </div>
                                        </div>
                                    </blockquote>
                                </div>
                                <!-- Quote 2 -->
                                <div class="item">
                                    <blockquote>
                                        <div class="row">
                                            <div class="col-sm-3 text-center">
                                                <img class="img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/mijustin/128.jpg" style="width: 100px;height:100px;">
                                            </div>
                                            <div class="col-sm-9">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam auctor nec lacus ut tempor. Mauris.</p>
                                                <small>Someone famous</small>
                                            </div>
                                        </div>
                                    </blockquote>
                                </div>
                                <!-- Quote 3 -->
                                <div class="item">
                                    <blockquote>
                                        <div class="row">
                                            <div class="col-sm-3 text-center">
                                                <img class="img-circle" src="https://s3.amazonaws.com/uifaces/faces/twitter/keizgoesboom/128.jpg" style="width: 100px;height:100px;">
                                            </div>
                                            <div class="col-sm-9">
                                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum elit in arcu blandit, eget pretium nisl accumsan. Sed ultricies commodo tortor, eu pretium mauris.</p>
                                                <small>Someone famous</small>
                                            </div>
                                        </div>
                                    </blockquote>
                                </div>
                            </div>

                            <!-- Carousel Buttons Next/Prev -->
                            <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                            <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
                        </div>                          
                    </div>
                </div>
            </div>
        </section>	
        <!-- End testimonials -->	

        <!-- footer  area -->	
        <div class="footer_area">
            <div class="container">
                <div class="row">

                    <!-- widget area -->
                    <div class="row">
                        <div class="col-md-2 col-sm-6">
                            <!--single widget item -->
                            <div class="single_widget">
                                <div class="widget_content">
                                    <h2>Quick Links</h2>
                                    <ul>
                                        <li><a href="shakti-centers.html">Shakti Centers</a></li>
                                        <li><a href="shop.html">Book & Products</a></li>
                                        <li><a href="faq.html">FAQs</a></li>
                                        <li><a href="terms-conditions.html">Terms & Conditions</a></li>
                                        <li><a href="refund-policy.html">Refund policy</a></li>
                                    </ul>
                                </div>
                            </div>
                            <!--single widget item -->

                        </div>
                        <div class="col-md-2 col-sm-6">
                            <div class="single_widget">
                                <div class="widget_content">
                                    <h2>Happenings</h2>
                                    <ul>
                                        <li><a href="upcoming-programs.html">Upcoming Programs</a></li>
                                        <li><a href="latest-update.html">Notice Board</a></li>
                                        <li><a href="shop.html">Latest Release</a></li>
                                        <li><a href="gallery.html">Image Gallery</a></li>
                                        <li><a href="testimonails.html">Testimonails</a></li>

                                    </ul>
                                </div>
                            </div>					
                        </div>
                        <div class="col-md-4 col-sm-6">
                            <!--single widget item -->

                            <div class="single_widget">
                                <div class="widget_content">
                                    <h2>Subscribe Newsletter</h2>
                                    <p>Get latest updates, news, survays & offers</p>
                                    <div class="inputbox">
                                        <input type="text" placeholder="your email here" name="name"/>
                                        <button type="submit" class="read_more buttons">Subscribe  <i class="fa fa-graduation-cap"></i></button>
                                    </div>
                                </div>
                            </div>					
                            <!--single widget item -->

                        </div>				
                        <div class="col-md-4 col-sm-6">
                            <!--single widget item -->
                            <div class="single_widget">
                                <div class="widget_content">
                                    <h2>Join Our Community</h2>
                                    <ul>
                                        <li><a href="#">Gyan- Murli Bhawan, 101 BHS, Bharadhwajpuram,</a></li>
                                        <li><a href="#">Prayag Raj, Allahabad- 211006, India</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="single_widget">
                                <div class="widget_content">
                                    <p><strong>Call:</strong> +91 7985 928 336</p>
                                    <p><strong>Email:</strong> info@shaktimultiversity.com shakti.multiversity@gmail.com</p>
                                </div>
                            </div>
                            <!--single widget item -->
                        </div>

                    </div>
                    <!-- end widget area -->
                </div>
            </div>
        </div>	
        <!-- end footer  area -->

        <!-- footer bottom area -->
        <div class="footer_bottom_area">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12">

                        <div class="footer_text">
                            <div class="col-sm-9 col-md-9 col-lg-9" style="float:left">
                                <p>
                                    Copyright © 2017 Shaktimultiversity. All Rights Reserved. 
                                </p>
                            </div>
                            <div class="col-sm-3 col-md-3 col-lg-3" style="float:right;">	
                                <p>
                                    <a href="#"><i class="fa fa-facebook-square font25" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-google-plus-square font25" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-twitter-square font25" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-linkedin-square font25" aria-hidden="true"></i></a>
                                </p>
                            </div>		
                        </div>
                    </div>			
                </div>
            </div>	
        </div>	



        <!-- end footer  area -->

        <!--dashboard-js-->
        <!--   Core JS Files   -->
        <script src="dashboard/js/jquery-3.2.1.min.js" type="text/javascript"></script>
        <script src="dashboard/js/material.min.js" type="text/javascript"></script>
        <!--  Charts Plugin -->
        <script src="dashboard/js/chartist.min.js"></script>
        <!--  Dynamic Elements plugin -->
        <script src="dashboard/js/arrive.min.js"></script>
        <!--  PerfectScrollbar Library -->
        <script src="dashboard/js/perfect-scrollbar.jquery.min.js"></script>
        <!--  Notifications Plugin    -->
        <script src="dashboard/js/bootstrap-notify.js"></script>
        <!--  Google Maps Plugin    -->
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
        <!-- Material Dashboard javascript methods -->
        <script src="dashboard/js/material-dashboard.js?v=1.2.0"></script>
        <!--dashboard-js-->	

        <!-- jquery
        ============================================ -->		

        <!-- bootstrap JS
        ============================================ -->		
        <script src="js/bootstrap.min.js"></script>
        <!-- wow JS
        ============================================ -->		
        <script src="js/wow.min.js"></script>
        <!-- Nivo Slider JS -->
        <script src="js/jquery.nivo.slider.pack.js"></script> 		
        <!-- meanmenu JS
        ============================================ -->		
        <script src="js/jquery.meanmenu.min.js"></script>
        <!-- owl.carousel JS
        ============================================ -->		
        <script src="js/owl.carousel.min.js"></script>
        <!-- scrollUp JS
        ============================================ -->		
        <script src="js/jquery.scrollUp.min.js"></script>
        <!-- Apple TV Effect -->
        <script src="js/atvImg-min.js"></script>
        <!-- Add venobox js -->
        <script type="text/javascript" src="venobox/venobox.min.js"></script>
        <!-- plugins JS
        ============================================ -->		
        <script src="js/plugins.js"></script>
        <!-- main JS
        ============================================ -->		
        <script src="js/main.js"></script>
    </body>
</html>