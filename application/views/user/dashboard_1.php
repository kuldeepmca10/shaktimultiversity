<!--body-->
<div class="dashboard">
    <div class="wrapper">
        <?php $this->load->view("user/left"); ?>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"> 
                            <span class="sr-only">Toggle navigation</span> 
                            <span class="icon-bar"></span> 
                            <span class="icon-bar"></span> 
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div>
            </nav>   
            <div class="content margin-top-20">        
                <div class="session-message">
                    <?php
                    if (!empty($this->session->flashdata('item'))) {
                        $message = $this->session->flashdata('item');
                        ?>
                        <div class="session-message-dtl <?php echo $message['class']; ?>"><?php echo $message['message']; ?></div>
                        <?php
                    }
                    ?>
                </div>

                <div class="container-fluid">
                    <div class="card">
                        <div class="card-header" data-background-color="purple">
                            <h4 class="title">Dashboard</h4>
                        </div>
                        <div class="card-content">
                            <?php if (isset($aUserData['profile_complete']) && $aUserData['profile_complete'] == 0) { ?>
                            <div class="row blink">
                                <div class="col-md-12">
                                    <div class="alert alert-danger" id="alertCompleteProfile">
                                        <button type="button" aria-hidden="true" style="font-size:25px;" class="close" onclick="$('#alertCompleteProfile').hide(100)">×</button>
                                        <a href="user/update-profile">
                                            <span class="comp-box" style="font-size:16px;">
                                                Attention Required! Please complete your profile. Click here to complete now
                                            </span>
                                        </a> 
                                    </div>                                   
                                </div>
                            </div>
                            <?php } ?>
                            <div class="panel with-nav-tabs panel-default">
                                <div class="panel-body">
                                    <div class="tab-content">
                                        <div class="panel-heading">
                                            <ul class="nav nav-tabs">
                                                <li class="active"><a href="#tab1default" data-toggle="tab">My Courses</a></li>
                                                <li><a href="#tab2default" data-toggle="tab">My Products</a></li>
                                                <li><a href="#tab3default" data-toggle="tab">My Consultations</a></li>
                                            </ul>
                                        </div>   
                                        <div class="tab-pane fade in active" id="tab1default">
                                            <table class="table-striped table-bordered table-hover table-condensed" style="width:100%"> 
                                                <tbody>  
                                                    <tr role="row" width="70%"> 
                                                        <td width="50%">Total Course Enrollment:</td>
                                                        <td><?php echo isset($aStats['totalEnrollment']) && !empty($aStats['totalEnrollment']) ? $aStats['totalEnrollment'] : 0 ?></td>
                                                    </tr>
                                                    <tr role="row" width="70%"> 
                                                        <td>Approved Enrollment:</td>
                                                        <td><?php echo isset($aStats['approved']) && !empty($aStats['approved']) ? $aStats['approved'] : 0 ?></td>
                                                    </tr>
                                                    <tr role="row" width="70%"> 
                                                        <td>Course Completed:</td>
                                                        <td><?php echo isset($aStats['completed']) && !empty($aStats['completed']) ? $aStats['completed'] : 0 ?></td>
                                                    </tr>
                                                    <tr role="row" width="70%"> 
                                                        <td>Enrollment Pending For Approval:</td>
                                                        <td><?php echo isset($aStats['pending for approval']) && !empty($aStats['pending for approval']) ? $aStats['pending for approval'] : 0 ?></td>
                                                    </tr>
                                                    <tr role="row" width="70%"> 
                                                        <td>Rejected Enrollment:</td>
                                                        <td><?php echo isset($aStats['rejected']) && !empty($aStats['rejected']) ? $aStats['rejected'] : 0 ?></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane fade" id="tab2default">
                                            <table class="table-striped table-bordered table-hover table-condensed" style="width:100%"> 
                                                <tbody>  
                                                    <tr role="row" width="70%"> 
                                                        <td style="padding:10px;">
                                                            <p>This section will show you all your product purchases once our Shop Module is LIVE.</p>
                                                            <p>Till then you shall be intimated about your books and products purchases on your registered email.</p>
                                                            <p>Thank you,<br/>Operations Team<br/>The Shakti Multiversity</p></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane fade" id="tab3default">
                                            <table class="table-striped table-bordered table-hover table-condensed" style="width:100%"> 
                                                <tbody>  
                                                    <tr role="row" width="70%"> 
                                                        <td style="padding:10px;">
                                                            <p>This section will show you all your product purchases once our Shop Module is LIVE.</p>
                                                            <p>Till then you shall be intimated about your books and products purchases on your registered email.</p>
                                                            <p>Thank you,<br/>Operations Team<br/>The Shakti Multiversity</p></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    &nbsp;
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>	
<!--body-->
<!--  
<div class="alert alert-info">
    <button type="button" aria-hidden="true" class="close">×</button>
    <span>
        50% discount on the courses
    </span>
</div>
<div class="alert alert-success">
    <button type="button" aria-hidden="true" class="close">×</button>
    <span>
        THANK YOU! Use the Coupon Code Get5Now at Checkout to Receive 5% Off Your Order
    </span>
</div>
<div class="alert alert-warning">
    <button type="button" aria-hidden="true" class="close">×</button>
    <span>
        Please updated your profile now
    </span>
</div>
<div class="alert alert-primary">
    <button type="button" aria-hidden="true" class="close">×</button>
    <span>
        This is a regular notification made with ".alert-primary"
    </span>
</div>
-->


<script src="assets/front/js/material.min.js" type="text/javascript"></script>
<script src="assets/front/js/perfect-scrollbar.jquery.min.js"></script>
<script src="assets/front/js/material-dashboard.js?v=1.2.0"></script>
<script src="assets/front/js/user.js?<?php echo VERSION ?>"></script>