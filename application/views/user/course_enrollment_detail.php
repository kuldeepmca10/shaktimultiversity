<div class="dashboard">
    <div class="wrapper">
        <?php $this->load->view("user/left"); ?>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div>
            </nav>            
                      
            <div class="content margin-top-20" >
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="card card-nav-tabs">
                                <div class="card-header" data-background-color="purple">
                                   <h4 class="title">Enrollment Details</h4>
                                    <!--<div class="nav-tabs-navigation">
                                        <div class="nav-tabs-wrapper">
                                            
                                            <ul class="nav nav-tabs" data-tabs="tabs">

                                            </ul>
                                        </div>
                                    </div>-->
                                </div>
                                <div class="card-content">
                                    <div class="tab-content">
                                        <div class="table-responsive">
                                            <table border="0"  class="table table-bordered">
                                                <tbody>
                                                    <?php
                                                    if (isset($aEnrollmentDetail) && !empty($aEnrollmentDetail)) {
                                                        ?>
                                                        <tr>
                                                            <td class="strong" style="width: 35%">Enrollment Number</td>
                                                            <td class="light"><?php echo $aEnrollmentDetail['enrollment_number']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="strong">Course Category</td>
                                                            <td class="light"><?php echo $aEnrollmentDetail['category_name']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="strong">Course Name</td>
                                                            <td class="light"><?php echo $aEnrollmentDetail['course_title']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="strong">Batch ID</td>
                                                            <td class="light"><?php echo $aEnrollmentDetail['batch_id']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="strong">Batch Start Date</td>
                                                            <td class="light"><?php echo get_date($aEnrollmentDetail['batch_date']); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="strong">First Session At</td>
                                                            <td class="light"><?php echo $aEnrollmentDetail['batch_time']; ?>&nbsp;&nbsp;<a href="javascript::;" id="sEnrollmentSessionDetail" class="enr-view-link">Click Here To View Detail</a></td>
                                                        </tr>
                                                        <?php if (isset($aEnrollmentDetail['price_type']) && $aEnrollmentDetail['price_type'] != 'free') { ?>
                                                            <tr>
                                                                <td class="strong">Course Fee</td>
                                                                <td class="light"><?php echo $aEnrollmentDetail['currency_type']; ?>&nbsp;<?php echo $aEnrollmentDetail['course_fees']; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="strong">Payment Mode</td>
                                                                <td class="light"><?php echo isset($aEnrollmentDetail['payment_gateway_name']) && !empty($aEnrollmentDetail['payment_gateway_name']) ? $aEnrollmentDetail['payment_gateway_name'] : 'N/A'; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="strong">Payment Gateway Id</td>
                                                                <td class="light"><?php echo isset($aEnrollmentDetail['payment_gateway_id']) && !empty($aEnrollmentDetail['payment_gateway_id']) ? $aEnrollmentDetail['payment_gateway_id'] : 'N/A'; ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="strong">Payment</td>
                                                                <td class="light"><?php echo isset($aEnrollmentDetail['payment_status']) && !empty($aEnrollmentDetail['payment_status']) ? ucwords(strtolower($aEnrollmentDetail['payment_status'])) : 'N/A'; ?></td>
                                                            </tr>
                                                        <?php } ?>
                                                        <tr>
                                                            <td class="strong">Email</td>
                                                            <td class="light"><?php echo $aEnrollmentDetail['email']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="strong">Phone</td>
                                                            <td class="light"><?php echo $aEnrollmentDetail['phone']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="strong">Download Brochure</td>
                                                            <td class="light"><a href="<?php echo URL . 'course-enrollment/recipt/' . $sEnrollmentNumber; ?>" class="enr-view-link">Click Here To Download</a></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="strong">Approval Status</td>
                                                            <td style="color: red">
                                                                <?php echo ucwords(strtolower($aEnrollmentDetail['approval_status'])); ?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="strong">Comment</td>
                                                            <td>
                                                                <?php echo isset($aEnrollmentDetail['comment']) ? ucfirst($aEnrollmentDetail['comment']) : 'N/A'; ?>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <button type="button" onclick="history.back()" class="btn btn-primary pull-right">Back To List</button>
                                    &nbsp;<button type="button" onclick="sendFeedback('<?php echo $aEnrollmentDetail['id']; ?>')" class="btn btn-primary pull-right">If you have see any discrepancy please report here</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="sMessageDialogSessionDetail" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>
                    <div id="sEnrolDetailDialogIcon" ><i class="fa fa-exclamation"></i></div>
                    <span id="sEnrolDetailDialogTitle">Session Detail</span>
                </h4>
            </div>
            <div class="modal-body">
                <div id="sEnrolDetailDialogContent">
                    <div id="sessionDetail_<?php echo $r['id'] ?>" class="sessionDetail">
                        <?php
                        $sSessionHtml = '<div class="session-detail-list" >';
                        if (isset($aSessionList) && !empty($aSessionList)) {
                            $sSessionHtml .= '<div class="row">'
                                    . '<div class="col-sm-3 session-detail-header">Title</div>'
                                    . '<div class="col-sm-3 session-detail-header">Date</div>'
                                    . '<div class="col-sm-3 session-detail-header">Time</div>'
                                    . '<div class="col-sm-3 session-detail-header">Duration</div>'
                                    . '</div>';
                            foreach ($aSessionList as $aSession) {
                                $sSessionHtml .= '<div class="row">';
                                $sSessionHtml .= '<div class="col-sm-3 session-detail-item session-name">' . $aSession['name'] . '</div>';
                                $sSessionHtml .= '<div class="col-sm-3 session-detail-item session-date">' . get_date($aSession['date']) . '</div>';
                                $sSessionHtml .= '<div class="col-sm-3 session-detail-item session-time">' . $aSession['time'] . '</div>';
                                $sSessionHtml .= '<div class="col-sm-3 session-detail-item session-duration">' . $aSession['duration_hr'] . ' Hrs ' . $aSession['duration_mn'] . ' Mins</div>';
                                $sSessionHtml .= '</div>';
                            }
                        } else {
                            $sSessionHtml .= 'No Session Available';
                        }
                        $sSessionHtml .= '</div>';
                        echo $sSessionHtml;
                        ?>
                    </div>
                </div>
                <!--<button type="button" class="black-btn popup-ok" data-dismiss="modal" aria-label="Close">OK</button>-->
            </div>
        </div>
    </div>
</div>

<script src="assets/front/js/material.min.js" type="text/javascript"></script>
<script src="assets/front/js/perfect-scrollbar.jquery.min.js"></script>
<script src="assets/front/js/material-dashboard.js?v=1.2.0"></script>
<script src="assets/front/js/user.js?<?php echo VERSION ?>"></script>

<!--end about  area -->	