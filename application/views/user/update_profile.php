<!--body-->
<script src="assets/front/js/jquery.validate.min.js?<?php echo VERSION ?>"></script>
<div class="dashboard">
    <div class="wrapper">
        <?php $this->load->view("user/left"); ?>
        <div class="main-panel">
           
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div>
            </nav>           
           
           <div class="content">
            <div class="session-message">
                <?php
                if (!empty($this->session->flashdata('item'))) {
                    $message = $this->session->flashdata('item');
                    ?>
                    <div class="session-message-dtl <?php echo $message['class']; ?>"><?php echo $message['message']; ?></div>
                    <?php
                }
                ?>
            </div>
            
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card">
                                <?php if (isset($aUserData['profile_complete']) && $aUserData['profile_complete'] == 1) { ?>
                                    <div class="card-header" data-background-color="purple">
                                        <h4 class="title">Update Profile</h4>
                                        <p class="category">Update your profile below</p>
                                    </div>   
                                    <div class="card-content">
                                        <form action="" method="post" id="sUpdateProfile" name="sUpdateProfile" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Name</label>
                                                        <input type="text" name="name" id="name" value="<?php echo isset($aUserData['name']) && !empty($aUserData['name']) ? $aUserData['name'] : '' ?>" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Email</label>
                                                        <input type="text" name="email" id="email" value="<?php echo isset($aUserData['email']) && !empty($aUserData['email']) ? $aUserData['email'] : '' ?>" class="form-control">
                                                    </div>
                                                </div>											
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating is-focused">
                                                        <label class="control-label">County</label>
                                                        <select id="country" name="country" class="form-control">
                                                            <option value=""> -- Select --</option>
                                                            <?php
                                                            if (isset($aCountries) && !empty($aCountries)) {
                                                                foreach ($aCountries as $aCountry) {
                                                                    $sSelected = (isset($aUserData['country']) && !empty($aUserData['country']) && $aUserData['country'] == $aCountry['id']) ? 'selected="selected"' : '';
                                                                    echo ' <option  value="' . $aCountry['id'] . '_' . $aCountry['phonecode'] . '" ' . $sSelected . '>' . ucwords(strtolower($aCountry['name'])) . '</option>';
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">State/Province</label>
                                                        <select id="state" name="state" class="form-control">
                                                            <option value=""></option>                                            
                                                            <?php
                                                            if (isset($aStateList) && !empty($aStateList)) {
                                                                foreach ($aStateList as $aState) {
                                                                    $sSelected = (isset($aUserData['state']) && !empty($aUserData['state']) && $aUserData['state'] == $aState['id']) ? 'selected="selected"' : '';
                                                                    echo ' <option value="' . $aState['id'] . '" ' . $sSelected . '>' . ucwords(strtolower($aState['name'])) . '</option>';
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>											
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">City</label>
                                                        <input type="text" name="city" id="city" value="<?php echo (isset($aUserData['city']) && !empty($aUserData['city'])) ? $aUserData['city'] : '' ?>"  class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Address</label>
                                                        <input type="text" name="address" id="address" value="<?php echo (isset($aUserData['address']) && !empty($aUserData['address'])) ? $aUserData['address'] : '' ?>"  class="form-control" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">                                            
                                                    <div class="form-group label-floating">
                                                        <div class="form-group label-floating">
                                                            <label class="control-label">Date of Birth</label> 
                                                            <div class="col-md-4">
                                                                <?php
                                                                // set start and end year range
                                                                $dateArray = range(1, 31);
                                                                $aDateOfBirth = explode('/', $aUserData['dob']);
                                                                ?>
                                                                <!-- displaying the dropdown list -->
                                                                <select name="date" class="form-control">
                                                                    <option value="">Select Date</option>
                                                                    <?php
                                                                    foreach ($dateArray as $date) {
                                                                        // if you want to select a particular year
                                                                        $selected = '';
                                                                        if (isset($aDateOfBirth[0]) && !empty($aDateOfBirth[0]) && $aDateOfBirth[0] == $date) {
                                                                            $selected = 'selected';
                                                                        }
                                                                        echo '<option ' . $selected . ' value="' . $date . '">' . $date . '</option>';
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-4">

                                                                <?php
                                                                // set the month array
                                                                $formattedMonthArray = array(
                                                                    "1" => "January", "2" => "February", "3" => "March", "4" => "April",
                                                                    "5" => "May", "6" => "June", "7" => "July", "8" => "August",
                                                                    "9" => "September", "10" => "October", "11" => "November", "12" => "December",
                                                                );
                                                                $monthArray = range(1, 12);
                                                                ?>
                                                                <!-- displaying the dropdown list -->
                                                                <select name="month" class="form-control">
                                                                    <option value="">Select Month</option>
                                                                    <?php
                                                                    foreach ($monthArray as $month) {
                                                                        // if you want to select a particular month
                                                                        $selected = '';
                                                                        if (isset($aDateOfBirth[1]) && !empty($aDateOfBirth[1]) && $aDateOfBirth[1] == $month) {
                                                                            $selected = 'selected';
                                                                        }
                                                                        // if you want to add extra 0 before the month uncomment the line below
                                                                        //$month = str_pad($month, 2, "0", STR_PAD_LEFT);
                                                                        echo '<option ' . $selected . ' value="' . $month . '">' . $formattedMonthArray[$month] . '</option>';
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <?php
                                                                // set start and end year range
                                                                $yearArray = range(date("Y", strtotime("-100 year")), date('Y'));
                                                                ?>
                                                                <!-- displaying the dropdown list -->
                                                                <select name="year"  class="form-control">
                                                                    <option value="">Select Year</option>
                                                                    <?php
                                                                    foreach ($yearArray as $year) {
                                                                        // if you want to select a particular year
                                                                        $selected = '';
                                                                        if (isset($aDateOfBirth[2]) && !empty($aDateOfBirth[2]) && $aDateOfBirth[2] == $year) {
                                                                            $selected = 'selected';
                                                                        }
                                                                        echo '<option ' . $selected . ' value="' . $year . '">' . $year . '</option>';
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">                                                    <div class="form-group label-floating">
                                                        <div class="form-group label-floating">                                             <div class="form-group label-floating">
                                                                <label class="control-label">Gender</label>
                                                                <div class="col-md-12">       
                                                                    <select id="gender" name="gender" class="form-control">
                                                                        <option value=""> -- Select --</option>
                                                                        <option value="male" <?php echo (isset($aUserData['gender']) && !empty($aUserData['gender']) && $aUserData['gender'] == 'male') ? 'selected="selected"' : ''; ?>>Male</option>
                                                                        <option value="female" <?php echo (isset($aUserData['gender']) && !empty($aUserData['gender']) && $aUserData['gender'] == 'female') ? 'selected="selected"' : ''; ?>>Female</option>
                                                                        <option value="other" <?php echo (isset($aUserData['gender']) && !empty($aUserData['gender']) && $aUserData['gender'] == 'other') ? 'selected="selected"' : ''; ?>>Other</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">                                                    
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Phone Number</label>
                                                        <div class="textclass" style="display:inline-block">
                                                            <!--<input type="text"  readonly="true" name="phone_code" minlength="2" style="width:20%" maxlength="5" id="phone_code" value="<?php echo (isset($aUserData['phone_code']) && !empty($aUserData['phone_code'])) ? $aUserData['phone_code'] : '' ?>" class="form-control" />-->
                                                            <input type="text" class="form-control" name="phone" minlength="8" maxlength="15" id="phone" value="<?php echo (isset($aUserData['phone']) && !empty($aUserData['phone'])) ? $aUserData['phone'] : 'eee' ?>" />
                                                        </div>                                                       
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Alternate Phone Number</label>
                                                        <div class="textclass" style="display:inline-block">
    <!--                                                            <input type="text" readonly="true" name="phone_code" minlength="2" style="width:20%" maxlength="5"  value="<?php echo (isset($aUserData['phone_code']) && !empty($aUserData['phone_code'])) ? $aUserData['phone_code'] : '' ?>" />-->
                                                            <input type="text" name="alternative_phone" minlength="8"  maxlength="15"  value="<?php echo (isset($aUserData['alternative_phone']) && !empty($aUserData['alternative_phone'])) ? $aUserData['alternative_phone'] : '' ?>" id="alternative_phone" class="form-control" />
                                                        </div>  
                                                    </div>
                                                </div>											
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Skype ID</label>
                                                        <input type="text" name="skype_id" id="skype_id"  value="<?php echo (isset($aUserData['skype_id']) && !empty($aUserData['skype_id'])) ? $aUserData['skype_id'] : '' ?>"  maxlength="90"  class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Hangout ID</label>
                                                        <input type="text" name="hangout_id" id="hangout_id"  value="<?php echo (isset($aUserData['hangout_id']) && !empty($aUserData['hangout_id'])) ? $aUserData['hangout_id'] : '' ?>"  maxlength="90"  class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Alternate Email ID</label>
                                                        <input type="text" name="alternative_email" id="alternative_email" value="<?php echo (isset($aUserData['alternative_email']) && !empty($aUserData['alternative_email'])) ? $aUserData['alternative_email'] : '' ?>" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">WhatsApp Number</label>
                                                        <input type="text" id="other_id" name="other_id" maxlength="90"  value="<?php echo (isset($aUserData['other_id']) && !empty($aUserData['other_id'])) ? $aUserData['other_id'] : '' ?>"  class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label-fix">Profile Photo</label>
                                                        <input type="file" <?php echo (isset($aUserData['profile_photo']) && !empty($aUserData['profile_photo'])) ? '' : 'class="required"'; ?>  onchange="ValidateSingleInput(this);"  id="profile_photo" name="profile_photo" value="" style="opacity: 100;top: 20px;">
                                                    </div>
                                                    <br>
                                                    <?php
                                                    if (isset($aUserData['profile_photo']) && !empty($aUserData['profile_photo'])) {
                                                        $sUserImage = "uploads/user/" . $aUserData['profile_photo'];
                                                        echo '<img src="' . $sUserImage . '"  style="width:150px;height:150px" />';
                                                    }
                                                    ?>
                                                </div>	

                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label-fix">Upload ID Proof</label>
                                                        <input type="file" onchange="ValidateSingleInputWithPdf(this);"  id="id_proof" name="id_proof" value="" style="opacity: 100;top: 20px;">
                                                    </div>
                                                    <br>
                                                    <?php
                                                    if (isset($aUserData['id_proof']) && !empty($aUserData['id_proof'])) {
                                                        $sUserImage = "uploads/user/" . $aUserData['id_proof'];
                                                        echo '<img src="' . $sUserImage . '" style="width:150px;height:150px" />';
                                                    }
                                                    ?>
                                                </div>	
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 pull-left">
                                                    <button type="submit" class="btn btn-primary">Update Profile</button>
                                                    <div class="clearfix"></div>
                                                    </form>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                <?php } else {
                                    ?>
                                    <div class="card-header" data-background-color="purple">
                                        <h4 class="title">Complete Profile</h4>
                                        <p class="category">Complete your profile</p>
                                    </div> 
                                    <div class="card-content">
                                        <form action="" method="post" id="sCompleteProfile" name="sCompleteProfile" enctype="multipart/form-data">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Alternate Email ID</label>
                                                        <input type="text" name="alternative_email" id="alternative_email" value="<?php echo (isset($aUserData['alternative_email']) && !empty($aUserData['alternative_email'])) ? $aUserData['alternative_email'] : '' ?>" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Alternate Phone Number</label>
                                                        <input type="text" name="alternative_phone" minlength="8" maxlength="15" value="<?php echo (isset($aUserData['alternative_phone']) && !empty($aUserData['alternative_phone'])) ? $aUserData['alternative_phone'] : '' ?>" id="alternative_phone" class="form-control">
                                                    </div>
                                                </div>											
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Skype ID</label>
                                                        <input type="text" name="skype_id" id="skype_id"  value="<?php echo (isset($aUserData['skype_id']) && !empty($aUserData['skype_id'])) ? $aUserData['skype_id'] : '' ?>"  maxlength="90"  class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Hangout ID</label>
                                                        <input type="text" name="hangout_id" id="hangout_id"  value="<?php echo (isset($aUserData['hangout_id']) && !empty($aUserData['hangout_id'])) ? $aUserData['hangout_id'] : '' ?>"  maxlength="90"  class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">WhatsApp Number</label>
                                                        <input type="text" id="other_id" name="other_id" maxlength="90"  value="<?php echo (isset($aUserData['other_id']) && !empty($aUserData['other_id'])) ? $aUserData['other_id'] : '' ?>"  class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label-fix">Profile Photo</label>
                                                        <input type="file"  <?php echo (isset($aUserData['profile_photo']) && !empty($aUserData['profile_photo'])) ? '' : 'class="required"'; ?> onchange="ValidateSingleInput(this);"  id="profile_photo" name="profile_photo" value="" style="opacity: 100;top: 20px;">
                                                    </div>
                                                    <br>
                                                    <?php
                                                    if (isset($aUserData['profile_photo']) && !empty($aUserData['profile_photo'])) {
                                                        $sUserImage = "uploads/user/" . $aUserData['profile_photo'];
                                                        echo '<img src="' . $sUserImage . '"  style="width:150px;height:150px" />';
                                                    }
                                                    ?>
                                                </div>	

                                                <div class="col-md-6">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label-fix">Upload ID Proof</label>
                                                        <input type="file" onchange="ValidateSingleInput(this);"  id="id_proof" name="id_proof" value="" style="opacity: 100;top: 20px;">
                                                    </div>
                                                    <br>
                                                    <?php
                                                    if (isset($aUserData['id_proof']) && !empty($aUserData['id_proof'])) {
                                                        $sUserImage = "uploads/user/" . $aUserData['id_proof'];
                                                        echo '<img src="' . $sUserImage . '" style="width:150px;height:150px" />';
                                                    }
                                                    ?>
                                                </div>	
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 pull-left">
                                                    <button type="submit" class="btn btn-primary">Update Profile</button>
                                                    <div class="clearfix"></div>
                                                    </form>
                                                </div>
                                            </div>
                                    </div>
                                <?php }
                                ?>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="orange">
                                    <h4 class="title">User Information</h4>
                                    <p class="category">Registered on <?php echo date('jS F Y', strtotime($aUserData['created'])); ?></p>
                                </div>
                                <div class="card-content table-responsive">
                                    <table class="table table-hover">
                                        <tbody>
                                            <tr>
                                                <th>Name</th>
                                                <td><?php echo isset($aUserData['name']) && !empty($aUserData['name']) ? $aUserData['name'] : ''; ?></td>												
                                            </tr>
                                            <tr>
                                                <th>Email</th>
                                                <td><?php echo isset($aUserData['email']) && !empty($aUserData['email']) ? $aUserData['email'] : ''; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Phone</th>
                                                <td><?php echo isset($aUserData['country_code']) && !empty($aUserData['country_code']) ? $aUserData['country_code'] : ''; ?>-<?php echo isset($aUserData['phone']) && !empty($aUserData['phone']) ? $aUserData['phone'] : ''; ?></td>
                                            </tr>	
                                            <tr>
                                                <th>Address</th>
                                                <td><?php echo isset($aUserData['address']) && !empty($aUserData['address']) ? $aUserData['address'] : 'N/A'; ?></td>
                                            </tr>	
                                            <tr>
                                                <th>City</th>
                                                <td><?php echo isset($aUserData['city']) && !empty($aUserData['city']) ? $aUserData['city'] : '--'; ?></td>
                                            </tr>
                                            <tr>
                                                <th>State</th>
                                                <td><?php echo isset($aUserData['state_name']) && !empty($aUserData['state_name']) ? $aUserData['state_name'] : '--'; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Country</th>
                                                <td><?php echo isset($aUserData['country_name']) && !empty($aUserData['country_name']) ? $aUserData['country_name'] : '--'; ?></td>
                                            </tr>											
                                            <tr>
                                                <th>Gender</th>
                                                <td><?php echo isset($aUserData['gender']) && !empty($aUserData['gender']) ? ucwords($aUserData['gender']) : '--'; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Birthday</th>
                                                <td><?php echo isset($aUserData['dob']) && !empty($aUserData['dob']) ? ucwords($aUserData['dob']) : '--'; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Alternative Email ID</th>
                                                <td><?php echo isset($aUserData['alternative_email']) && !empty($aUserData['alternative_email']) ? ucwords($aUserData['alternative_email']) : '--'; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Alternative Phone</th>
                                                <td><?php echo isset($aUserData['alternative_phone']) && !empty($aUserData['alternative_phone']) ? ucwords($aUserData['alternative_phone']) : '--'; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Skype ID</th>
                                                <td><?php echo isset($aUserData['skype_id']) && !empty($aUserData['skype_id']) ? ucwords($aUserData['skype_id']) : '--'; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Hangout ID</th>
                                                <td><?php echo isset($aUserData['hangout_id']) && !empty($aUserData['hangout_id']) ? ucwords($aUserData['hangout_id']) : '--'; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Whats UP Number</th>
                                                <td><?php echo isset($aUserData['other_id']) && !empty($aUserData['other_id']) ? ucwords($aUserData['other_id']) : '--'; ?></td>
                                            </tr>
                                            <?php
                                            if (isset($aUserData['id_proof']) && !empty($aUserData['id_proof'])) {
                                                $sIDProof = "uploads/user/" . $aUserData['id_proof'];
                                                ?>
                                                <tr>
                                                    <th style="vertical-align: top">ID Proof</th>
                                                    <td><img src="<?php echo $sIDProof; ?>" style="height:150px;" alt="" /></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>	
</div>	
<!--body-->
<script src="assets/front/js/material.min.js" type="text/javascript"></script>
<script src="assets/front/js/perfect-scrollbar.jquery.min.js"></script>
<script src="assets/front/js/material-dashboard.js?v=1.2.0"></script>
<script src="assets/front/js/user.js?<?php echo VERSION ?>"></script>