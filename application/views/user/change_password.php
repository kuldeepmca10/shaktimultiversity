<div class="dashboard">
    <div class="wrapper">
        <?php $this->load->view("user/left"); ?>
        <div class="main-panel ps-container ps-theme-default">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div>
            </nav>
            <div class="content margin-top-20">
                <div class="session-message">
                    <?php
                    if (!empty($this->session->flashdata('item'))) {
                        $message = $this->session->flashdata('item');
                        ?>
                        <div class="session-message-dtl <?php echo $message['class']; ?>"><?php echo $message['message']; ?></div>
                        <?php
                    }
                    ?>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">Change Password</h4>
                                </div>
                                <div class="card-content">
                                    <?php
                                    if ($this->session->flashdata('item')) {
                                        echo $message = $this->session->flashdata('item');
                                    }
                                    ?>
                                    <form name="sUpdatePassword" id="sUpdatePassword" method="post" action="">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group label-floating is-empty">
                                                    <label class="control-label">Current Password</label>
                                                    <input type="password" class="form-control" minlength="8" maxlength="15" id="sCurrentPassword" tabindex="1" name="sCurrentPassword"  value="">
                                                    <span class="material-input"></span></div>
                                            </div>									
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group label-floating is-empty">
                                                    <label class="control-label">New Password</label>
                                                    <input class="form-control"  minlength="8" maxlength="15"  type="password" id="sNewPassword" tabindex="2" name="sNewPassword" value="">
                                                    <span class="material-input"></span></div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group label-floating is-empty">
                                                    <label class="control-label">Confirm New Password</label>
                                                    <input class="form-control"  minlength="8" maxlength="15"  type="password" id="sNewConfirmPassword" tabindex="3" name="sNewConfirmPassword" value="">
                                                    <span class="material-input"></span></div>
                                            </div>
                                        </div>										
                                        <button type="button" id="sUpdatePasswordSubmit" class="btn btn-primary pull-right">Update Password</button>
                                        <div class="clearfix"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>		
    <!--end about  area -->	
</div>

<script src="assets/front/js/user.js?<?php echo VERSION ?>"></script>
<script src="assets/front/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="assets/front/js/material.min.js" type="text/javascript"></script>
<script src="assets/front/js/perfect-scrollbar.jquery.min.js"></script>
<script src="assets/front/js/material-dashboard.js?v=1.2.0"></script>
