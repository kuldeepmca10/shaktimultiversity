<script>
    $(document).ready(function () {
        $('#sEnrollmentList').DataTable();
    });
</script>

<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<div class="dashboard">
    <div class="wrapper">
        <?php $this->load->view("user/left"); ?>
        <div class="main-panel ps-container ps-theme-default ps-active-y">
           
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div>
            </nav>            
           
            <div class="content margin-top-20">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="card card-nav-tabs">
                                <div class="card-header" data-background-color="purple">
									<h4 class="title">Mark Your Attendance</h4>
	                                 <!--<div class="nav-tabs-navigation">
                                        <div class="nav-tabs-wrapper">
                                            
                                            <ul class="nav nav-tabs" data-tabs="tabs">

                                            </ul>
                                        </div>
                                    </div>-->
                                </div>
                                <div class="card-content">
                                    <div class="tab-content">
                                        <div class="tab-pane active table-responsive" id="profile">
                                            <table class=" table-striped table-bordered table-hover table-condensed" id="sEnrollmentList">
                                                <thead class="text-primary">
                                                    <tr>
                                                        <th>S No.</th>
                                                        <th>Course Name</th>
                                                        <th>Enrollment No</th>
                                                        <th>Batch</th>
                                                        <th>Batch Date</th>
                                                        <th class="text-center">Mark Attendance</th>
                                                    </tr>
                                                </thead> 
                                                <?php
                                                $i = 0;

                                                if (isset($aEnrollmentList) && !empty($aEnrollmentList)) {
                                                    echo ' <tbody>';
                                                    foreach ($aEnrollmentList as $aEnrollment) {
                                                        $i++;
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $i; ?></td>
                                                            <td><?php echo $aEnrollment['courseTitle']; ?></td>
                                                            <td><?php echo $aEnrollment['enrollment_number']; ?></td>
                                                            <td><?php echo $aEnrollment['title']; ?></td>
                                                            <td>
                                                                <?php
                                                                if (isset($aEnrollment['batch_date']) && !empty($aEnrollment['batch_date'])) {
                                                                    echo isset($aEnrollment['batch_date']) && !empty($aEnrollment['batch_date']) ? get_date($aEnrollment['batch_date'], false, 'd M Y') : 'N/A';
//                                                                    echo isset($aEnrollment['batch_time']) && !empty($aEnrollment['batch_time']) ? get_date($aEnrollment['batch_time'], false, 'h:i A') : 'N/A';
                                                                } else {
                                                                    echo 'N/A';
                                                                }
                                                                ?>
                                                            </td>
                                                            <td class="text-center">
                                                                <a onclick="fnMarkAttendance('<?php echo $aEnrollment['id']; ?>', '<?php echo $aEnrollment['batch_id']; ?>')" title="Mark Detail"><i class="fa fa-eye fa-lg" aria-hidden="true"></i></a>
                                                                <a onclick="sendFeedback('<?php echo $aEnrollment['id']; ?>')" title="Contact To Admin"><i class="fa fa-envelope-o fa-lg" aria-hidden="true"></i></a>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    echo '</tbody>';
                                                }
                                                ?>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="sMessageDialogSessionDetail" class="modal fade" role="dialog" >
    <div class="modal-dialog width_600">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>
                    <div id="sEnrolDetailDialogIcon" ><i class="fa fa-exclamation"></i></div>
                    <span id="sEnrolDetailDialogTitle">Session Detail</span>
                </h4>
            </div>
            <div class="modal-body" id="sMarkAttendanceContent">
            </div>
        </div>
    </div>
</div>

<script>
    function markAttandence(status, batch_id, enrollment_id, session_id) {
        $.ajax({
            type: "POST",
            url: "user/update_attendance",
            data: {batch_id: batch_id, status: status, enrollment_id: enrollment_id, session_id: session_id},
            cache: false,
            success: function (data) {
                var data = jQuery.parseJSON(data);
                if (data.status == true) {
                    $("#sMessageDialogIcon").html(data.icon);
                    $("#sMessageDialogTitle").html(data.title);
                    $("#sMessageDialogContent").html(data.message);
                    $("#name").val(data.data.name);
                    $("#email").val(data.data.email);
                    $("#phone_code").attr('selected', data.data.phone_code);
                    $("#phone").html(data.data.phone);
                    $("#sMessageDialog").modal('show');
                } else {
                    $("#sMessageDialogIcon").html(data.icon);
                    $("#sMessageDialogTitle").html(data.title);
                    $("#sMessageDialogContent").html(data.message);
                    $("#sMessageDialog").modal('show');
                }
            }
        });
    }
</script>


<script src="assets/front/js/material.min.js" type="text/javascript"></script>
<script src="assets/front/js/perfect-scrollbar.jquery.min.js"></script>
<script src="assets/front/js/material-dashboard.js?v=1.2.0"></script>
<script src="assets/front/js/user.js?<?php echo VERSION ?>"></script>