<div class="dashboard">
    <div class="wrapper">
        <?php $this->load->view("user/left"); ?>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                </div>
            </nav>            

            <div class="content margin-top-20" >
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="card card-nav-tabs">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">Enrollment Details</h4>
                                </div>
                                <div class="card-content">
                                    <div class="tab-content">
                                        <div class="table-responsive">
                                            <table border="0"  class="table table-bordered">
                                                <tbody>
                                                    <?php
                                                    if (isset($aEnrollmentDetail) && !empty($aEnrollmentDetail)) {
                                                        ?>
                                                        <tr>
                                                            <td class="strong" style="width: 20%">Enrollment Number</td>
                                                            <td class="light"><?php echo $aEnrollmentDetail['enrollment_number']; ?></td>
                                                            <td class="strong" style="width: 20%">Course Category</td>
                                                            <td class="light"><?php echo $aEnrollmentDetail['category_name']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="strong">Course Name</td>
                                                            <td class="light"><?php echo $aEnrollmentDetail['course_title']; ?></td>
                                                            <td class="strong">Batch ID</td>
                                                            <td class="light"><?php echo $aEnrollmentDetail['batch_id']; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="strong">Batch Start Date</td>
                                                            <td class="light"><?php echo get_date($aEnrollmentDetail['batch_date']); ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="4">
                                                                <h3>Curriculum For This Course</h3>
                                                                <div id="accordion" class="purple">
                                                                    <?php
                                                                    if (isset($aSessionList) && !empty($aSessionList)) {
                                                                        foreach ($aSessionList as $aSession) {
                                                                            echo '<h3>' . $aSession['name'] . ' (' . get_date($aSession['date']) . ')</h3>';
                                                                            echo '<table border="0" style="height:auto !important" class="table table-bordered"> ';
                                                                            echo '<tr>';
                                                                            echo '<th>Lesson</th>';
                                                                            echo '<th>Lesson Type</th>';
                                                                            echo '<th>View</td>';
                                                                            echo '</tr>';
                                                                            if (isset($aSession['lession']) && !empty($aSession['lession'])) {
                                                                                foreach ($aSession['lession'] as $aLession) {

                                                                                    echo '<tr>';
                                                                                    echo '<td>' . ucwords(strtolower($aLession['title'])) . '</td>';
                                                                                    echo '<td>' . ucwords(strtolower($aLession['type'])) . '</td>';
                                                                                    if (isset($aLession['type']) && !empty($aLession['type']) && $aLession['type'] == 'STUDY MATERIAL') {
                                                                                        ?>
                                                                                        <td>
                                                                                            <a href="<?php echo UP_URL . "lesson/" . $aLession['file']; ?>" target="_blank">
                                                                                                Preview
                                                                                            </a>
                                                                                        </td>
                                                                                        <?php
                                                                                    } else if (isset($aLession['type']) && !empty($aLession['type']) && $aLession['type'] == 'VIDEO LESSONS') {
                                                                                        if (isset($aLession['link']) && !empty($aLession['link'])) {
                                                                                            $aVideoData = explode('watch?v=', $aLession['link']);
                                                                                            $sVideoData = explode('&', $aVideoData[1]);
                                                                                            $sVideoId = isset($sVideoData[0]) && !empty($sVideoData[0]) ? $sVideoData[0] : '';
                                                                                            ?>
                                                                                            <td>
                                                                                                <a href="javascript::void(0)" class="play-video"  data-title="<?php echo isset($aLession) && !empty($aLession) ? $aLession['title'] : ''; ?>" data-src="<?php echo isset($sVideoId) && !empty($sVideoId) ? $sVideoId : ''; ?>">
                                                                                                    Preview
                                                                                                </a>
                                                                                            </td>
                                                                                            <?php
                                                                                        }
                                                                                    } else {
                                                                                        ?>
                                                                                        <td>
                                                                                            <a href="javascript::void(0)" class="view-description"  data-title="<?php echo isset($aLession) && !empty($aLession) ? $aLession['title'] : ''; ?>" data-description="<?php echo isset($aLession['description']) && !empty($aLession['description']) ? $aLession['description'] : ''; ?>">
                                                                                                Preview
                                                                                            </a>
                                                                                        </td>
                                                                                        <?php
                                                                                    }
                                                                                    echo '</tr>';
                                                                                }
                                                                            }
                                                                            echo '</table>';
                                                                        }
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </td>
                                                        </tr>                                                        
                                                        <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <button type="button" onclick="history.back()" class="btn btn-primary pull-right">Back To List</button>
                                    &nbsp;<button type="button" onclick="sendFeedback('<?php echo $aEnrollmentDetail['id']; ?>')" class="btn btn-primary pull-right">If you have see any discrepancy please report here</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span id="sVideoTitle"></span>
                <button type="button" class="close" id="stopvideo" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <p><iframe src="" id="video" allowfullscreen="" height="400" frameborder="0" width="100%"></iframe></p>
            </div>
        </div>

    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myDescriptionModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <span id="sDescriptionTitle"></span>
                <button type="button" class="close" id="stopvideo" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="sDescriptionInformation">
            </div>
        </div>

    </div>
</div>

<div id="sMessageDialogSessionDetail" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>
                    <div id="sEnrolDetailDialogIcon" ><i class="fa fa-exclamation"></i></div>
                    <span id="sEnrolDetailDialogTitle">Session Detail</span>
                </h4>
            </div>
            <div class="modal-body">
                <div id="sEnrolDetailDialogContent">

                    <!-- $sSessionHtml .= '<div class="col-sm-3 session-detail-item session-date">' . get_date($aSession['date']) . '</div>';
                                                    $sSessionHtml .= '<div class="col-sm-3 session-detail-item session-time">' . $aSession['time'] . '</div>';
                                                    $sSessionHtml .= '<div class="col-sm-3 session-detail-item session-duration">' . $aSession['duration_hr'] . ' Hrs ' . $aSession['duration_mn'] . ' Mins</div>';-->

                </div>
                <!--<button type="button" class="black-btn popup-ok" data-dismiss="modal" aria-label="Close">OK</button>-->
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.view-description').on('click', function (ev) {
            var description = $(this).attr('data-description');
            $('#sDescriptionTitle').html($(this).attr('data-title'));
            $('#sDescriptionInformation').html($(this).attr('data-description'));
            $("#myDescriptionModal").modal('show');
            ev.preventDefault();
        });

        $('.play-video').on('click', function (ev) {
            var videoURL = $(this).attr('data-src');
            $('#sVideoTitle').html($(this).attr('data-title'));
            $("#video")[0].src = "https://www.youtube.com/embed/" + videoURL + "?feature=oembed&autoplay=1";
            $("#myModal").modal('show');
            ev.preventDefault();
        });

        $('#stopvideo').on('click', function (ev) {
            $("#video")[0].src = "";
            $("#myModal").modal('hide');
            ev.preventDefault();
        });
    });

    $(function () {
        $("#accordion").accordion();
    });
</script>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="assets/front/js/material.min.js" type="text/javascript"></script>
<script src="assets/front/js/perfect-scrollbar.jquery.min.js"></script>
<script src="assets/front/js/material-dashboard.js?v=1.2.0"></script>
<script src="assets/front/js/user.js?<?php echo VERSION ?>"></script>

<!--end about  area -->	
