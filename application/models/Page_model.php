<?php

class Page_model extends CI_Model {

    /** Courses * */
    function batch_courses($p = 1, $ps = 1, $cat_id = 0, $free = FALSE, $teacher_id, $sBatchDate = FALSE) {
        $date = date('Y-m-d');
        $cond = '1=1';
        $cond .= " AND date(B.start_date) > '$date'";

        if (isset($sBatchDate) && !empty($sBatchDate)) {
            $sMonth = date('m', strtotime($sBatchDate));
            $sYear = date('Y', strtotime($sBatchDate));
            $cond .= " AND month(B.start_date) = '$sMonth' and year(B.start_date) = '$sYear' ";
        }
        if ($cat_id != 0) {
            $cond .= ' AND C.cat_id=' . $cat_id;
        }
        $q['fields'] = "B.start_date as batch_date, B.code as batch_code, C.id, C.title, C.slug, C.short_description, C.display_order, C.batch_start_date, C.duration_hr, C.duration_mn, C.price_type, C.price, C.mrp, C.price_usd, C.mrp_usd, C.type, C.show_on_home, C.status, C.created, C.image, CC.title cat, T.title teacher, T.slug teacher_slug";
        $q['from'] = " batches B JOIN courses C ON B.course_id = C.id JOIN cats CC ON C.cat_id=CC.id LEFT JOIN teachers T ON T.id=C.teacher_id";
        $q['where'] = $cond;
        $q['extra'] = "ORDER BY B.start_date ASC";
        $q['countField'] = "C.id";

        $rs = $this->dba->paged_rows($q, $p, $ps);

        return $rs;
    }

    /** Courses * */
    function courses($p = 1, $ps = 50, $cat_id = 0, $free = FALSE, $teacher_id, $sBatchDate = FALSE, $lifetime = 0) {
        $cond = " C.status=1 AND C.lifetime_member = '$lifetime' ";
//        $cond = "C.status=1  AND B.start_date >= CURRENT_DATE() ";
        if ($free) {
            $cond .= " AND C.price_type = '$free' ";
        }

        $qs = escape_str(trim_array(($_GET)));

        if ($k = $qs['k']) {
            $cond .= " AND C.title LIKE '%$k%'";
        }
        if ($k = $cat_id) {
            $cond .= " AND C.cat_id='$k'";
        } else if ($k = $qs['cat_id']) {
            $cond .= " AND C.cat_id='$k'";
        }
        if ($k = $qs['type']) {
            $cond .= " AND C.type='$k'";
        }
        if ($teacher_id && $teacher_id != '') {
            $cond .= " AND C.teacher_id='$teacher_id'";
        }
        if ($sBatchDate && $sBatchDate != FALSE) {
            $cond .= " AND DATE(C.batch_start_date) >= '$sBatchDate'";
        }

        $q['fields'] = "C.id, C.title, C.slug, C.short_description, C.display_order, C.batch_start_date, C.duration_hr, C.duration_mn, C.price_type, C.price, C.mrp, C.price_usd, C.mrp_usd, C.type, C.show_on_home, C.status, C.created, C.image, CC.title cat, T.title teacher, T.slug teacher_slug";

        $q['from'] = "courses C  JOIN cats CC ON C.cat_id=CC.id "
//                . "LEFT JOIN batches B ON B.course_id = C.id "
                . "LEFT JOIN teachers T ON T.id=C.teacher_id";
        $q['where'] = $cond;
        $q['extra'] = " ORDER BY C.display_order, C.id ASC";
        $q['countField'] = "C.id";

        $rs = $this->dba->paged_rows($q, $p, $ps);

        $aData = array();
        for ($i = 0; $i < count($rs['result']); $i++) {
            $aBatchDetail = $this->comming_batch($rs['result'][$i]['id']);
            $rs['result'][$i]['batch_start_date'] = $aBatchDetail[0]['start_date'];
        }

        return $rs ? $rs : array();
    }

    function comming_batch($courseId) {
        $q = "SELECT B.*, "
                . "(SELECT COUNT(id) FROM batch_sessions "
                . "WHERE batch_id=B.id AND is_deleted='n' AND date >= CURRENT_DATE ) sessions "
                . "FROM batches B JOIN batch_sessions BS ON BS.batch_id = B.id "
                . "WHERE B.course_id='$courseId' AND  B.start_date >= CURRENT_DATE() "
                . "AND B.is_deleted = 'n' AND BS.is_deleted = 'n' and B.status = 1 "
                . "AND BS.date >= CURRENT_DATE() GROUP BY B.id ORDER BY B.start_date ASC";
        $rs = $this->dba->query($q);
        return $rs;
    }

    function course_dtl($slug, $id = '') {
        $cond = " 1=1";
        if ($id != '') {
            $cond .= " AND id ='" . escape_str($id) . "' ";
        }
        if ($slug != '') {
            $cond .= " AND slug='" . escape_str($slug) . "' ";
        }
        $cond .= ' AND status=1';

        $rs = $this->dba->row("courses", $cond);
        if ($rs) {
            $rs['batches'] = $this->comming_batch($rs['id']);
        }
        return $rs;
    }

    function course_batch_dtl($aParams = array()) {
        $rs = array();
        if (isset($aParams) && !empty($aParams)) {
            $cond = " 1=1";
            if (isset($aParams['id']) && !empty($aParams['id'])) {
                $cond .= " AND id  ='" . escape_str($aParams['id']) . "' ";
            }
            if (isset($aParams['code']) && !empty($aParams['code'])) {
                $cond .= " AND code  ='" . escape_str($aParams['code']) . "' ";
            }
            if (isset($aParams['course_id']) && !empty($aParams['course_id'])) {
                $cond .= " AND course_id  ='" . escape_str($aParams['course_id']) . "' ";
            }
            $rs = $this->dba->row("batches", $cond);
        }
        return $rs;
    }

    function batch_session_list($iBatchId, $sDate = false) {

        $sDate = date('Y-m-d');
        $rs = $this->dba->rows("batch_sessions", " 1=1 AND batch_id = '" . escape_str($iBatchId) . "' AND is_deleted = 'n' and date >= '" . $sDate . "' order by date ");
        return $rs;
    }

    function batch_session_lession_list($iBatchId, $sDate = false) {

        $sDate = date('Y-m-d');
        $rs = $this->dba->rows("batch_sessions", " 1=1 AND batch_id = '" . escape_str($iBatchId) . "' AND is_deleted = 'n' and date >= '" . $sDate . "' order by date ");
        foreach ($rs as $aData) {
            $aResultData = array();
            $aResultData = $aData;
            $aLessionData = $this->dba->rows("course_lession", " 1=1 AND id IN (" . escape_str($aData['lesson_ids']) . ")  ");
            $aResultData['lession'] = $aLessionData;
            $aResult[] = $aResultData;
        }
        return $aResult;
    }

    function batch_user_session_list($aParams) {

        $cond = " 1=1 ";
        $qs = escape_str(trim_array(($_GET)));

        if (isset($aParams['enrollment_id']) && !empty($aParams['enrollment_id'])) {
            $cond .= " AND CES.enrollment_id = " . $aParams['enrollment_id'];
        }

        if (isset($aParams['batch_id']) && !empty($aParams['batch_id'])) {
            $cond .= " AND CES.batch_id = " . $aParams['batch_id'];
        }

        if (isset($aParams['user_id']) && !empty($aParams['user_id'])) {
            $cond .= " AND CES.user_id = " . $aParams['user_id'];
        }

        if (isset($aParams['session_id']) && !empty($aParams['session_id'])) {
            $cond .= " AND CES.session_id = " . $aParams['session_id'];
        }


        $q['fields'] = "CES.*,CES.status as attendance_status";

        $q['from'] = " course_enrollment_sessions CES ";
        $q['where'] = $cond;
        $q['extra'] = " ORDER BY CES.id ASC";
        $q['countField'] = " CES.id ";

        $rs = $this->dba->paged_rows($q, $p, 10);
        return $rs;
    }

    function all_batch_session_list($iBatchId, $sDate = false) {
        $sDate = date('Y-m-d');
        $rs = $this->dba->rows("batch_sessions", " 1=1 AND batch_id = '" . escape_str($iBatchId) . "' AND is_deleted = 'n' order by date ");
        return $rs;
    }

    /** Products * */
    function products($p = 1, $ps = 50) {
        $cond = "P.status=1";
        $qs = escape_str(trim_array(($_GET)));

        if ($k = $qs['k']) {
            $cond .= " AND P.title LIKE '%$k%'";
        }
        if ($k = $qs['cat_id']) {
            $cond .= " AND P.cat_id='$k'";
        }

        $q['fields'] = "P.id, P.title, P.slug, P.writer, P.display_order, P.price, P.mrp, P.price_usd, P.mrp_usd, P.short_description, P.price_type, P.show_on_home, P.status, P.created, CC.title cat, 
					  GROUP_CONCAT(PI.image ORDER BY PI.display_order) images";

        $q['from'] = "products P JOIN cats CC ON P.cat_id=CC.id LEFT JOIN product_images PI ON P.id=PI.pro_id";
        $q['where'] = $cond;
        $q['extra'] = "GROUP BY P.id ORDER BY P.display_order, P.id DESC";
        $q['countField'] = "P.id";

        $rs = $this->dba->paged_rows($q, $p, $ps);
        return $rs;
    }

    function product_dtl($slug) {
        $rs = $this->dba->row("products", "slug='" . escape_str($slug) . "' AND status=1");
        if ($rs) {
            $rs['images'] = $this->dba->rows("product_images", "pro_id='" . $rs['id'] . "' ORDER BY display_order");
        }
        return $rs;
    }

    /** Happenings * */
    function happenings($slug, $p = 1, $ps = 50, $category = '') {
        $cond = "1";
        $qs = escape_str(trim_array(($_GET)));

        if (isset($slug) && !empty($slug)) {
            $cond .= " AND N.slug = '$slug'";
        }

        if (isset($category) && !empty($category)) {
            $cond .= " AND C.slug = '$category'";
        }

        if ($k = $qs['k']) {
            $cond .= " AND N.title LIKE '%$k%'";
        }

        $q['fields'] = "N.id, N.title, N.slug, N.source,N.short_description,N.description,N.source,N.source_subtitle, N.display_order, N.show_on_home, N.status, N.publish_date, N.image";

        $q['from'] = "happenings N JOIN cats C ON N.cat_id= C.id";
        $q['where'] = $cond;
//        $q['extra'] = "ORDER BY N.publish_date DESC, N.display_order";
        $q['extra'] = "ORDER BY N.publish_date DESC";
        $q['countField'] = "N.id";

        $rs = $this->dba->paged_rows($q, $p, $ps);
        return $rs;
    }

    function happenings_dtl($slug) {
        $rs = $this->dba->row("happenings", "slug='" . escape_str($slug) . "' AND status=1");
        return $rs;
    }

    function master_dtl($slug) {
        $rs = $this->dba->row("teachers", "slug='" . escape_str($slug) . "' AND status=1");
        return $rs;
    }

    /** Gallery * */
    function gallery($slug, $p = 1, $ps = 100, $sType = 'I') {
        $cond = "G.status=1";
        $qs = escape_str(trim_array(($_GET)));

        if ($k = $qs['k']) {
            $cond .= " AND G.title LIKE '%$k%'";
        }
        if ($k = $qs['cat_id']) {
            $cond .= " AND G.cat_id='$k'";
        }

        if (isset($sType) && !empty($sType)) {
            $cond .= " AND G.type='$sType'";
        }

        if (isset($slug) && !empty($slug)) {
            $cond .= " AND C.slug='$slug'";
        }

        $q['fields'] = "G.id, G.type, G.title, G.display_order,G.slug, G.status, G.created, G.image, G.video_id, G.video_url, C.title cat";

        $q['from'] = "gallery G JOIN cats C ON G.cat_id=C.id";
        $q['where'] = $cond;
        $q['extra'] = "ORDER BY G.display_order, G.id ASC";
        $q['countField'] = "G.id";

        $rs = $this->dba->paged_rows($q, $p, $ps);
//      echo $this->db->last_query();
//        die;

        return $rs;
    }

    /** Gallery * */
    function gallery_detail($id = '', $slug = '', $p = 1, $ps = 100) {

        $cond = "G.status=1";
        $qs = escape_str(trim_array(($_GET)));

        if ($k = $qs['k']) {
            $cond .= " AND G.title LIKE '%$k%'";
        }
        if ($k = $qs['cat_id']) {
            $cond .= " AND G.cat_id='$k'";
        }

        if (isset($slug) && !empty($slug)) {
            $cond .= " AND G.slug='$slug'";
        }

        $q['fields'] = "G.id, G.type, G.title, G.seo_title, G.seo_keywords, G.seo_description, G.display_order,G.slug, G.status, G.created, G.image, G.video_id, G.video_url, C.title cat";

        $q['from'] = "gallery G JOIN cats C ON G.cat_id=C.id";
        $q['where'] = $cond;
        $q['extra'] = "ORDER BY G.display_order, G.id ASC";
        $q['countField'] = "G.id";

        $rs = $this->dba->paged_rows($q, $p, $ps);

        return isset($rs['result'][0]) ? $rs['result'][0] : array();
    }

    function gallery_image_detail($id = '', $gallery_id = '', $p = 1, $ps = 100) {

        $cond = "G.status=1";
        $qs = escape_str(trim_array(($_GET)));

        if ($k = $qs['k']) {
            $cond .= " AND G.title LIKE '%$k%'";
        }

        if (isset($id) && !empty($id)) {
            $cond .= " AND G.id='$id'";
        }

        if (isset($gallery_id) && !empty($gallery_id)) {
            $cond .= " AND G.gallery_id='$gallery_id'";
        }

        $q['fields'] = "G.id, G.gallery_id, G.title, G.image, G.display_order,  G.status, G.created ";

        $q['from'] = "gallery_images G";
        $q['where'] = $cond;
        $q['extra'] = "ORDER BY G.display_order, G.id ASC";
        $q['countField'] = "G.id";

        $rs = $this->dba->paged_rows($q, $p, $ps);

        return $rs['result'];
    }

    /** Testimonials * */
    function testimonials($catid, $p, $ps = 9) {

        $cond = "1";
        $qs = escape_str(trim_array(($_GET)));

        if ($k = $qs['k']) {
            $cond .= " AND T.title LIKE '%$k%'";
        }
        if (isset($catid) && $catid != '') {
            $cond .= " AND T.cat_id = '$catid'";
        }

        $q['fields'] = "T.id, T.title, T.subtitle, T.description, T.display_order, T.status, T.publish_date, T.image";

        $q['from'] = "testimonials T";
        $q['where'] = $cond;
        $q['extra'] = "ORDER BY T.display_order, T.id ASC";
        $q['countField'] = "T.id";

        $rs = $this->dba->paged_rows($q, $p, $ps);
        return $rs;
    }

    /** Shakti Center * */
    function shakticenters($p, $ps = 45) {

        $cond = "1";
        $qs = escape_str(trim_array(($_GET)));

        if ($k = $qs['k']) {
            $cond .= " AND SC.title LIKE '%$k%'";
        }

        $q['fields'] = "SC.id, SC.title, SC.subtitle,SC.description, SC.location, SC.display_order, SC.status, SC.image";

        $q['from'] = "shakti_centers SC";
        $q['where'] = $cond;
        $q['extra'] = "ORDER BY SC.display_order, SC.id ASC";
        $q['countField'] = "SC.id";

        $rs = $this->dba->paged_rows($q, $qs['p'], 50);

//        if ($rs['result']) {
//            foreach ($rs['result'] as &$r) {
//                if ($r['image']) {
//                    $r['image_url'] = UP_URL . "shakticenters/" . $r['image'];
//                }
//            }
//        }

        return $rs;
    }

    /** FAQ * */
    function faq($cat_id = '') {
        $cond = "status=1";
        if ($cat_id) {
            $cond .= " AND cat_id='" . intval($cat_id) . "'";
        }
        $rs = $this->dba->rows("faq", $cond . " ORDER BY display_order");
        return $rs ? $rs : array();
    }

    function fnSidebarBannerDetail($id) {
        $rs = $this->dba->row("sidebar", "id='" . intval($id) . "'");
        return $rs ? $rs : array();
    }

}

//End of file