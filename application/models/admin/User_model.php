<?php

class User_model extends CI_Model {

    function save($data, $tbl = "system_users") {
        $id = $data['id'];
        unset($data['id']);
        if ($id) {
            $data['updated'] = currentDT();
            $this->dba->update($tbl, $data, "id='" . intval($id) . "'");
        } else {
            $data['created'] = $data['updated'] = currentDT();
            $id = $this->dba->insert($tbl, $data);
        }
        return $id;
    }

    function detail($id) {
        $id = intval($id);
        $rs = $this->dba->row("system_users", "id='$id'");
        return $rs ? $rs : array();
    }

    /** Roles * */
    function roles($p, $ps) {
        $cond = "1";
        $qs = escape_str(trim_array(($_GET)));
        if ($k = $qs['k']) {
            $cond .= " AND R.role_name LIKE '%$k%'";
        }

        $q['fields'] = "R.*";
        $q['from'] = "system_users_roles R";
        $q['where'] = $cond;
        $q['extra'] = "ORDER BY R.id";
        $q['countField'] = "R.id";

        return $this->dba->paged_rows($q, $p, $ps);
    }

    function role_detail($id) {
        $id = intval($id);
        return $this->dba->row("system_users_roles", "id='$id'");
    }

    function save_role($data) {
        if ($id = $data['id']) {
            $data['updated'] = currentDT();
            $this->dba->update("system_users_roles", $data, "id='$id'");
        } else {
            $data['created'] = $data['updated'] = currentDT();
            $id = $this->dba->insert("system_users_roles", $data);
        }
        return $id;
    }

    function access_controls() {
        return $this->dba->rows("system_modules", "1 ORDER BY id");
    }

    function access_permissions($role_id) {
        return $this->dba->rows("sytem_users_roles_modules", "role_id='" . intval($role_id) . "'", "module_id, p_create, p_read, p_update, p_delete");
    }

    function save_permissions($data, $role_id) {
        $err = FALSE;
        $this->db->trans_strict(FALSE);
        $this->db->trans_begin();

        try {

            $this->dba->delete("sytem_users_roles_modules", "role_id='$role_id'");
            $inf = array();

            foreach ($data['module_ids'] as $aid) {
                $inf[] = array(
                    'role_id' => $role_id,
                    'module_id' => $aid,
                    'p_create' => isset($data['p_create'][$aid]) ? 'Y' : 'N',
                    'p_read' => isset($data['p_read'][$aid]) ? 'Y' : 'N',
                    'p_update' => isset($data['p_update'][$aid]) ? 'Y' : 'N',
                    'p_delete' => isset($data['p_delete'][$aid]) ? 'Y' : 'N',
                    'created' => currentDT(),
                    'updated' => currentDT(),
                );
            }

            if ($inf) {
                $this->db->insert_batch("sytem_users_roles_modules", $inf);
            }
        } catch (Exception $e) {
            $err = TRUE;
            $msg = $e->getMessage();
        }

        if ($this->db->trans_status() === FALSE) {
            $err = TRUE;
        }

        if ($err) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    /** Users * */
    function users() {
        $cond = "1";
        $qs = escape_str(trim_array(($_GET)));
        if ($k = $qs['k']) {
            $cond .= " AND (CONCAT(U.fname, ' ', U.lname) LIKE '%$k%' OR U.email LIKE '%$k%' OR R.role_name LIKE '%$k%')";
        }

        if ($k = $qs['role_id']) {
            $cond .= " AND U.role_id='$k'";
        }

        $q['fields'] = "U.id, TRIM(CONCAT(U.fname, ' ', U.lname)) name, U.email, U.status, U.created, R.role_name, IF(U.role_id=1, 1, 0) sortid";
        $q['from'] = "system_users U JOIN system_users_roles R ON U.role_id=R.id";
        $q['where'] = $cond;
        $q['extra'] = "ORDER BY sortid DESC, U.id DESC";
        $q['countField'] = "U.id";

        return $this->dba->paged_rows($q, $qs['p'], 50);
    }

    function newsletter_list() {

        $cond = ' 1=1 AND newsletter.is_deleted = \'n\' ';
        if (isset($_REQUEST['k']) && !empty($_REQUEST['k'])) {
            $cond .= " AND newsletter.email LIKE '%" . $_REQUEST['k'] . "%' ";
        }

        $q['fields'] = " newsletter.*";

        $q['from'] = " newsletter_subscriber newsletter ";

        $q['where'] = $cond;

        $q['extra'] = " ORDER BY newsletter.email ASC";

        $q['countField'] = "newsletter.id";

        $rs = $this->dba->paged_rows($q);

        return $rs ? $rs : array();
    }

    function newsletter_detail($id) {
        $cond = ' 1=1 ';

        if (isset($id) && !empty($id)) {
            $cond .= ' AND id = ' . $id;
        }
        $cond .= ' AND is_deleted = \'n\' ';

        return $this->dba->row("newsletter_subscriber", $cond, " newsletter_subscriber.* ");
    }

    function service_request_detail($email = '') {

        $sWhere = ' 1=1 AND is_deleted = \'n\'';

        if (isset($email) && !empty($email)) {
            $sWhere .= " AND email='" . trim($email) . "'";
        }

        $rs = $this->dba->row("service_request", $sWhere);


        return $rs ? $rs : array();
    }

    function service_request() {

        $cond = ' 1=1 AND SR.is_deleted = \'n\' ';

        if (isset($_REQUEST['k']) && !empty($_REQUEST['k'])) {
            $cond .= " AND SR.email LIKE '%" . $_REQUEST['k'] . "%' ";
        }

        $q['fields'] = " SR.id, SR.name, SR.email, SR.country, SR.message, SR.created_date ";

        $q['from'] = " service_request SR ";

        $q['where'] = $cond;

        $q['extra'] = " ORDER BY SR.email ASC";

        $q['countField'] = "SR.id";

        $rs = $this->dba->paged_rows($q);

        return $rs ? $rs : array();
    }

    function book_request() {

        $cond = ' 1=1 AND BR.is_deleted = \'n\' ';

        if (isset($_REQUEST['k']) && !empty($_REQUEST['k'])) {
            $cond .= " AND BR.email LIKE '%" . $_REQUEST['k'] . "%' ";
        }

        $q['fields'] = " BR.*, P.title as book_name ";

        $q['from'] = " book_order_request BR LEFT JOIN products P ON P.id = BR.book_id ";

        $q['where'] = $cond;

        $q['extra'] = " ORDER BY BR.id DESC";

        $q['countField'] = "BR.id";

        $rs = $this->dba->paged_rows($q);

        return $rs ? $rs : array();
    }

}

//End of file