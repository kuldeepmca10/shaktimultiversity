<?php

class Student_model extends CI_Model {

    function save($data) {
        if ($id = $data['id']) {
            $data['updated'] = currentDT();
            $this->dba->update("users", $data, "id='$id'");
        } else {
            $data['created'] = $data['updated'] = currentDT();
            $id = $this->dba->insert("users", $data);
        }
        return $id;
    }

    function save_enrollment($data) {
        $id = $data['id'];
        unset($data['id']);
        $tbl = "course_enrollment";
        if ($id) {
            $data['updated'] = currentDT();
            $this->dba->update($tbl, $data, "id='" . intval($id) . "'");
        } else {
            $data['created'] = $data['updated'] = currentDT();
            $id = $this->dba->insert($tbl, $data);
        }
        return $id;
    }

    function detail($id) {
        $id = intval($id);
        $rs = $this->dba->row("users", "id='$id'");
        return $rs ? $rs : array();
    }

    function country_phonecodes() {
        $rs = $this->dba->rows("country_phonecode", " 1=1 ");

        return $rs ? $rs : array();
    }

    /** Roles * */
    function roles($p, $ps) {
        $cond = "1";
        $qs = escape_str(trim_array(($_GET)));
        if ($k = $qs['k']) {
            $cond .= " AND R.role_name LIKE '%$k%'";
        }

        $q['fields'] = "R.*";
        $q['from'] = "users_roles R";
        $q['where'] = $cond;
        $q['extra'] = "ORDER BY R.id";
        $q['countField'] = "R.id";

        return $this->dba->paged_rows($q, $p, $ps);
    }

    function role_detail($id) {
        $id = intval($id);
        return $this->dba->row("users_roles", "id='$id'");
    }

    function save_role($data) {
        if ($id = $data['id']) {
            $data['updated'] = currentDT();
            $this->dba->update("users_roles", $data, "id='$id'");
        } else {
            $data['created'] = $data['updated'] = currentDT();
            $id = $this->dba->insert("users_roles", $data);
        }
        return $id;
    }

    function access_controls() {
        return $this->dba->rows("system_modules", "1 ORDER BY id");
    }

    function access_permissions($role_id) {
        return $this->dba->rows("sytem_users_roles_modules", "role_id='" . intval($role_id) . "'", "module_id, p_create, p_read, p_update, p_delete");
    }

    function save_permissions($data, $role_id) {
        $err = FALSE;
        $this->db->trans_strict(FALSE);
        $this->db->trans_begin();

        try {

            $this->dba->delete("sytem_users_roles_modules", "role_id='$role_id'");
            $inf = array();

            foreach ($data['module_ids'] as $aid) {
                $inf[] = array(
                    'role_id' => $role_id,
                    'module_id' => $aid,
                    'p_create' => isset($data['p_create'][$aid]) ? 'Y' : 'N',
                    'p_read' => isset($data['p_read'][$aid]) ? 'Y' : 'N',
                    'p_update' => isset($data['p_update'][$aid]) ? 'Y' : 'N',
                    'p_delete' => isset($data['p_delete'][$aid]) ? 'Y' : 'N',
                    'created' => currentDT(),
                    'updated' => currentDT(),
                );
            }

            if ($inf) {
                $this->db->insert_batch("sytem_users_roles_modules", $inf);
            }
        } catch (Exception $e) {
            $err = TRUE;
            $msg = $e->getMessage();
        }

        if ($this->db->trans_status() === FALSE) {
            $err = TRUE;
        }

        if ($err) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    /** Users * */
    function students() {
        $cond = "1";

        $qs = escape_str(trim_array(($_GET)));
        if ($k = $qs['k']) {
            $cond .= " AND ( U.name LIKE '%$k%' OR U.email LIKE '%$k%' OR studentId LIKE '$k%' )";
        }

        if (isset($qs['isLifetimeMember'])) {
            $k = $qs['isLifetimeMember'];
            $cond .= " AND U.isLifetimeMember = '$k' ";
        }

        if (isset($qs['status'])) {
            $k = $qs['status'];
            $cond .= " AND U.status = '$k' ";
        }

        $q['fields'] = " U.* ";
        $q['from'] = "users U ";
        $q['where'] = $cond;
        $q['extra'] = "ORDER BY U.id DESC";

        $aUserData = $this->dba->paged_rows($q, $qs['p'], 20);
        if (isset($aUserData) && !empty($aUserData)) {
            for ($i = 0; $i < count($aUserData['result']); $i++) {
                $cond = " course_enrollment.user_id = '" . $aUserData['result'][$i]['id'] . "' ";
                $qNew['fields'] = " course_enrollment.course_id, course_enrollment.enrollment_number";
                $qNew['from'] = " course_enrollment ";
                $qNew['where'] = $cond;
                $aEnrollList = $this->dba->paged_rows($qNew, $qs['p']);
                $aUserData['result'][$i]['total_enroll'] = isset($aEnrollList['result']) && !empty($aEnrollList['result']) ? count($aEnrollList['result']) : 0;
            }
        }

        return $aUserData;
    }

    function userEnrollmentDetail($id = '', $userid = '', $courseid = '') {
//        $cond = " user.status = '1'";
        $cond = "1";
        $qs = trim_array(($_GET));

        if ($k = $qs['k'] && $qs['k'] != 'undefined') {
            $k = $qs['k'];
            $cond .= " AND ( enroll.enrollment_number = '$k' OR course.title LIKE '%$k%' OR enroll.batch_no LIKE '%$k%' OR course.code LIKE '%$k%' OR user.email LIKE '%$k%' OR user.name LIKE '%$k%' OR user.studentId = '$k' )";
        }

        $status = $qs['status'];
        if (isset($status) && $status != '' && $status != 'undefined') {
            $cond .= " AND ( enroll.approval_status = '$status')";
        }
        if (isset($qs['payment_gateway_name']) && $qs['payment_gateway_name'] != '' && $qs['payment_gateway_name'] != 'undefined') {
            $cond .= " AND ( enroll.payment_gateway_name = '" . $qs['payment_gateway_name'] . "')";
        }

        if (isset($qs['cat_id']) && $qs['cat_id'] != '' && $qs['cat_id'] != 'undefined') {
            $cond .= " AND ( course.cat_id = '" . $qs['cat_id'] . "' ) ";
        }

        if (isset($qs['userid']) && !empty($qs['userid']) && $qs['userid'] != 'undefined') {
            $cond .= " AND user.id = '" . $qs['userid'] . "' ";
        } elseif ($userid != '') {
            $cond .= " AND user.id = '$userid' ";
        }

        if ($id != '' && $id != 'undefined') {
            $cond .= " AND enroll.id = '$id' ";
        }

        if ($courseid != '' && $courseid != 'undefined') {
            $cond .= " AND course.id = '$courseid'";
        }
        $q['fields'] = "enroll.id as enrollId,enroll.enrollment_number, course.title as course_title, course.type as course_type, course.code as course_code, user.name, user.email,user.phone, user.studentId, course.price_type as course_price_type, "
                . " enroll.batch_no, enroll.batch_title, enroll.batch_date, course.price, enroll.price_type, enroll.currency_type, course.price_usd, "
                . " enroll.payment_gateway_name, enroll.payment_gateway_id, enroll.payment_status, "
                . " enroll.created, enroll.approval_status, enroll.comment ";
        $q['from'] = "users user JOIN course_enrollment enroll ON user.id = enroll.user_id LEFT JOIN courses course ON course.id = enroll.course_id";
        $q['where'] = $cond;

        $rs = $this->dba->paged_rows($q, $qs['p'], 100);

        return $rs;
    }

    function batch_session_list($iBatchId) {
        $aBatches = $this->dba->row("batches", " 1=1 AND code = '" . escape_str($iBatchId) . "' AND is_deleted = 'n' ");

        if (isset($aBatches) && !empty($aBatches)) {
            $rs = $this->dba->row("batch_sessions", " 1=1 AND batch_id = '" . escape_str($aBatches['id']) . "' AND is_deleted = 'n' order by date ");
            return $rs;
        } else {
            return array();
        }
    }

}

//End of file