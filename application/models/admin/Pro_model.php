<?php

class Pro_model extends CI_Model {

    function save($data, $tbl = "") {
        $id = $data['id'];
        unset($data['id']);
        if ($id) {
            $data['updated'] = currentDT();
            $this->dba->update($tbl, $data, "id='" . intval($id) . "'");
        } else {
            $data['created'] = $data['updated'] = currentDT();
            $id = $this->dba->insert($tbl, $data);
        }
        return $id;
    }

    /** Courses * */
    function courses() {
        $cond = "1";
        $qs = escape_str(trim_array(($_GET)));

        if ($k = $qs['k']) {
            $cond .= " AND (C.title LIKE '%$k%' OR C.code LIKE '%$k%')";
        }
        if ($k = $qs['cat_id']) {
            $cond .= " AND C.cat_id='$k'";
        }
        if ($k = $qs['type']) {
            $cond .= " AND C.type='$k'";
        }

        $q['fields'] = "C.id, C.title, C.code, C.display_order,C.price_type, C.duration_hr, C.duration_mn, C.price, C.mrp,C.price_usd, C.mrp_usd, C.type, C.show_on_home, C.featured, C.status, C.created, C.image, CC.title cat, T.title teacher";

        $q['from'] = "courses C JOIN cats CC ON C.cat_id=CC.id LEFT JOIN teachers T ON T.id=C.teacher_id";
        $q['where'] = $cond;
        $q['extra'] = "ORDER BY C.display_order, C.id ASC";
        $q['countField'] = "C.id";

        $rs = $this->dba->paged_rows($q, $qs['p'], 50);

        if ($rs['result']) {
            foreach ($rs['result'] as &$r) {
                if ($r['image']) {
                    $r['image_url'] = UP_URL . "course-sm/" . $r['image'];
                }
                $r['created'] = get_date($r['created']);
            }
        }

        return $rs;
    }

    function course_dtl($id) {
        $rs = $this->dba->row("courses", "id='" . intval($id) . "'");
        return $rs ? $rs : array();
    }

    function course_batches($course_id) {
        $rs = $this->dba->rows("batches", "course_id='" . intval($course_id) . "' AND is_deleted = 'n' AND start_date >= CURRENT_DATE() ORDER BY start_date ASC");
//        $rs = $this->dba->rows("batches", "course_id='" . intval($course_id) . "' AND '" . date('Y-m-d') . "' BETWEEN start_date AND end_date ORDER BY start_date");
        if ($rs) {
            foreach ($rs as &$r) {
                $session = array();
                $aSessionList = $this->dba->rows("batch_sessions", "batch_id='" . intval($r['id']) . "' AND date >= CURRENT_DATE() AND is_deleted = 'n' ORDER BY date");
                $r['sessionCount'] = count($aSessionList);
                $r['start_date'] = get_date($r['start_date']);
                $r['end_date'] = get_date($r['end_date']);
                $r['batch_time'] = date('h:i A', strtotime($r['batch_time']));
            }
        }
        return $rs ? $rs : array();
    }

    function batch_sessions($batch_id) {
        $rs = $this->dba->rows("batch_sessions", "batch_id='" . intval($batch_id) . "' AND date >= CURRENT_DATE() AND status = 1 AND is_deleted = 'n' ORDER BY date");
        if ($rs) {
            foreach ($rs as &$r) {
                $r['start_date'] = get_date($r['start_date']);
                $r['end_date'] = get_date($r['end_date']);
                $r['batch_time'] = date('h:i A', strtotime($r['batch_time']));
            }
        }
        return $rs ? $rs : array();
    }

    function course_lession($course_id) {
        $rs = $this->dba->rows("course_lession", "course_id='" . intval($course_id) . "' ORDER BY title asc");
        return $rs ? $rs : array();
    }

    function lession_dtl($id, $title, $course_id) {
        $sCond = ' 1=1 ';
        if ($id != '') {
            $sCond .= " AND id = '" . trim($id) . "'";
        }
        if ($title != '') {
            $sCond .= " AND title = '" . trim($title) . "'";
        }
        if ($course_id != '') {
            $sCond .= " AND course_id = '" . trim($course_id) . "'";
        }
        $rs = $this->dba->row("course_lession", $sCond);
        return $rs ? $rs : array();
    }

    function batch_dtl($id) {
        $rs = $this->dba->row("batches", "id='" . intval($id) . "'");
        if ($rs) {
            $rs['start_date'] = get_date($rs['start_date']);
            $rs['end_date'] = get_date($rs['end_date']);
            $rs['batch_days'] = explode(",", $rs['batch_days']);

            $bt = strtotime($rs['batch_time']);
            $rs['time_hr'] = date('h', $bt);
            $rs['time_mn'] = date('i', $bt);
            $rs['time_type'] = date('A', $bt);
        }
        return $rs ? $rs : array();
    }

    function batch_session_dtl($id) {
        $rs = $this->dba->row("batch_sessions", "id='" . intval($id) . "'");
        if ($rs) {
            $rs['date'] = get_date($rs['date']);
//            $rs['batch_days'] = explode(",", $rs['batch_days']);

            $bt = strtotime($rs['time']);
            $rs['time_hr'] = date('h', $bt);
            $rs['time_mn'] = date('i', $bt);
            $rs['time_type'] = date('A', $bt);
        }
        return $rs ? $rs : array();
    }

//     function batch_session_dtl($id) {
//        $rs = $this->dba->row("batch_sessions", "id='" . intval($id) . "'");
//        if ($rs) {
//            $rs['date'] = get_date($rs['date']);
////            $rs['batch_days'] = explode(",", $rs['batch_days']);
//
//            $bt = strtotime($rs['time']);
//            $rs['time_hr'] = date('h', $bt);
//            $rs['time_mn'] = date('i', $bt);
//            $rs['time_type'] = date('A', $bt);
//        }
//        return $rs ? $rs : array();
//    }

    /** Products * */
    function products() {
        $cond = "1";
        $qs = escape_str(trim_array(($_GET)));

        if ($k = $qs['k']) {
            $cond .= " AND P.title LIKE '%$k%'";
        }
        if ($k = $qs['cat_id']) {
            $cond .= " AND P.cat_id='$k'";
        }

        $q['fields'] = "P.id, P.title, P.writer, P.display_order, P.price, P.mrp, P.show_on_home, P.status, P.created, CC.title cat";

        $q['from'] = "products P JOIN cats CC ON P.cat_id=CC.id";
        $q['where'] = $cond;
        $q['extra'] = "ORDER BY P.display_order, P.id DESC";
        $q['countField'] = "P.id";

        $rs = $this->dba->paged_rows($q, $qs['p'], 50);

        if ($rs['result']) {
            foreach ($rs['result'] as &$r) {
                if ($r['image']) {
                    $r['image_url'] = UP_URL . "pro-sm/" . $r['image'];
                }
                $r['created'] = get_date($r['created']);
            }
        }

        return $rs;
    }

    function product_dtl($id) {
        $rs = $this->dba->row("products", "id='" . intval($id) . "'");
        return $rs ? $rs : array();
    }

    function product_images($pro_id) {
        $rs = $this->dba->rows("product_images", "pro_id='" . intval($pro_id) . "' ORDER BY display_order,id");
        return $rs ? $rs : array();
    }

    function product_image_dtl($id) {
        $rs = $this->dba->row("product_images", "id='" . intval($id) . "'");
        return $rs ? $rs : array();
    }

    /** Happenings * */
    function happenings() {
        $cond = "1";
        $qs = escape_str(trim_array(($_GET)));

        if ($k = $qs['k']) {
            $cond .= " AND N.title LIKE '%$k%'";
        }
        if ($cat_id = $qs['cat_id']) {
            $cond .= " AND N.cat_id = '$cat_id'";
        }

        $q['fields'] = "N.id, N.title,C.title as cat_name, N.source, N.display_order, N.show_on_home, N.status, N.publish_date, N.image";

        $q['from'] = "happenings N LEFT JOIN cats C ON N.cat_id=C.id";
        $q['where'] = $cond;
        $q['extra'] = "ORDER BY N.display_order, N.publish_date DESC";
        $q['countField'] = "N.id";

        $rs = $this->dba->paged_rows($q, $qs['p'], 50);

        if ($rs['result']) {
            foreach ($rs['result'] as &$r) {
                if ($r['image']) {
                    $r['image_url'] = UP_URL . "happenings-sm/" . $r['image'];
                }
                $r['publish_date'] = get_date($r['publish_date']);
            }
        }

        return $rs;
    }

    /** cms_widget * */
    function cms_widget() {
        $cond = "1";
        $qs = escape_str(trim_array(($_GET)));

        if ($k = $qs['k']) {
            $cond .= " AND N.title LIKE '%$k%'";
        }
        $q['fields'] = "CW.*";

        $q['from'] = "cmswidget CW";
        $q['where'] = $cond;
        $q['extra'] = "ORDER BY CW.id DESC";
        $q['countField'] = "CW.id";

        $rs = $this->dba->paged_rows($q, $qs['p'], 50);

        return $rs;
    }

    function happenings_dtl($id) {
        $rs = $this->dba->row("happenings", "id='" . intval($id) . "'");
        return $rs ? $rs : array();
    }

    /** Gallery * */
    function galleries() {
        $cond = "1";
        $qs = escape_str(trim_array(($_GET)));

        if ($k = $qs['k']) {
            $cond .= " AND G.title LIKE '%$k%'";
        }
        if ($k = $qs['cat_id']) {
            $cond .= " AND G.cat_id='$k'";
        }
        if ($k = $qs['type']) {
            $cond .= " AND G.type='$k'";
        }

        $q['fields'] = "G.id, G.type, G.title, G.display_order, G.status, G.created, G.image, G.video_id, G.video_url, C.title cat";

        $q['from'] = "gallery G JOIN cats C ON G.cat_id=C.id";
        $q['where'] = $cond;
        $q['extra'] = "ORDER BY G.display_order, G.id ASC";
        $q['countField'] = "G.id";

        $rs = $this->dba->paged_rows($q, $qs['p'], 50);

        if ($rs['result']) {
            foreach ($rs['result'] as &$r) {
                if ($r['image']) {
                    $r['image_url'] = UP_URL . "gal-sm/" . $r['image'];
                    $r['publish_date'] = get_date($r['publish_date']);
                    $imagelist = $this->dba->rows("gallery_images", "gallery_id='" . intval($r['id']) . "'");

                    $r['count'] = count($imagelist);
                }
                $r['created'] = get_date($r['created']);
            }
        }

        return $rs;
    }

    function gallery_dtl($id) {
        $rs = $this->dba->row("gallery", "id='" . intval($id) . "'");
        return $rs ? $rs : array();
    }

    function gallery_images_dtl($id = '', $gallery_id = '') {
        if (isset($id) && !empty($id)) {
            $cond = "id='" . intval($id) . "'";
        }

        if (isset($gallery_id) && !empty($gallery_id)) {
            $cond = "gallery_id='" . intval($gallery_id) . "'";
        }

        $rs = $this->dba->rows("gallery_images", $cond);

        return $rs ? $rs : array();
    }

    /** CMS Pages * */
    function cms_pages() {
        $cond = "1";
        $qs = escape_str(trim_array(($_GET)));

        if ($k = $qs['k']) {
            $cond .= " AND P.title LIKE '%$k%'";
        }

        $q['fields'] = "P.id, P.title, P.display_order, P.image, P.status, IF(P.id=1, 1,2) sortid";

        $q['from'] = "cms_pages P";
        $q['where'] = $cond;
        $q['extra'] = "ORDER BY sortid, P.display_order, P.id ASC";
        $q['countField'] = "P.id";

        $rs = $this->dba->paged_rows($q, $qs['p'], 50);

        return $rs;
    }

    function cms_page_dtl($id) {
        $rs = $this->dba->row("cms_pages", "id='" . intval($id) . "'");
        return $rs ? $rs : array();
    }

    /** Page slider * */
    function slides($page_id, $type = 'CMS') {
        $rs = $this->dba->rows("page_slider", "page_id='" . intval($page_id) . "' AND type='$type' ORDER BY display_order, id");
        return $rs ? $rs : array();
    }

    function slide_dtl($id) {
        $rs = $this->dba->row("page_slider", "id='" . intval($id) . "'");
        return $rs ? $rs : array();
    }

    /** Banner List * */
    function fnSidebarBannerList() {
        $cond = "1";
        $qs = escape_str(trim_array(($_GET)));

        if ($k = $qs['k']) {
            $cond .= " AND G.title LIKE '%$k%'";
        }
        if ($k = $qs['section']) {
            $cond .= " AND G.section='$k'";
        }
        if ($k = $qs['type']) {
            $cond .= " AND G.type='$k'";
        }

        $q['fields'] = "G.id, G.type, G.title, G.type, G.url, G.description, G.section, G.display_order, G.show_title, G.status, G.created_date, G.image";

        $q['from'] = "sidebar G ";
        $q['where'] = $cond;
        $q['extra'] = "ORDER BY G.display_order, G.id ASC";
        $q['countField'] = "G.id";

        $rs = $this->dba->paged_rows($q, $qs['p'], 50);

        return $rs;
    }

    function fnSidebarBannerDetail($id) {
        $rs = $this->dba->row("sidebar", "id='" . intval($id) . "'");
        return $rs ? $rs : array();
    }

    function sessionDuration($aParams) {
        $rs = array();
        if (isset($aParams['batch_id']) && !empty($aParams['batch_id'])) {

            if (isset($aParams['id']) && !empty($aParams['id'])) {
                $query = $this->db
                        ->select('id, sum(duration_hr) AS duration_hr, sum(duration_mn) AS duration_mn')
                        ->where('batch_id=' . $aParams['batch_id'] . ' AND id != ' . $aParams['id'] . '  AND is_deleted= \'n\' AND status= \'1\' ')
                        ->group_by('batch_id')
                        ->get('batch_sessions');
            } else {
                $query = $this->db
                        ->select('id, sum(duration_hr) AS duration_hr, sum(duration_mn) AS duration_mn')
                        ->where('batch_id=' . $aParams['batch_id'] . ' AND is_deleted= \'n\' AND status= \'1\' ')
                        ->group_by('batch_id')
                        ->get('batch_sessions');
            }
            $rs = $query->result_array();
        }
        return isset($rs[0]) && count($rs[0]) > 0 ? $rs[0] : array();
    }

}

//End of file