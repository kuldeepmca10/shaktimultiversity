<?php

class Master_model extends CI_Model {

    function save($data, $tbl = "") {
        $id = $data['id'];
        unset($data['id']);
        if ($id) {
            $data['updated'] = currentDT();
            $this->dba->update($tbl, $data, "id='" . intval($id) . "'");
        } else {
            $data['created'] = $data['updated'] = currentDT();
            $id = $this->dba->insert($tbl, $data);
        }
        return $id;
    }

    function cats($type = '') {
        $cond = "1";
        if ($type) {
            $cond = "type='$type'";
        }
        $rs = $this->dba->rows("cats", $cond . " ORDER BY display_order,id", "id,title,type,slug,icon_class,display_order");
        return $rs ? $rs : array();
    }

    function cat_dtl($id) {
        $rs = $this->dba->row("cats", "id='" . intval($id) . "'");
        return $rs ? $rs : array();
    }

    /** Teachers * */
    function teachers() {
        $cond = "1";
        $qs = escape_str(trim_array(($_GET)));

        if ($k = $qs['k']) {
            $cond.=" AND T.title LIKE '%$k%'";
        }
        if ($k = $qs['cat_id']) {
            $cond.=" AND T.cat_id='$k'";
        }

        $q['fields'] = "T.id, T.title, T.display_order, T.show_on_home, T.status, T.created, T.image, CC.title cat";

        $q['from'] = "teachers T JOIN cats CC ON T.cat_id=CC.id";
        $q['where'] = $cond;
        $q['extra'] = "ORDER BY T.display_order, T.id ASC";
        $q['countField'] = "T.id";

        $rs = $this->dba->paged_rows($q, $qs['p'], 50);

        if ($rs['result']) {
            foreach ($rs['result'] as &$r) {
                if ($r['image']) {
                    $r['image_url'] = UP_URL . "teachers/" . $r['image'];
                }
                $r['created'] = get_date($r['created']);
            }
        }

        return $rs;
    }

    function teacher_dtl($id) {
        $rs = $this->dba->row("teachers", "id='" . intval($id) . "'");
        return $rs ? $rs : array();
    }

    function all_teachers() {
        $rs = $this->dba->rows("teachers", "1 ORDER BY title", "id,title");
        return $rs ? $rs : array();
    }

    /** Faq * */
    function faq() {
        $cond = "1";
        $qs = escape_str(trim_array(($_GET)));

        if ($k = $qs['k']) {
            $cond.=" AND T.title LIKE '%$k%'";
        }
        if ($k = $qs['cat_id']) {
            $cond.=" AND T.cat_id='$k'";
        }

        $q['fields'] = "T.id, T.title, T.display_order, T.status, T.created, CC.title cat";

        $q['from'] = "faq T JOIN cats CC ON T.cat_id=CC.id";
        $q['where'] = $cond;
        $q['extra'] = "ORDER BY T.display_order, T.id ASC";
        $q['countField'] = "T.id";

        $rs = $this->dba->paged_rows($q, $qs['p'], 50);

        if ($rs['result']) {
            foreach ($rs['result'] as &$r) {
                $r['created'] = get_date($r['created']);
            }
        }
        return $rs;
    }

    function faq_dtl($id) {
        $rs = $this->dba->row("faq", "id='" . intval($id) . "'");
        return $rs ? $rs : array();
    }

    /** Testimonials * */
    function testimonials() {
        $cond = "1";
        $qs = escape_str(trim_array(($_GET)));

        if ($k = $qs['k']) {
            $cond.=" AND T.title LIKE '%$k%'";
        }
        if ($k = $qs['cat_id']) {
            $cond.=" AND T.cat_id='$k'";
        }

        $q['fields'] = "T.id, T.title, T.subtitle, T.display_order, T.status, T.publish_date, T.image";

        $q['from'] = "testimonials T";
        $q['where'] = $cond;
        $q['extra'] = "ORDER BY T.display_order, T.id ASC";
        $q['countField'] = "T.id";

        $rs = $this->dba->paged_rows($q, $qs['p'], 50);

        if ($rs['result']) {
            foreach ($rs['result'] as &$r) {
                if ($r['image']) {
                    $r['image_url'] = UP_URL . "testimonials/" . $r['image'];
                }
                $r['publish_date'] = get_date($r['publish_date']);
            }
        }

        return $rs;
    }

    function testimonial_dtl($id) {
        $rs = $this->dba->row("testimonials", "id='" . intval($id) . "'");
        return $rs ? $rs : array();
    }

    /** Shakti Center * */
    function shakticenters() {
        $cond = "1";
        $qs = escape_str(trim_array(($_GET)));

        if ($k = $qs['k']) {
            $cond.=" AND SC.title LIKE '%$k%'";
        }

        $q['fields'] = "SC.id, SC.title, SC.subtitle, SC.location, SC.display_order, SC.status, SC.image";

        $q['from'] = "shakti_centers SC";
        $q['where'] = $cond;
        $q['extra'] = "ORDER BY SC.display_order, SC.id ASC";
        $q['countField'] = "SC.id";

        $rs = $this->dba->paged_rows($q, $qs['p'], 50);

        if ($rs['result']) {
            foreach ($rs['result'] as &$r) {
                if ($r['image']) {
                    $r['image_url'] = UP_URL . "shakticenters/" . $r['image'];
                }
            }
        }

        return $rs;
    }

    function shakticenters_dtl($id) {
        $rs = $this->dba->row("shakti_centers", "id='" . intval($id) . "'");
        return $rs ? $rs : array();
    }

    /** Menues * */
    function menues($type = '') {
        $cond = "parent_id=0";
        if ($type) {
            $cond.=" AND type='$type'";
        }
        $rs = $this->dba->rows("menues", $cond . " ORDER BY display_order,id");
        return $rs ? $rs : array();
    }

    function menu_dtl($id) {
        $rs = $this->dba->row("menues", "id='" . intval($id) . "'");
        return $rs ? $rs : array();
    }

    function sub_menues($parent_id) {
        $cond = "parent_id='" . intval($parent_id) . "'";
        $rs = $this->dba->rows("menues", $cond . " ORDER BY display_order,id");
        return $rs ? $rs : array();
    }

    function cms_pages() {
        $q = "SELECT id, title FROM cms_pages WHERE id>1 AND status=1 ORDER BY display_order,id";
        $rs = $this->dba->query($q);
        return $rs ? $rs : array();
    }

    /** Quick Links * */
    function quick_links() {
        $rs = $this->dba->rows("quick_links", "1 ORDER BY display_order,id");
        return $rs ? $rs : array();
    }

    function quick_link_dtl($id) {
        $rs = $this->dba->row("quick_links", "id='" . intval($id) . "'");
        return $rs ? $rs : array();
    }

}

//End of file