<?php

class Auth_model extends CI_Model {

    function login($post) {
        $user = escape_str($post['username']);
        $pass = encrypt_text($post['password']);
        $dtl = $this->dba->row("system_users", "username='$user' OR email='$user'", "id, role_id, fname, lname, email, status, password");

        if (!$dtl) {
            return FALSE;
        }
        if ($dtl['password'] != $pass) {
            return FALSE;
        }

        return $dtl;
    }

    function checkEmail($post) {
        $user = escape_str($post['username']);
        $dtl = $this->dba->row("system_users", "username='$user' OR email='$user'", "id, role_id, fname, username, lname, email, status, password");

        if (!$dtl) {
            return FALSE;
        }

        return $dtl;
    }

}

//End of file