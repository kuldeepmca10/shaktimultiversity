<?php

class Common_model extends CI_Model {

    function roles() {
        return $this->dba->rows("system_users_roles", "1 ORDER BY id", "id, role_name");
    }

    function menues($type = 'Header') {
        $cond = "type='$type'";
        $q = "SELECT M.*, P.slug FROM menues M LEFT JOIN cms_pages P ON P.id=M.page_id WHERE $cond ORDER BY M.display_order, M.id";
        $rs = $this->dba->query($q);
        return $rs ? $rs : array();
    }

    function cats($type = 'Course') {
        $cond = "type='$type' AND status=1";
        $q = "SELECT id, title, icon_class, slug FROM cats WHERE $cond ORDER BY display_order, id";
        $rs = $this->dba->query($q);
        return $rs ? $rs : array();
    }

    function cats_detail($id, $type = 'Course') {

        $cond = " type='$type' ";
        if ($id != '') {
            $cond .= " AND id ='" . escape_str($id) . "' ";
        }
        $cond .= ' AND status=1 ORDER BY display_order, id';

        $rs = $this->dba->row("cats", $cond);

        return $rs ? $rs : array();
    }

    function cms_widget_detail($type = 'home') {
        $cond = " status=1 AND type='home'";
        $q = "SELECT id, description FROM cmswidget WHERE $cond ORDER BY id";
        $rs = $this->dba->query($q);

        return $rs ? $rs[0] : array();
    }

    function page_dtl($slug) {
        $rs = $this->dba->row("cms_pages", "slug='" . escape_str($slug) . "'");
        return $rs;
    }

    function comming_batch($courseId) {
        $q = "SELECT B.*, "
                . "(SELECT COUNT(id) FROM batch_sessions "
                . "WHERE batch_id=B.id AND is_deleted='n' AND date >= CURRENT_DATE ) sessions "
                . "FROM batches B JOIN batch_sessions BS ON BS.batch_id = B.id "
                . "WHERE B.course_id='$courseId' AND  B.start_date >= CURRENT_DATE() "
                . "AND B.is_deleted = 'n' AND BS.is_deleted = 'n' "
                . "AND  BS.date >= CURRENT_DATE() GROUP BY B.id ORDER BY B.start_date ASC LIMIT 0,1";
        $rs = $this->dba->query($q);
        return $rs;
    }

    function page_sliders($page_id, $type = 'CMS') {
        $rs = $this->dba->rows("page_slider", "page_id='" . escape_str($page_id) . "' AND type='$type' ORDER BY display_order ASC");
        
        return $rs ? $rs : array();
    }

    function selected_courses($type = '') {
//        $cond = "C.status=1 AND C.$type=1 AND B.start_date >= CURRENT_DATE() ";
        $cond = "C.status=1 AND C.$type=1 ";
        $q = "SELECT 
				C.id, C.title, C.slug, C.short_description, C.duration_hr, C.duration_mn, C.price_usd, C.mrp_usd,C.price, C.mrp, C.image, C.batch_start_date,
				T.title teacher,T.slug teacher_slug
			FROM courses C
			LEFT JOIN teachers T ON C.teacher_id=T.id
			WHERE $cond ORDER BY C.display_order, C.id DESC ";

        $rs = $this->dba->query($q);

        $aData = array();
        for ($i = 0; $i < count($rs); $i++) {
            $aBatchDetail = $this->comming_batch($rs[$i]['id']);
            $rs[$i]['batch_start_date'] = $aBatchDetail[0]['start_date'];
        }
        return $rs ? $rs : array();
    }

    function selected_happening($type = '') {
        $cond = "status=1";
        if ($type) {
            $cond .= " AND $type=1";
        }
        $q = "SELECT 
				id, title, slug, short_description, source, source_subtitle, image, publish_date
			FROM happenings
			WHERE $cond
			ORDER BY publish_date DESC";
//			ORDER BY display_order, id DESC";

        $rs = $this->dba->query($q);
        return $rs ? $rs : array();
    }

    function selected_products($type = '') {
        $cond = "P.status=1 AND P.$type=1";
        $q = "SELECT 
				P.id, P.title, P.slug, P.writer, P.mrp, P.price,P.mrp_usd, P.price_usd, GROUP_CONCAT(PI.image ORDER BY PI.display_order) images
			FROM products P
			LEFT JOIN product_images PI ON P.id=PI.pro_id
			WHERE $cond GROUP BY P.id
			ORDER BY P.display_order, P.id DESC";

        $rs = $this->dba->query($q);
        return $rs ? $rs : array();
    }

    function home_teachers() {
        $cond = "T.status=1 AND T.show_on_home=1";
        $q = "SELECT 
				T.*, C.title cat
			FROM teachers T
			JOIN cats C ON C.id=T.cat_id
			WHERE $cond
			ORDER BY T.display_order, T.id DESC";

        $rs = $this->dba->query($q);
        return $rs ? $rs : array();
    }

    function quick_links() {
        $rs = $this->dba->rows("quick_links", "1 ORDER BY display_order");
        return $rs ? $rs : array();
    }

    function testimonials() {
        $rs = $this->dba->rows("testimonials", "1 ORDER BY display_order");
        return $rs ? $rs : array();
    }

    function footer_testimonials() {
        $rs = $this->dba->rows("testimonials", "footer_display = 'yes' ORDER BY display_order");
        return $rs ? $rs : array();
    }

    function fnSidebarBannerDetail($id) {
        $rs = $this->dba->row("sidebar", "id='" . intval($id) . "'");
        return $rs ? $rs : array();
    }

    /** Banner List * */
    function fnSidebarBannerList($aPostedData) {
        $cond = "1";

        if ($k = $aPostedData['k']) {
            $cond .= " AND G.title LIKE '%$k%'";
        }
        if ($k = $aPostedData['section']) {
            $cond .= " AND G.section='$k'";
        }
        if ($k = $aPostedData['type']) {
            $cond .= " AND G.type='$k'";
        }

        $q['fields'] = "G.id, G.type, G.title, G.type, G.url, G.description, G.section, G.display_order, G.show_title, G.status, G.created_date, G.image";

        $q['from'] = "sidebar G ";
        $q['where'] = $cond;
        $q['extra'] = "ORDER BY G.display_order, G.id ASC";
        $q['countField'] = "G.id";

        $rs = $this->dba->paged_rows($q, $qs['p'], 50);

        return $rs;
    }

    function cat_dtl($slug) {
        return $this->dba->row("cats", "slug='" . escape_str($slug) . "'");
    }

    function country_phonecodes() {
        $rs = $this->dba->rows("country_phonecode", " 1=1 ");

        return $rs ? $rs : array();
    }

    function countries() {
        $rs = $this->dba->rows("countries", " status = 1 ORDER BY name");
        return $rs ? $rs : array();
    }

    function state($country) {
        $rs = $this->dba->rows("states", " country_id = '$country' AND status = 1 ORDER BY name");

        return $rs ? $rs : array();
    }

}

//End of file