<?php 
class Dba extends CI_Model {
    function row($tbl, $cond="1", $field="*", $debug=FALSE){
        $rs=$this->db->query("SELECT $field FROM $tbl WHERE $cond LIMIT 0,1;")->result_array();
        if($debug){
            echo $this->db->last_query(); die;
        }
        return isset($rs[0])?$rs[0]:array();
    }

    function val($tbl, $cond="1", $field="", $debug=FALSE){
        $rs=$this->db->query("SELECT $field FROM $tbl WHERE $cond LIMIT 0,1;")->result_array();
        if($debug){
            echo $this->db->last_query(); die;
        }
        return isset($rs[0])?$rs[0][$field]:'';
    }

    function rows($tbl, $cond="1", $field="*", $debug=FALSE){
        $rs=$this->db->query("SELECT $field FROM $tbl WHERE $cond;")->result_array();
        if($debug){
            echo $this->db->last_query(); die;
        }
        return isset($rs)?$rs:array();
    }

    function query($query, $debug=FALSE, $exit_on_debug=TRUE){
		$query=trim($query);
		$q=strtolower(substr($query,0,6));
		if($q=="delete" || $q=="insert" || $q=="update"){
			$this->db->query($query);
			$rs=$this->db->affected_rows();
		}
		else{
        	$rs=$this->db->query($query)->result_array();
		}
		
        if($debug){
            echo $this->db->last_query(); 
			if($exit_on_debug){
				die;
			}
        }
        return isset($rs)?$rs:array();
    }

    public function paged_rows($q, $pageno=1, $pagesize=20, $debug=FALSE) {
		$pageno=intval($pageno);
		if(!$pageno) {$pageno=1;}
		$pagesize=intval($pagesize);
        if(!isset($q['where'])) $q['where']="1";
        if(!isset($q['extra'])) $q['extra']="";
        if(!isset($q['countExtra'])) $q['countExtra']="";
		
		if(!$q['NO_COUNT']){
			if($q['countField']){
				$csql=trim("SELECT COUNT({$q['countField']}) as _total FROM {$q['from']} WHERE {$q['where']} {$q['countExtra']}");
				if($debug){
					echo $csql.'<br>';
				}
				$cr=$this->query($csql, FALSE, FALSE);
				$total_records=$cr[0]['_total'];
		
				$sql=trim("SELECT {$q['fields']} FROM {$q['from']} WHERE {$q['where']} {$q['extra']}");
				$md5sql="N_".md5($sql);
			}else{
				$sql=trim("SELECT SQL_CALC_FOUND_ROWS {$q['fields']} FROM {$q['from']} WHERE {$q['where']} {$q['extra']}");
				$md5sql="N_".md5($sql);
			}
		}else{
			$sql=trim("SELECT {$q['fields']} FROM {$q['from']} WHERE {$q['where']} {$q['extra']}");
		}

        $start=0;
		$start=$pagesize*($pageno-1);
		$sql=$sql." LIMIT ".$start.", ".($q['NEXT_PREV']?($pagesize+1):$pagesize).";";

        $list['result'] = $this->query($sql, $debug);
		
		if(!$q['countField'] and !$q['NO_COUNT']){
			$t=$this->query("SELECT FOUND_ROWS() AS $md5sql");
			$total_records=$t[0][$md5sql];
		}
		
        $total=count($list['result']);
		if($q['NEXT_PREV']){
			if($pageno>1){
				$list['page']['isprev']=TRUE;
			}else{
				$list['page']['isprev']=FALSE;
			}
			
			if($total>$pagesize){
				$total=$pagesize;
				$list['page']['isnext']=TRUE;
			}else{
				$list['page']['isnext']=FALSE;
			}
		}
		
        if($total) {
            $list['page']['cur_page']=intval($pageno);
            $list['page']['total_records']=intval($total_records);
            $list['page']['total']=intval($total);
            $list['page']['total_pages']=ceil((float)$total_records/(float)$pagesize);
            $list['page']['start']=$start;

            return $list;
        }else{
            return false;
		}
    }

    function insert($tbl, $data, $filter=TRUE, $debug=FALSE){
		if($filter){
			$cols=$this->filterData($tbl, $data);
			$info=array();
			if($cols){
				foreach($cols as $i=>$f){
					$info[$f]=$data[$f];
				}
			}
			if(!$info){
				return;
			}
			$this->db->insert($tbl, $info);
		}else{
			$this->db->insert($tbl, $data);
		}
		
        if($debug){
            echo $this->db->last_query(); die;
        }
        return $this->db->insert_id();
    }

    function update($tbl, $data, $cond, $filter=TRUE, $debug=FALSE){
		$this->db->where($cond, NULL, FALSE);
		if($filter){
			$cols=$this->filterData($tbl, $data);
			$info=array();
			if($cols){
				foreach($cols as $i=>$f){
					$info[$f]=$data[$f];
				}
			}
			if(!$info){
				return;
			}
			$this->db->update($tbl, $info);
		}else{
			$this->db->update($tbl, $data);
		}
		
        if($debug){
             echo $this->db->last_query(); die;
        }
        return $this->db->affected_rows();
    }

    function delete($tbl, $cond, $debug=FALSE){
        $this->db->where($cond, NULL, FALSE);
        $this->db->delete($tbl);
        if($debug){
            echo $this->db->last_query(); die;
        }
        return $this->db->affected_rows();
    }

    function filterData($tbl, $data){
        $sql = "DESCRIBE " . $tbl . ";";
        if($list = $this->query($sql)){
            $fields = array();
            foreach($list as $record)
                    $fields[] = $record['Field'];
            $rs=array_values(array_intersect($fields, array_keys($data)));

            return $rs;
        }
        return array();
    }
}

//End of file