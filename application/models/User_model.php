<?php

class User_model extends CI_Model {

    function save($data, $tbl = "users") {
        $id = $data['id'];
        unset($data['id']);
        if ($id) {
            $data['updated'] = currentDT();
            $this->dba->update($tbl, $data, "id='" . intval($id) . "'");
        } else {
            $data['created'] = $data['updated'] = currentDT();
            $id = $this->dba->insert($tbl, $data);
        }
        return $id;
    }

    function detail($id = '', $email = '', $password = '') {

        $sWhere = ' 1=1 AND is_deleted = \'n\'';

        if (isset($password) && !empty($password)) {
            $sWhere .= " AND user.password='" . trim($password) . "'";
        }

        if (isset($email) && !empty($email)) {
            $sWhere .= " AND user.email='" . trim($email) . "'";
        }
        if (isset($id) && !empty($id)) {
            $sWhere .= " AND user.id='" . intval($id) . "'";
        }


        $q['fields'] = "user.*, countries.name as country_name, states.name as state_name ";

        $q['from'] = " users user  "
                . " LEFT JOIN countries countries ON countries.id=user.country "
                . " LEFT JOIN states states ON states.id = user.state ";

        $q['where'] = $sWhere;

        $rs = $this->dba->paged_rows($q, 1, 50);

        return isset($rs['result'][0]) && !empty($rs['result'][0]) ? $rs['result'][0] : array();
    }

    function newsletter_detail($email = '') {

        $sWhere = ' 1=1 AND is_deleted = \'n\'';

        if (isset($email) && !empty($email)) {
            $sWhere .= " AND email='" . trim($email) . "'";
        }

        $rs = $this->dba->row("newsletter_subscriber", $sWhere);


        return $rs ? $rs : array();
    }

    function newsletter_list() {

        $cond = ' 1=1 AND newsletter.is_deleted = \'n\' ';

        if (isset($_REQUEST['k']) && !empty($_REQUEST['k'])) {
            $cond .= " AND newsletter.email LIKE '%" . $_REQUEST['k'] . "%' ";
        }

        $q['fields'] = " newsletter.*";

        $q['from'] = " newsletter_subscriber newsletter ";

        $q['where'] = $cond;

        $q['extra'] = " ORDER BY newsletter.email ASC";

        $q['countField'] = "newsletter.id";

        $rs = $this->dba->paged_rows($q);

        return $rs ? $rs['result'] : array();
    }

    function updateCourseEnrollment($data, $tbl = "course_enrollment") {
        $id = $data['id'];
        unset($data['id']);
        if ($id) {
            $data['updated'] = currentDT();
            $this->dba->update($tbl, $data, "id='" . intval($id) . "'");
        } else {
            $data['created'] = $data['updated'] = currentDT();
            $id = $this->dba->insert($tbl, $data);
        }
        return $id;
    }

    function updateAttandence($data, $tbl = "course_enrollment_sessions") {
        $id = $data['id'];
        unset($data['id']);
        if ($id) {
            $data['updated'] = currentDT();
            $this->dba->update($tbl, $data, "id='" . intval($id) . "'");
        } else {
            $data['created'] = $data['updated'] = currentDT();
            $id = $this->dba->insert($tbl, $data);
        }
        return $id;
    }

    function courseEnrollmentDetail($id = '', $userid = '', $courseid = '', $enrollmentID = '') {
        $sWhere = ' 1=1 ';

        if (isset($id) && !empty($id)) {
            $sWhere .= " AND id='" . intval($id) . "'";
        }

        if (isset($enrollmentID) && !empty($enrollmentID)) {
            $sWhere .= " AND enrollment_number='" . trim(strtoupper($enrollmentID)) . "'";
        }

        if (isset($courseid) && !empty($courseid)) {
            $sWhere .= " AND course_id='" . trim($courseid) . "'";
        }

        if (isset($userid) && !empty($userid)) {
            $sWhere .= " AND user_id='" . trim($userid) . "'";
        }

        $rs = $this->dba->row("course_enrollment", $sWhere);

        return $rs ? $rs : array();
    }

    function courseEnrollmentList($id = '', $userid = '', $courseid = '', $sApproved = false) {

        $cond = ' 1=1 ';

        if (isset($id) && !empty($id)) {
            $cond .= " AND CE.id='" . intval($id) . "'";
        }

        if (isset($courseid) && !empty($courseid)) {
            $cond .= " AND CE.course_id='" . trim($courseid) . "'";
        }

        if (isset($userid) && !empty($userid)) {
            $cond .= " AND CE.user_id='" . trim($userid) . "'";
        }

        $cond .= " AND ((CE.price_type = 'paid' AND CE.payment_status IN('failure', 'success', 'not required')) OR (CE.price_type = 'free')) ";

        if ($sApproved == true) {
            $cond .= " AND CE.approval_status = 'approved' ";
        }

        $q['fields'] = " C.id as courseID, C.title as courseTitle, C.image as course_image, C.slug as course_slug , batch.*, CE.* ";

        $q['from'] = " course_enrollment CE LEFT JOIN courses C ON CE.course_id = C.id "
                . " LEFT JOIN batches batch ON batch.course_id=CE.course_id ";

        $q['where'] = $cond;

        $q['extra'] = " GROUP BY CE.enrollment_number ORDER BY CE.id DESC ";

        $q['countField'] = " CE.id ";

        $rs = $this->dba->paged_rows($q);

        return $rs ? $rs['result'] : array();
    }

    function userEnrollmentDetail($id = '', $userid = '', $courseid = '', $enrollmentID = '') {
//        $cond = " user.status = '1'";
        $cond = " 1=1 ";

        if ($id != '') {
            $cond .= " AND enroll.id='$id'";
        }

        if ($userid != '') {
            $cond .= " AND user.id='$userid'";
        }

        if ($courseid != '') {
            $cond .= " AND course.id='$courseid'";
        }

        if ($enrollmentID != '') {
            $cond .= " AND enroll.enrollment_number='" . trim($enrollmentID) . "'";
        }
        $q['fields'] = "user.*, enroll.*, course.title as course_title, country.name as country_name, state.name as state_name, course.title as course_title, course.price_type as price_type, cat.title as category_name";

        $q['from'] = "users user JOIN course_enrollment enroll ON user.id=enroll.user_id LEFT JOIN courses course ON course.id=enroll.course_id "
                . "LEFT JOIN cats cat ON course.cat_id=cat.id "
                . " LEFT JOIN countries country ON country.id = user.country "
                . " LEFT JOIN states state ON state.id = user.state ";
        $q['where'] = $cond;

        $rs = $this->dba->paged_rows($q, 1, 50);
        return $rs;
    }

    function fnCourseEnrollmentStats($iUserId = '') {

        $cond = ' 1=1 ';

        if (isset($id) && !empty($id)) {
            $cond .= " AND CE.id='" . intval($id) . "'";
        }

        if (isset($courseid) && !empty($courseid)) {
            $cond .= " AND CE.course_id='" . trim($courseid) . "'";
        }

        if (isset($iUserId) && !empty($iUserId)) {
            $cond .= " AND CE.user_id='" . trim($iUserId) . "'";
        }

        $cond .= " AND ((CE.price_type = 'paid' AND CE.payment_status IN('failure','success')) OR (CE.price_type = 'free')) ";

        if ($sApproved == true) {
            $cond .= " AND CE.approval_status = 'approved' ";
        }

        $q['fields'] = " count(CE.id) as totalEnrollment, CE.approval_status, (SELECT count(CE.id) FROM course_enrollment CE WHERE 1=1 AND CE.user_id='" . trim($iUserId) . "' AND  ((CE.price_type = 'paid' AND CE.payment_status IN('failure','success')) OR (CE.price_type = 'free')))  as totalCourse ";

        $q['from'] = " course_enrollment CE ";

        $q['where'] = $cond;

        $q['extra'] = " GROUP BY CE.approval_status ORDER BY CE.id DESC ";

        $q['countField'] = " CE.id ";

        $rs = $this->dba->paged_rows($q);

        return $rs ? $rs['result'] : array();
    }

    function service_request_detail($email = '') {

        $sWhere = ' 1=1 AND is_deleted = \'n\'';

        if (isset($email) && !empty($email)) {
            $sWhere .= " AND email='" . trim($email) . "'";
        }

        $rs = $this->dba->row("service_request", $sWhere);


        return $rs ? $rs : array();
    }

    function service_request() {

        $cond = ' 1=1 AND SR.is_deleted = \'n\' ';

        if (isset($_REQUEST['k']) && !empty($_REQUEST['k'])) {
            $cond .= " AND SR.email LIKE '%" . $_REQUEST['k'] . "%' ";
        }

        $q['fields'] = " SR.*";

        $q['from'] = " service_request SR ";

        $q['where'] = $cond;

        $q['extra'] = " ORDER BY SR.email ASC";

        $q['countField'] = "SR.id";

        $rs = $this->dba->paged_rows($q);


        echo '<pre>';
        print_r($rs);
        die;

        return $rs ? $rs['result'] : array();
    }

}
