<?php 
class Files_model extends CI_Model {
	function save($data) {
		$data['ext']=strtolower(get_ext($data['filename']));
		$data['created']=currentDT();
		return $this->dba->insert("files", $data);
	}

	function lists($p, $ps) {
		$cond="ext IN('jpg','jpeg','png','gif')";
		$qs=escape_str(trim_array(($_GET)));
		
		$q['fields']="*";
		$q['from']="files";
		$q['where']=$cond;
		$q['extra']="ORDER BY id DESC";
        $q['countField']="id";

		return $this->dba->paged_rows($q, $p, $ps);
	}
}