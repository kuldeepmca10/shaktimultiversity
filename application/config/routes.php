<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'page/home';
$route['404_override'] = 'page/home'; //'common/error404';

$route['login'] = 'auth/login';
$route['register'] = 'auth/register';
$route['forgot-password'] = 'auth/forgot_password';
$route['logout'] = 'auth/logout';

$route['services'] = 'page/cat_lifetime_guidance';
$route['services/(:any)'] = 'page/cat_lifetime_guidance/$1';
$route['services/(:any)/(:num)'] = 'page/cat_lifetime_guidance/$1';
$route['service/(:any)'] = 'page/service_dtl/$1';
$route['service/(:any)/(:any)'] = 'page/service_dtl/$1/$2';

$route['(:any).html'] = 'page/page_dtl/$1';
//$route['courses'] = 'page/courses';
//$route['courses/(:num)'] = 'page/courses/$1';
//$route['courses/(:num)'] = 'page/courses/$1';
$route['courses'] = 'page/cat_courses';
$route['events-calendar'] = 'page/events_calendar';
$route['events-calendar/(:any)'] = 'page/events_calendar/$1';
$route['courses/(:any)'] = 'page/cat_courses/$1';
$route['courses/(:any)/(:any)'] = 'page/cat_courses/$1/$2';
$route['courses/(:any)/(:num)'] = 'page/cat_courses/$1/$2';
$route['courses/(:any)/(:any)/(:num)'] = 'page/cat_courses/$1/$2/$3';
$route['course/(:any)'] = 'page/course_dtl/$1';
$route['course/(:any)/(:any)'] = 'page/course_dtl/$1/$2';
$route['course-enrollment/complete'] = 'page/course_thankyou';
$route['course-enrollment/recipt/(:any)'] = 'page/download_recipt/$1';
$route['course-enrollment/brochure/(:any)'] = 'page/download_brochure/$1';
$route['payment/failure'] = 'page/payment_fail';
$route['payment/invalid'] = 'page/payment_invalid';
$route['payment/cancelled'] = 'page/payment_cancelled';

$route['free-courses'] = 'page/free_courses';
$route['free-courses/(:num)'] = 'page/free_courses/$1';

$route['free-courses'] = 'page/free_courses';
$route['offline-registeration/(:any)'] = 'page/offline_registeration/$1';
$route['offline-registeration/(:any)/(:any)'] = 'page/offline_registeration/$1/$2';
$route['registeration/(:any)/(:any)'] = 'page/registeration/$1/$2';
$route['online-registeration/(:any)'] = 'page/online_registeration/$1';
$route['online-registeration/(:any)/(:any)'] = 'page/online_registeration/$1/$2';
$route['paypaymentresponse'] = 'page/pay_payment_response';
$route['paypalpaymentresponse'] = 'page/paypal_payment_response';

$route['shop'] = 'page/products';
$route['shop/(:num)'] = 'page/products/$1';
$route['shop/(:any)'] = 'page/product_dtl/$1';

$route['state/(:num)'] = 'common/getstate/$1';

$route['news'] = 'page/news';
$route['news/(:num)'] = 'page/news/$1';
$route['news/(:any)'] = 'page/news_dtl/$1';

$route['announcement'] = 'page/announcement';
$route['announcement/(:any)'] = 'page/announcement/$1';
$route['announcement-detail/(:any)'] = 'page/announcement_detail/$1';
$route['media-gallery'] = 'page/media_gallery';
$route['image-gallery'] = 'page/image_gallery';
$route['image-gallery/(:any)'] = 'page/image_gallery/$1';
$route['gallery-detail/(:any)'] = 'page/gallery_detail/$1';

$route['video-gallery'] = 'page/video_gallery';
$route['video-gallery/(:any)'] = 'page/video_gallery/$1';

$route['testimonials'] = 'page/testimonials';
$route['testimonials/(:any)'] = 'page/testimonials/$1';
$route['testimonials/(:any)/(:num)'] = 'page/testimonials/$1/$2';

$route['master-detail/(:any)'] = 'page/master_detail/$1';

$route['faq'] = 'page/faq';
$route['shakti-centers'] = 'page/shakti_centers';

/** Front * */
/* ---------------- Dashboard Start ----------- */
$route['user/dashboard'] = 'user/dashboard';
$route['user/update-profile'] = 'user/update_profile';
$route['user/change-password'] = 'user/change_password';
$route['user/course-enrollment'] = 'user/course_enrollment';
$route['user/mark-your-attendance'] = 'user/mark_your_attendance';
$route['user/enrollment-detail/(:num)'] = 'user/course_enrollment_detail/$1';
$route['user/unsubscribe-enrollment/(:num)'] = 'user/unsubscribe_enrollment/$1';
$route['user/enrollment-feedback'] = 'user/enrollment_feedback';
$route['user/course-information/(:num)'] = 'user/course_information/$1';

/* ---------------- Dashboard End ----------- */

/* Admin * */
$route['admin'] = 'admin/auth/login';
$route['admin/p/(.*)'] = 'admin/dashboard/angular_layout';
$route['admin/logout'] = 'admin/auth/logout';

/** * */
$route['translate_uri_dashes'] = FALSE;
