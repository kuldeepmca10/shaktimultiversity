<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array();

$autoload['libraries'] = array('database', 'session');

$autoload['drivers'] = array();

$autoload['helper'] = array('url', 'form', 'file', 'util', 'lookup', 'site');

$autoload['config'] = array();

$autoload['language'] = array();

$autoload['model'] = array('dba');

date_default_timezone_set('Asia/Kolkata');
