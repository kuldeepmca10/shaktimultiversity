<?php

define('sSITE_MODE', 'live');

defined('BASEPATH') OR exit('No direct script access allowed');
if (sSITE_MODE == 'live') {
    define('URL', 'https://www.shaktimultiversity.com/');
} else if (sSITE_MODE == 'beta') {
    define('URL', 'https://www.shaktimultiversity.com/beta/');
} else {
    define('URL', 'http://localhost/shaktimultiversity/');
//    define('URL', 'https://magicinfra.com/shaktimultiversity/');
}

define('ADM_URL', URL . 'admin/');
define('THEME_URL', URL . 'theme/');
define('UP_URL', URL . 'uploads/');
define('UP_PATH', FCPATH . 'uploads/');

define('CURRENT_URL', 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
define('CURRENCY', 'Rs.');

define('sENC_KEY', '1222223322229000');

/*
  |--------------------------------------------------------------------------
  | Display Debug backtrace
  |--------------------------------------------------------------------------
  |
  | If set to TRUE, a backtrace will be displayed along with php errors. If
  | error_reporting is disabled, the backtrace will not display, regardless
  | of this setting
  |
 */
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
defined('FILE_READ_MODE') OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') OR define('DIR_WRITE_MODE', 0755);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */
defined('FOPEN_READ') OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
  |--------------------------------------------------------------------------
  | Exit Status Codes
  |--------------------------------------------------------------------------
  |
  | Used to indicate the conditions under which the script is exit()ing.
  | While there is no universal standard for error codes, there are some
  | broad conventions.  Three such conventions are mentioned below, for
  | those who wish to make use of them.  The CodeIgniter defaults were
  | chosen for the least overlap with these conventions, while still
  | leaving room for others to be defined in future versions and user
  | applications.
  |
  | The three main conventions used for determining exit status codes
  | are as follows:
  |
  |    Standard C/C++ Library (stdlibc):
  |       https://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
  |       (This link also contains other GNU-specific conventions)
  |    BSD sysexits.h:
  |       https://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
  |    Bash scripting:
  |       https://tldp.org/LDP/abs/html/exitcodes.html
  |
 */
defined('EXIT_SUCCESS') OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

defined('sSITE_ADDRESS') OR define('sSITE_ADDRESS', 'Ground Floor, D-607, West Vinod Nagar<br/>Delhi 110092');
defined('sSITE_GOOGLE_MAP_ADDRESS') OR define('sSITE_GOOGLE_MAP_ADDRESS', 'https://www.google.com/maps/place/The+Shakti+Multiversity/@28.6259854,77.3016563,17z/data=!3m1!4b1!4m5!3m4!1s0x390ce5f0165ed5af:0x6657c966dc66f458!8m2!3d28.6259854!4d77.303845?shorturl=1');
defined('sSITE_CONTACT_NO') OR define('sSITE_CONTACT_NO', '+91-7985 928 336');
defined('sSITE_EMAIL_ADDRESS_PRIMARY') OR define('sSITE_EMAIL_ADDRESS_PRIMARY', 'info@shaktimultiversity.com');
defined('sSITE_EMAIL_ADDRESS_SECONDARY') OR define('sSITE_EMAIL_ADDRESS_SECONDARY', 'shakti.multiversity@gmail.com');
defined('sSITE_FROM_EMAIL') OR define('sSITE_FROM_EMAIL', 'info@shaktimultiversity.com');
defined('sSITE_FROM_NAME') OR define('sSITE_FROM_NAME', 'Shakti Multiversity');
defined('sSITE_RECIEVE_EMAIL') OR define('sSITE_RECIEVE_EMAIL', 'shakti.multiversity@gmail.com');
defined('sMAIL_SIGNATURE') OR define('sMAIL_SIGNATURE', 'Thank you<br>Operations Team<br>The Shakti Multiversity<br><a href="' . URL . '">' . URL . '</a>');


/* ------------------------ Paypal Account Information Start ----------------------------------- */
define('sPAYPAL_MODE', 'live');
if (sPAYPAL_MODE == 'live') {
    define('sPAYPAL_PAYMENT_URL', 'https://www.paypal.com/cgi-bin/webscr');
    define('sPAYPAL_EMAIL', 'shakti.multiversity@gmail.com');
} else {
    define('sPAYPAL_PAYMENT_URL', 'https://www.sandbox.paypal.com/cgi-bin/webscr');
    define('sPAYPAL_EMAIL', 'aditya.kumar@kleward.com');
}

define('sPAYPAL_CANCEL_URL', URL . 'paypalpaymentresponse');
define('sPAYPAL_NOTIFY_URL', URL . 'paypalpaymentresponse');
//define('sPAYPAL_RETURN_URL', URL . 'paypalpaymentresponse');
define('sPAYPAL_RETURN_URL', URL . 'paypalpaymentresponse');
/* ------------------------ Paypal Account Information End ----------------------------------- */


/* ------------------------ PayU Money Account Information Start ----------------------------------- */
define('sPAYU_MERCHANT_ID', '5909346');
define('sPAYU_MERCHANT_KEY', '7lUtugoU');
define('sPAYU_SALT', 'qzC1RpcKUt');
define('sPAYU_PAYMENT_URL', 'https://secure.payu.in/_payment'); // https://test.payu.in
define('sPAYU_SUCCESS_URL', URL . 'paypaymentresponse');
define('sPAYU_FAILURE_URL', URL . 'paypaymentresponse');
/* ------------------------ PayU Money Account Information End ----------------------------------- */

/* ------------------------ Social Media Link Start ----------------------------------- */
define('sSITE_FACEBOOK_LINK', 'https://www.facebook.com/TheShaktiMultiversity');
define('sSITE_TWITTER_LINK', 'https://twitter.com/TattvaShakti');
define('sSITE_GOOGLE_PLUS_LINK', 'https://plus.google.com/u/1/112740701519860427215');
define('sSITE_YOUTUBE_LINK', 'https://www.youtube.com/channel/UClrq82QxybvPYrWA3H1OqWA');
define('sSITE_BLOG_LINK', 'https://shaktimultiversity.com/blog');
/* ------------------------ PayU Money Account Information End ----------------------------------- */


/* ------------------------ Message Start ----------------------------------- */
define('sMSG_100', '<b>Note :-</b> Please recheck <b>your availability for all the sessions</b> before choosing a batch. Rescheduling a missed session shall not be entertained.');
define('sMSG_101', 'No Course Listed Yet');
define('sMSG_102', 'No Batch Avaliable');
define('sMSG_103', 'No Batch Avaliable. If you are interested please <a href="javascript:;" onclick="sCourseEnquery({COURSEID},{COURSENAME})">contact us</a>');
define('sMSG_104', 'Registeration has been closed for this batch.');
/* ------------------------ Message End ----------------------------------- */

