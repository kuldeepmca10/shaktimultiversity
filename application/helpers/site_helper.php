<?php

/** Constrants * */
define('SITE_NAME', 'Shakti Multiversity');
define('VERSION', time());
define('USR_SESSION_NAME', 'SKUser');
define('ADM_SESSION_NAME', 'SKAdmin');

function menues($type = 'Header') {
    $ci = &get_instance();
    $rs = $ci->common->menues($type);

    $res = array();
    $c = 0;
    foreach ($rs as $r) {
        if ($r['parent_id'] == 0) {
            $res[$c] = menu_dtl($r);
            $res[$c]['submenues'] = array();
            foreach ($rs as $r1) {
                if ($r1['parent_id'] == $r['id']) {
                    $res[$c]['submenues'][] = menu_dtl($r1);
                }
            }
            $c++;
        }
    }

    return $res;
}

function menu_dtl($r) {
    $a = array('title' => $r['title'], 'link_open_type' => $r['link_open_type']);
    switch ($r['menu_type']) {
        case 'NOLINK':
            $a['link'] = 'javascript:void(0)';
            break;

        case 'CMS_PAGE':
            $a['link'] = URL . $r['slug'] . '.html';
            break;

        case 'OTHER_PAGE':
            if ($r['other_page'] == 'products') {
                $a['link'] = URL . 'shop';
            } elseif ($r['other_page'] == 'testimonials') {
                $a['link'] = URL . 'testimonials';
            } elseif ($r['other_page'] == 'gallery') {
                $a['link'] = URL . 'gallery';
            } elseif ($r['other_page'] == 'courses') {
                $a['link'] = URL . 'courses';
            } elseif ($r['other_page'] == 'free_courses') {
                $a['link'] = URL . 'free-courses';
            } elseif ($r['other_page'] == 'login') {
                $a['link'] = URL . 'login';
            } elseif ($r['other_page'] == 'registration') {
                $a['link'] = URL . 'registration';
            } elseif ($r['other_page'] == 'faq') {
                $a['link'] = URL . 'faq';
            } elseif ($r['other_page'] == 'shakti_centers') {
                $a['link'] = URL . 'shakti-centers';
            }
            break;

        case 'LINK':
            $a['link'] = $r['link'];
            break;
    }

    return $a;
}

function cats($type = 'Course') {
    $ci = &get_instance();
    $rs = $ci->common->cats($type);
    return $rs;
}

function quick_links() {
    $ci = &get_instance();
    return $ci->common->quick_links();
}

function testimonials() {
    $ci = &get_instance();
    return $ci->common->testimonials();
}

function footer_testimonials() {
    $ci = &get_instance();
    return $ci->common->footer_testimonials();
}

function sidebar_banner($id) {
    $ci = &get_instance();
    return $ci->common->fnSidebarBannerDetail($id);
}

function sidebar_banner_list($aPostedData) {
    $ci = &get_instance();
    return $ci->common->fnSidebarBannerList($aPostedData);
}

function batch_status_name($st = '') {
    $a = array('A' => 'Available', 'FF' => 'Few Seats Left', 'F' => 'Full');
    return $a[$st];
}

function countries() {
    $c = array(
        'India' => 'India',
        'Bahamas' => 'Bahamas',
        'Cambodia' => 'Cambodia',
        'Denmark' => 'Denmark',
    );
    return $c;
}

//EOF