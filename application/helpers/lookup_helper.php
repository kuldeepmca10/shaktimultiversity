<?php
function status_lookup($k='') {
	$a=array('1'=>'Active', '0'=>'Inactive');
	if($k!=='') {return $a[$k];}
	return $a;
}

function approval_lookup($k='') {
	$a=array('1'=>'Approved', '0'=>'Not Approved');
	if($k!=='') {return $a[$k];}
	return $a;
}

function song_types($k='', $top=FALSE){
	$a=array('new'=>'New', 'old'=>'Old', 'hot'=>'Hot', 'sad'=>'Sad', 'holi'=>'Holi', 'dj'=>'DJ', 'chaita'=>'Chaita', 'bhakti'=>'Bhakti', 'featured'=>'Popular');
	
	if($top){
		$a=array('new'=>'New', 'hot'=>'Hot', 'dj'=>'DJ', 'chaita'=>'Chaita');
	}
	
	if($k!=='') {return $a[$k];}
	return $a;
}

//EOF