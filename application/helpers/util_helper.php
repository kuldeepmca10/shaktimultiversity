<?php

/** Constrants * */
define('URL', base_url());
define('ADM_URL', base_url('admin') . '/');
define('THEME_URL', base_url('theme') . '/');
define('UP_URL', base_url('uploads') . '/');
define('UP_PATH', FCPATH . 'uploads' . '/');

define('CURRENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
define('CURRENCY', 'Rs.');

/** Functions * */
function pr($data) {
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

function h($data) {
    return htmlspecialchars($data);
}

function view($view, $data = array(), $layout = "default") {
    $ci = &get_instance();
    $data['page_view'] = $view;
    $data['page_title'] = isset($data['page_title']) ? $data['page_title'] : SITE_NAME;

    if ($ci->input->is_ajax_request()) {
        $html = $ci->load->view($view, $data, TRUE);
    } else {
        if ($layout) {
            $html = $ci->load->view('layouts/' . $layout, $data, TRUE);
        } else {
            $html = $ci->load->view($view, $data, TRUE);
        }
    }
    echo compress_html($html);
}

function admin_view($view, $data = array(), $layout = "admin") {
    view($view, $data, $layout);
}

function client_view($view, $data = array(), $layout = "client") {
    view($view, $data, $layout);
}

function compress_html($buffer) {
    ini_set("pcre.recursion_limit", "16777");
    $re = '%# Collapse whitespace everywhere but in blacklisted elements.
        (?>             # Match all whitespans other than single space.
          [^\S ]\s*     # Either one [\t\r\n\f\v] and zero or more ws,
        | \s{2,}        # or two or more consecutive-any-whitespace.
        ) # Note: The remaining regex consumes no text at all...
        (?=             # Ensure we are not in a blacklist tag.
          [^<]*+        # Either zero or more non-"<" {normal*}
          (?:           # Begin {(special normal*)*} construct
            <           # or a < starting a non-blacklist tag.
            (?!/?(?:textarea|pre|script)\b)
            [^<]*+      # more non-"<" {normal*}
          )*+           # Finish "unrolling-the-loop"
          (?:           # Begin alternation group.
            <           # Either a blacklist start tag.
            (?>textarea|pre|script)\b
          | \z          # or end of file.
          )             # End alternation group.
        )  # If we made it here, we are not in a blacklist tag.
        %Six';

    $new_buffer = preg_replace($re, " ", $buffer);
    if ($new_buffer === null) {
        $new_buffer = $buffer;
    }
    return $new_buffer;
}

function set_error_css($errors, $class = 'has-error', $errMsg = TRUE, $errBdr = TRUE, $parent = '') {
    if (is_array($errors)) {
        echo "<script>";
        $ids = "";
        foreach ($errors as $k => $v) {
            if ($v) {
                $ids .= "$parent [name='$k'],";
            }
            if ($errMsg) {
                echo "jQuery('$parent [name=\"$k\"]').parent().append('<div class=\"text-danger ferr\">$v</div>');";
            }
        }
        if ($ids) {
            $ids = substr($ids, 0, -1);
            if ($errBdr) {
                echo "jQuery(\"$ids\").parent().addClass('$class');";
            }
        }
        echo "</script>";
    }
}

function set_flash($msg, $key = 'tmp_flash') {
    $ci = &get_instance();
    $ci->session->set_flashdata($key, $msg);
}

function get_flash($key = 'tmp_flash') {
    $ci = &get_instance();
    return $ci->session->flashdata($key);
}

function encrypt_text($txt) {
    $ci = &get_instance();
    return hash_hmac('sha256', $txt, $ci->config->item('encryption_key'));
}

function array_to_hidden_inputs($arr) {
    $str = "";
    foreach ($arr as $k => $v) {
        $str .= '<input type="hidden" name="' . $k . '" value="' . $v . '" />';
    }
    return $str;
}

function encode($arg) {
    return base64_encode($arg);
}

function decode($arg) {
    return base64_decode($arg);
}

function set_checked($cond = FALSE) {
    if ($cond) {
        echo 'checked="checked"';
    }
}

function trim_array($arr) {
    if (!$arr) {
        return $arr;
    } foreach ($arr as &$v) {
        if (!is_array($v)) {
            $v = trim($v);
        }
    } return $arr;
}

function key_val_array($arr, $key, $val) {
    $ar = array();
    if ($arr and is_array($arr)) {
        foreach ($arr as $d) {
            if ($key and $d[$key]) {
                $ar[$d[$key]] = $d[$val];
            } else {
                $ar[] = $d[$val];
            }
        }
    }
    return $ar;
}

function group_array($arr, $key) {
    $result = array();
    foreach ($arr as $data) {
        $k = $data[$key];
        if (isset($result[$k])) {
            $result[$k][] = $data;
        } else {
            $result[$k] = array($data);
        }
    }
    return $result;
}

function make_groups_array($arr, $key1, $key2 = '') {
    $rs = array();
    foreach ($arr as $data) {
        $k = $data[$key1];

        if (isset($rs[$k])) {
            $rs[$k]['list'][] = $data;
        } else {
            $rs[$k]['group'][$key1] = $data[$key1];
            if ($key2) {
                $rs[$k]['group'][$key2] = $data[$key2];
            }
            $rs[$k]['list'] = array($data);
        }
    }
    return $rs;
}

function rename_key(&$array, $old_keys, $new_keys) {
    if (!is_array($array)) {
        return $array;
    }
    if (is_array($old_keys)) {
        foreach ($new_keys as $k => $new_key) {
            if (isset($array[$old_keys[$k]])) {
                $array[$new_key] = $array[$old_keys[$k]];
                unset($array[$old_keys[$k]]);
            }
        }
    } else {
        if (isset($array[$old_keys])) {
            $array[$new_keys] = $array[$old_keys];
            unset($array[$old_keys]);
        }
    }
    return $array;
}

function filter_post_data($post, $keys) {
    $data = array();
    foreach ($post as $k => $v) {
        if (in_array($k, $keys)) {
            $data[$k] = $v;
        }
    }
    return $data;
}

function str_short($string = '', $len = 0) {
    $string = strip_tags($string);
    $tmp = substr($string, 0, $len);
    if (strlen($string) <= $len) {
        return $string;
    }
    return trim($tmp) . ((strlen($string) <= $len) ? '' : '...');
}

function get_ext($file_name = '') {
    $fn = explode(".", $file_name);
    return end($fn);
}

function append_to_filename($fn = '', $appendTxt = '') {
    $fn = str_replace(" ", "-", $fn);
    $arr = explode(".", $fn);
    $ext = end($arr);
    $n = substr(str_replace("." . $ext, "", $fn), 0, 80);
    return $n . $appendTxt . "." . $ext;
}

function to_date_format($T) {
    if (!$T) {
        return '';
    }
    if (!is_numeric($T) and is_date_blank($T)) {
        return '';
    }
    if (!is_numeric($T)) {
        $T = strtotime($T);
    }
    return date('Y-m-d H:i:s', $T);
}

function is_date_blank($date) {
    $date = trim($date);
    if (!$date || $date == "" || $date == "0000-00-00 00:00:00" || $date == "0000-00-00") {
        return TRUE;
    }
}

function is_valid_date($d, $format = "d M Y") {
    $nd = get_date($d, FALSE, $format);
    if ($nd == $d) {
        return TRUE;
    }
    return FALSE;
}

function get_date($timestamp = FALSE, $long = FALSE, $format = '') {
    if ($timestamp) {
        if (!is_numeric($timestamp) && is_date_blank($timestamp)) {
            return;
        }
        if (!is_numeric($timestamp)) {
            $timestamp = strtotime($timestamp);
        }
        if ($format) {
            return date($format, $timestamp);
        }

        if ($long) {
            return date('d M Y - h:i A', $timestamp);
        } else {
            return date('d M Y', $timestamp);
        }
    }
}

function currentDT() {
    return date('Y-m-d H:i:s');
}

function escape_str($data, $like = FALSE) {
    $ci = &get_instance();
    if (is_array($data)) {
        $inf = array();
        foreach ($data as $field => $val) {
            if (!is_array($val)) {
                $inf[$field] = $ci->db->escape_str($val, $like);
            } else {
                $inf[$field] = $val;
            }
        }
    } else {
        $inf = $ci->db->escape_str($data, $like);
    }
    return $inf;
}

function replace_special_chars($arg, $repW = '-') {
    return preg_replace('/[^a-zA-Z0-9_\-]/', $repW, $arg);
}

function replace_non_digits($str) {
    return preg_replace("/[^0-9]/", "", $str);
}

function replace_null($arr) {
    if (!is_array($arr)) {
        if (is_null($arr)) {
            $arr = '';
        }
    } else {
        foreach ($arr as $k => $v) {
            if (is_null($v)) {
                $arr[$k] = '';
            }
        }
    }
    return $arr;
}

function encode_html($arg) {
    return str_replace(array('<', '>'), array('&lt;', '&gt;'), $arg);
}

function encode_script($arg) {
    return str_replace(array('<script>', '</script>'), array('&lt;script&gt;', '&lt;/script&gt;'), $arg);
}

function check_image_ext($filename) {
    if (in_array(strtolower(get_ext($filename)), array('jpg', 'jpeg', 'png', 'gif'))) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function check_doc_ext($filename) {
    if (in_array(strtolower(get_ext($filename)), array('pdf', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx'))) {
        return TRUE;
    } else {
        return FALSE;
    }
}

function ext_type($filename) {
    $ext = strtolower(get_ext($filename));
    $type = '';
    if ($ext == 'pdf') {
        $type = 'PDF';
    } elseif ($ext == 'doc' or $ext == 'docx') {
        $type = 'WORD';
    } elseif ($ext == 'xls' or $ext == 'xlsx') {
        $type = 'EXCEL';
    } elseif ($ext == 'ppt' or $ext == 'pptx') {
        $type = 'POWER POINT';
    } elseif (check_image_ext($filename)) {
        $type = 'IMAGE';
    }
    return $type;
}

function del_file($file) {
    if (file_exists($file)) {
        unlink($file);
    }
}

function rename_exist_file($path, $file) {
    $file = str_replace(' ', '-', $file);
    if (file_exists($path . $file)) {
        $fn = str_replace(" ", "-", $file);
        $ext = end(explode(".", $fn));
        $n = str_replace("." . $ext, "", $fn);
        $file = $n . '-' . time() . "." . $ext;
    }
    return $file;
}

function xml2array($xml) {
    return simplexml_load_string(file_get_contents($xml));
}

function is_email($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL);
}

function set_session($sname, $val, $key = '') {
    if ($key) {
        $_SESSION[$sname][$key] = $val;
    } else {
        $_SESSION[$sname] = $val;
    }
}

function get_session($sname, $key = '') {
    if ($key) {
        return isset($_SESSION[$sname][$key]) ? $_SESSION[$sname][$key] : '';
    } else {
        return isset($_SESSION[$sname]) ? $_SESSION[$sname] : '';
    }
}

function delete_session($sname, $key = '') {
    if ($key) {
        unset($_SESSION[$sname][$key]);
    } else {
        unset($_SESSION[$sname]);
    }
}

function destroy_session() {
    session_destroy();
}

function paging_links($data, $url, $activeClass = '', $sep = '', $page_limit = 10) {
    if (!$data) {
        return;
    }
    $start = $data['start'];
    $total_pages = $data['total_pages'];
    $cur_page = $data['cur_page'];

    $pages = array();
    if ($total_pages > 1) {
        $qs = http_build_query($_GET);
        if (!empty($qs)) {
            $qs = '/?' . $qs;
        }
        if (substr($url, -1, 1) == '/') {
            $url = substr($url, 0, -1);
        }
        $start = 1;
        $end = $total_pages;
        if ($total_pages > $page_limit) {
            $end = $page_limit;
        }

        $half = round($page_limit / 2);
        if ($cur_page > $half and $total_pages > $page_limit) {
            $start = $cur_page - $half + 1;
            $end = $page_limit + $cur_page - $half;
            if ($end > $total_pages) {
                $start = $total_pages - $page_limit + 1;
                $end = $total_pages;
            }
        }

        if ($cur_page > 1) {
            echo '<a href="' . $url . '/' . ($cur_page - 1) . $qs . '"> < </a>';
        }
        for ($i = $start; $i <= $end; $i++) {
            if ($cur_page == $i) {
                $link = '<a href="' . $url . '/' . $i . $qs . '" class="' . $activeClass . '">' . $i . '</a>';
            } else {
                $link = '<a href="' . $url . '/' . $i . $qs . '">' . $i . '</a>';
            }
            echo $link;
            if ($sep and $i < $total_pages) {
                echo '' . $sep . '';
            }
        }
        if ($cur_page < $total_pages) {
            echo '<a href="' . $url . '/' . ($cur_page + 1) . $qs . '"> > </a>';
        }
    }
}

function next_prev_links($data, $url) {
    if (!$data) {
        return;
    }
    $cur_page = $data['cur_page'];
    $qs = http_build_query($_GET);
    if (!empty($qs)) {
        $qs = '/?' . $qs;
    }
    if (substr($url, -1, 1) == '/') {
        $url = substr($url, 0, -1);
    }
    $prevdis = $nextdis = '';
    if (!$data['isprev']) {
        $prevdis = 'disabled';
    }
    if (!$data['isnext']) {
        $nextdis = 'disabled';
    }
    $prev = '<a class="btn btn-default ' . $prevdis . '" href="' . $url . '/' . ($data['cur_page'] - 1) . $qs . '"><i class="fa fa-chevron-left"></i></a>';
    $next = '<a class="btn btn-default ' . $nextdis . '" href="' . $url . '/' . ($data['cur_page'] + 1) . $qs . '"><i class="fa fa-chevron-right"></i></a>';
    if ($data['isprev'] or $data['isnext']) {
        echo $prev . '&nbsp;' . $next;
    }
}

function curl($url, $data = null) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $output = curl_exec($ch);
    //$info = curl_getinfo($ch);
    curl_close($ch);
    return $output;
}

function get_lat_long($address) {
    $address = str_replace(" ", "+", $address);
    $url = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $response = curl_exec($ch);
    curl_close($ch);
    $response_a = json_decode($response);
    $lat = $response_a->results[0]->geometry->location->lat;
    $long = $response_a->results[0]->geometry->location->lng;
    return array("Lat" => $lat, "Long" => $long);
}

function send_email($to, $subject, $message, $fromname = '', $fromemail = '', $smtp = TRUE, $smtpdtl = array(
    'host' => 'ssl://smtp.gmail.com',
    'user' => 'noreply.eid.naco@gmail.com',
    'pass' => 'eid@chai123',
    'port' => '465',
)
) {
    if (!$fromname) {
        $fromname = "ImagesMed Team";
    }
    if (!$fromemail) {
        $fromemail = "enquiries@imagesmed.com";
    }

    $CI = & get_instance();
    $CI->load->library('email');
    $mail = $CI->email;

    $mail->clear();

    $config['charset'] = 'utf-8';
    $config['wordwrap'] = TRUE;
    $config['mailtype'] = 'html';

    if ($smtp) {
        $config['protocol'] = "smtp";
        $config['smtp_host'] = $smtpdtl['host'];
        $config['smtp_user'] = $smtpdtl['user'];
        $config['smtp_pass'] = $smtpdtl['pass'];
        $config['smtp_port'] = $smtpdtl['port'];
        $config['_auth_smtp'] = TRUE;
        $config['newline'] = "\r\n";
        $config['crlf'] = "\r\n";
    }

    $mail->initialize($config);

    $mail->from($fromemail, $fromname);
    $mail->to($to);
    $mail->reply_to('noreply@imagesmed.com', $fromname);

    $mail->subject($subject);
    $mail->message($message);

    $res = $mail->send();
    //echo $mail->print_debugger();
    return $res;
}

function str_for_fulltext($k, $partsearch = FALSE, $allmatch = FALSE) {
    $c = array('/\(/', '/\)/', '/\*/', '/@/');
    $r = array(':', ':', ':', ':');

    if ($allmatch) {
        $words = array_unique(str_word_count($k, 1));
        if ($words) {
            foreach ($words as $w) {
                $k = str_replace($w, "+" . $w, $k);
            }
        }
    }

    $k = rtrim($k, '-+()~');

    $k = rtrim($k, '><');
    $k = preg_replace($c, $r, $k);
    if ($partsearch and substr($k, 0, 1) != '"' and substr($k, -1, 1) != '"') {
        $k = $k . '*';
    }

    return $k;
}

function json_data($res) {
    header('Content-Type: application/json');
    $res = $res ? $res : array();
    echo json_encode($res);
    die;
}

function convertTimeInMinutes($sHours, $iMinutes) {
    return $sHours * 60 + $iMinutes;
}

function convertTimeInHours($iMinutes) {
    return floor($iMinutes / 60) . ' Hr ' . ($iMinutes % 60) . ' Min ';
}

function add_jquery_ui() {
    echo '<link href="' . THEME_URL . 'plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>';
    echo '<script src="' . THEME_URL . 'plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>';
}

/** Login * */
function logged_data($type = 'admin') {
    switch ($type) {
        case 'user': return get_session(USR_SESSION_NAME);
            break;
        case 'admin': return get_session(ADM_SESSION_NAME);
            break;
    }
}

function logged_data_val($key = '', $type = 'admin') {
    $dtl = logged_data($type);
    return isset($dtl[$key]) ? $dtl[$key] : '';
}

function not_logged_res($type = 'admin') {
    if (!IS_AJAX) {
        redirect_not_logged($type);
    }
    $res = logged_data($type);
    if (!$res) {
        json_data(array('NotLogged' => TRUE));
    }
}

function redirect_logged($type = 'admin') {
    if (logged_data($type)) {
        switch ($type) {
            case 'user': redirect(URL);
                break;
            case 'admin': redirect(ADM_URL . 'dashboard');
                break;
        }
    }
}

function redirect_not_logged($type = 'admin') {
    if (!logged_data($type)) {
        switch ($type) {
            case 'user': redirect(URL);
                break;
            case 'admin': redirect(ADM_URL);
                break;
        }
    }
}

//EOF