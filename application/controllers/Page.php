<?php

class Page extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("page_model", "page");
        $this->load->model("user_model", "user");
        $this->load->library('encryption');
    }

    function checklogin() {

        $aUserData = get_session(USR_SESSION_NAME);
        $sStatus = false;
        if (isset($aUserData) && !empty($aUserData)) {
            $sStatus = true;
        } else {
            set_session('sRedirectTo', $_POST['url']);
        }

        $aResponse = array();
        $aResponse['status'] = $sStatus;
        $aResponse['icon'] = $sIcon;
        $aResponse['title'] = $sTitle;
        $aResponse['message'] = $sMessage;

        echo json_encode($aResponse);
        die;
    }

    function checkbatchavaliablity() {
        $aPostedData = trim_array($this->input->post());
        $aUserData = get_session(USR_SESSION_NAME);
        $sStatus = "0";

        $aBatchDetail = $this->page->course_batch_dtl(array('id' => $aPostedData['batch_id']));
        if (isset($aBatchDetail) && !empty($aBatchDetail)) {
            if (isset($aBatchDetail['registeration_close_before']) && !empty($aBatchDetail['registeration_close_before'])) {
                $sResCloseDate = date("Y-m-d", strtotime('+' . $aBatchDetail['registeration_close_before'] . ' hours'));
            } else {
                $sResCloseDate = date("Y-m-d");
            }

            if (isset($aBatchDetail['start_date']) && $sResCloseDate >= ($aBatchDetail['start_date'])) {
                $sMessage = sMSG_104;
                $sStatus = "2";
            } else {
                if (isset($aUserData) && !empty($aUserData)) {
                    $sStatus = true;
                    $sStatus = "1";
                } else {
                    set_session('sRedirectTo', $aPostedData['url']);
                    $sStatus = "0";
                }
            }
        }
        $aResponse = array();
        $aResponse['status'] = $sStatus;
        $aResponse['icon'] = $sIcon;
        $aResponse['title'] = $sTitle;
        $aResponse['message'] = $sMessage;
        echo json_encode($aResponse);
        die;
    }

    function home() {

        /* Check And Set Currency Start */
        $sCurrencyType = isset($_POST['currencyType']) && !empty($_POST['currencyType']) ? $_POST['currencyType'] : '';
        if ($sCurrencyType == '' && get_session('sCurrencyType') == '') {
            $sCurrencyType = 'INR';
            set_session('sCurrencyType', $sCurrencyType);
        } else if ($sCurrencyType != '') {
            set_session('sCurrencyType', $sCurrencyType);
        }
        $sCurrencyType = get_session('sCurrencyType');
        /* Check And Set Currency End */

        $aUserData = get_session(USR_SESSION_NAME);
        if (isset($aUserData['aUserData']) && !empty($aUserData['aUserData'])) {
            $aUserData = $this->user->detail($aUserData['aUserData']['id']);
        }
        /* Check And Set Currency Start */
        $sCurrencyType = get_session('sCurrencyType');

        if ($sCurrencyType == '') {
            $sCurrencyType = 'INR';
            set_session('sCurrencyType', $sCurrencyType);
        }
        /* Check And Set Currency End */

        $data['block'] = $this->common->cms_widget_detail('home');

        $data['dtl'] = $this->common->page_dtl('home');
        $data['sliders'] = $this->common->page_sliders($data['dtl']['id']);
        $data['courses'] = $this->common->selected_courses('show_on_home');
        $data['news'] = $this->common->selected_happening('show_on_home');
        $data['products'] = $this->common->selected_products('show_on_home');
        $data['teachers'] = $this->common->home_teachers();

        $data['seo_description'] = $data['dtl']['seo_description'];
        $data['seo_keywords'] = $data['dtl']['seo_keywords'];
        $page_title = $data['dtl']['seo_title'] ? $data['dtl']['seo_title'] : SITE_NAME;
        $data['page_title'] = $page_title;
        $data['sCurrencyType'] = $sCurrencyType;
        $data['aUserData'] = isset($aUserData) && !empty($aUserData) ? $aUserData : '';

        view('page/home', $data);
    }

    function page_dtl($slug = '') {
        $data['dtl'] = $this->common->page_dtl($slug);
        if (!$data['dtl']) {
            show_404();
        }
        $data['sliders'] = $this->common->page_sliders($data['dtl']['id']);

        $data['seo_description'] = $data['dtl']['seo_description'];
        $data['seo_keywords'] = $data['dtl']['seo_keywords'];
        $page_title = $data['dtl']['seo_title'] ? $data['dtl']['seo_title'] : $data['dtl']['title'];
        $data['page_title'] = $page_title;

        if ($data['dtl']['layout'] == 'CONTACT') {
            view('page/cms_contact', $data);
        } else {
            view('page/cms', $data);
        }
    }

    /** Courses * */
    function courses($p = 1) {

        $data = $this->page->courses($p);
        $data['cats'] = key_val_array($this->common->cats('Course'), 'slug', 'title');

        $data['page_title'] = 'Our Courses';
        view('page/courses', $data);
    }

    function free_courses($p = 1) {
        $data = $this->page->courses($p, 8, 0, 'free', '');
        $data['page_title'] = 'Free Courses';
        view('page/free_courses', $data);
    }

    function cat_courses($cat = '', $currency, $p = 1) {
        $aUserData = get_session(USR_SESSION_NAME);
        if (isset($aUserData['aUserData']) && !empty($aUserData['aUserData'])) {
            $aUserData = $this->user->detail($aUserData['aUserData']['id']);
        }
        /* Check And Set Currency Start */
        $sCurrencyType = isset($_POST['currencyType']) && !empty($_POST['currencyType']) ? $_POST['currencyType'] : '';
        if ($sCurrencyType == '' && get_session('sCurrencyType') == '') {
            $sCurrencyType = 'INR';
            set_session('sCurrencyType', $sCurrencyType);
        } else if ($sCurrencyType != '') {
            set_session('sCurrencyType', $sCurrencyType);
        }
        $sCurrencyType = get_session('sCurrencyType');
        /* Check And Set Currency End */

        $iCategoryID = '';
        if (isset($cat) && !empty($cat) && $cat != 'all-course') {
            $sCourseListType = 'paid';

            $cat_dtl = $this->common->cat_dtl($cat);

            if (!$cat_dtl) {
                show_404();
            }
            $data['cat'] = $cat_dtl;
            $iCategoryID = $cat_dtl['id'];
        } else if (isset($cat) && !empty($cat) && $cat == 'all-course') {
            $sTitle = 'All Courses';
            $sCourseListType = 'paid';
        } else {
            $sCourseListType = FALSE;
        }
        $sCourseListType = FALSE;
        $data = $this->page->courses($p, 6, $iCategoryID, $sCourseListType, '');
        $data['seo_description'] = isset($cat_dtl['seo_description']) && !empty($cat_dtl['seo_description']) ? $cat_dtl['seo_description'] : $sTitle;
        $data['seo_keywords'] = isset($cat_dtl['seo_keywords']) && !empty($cat_dtl['seo_keywords']) ? $cat_dtl['seo_keywords'] : $sTitle;
        $page_title = $cat_dtl['seo_title'] ? $cat_dtl['seo_title'] : $sTitle . ' Courses';
        $data['page_title'] = $page_title;
        $data['sCurrencyType'] = $sCurrencyType;
        $data['category'] = $cat;
        $data['category_name'] = isset($cat_dtl['title']) && !empty($cat_dtl['title']) ? $cat_dtl['title'] : $cat_dtl['title'];
        $data['aUserData'] = isset($aUserData) && !empty($aUserData) ? $aUserData : '';
        $data['cats'] = key_val_array($this->common->cats('Course'), 'slug', 'title');

        view('page/courses', $data);
    }

    function cat_lifetime_guidance($cat = '', $currency = '', $p = 1) {

        /* Check And Set Currency Start */
        $sCurrencyType = isset($_POST['currencyType']) && !empty($_POST['currencyType']) ? $_POST['currencyType'] : '';
        if ($sCurrencyType == '' && get_session('sCurrencyType') == '') {
            $sCurrencyType = 'INR';
            set_session('sCurrencyType', $sCurrencyType);
        } else if ($sCurrencyType != '') {
            set_session('sCurrencyType', $sCurrencyType);
        }
        $sCurrencyType = get_session('sCurrencyType');
        /* Check And Set Currency End */

        $iCategoryID = '';
        if (isset($cat) && !empty($cat) && $cat != 'all-course') {
            $sCourseListType = 'paid';

            $cat_dtl = $this->common->cat_dtl($cat);

            if (!$cat_dtl) {
                show_404();
            }
            $data['cat'] = $cat_dtl;
            $iCategoryID = $cat_dtl['id'];
        } else if (isset($cat) && !empty($cat) && $cat == 'all-course') {
            $sTitle = 'All Courses';
            $sCourseListType = 'paid';
        } else {
            $sCourseListType = FALSE;
        }
        $sCourseListType = FALSE;
        $data = $this->page->courses($p, 6, $iCategoryID, $sCourseListType, '', FALSE, $lifetime = 1);
        $data['seo_description'] = isset($cat_dtl['seo_description']) && !empty($cat_dtl['seo_description']) ? $cat_dtl['seo_description'] : $sTitle;
        $data['seo_keywords'] = isset($cat_dtl['seo_keywords']) && !empty($cat_dtl['seo_keywords']) ? $cat_dtl['seo_keywords'] : $sTitle;
        $page_title = $cat_dtl['seo_title'] ? $cat_dtl['seo_title'] : $sTitle . ' Courses';
        $data['page_title'] = $page_title;
        $data['sCurrencyType'] = $sCurrencyType;
        $data['category'] = $cat;
        $data['category_name'] = isset($cat_dtl['title']) && !empty($cat_dtl['title']) ? $cat_dtl['title'] : $cat_dtl['title'];
        $data['cats'] = key_val_array($this->common->cats('Course'), 'slug', 'title');

        view('page/service', $data);
    }

    function events_calendar($p = 1) {
        $aUserData = get_session(USR_SESSION_NAME);
        if (isset($aUserData['aUserData']) && !empty($aUserData['aUserData'])) {
            $aUserData = $this->user->detail($aUserData['aUserData']['id']);
        }

        $post = trim_array($this->input->get());
        $cat = $post['category_name'];
        $sBatchDate = isset($post['batch_date']) && !empty($post['batch_date']) ? date('Y-m-d', strtotime($post['batch_date'])) : FALSE;

        /* Check And Set Currency Start */
        $sCurrencyType = isset($_POST['currencyType']) && !empty($_POST['currencyType']) ? $_POST['currencyType'] : '';
        if ($sCurrencyType == '' && get_session('sCurrencyType') == '') {
            $sCurrencyType = 'INR';
            set_session('sCurrencyType', $sCurrencyType);
        } else if ($sCurrencyType != '') {
            set_session('sCurrencyType', $sCurrencyType);
        }
        $sCurrencyType = get_session('sCurrencyType');
        /* Check And Set Currency End */

        $iCategoryID = '';
        $sCourseListType = FALSE;
        if (isset($cat) && !empty($cat) && $cat != 'all-course') {

            if ($cat == 'free-course') {
                $sCourseListType = 'free';
            } else {
                $cat_dtl = $this->common->cat_dtl($cat);

                if (!$cat_dtl) {
                    show_404();
                }
                $data['cat'] = $cat_dtl;
                $iCategoryID = $cat_dtl['id'];
            }
        } else if (isset($cat) && !empty($cat) && $cat == 'all-course') {
            $sTitle = 'All Courses';
        }

        $data = $this->page->batch_courses($p, 8, $iCategoryID, $sCourseListType, '', $sBatchDate);
        $data['seo_description'] = isset($cat_dtl['seo_description']) && !empty($cat_dtl['seo_description']) ? $cat_dtl['seo_description'] : $sTitle;
        $data['seo_keywords'] = isset($cat_dtl['seo_keywords']) && !empty($cat_dtl['seo_keywords']) ? $cat_dtl['seo_keywords'] : $sTitle;
        $page_title = $cat_dtl['seo_title'] ? $cat_dtl['seo_title'] : $sTitle . ' Event Calendar';
        $data['page_title'] = $page_title;
        $data['sCurrencyType'] = $sCurrencyType;
        $data['category'] = $cat;
        $data['category_name'] = isset($cat_dtl['title']) && !empty($cat_dtl['title']) ? $cat_dtl['title'] : $cat_dtl['title'];
        $data['aUserData'] = isset($aUserData) && !empty($aUserData) ? $aUserData : '';

        $data['cats'] = key_val_array($this->common->cats('Course'), 'slug', 'title');
        $data['aPostedData'] = $post;
        view('page/events_calendar', $data);
    }

    function course_dtl($slug = '', $sCurrencyType = '') {
        $aUserData = get_session(USR_SESSION_NAME);
        if (isset($aUserData['aUserData']) && !empty($aUserData['aUserData'])) {
            $aUserData = $this->user->detail($aUserData['aUserData']['id']);
        }

        /* Check And Set Currency Start */
        $sCurrencyType = isset($_POST['currencyType']) && !empty($_POST['currencyType']) ? $_POST['currencyType'] : '';
        if ($sCurrencyType == '' && get_session('sCurrencyType') == '') {
            $sCurrencyType = 'INR';
            set_session('sCurrencyType', $sCurrencyType);
        } else if ($sCurrencyType != '') {
            set_session('sCurrencyType', $sCurrencyType);
        }
        $sCurrencyType = get_session('sCurrencyType');
        /* Check And Set Currency End */

        $data['dtl'] = $this->page->course_dtl($slug);

        if (!$data['dtl']) {
            show_404();
        }

        $data['sliders'] = $this->common->page_sliders($data['dtl']['id'], 'COURSE');

        $data['seo_description'] = $data['dtl']['seo_description'];
        $data['seo_keywords'] = $data['dtl']['seo_keywords'];
        $page_title = $data['dtl']['seo_title'] ? $data['dtl']['seo_title'] : $data['dtl']['title'];
        $data['page_title'] = $page_title;
        $data['sCurrencyType'] = $sCurrencyType;
        $data['slug'] = $slug;
        $data['aUserData'] = isset($aUserData) && !empty($aUserData) ? $aUserData : '';


        view('page/course_dtl', $data);
    }

    function service_dtl($slug = '', $sCurrencyType = '') {

        /* Check And Set Currency Start */
        $sCurrencyType = isset($_POST['currencyType']) && !empty($_POST['currencyType']) ? $_POST['currencyType'] : '';
        if ($sCurrencyType == '' && get_session('sCurrencyType') == '') {
            $sCurrencyType = 'INR';
            set_session('sCurrencyType', $sCurrencyType);
        } else if ($sCurrencyType != '') {
            set_session('sCurrencyType', $sCurrencyType);
        }
        $sCurrencyType = get_session('sCurrencyType');
        /* Check And Set Currency End */

        $data['dtl'] = $this->page->course_dtl($slug);

        if (!$data['dtl']) {
            show_404();
        }

        $data['sliders'] = $this->common->page_sliders($data['dtl']['id'], 'COURSE');

        $data['seo_description'] = $data['dtl']['seo_description'];
        $data['seo_keywords'] = $data['dtl']['seo_keywords'];
        $page_title = $data['dtl']['seo_title'] ? $data['dtl']['seo_title'] : $data['dtl']['title'];
        $data['page_title'] = $page_title;
        $data['sCurrencyType'] = $sCurrencyType;
        $data['slug'] = $slug;


        view('page/service_dtl', $data);
    }

    function course_thankyou() {
        $sEnrollmentId = get_session('sEnrollmentId');

        if (isset($sEnrollmentId) && !empty($sEnrollmentId)) {
            $aEnrollmentDetail = $this->user->userEnrollmentDetail($sEnrollmentId);
            $aCourseDetail = $this->page->course_dtl($slug, $aEnrollmentDetail['result'][0]['course_id']);

            if (!$aEnrollmentDetail) {
                show_404();
            }

            if ((isset($aEnrollmentDetail['result'][0]['payment_status']) && !empty($aEnrollmentDetail['result'][0]['payment_status']) && strtolower($aEnrollmentDetail['result'][0]['payment_status']) == 'success' && strtolower($aEnrollmentDetail['result'][0]['price_type']) == 'paid') ||
                    (isset($aCourseDetail['price_type']) && !empty($aCourseDetail['price_type']) && strtolower($aCourseDetail['price_type']) == 'free') || (isset($aEnrollmentDetail['result'][0]['payment_status']) && !empty($aEnrollmentDetail['result'][0]['payment_status']) && strtolower($aEnrollmentDetail['result'][0]['payment_status']) == 'not required')) {
                $aEnrollmentDetail['sliders'] = $this->common->page_sliders($aEnrollmentDetail['result'][0], 'COURSE THANK YOU');

                $aEnrollmentDetail['seo_description'] = $data['dtl']['seo_description'];
                $aEnrollmentDetail['seo_keywords'] = $data['dtl']['seo_keywords'];
                $page_title = $aEnrollmentDetail['dtl']['seo_title'] ? $aEnrollmentDetail['dtl']['seo_title'] : $aEnrollmentDetail['dtl']['title'];
                $aEnrollmentDetail['page_title'] = $page_title;
                $aEnrollmentDetail['sCurrencyType'] = $sCurrencyType;
                $aEnrollmentDetail['slug'] = $slug;
                $aEnrollmentDetail['price_type'] = isset($aEnrollmentDetail['result'][0]['price_type']) && !empty($aEnrollmentDetail['result'][0]['price_type']) ? $aEnrollmentDetail['result'][0]['price_type'] : $aCourseDetail['price_type'];

                $aEnrollmentDetail['sEnrollmentNumber'] = self::encrypt($aEnrollmentDetail['result'][0]['enrollment_number']);
                view('page/course_thankyou', $aEnrollmentDetail);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    function payment_fail() {
        $sEnrollmentId = get_session('sEnrollmentId');

        if (isset($sEnrollmentId) && !empty($sEnrollmentId)) {
            $aEnrollmentDetail = $this->user->userEnrollmentDetail($sEnrollmentId);

            if (!$aEnrollmentDetail) {
                show_404();
            }
            if (isset($aEnrollmentDetail['result'][0]['payment_status']) && !empty($aEnrollmentDetail['result'][0]['payment_status']) && strtolower($aEnrollmentDetail['result'][0]['payment_status']) == 'failure') {
                $aEnrollmentDetail['sliders'] = $this->common->page_sliders($aEnrollmentDetail['result'][0], 'COURSE THANK YOU');

                $aEnrollmentDetail['seo_description'] = $data['dtl']['seo_description'];
                $aEnrollmentDetail['seo_keywords'] = $data['dtl']['seo_keywords'];
                $page_title = $aEnrollmentDetail['dtl']['seo_title'] ? $aEnrollmentDetail['dtl']['seo_title'] : $aEnrollmentDetail['dtl']['title'];
                $aEnrollmentDetail['page_title'] = $page_title;
                $aEnrollmentDetail['sCurrencyType'] = $sCurrencyType;
                $aEnrollmentDetail['slug'] = $slug;

                view('payment/failure', $aEnrollmentDetail);
            } else {
                show_404();
            }
        } else {
            show_404();
        }
    }

    function payment_cancelled() {
        $sEnrollmentId = get_session('sEnrollmentId');

        if (isset($sEnrollmentId) && !empty($sEnrollmentId)) {
            $aEnrollmentDetail = $this->user->userEnrollmentDetail($sEnrollmentId);

            if (!$aEnrollmentDetail) {
                show_404();
            }
            view('payment/cancelled', $aEnrollmentDetail);
        } else {
            show_404();
        }
    }

    function payment_invalid() {
        $sEnrollmentId = get_session('sEnrollmentId');

        if (isset($sEnrollmentId) && !empty($sEnrollmentId)) {
            $aEnrollmentDetail = $this->user->userEnrollmentDetail($sEnrollmentId);

            if (!$aEnrollmentDetail) {
                show_404();
            }
            view('payment/invalid', $aEnrollmentDetail);
        } else {
            show_404();
        }
    }

    /** Products * */
    function products($p = 1) {

        $aUserData = get_session(USR_SESSION_NAME);
        if (isset($aUserData['aUserData']) && !empty($aUserData['aUserData'])) {
            $aUserData = $this->user->detail($aUserData['aUserData']['id']);
        }

        $data = $this->page->products($p);
        $data['page_title'] = 'Our Products';
        /* Check And Set Currency Start */
        $sCurrencyType = isset($_POST['currencyType']) && !empty($_POST['currencyType']) ? $_POST['currencyType'] : '';
        if ($sCurrencyType == '' && get_session('sCurrencyType') == '') {
            $sCurrencyType = 'INR';
            set_session('sCurrencyType', $sCurrencyType);
        } else if ($sCurrencyType != '') {
            set_session('sCurrencyType', $sCurrencyType);
        }
        $sCurrencyType = get_session('sCurrencyType');
        $data['sCurrencyType'] = $sCurrencyType;
        /* Check And Set Currency End */

        $data['aUserData'] = isset($aUserData) && !empty($aUserData) ? $aUserData : '';
        view('page/products', $data);
    }

    function product_dtl($slug = '') {

        $aUserData = get_session(USR_SESSION_NAME);
        if (isset($aUserData['aUserData']) && !empty($aUserData['aUserData'])) {
            $aUserData = $this->user->detail($aUserData['aUserData']['id']);
        }

        $data['dtl'] = $this->page->product_dtl($slug);
        if (!$data['dtl']) {
            show_404();
        }

        $data['seo_description'] = $data['dtl']['seo_description'];
        $data['seo_keywords'] = $data['dtl']['seo_keywords'];
        $page_title = $data['dtl']['seo_title'] ? $data['dtl']['seo_title'] : $data['dtl']['title'];
        $data['page_title'] = $page_title;

        /* Check And Set Currency Start */
        $sCurrencyType = isset($_POST['currencyType']) && !empty($_POST['currencyType']) ? $_POST['currencyType'] : '';
        if ($sCurrencyType == '' && get_session('sCurrencyType') == '') {
            $sCurrencyType = 'INR';
            set_session('sCurrencyType', $sCurrencyType);
        } else if ($sCurrencyType != '') {
            set_session('sCurrencyType', $sCurrencyType);
        }
        $sCurrencyType = get_session('sCurrencyType');
        $data['sCurrencyType'] = $sCurrencyType;
        /* Check And Set Currency End */

        $data['aUserData'] = isset($aUserData) && !empty($aUserData) ? $aUserData : '';

        view('page/product_dtl', $data);
    }

    /** News * */
    function news($p = 1) {
        $data = $this->page->news($p);
        $data['page_title'] = 'News';
        view('page/news', $data);
    }

    function news_dtl($slug = '') {
        $data['dtl'] = $this->page->news_dtl($slug);
        if (!$data['dtl']) {
            show_404();
        }

        $data['seo_description'] = $data['dtl']['seo_description'];
        $data['seo_keywords'] = $data['dtl']['seo_keywords'];
        $page_title = $data['dtl']['seo_title'] ? $data['dtl']['seo_title'] : $data['dtl']['title'];
        $data['page_title'] = $page_title;

        view('page/news_dtl', $data);
    }

    /** happenings * */
    function announcement($category = '') {

        $data = $this->page->happenings($slug, $p, 100, $category);

        $data['cats'] = key_val_array($this->common->cats('happenings'), 'slug', 'title');
        $data['page_title'] = 'Announcements';
        $data['category_slug'] = $category;
        view('page/announcement', $data);
    }

    /** happenings * */
    function announcement_detail($slug = '', $p = 1) {

        $data = $this->page->happenings($slug, $p, 100);

        $data['page_title'] = 'Announcement';
        $data['category_slug'] = $slug;
        view('page/announcement_detail', array(
            'aAnnouncementDetail' => isset($data['result'][0]) && !empty($data['result'][0]) ? $data['result'][0] : array())
        );
    }

    /** master * */
    function master_detail($slug) {
        $aMasterDetail = $this->page->master_dtl($slug);

        $data['course'] = $this->page->courses('', 5, '', '', $aMasterDetail['id']);
        $data['seo_description'] = $aMasterDetail['seo_description'];
        $data['seo_keywords'] = $aMasterDetail['seo_keywords'];
        $page_title = $aMasterDetail['seo_title'] ? $aMasterDetail['seo_title'] : $aMasterDetail['title'];
        $data['page_title'] = $page_title;
        $data['aMasterDetail'] = $aMasterDetail;

        view('page/master_detail', $data);
    }

    /** Gallery * */
    function image_gallery($slug = '', $p = 1) {

        $data = $this->page->gallery($slug, $p, 100, 'I');

        $data['cats'] = key_val_array($this->common->cats('Gallery'), 'slug', 'title');
        $data['page_title'] = 'Image Gallery';
        $data['category_slug'] = $slug;
        view('page/image_gallery', $data);
    }

    /** Gallery * */
    function media_gallery($slug = '', $p = 1) {

        $data = $this->page->gallery($slug, $p, 100, 'I');

        $data['cats'] = key_val_array($this->common->cats('Gallery'), 'slug', 'title');
        $data['page_title'] = 'Media Gallery';
        $data['category_slug'] = $slug;
        view('page/media_gallery', $data);
    }

    /** Gallery * */
    function video_gallery($slug = '', $p = 1) {
        $data = $this->page->gallery($slug, $p, 100, 'V');

        $data['cats'] = key_val_array($this->common->cats('Gallery'), 'slug', 'title');
        $data['page_title'] = 'Video Gallery';
        $data['category_slug'] = $slug;
        view('page/video_gallery', $data);
    }

    /** Gallery * */
    function gallery_detail($slug = '', $p = 1) {

        $data = array();
        $data['gallery'] = $this->page->gallery_detail($id, $slug, $p);
        $data['gallery_images'] = $this->page->gallery_image_detail('', $data['gallery']['id'], $p);
        $data['cats'] = key_val_array($this->common->cats('Gallery'), 'id', 'title');
        $data['page_title'] = 'Gallery';

        view('page/gallery_detail', $data);
    }

    /** Testimonials * */
    function testimonials($slug = '', $p = 1) {
        $catid = '';
        if (isset($slug) && !empty($slug) && $slug != 'all-course') {
            $category = $this->common->cat_dtl($slug);
            $catid = $category['id'];
        }
        $data = $this->page->testimonials($catid, $p);
//        $data['cats'] = $this->common->cats('Course');
        $data['cats'] = key_val_array($this->common->cats('Course'), 'slug', 'title');
        $data['page_title'] = 'Testimonials';
        $data['category_slug'] = $slug;
        view('page/testimonials', $data);
    }

    /**     * */
    function shakti_centers() {
        $data = $this->page->shakticenters($p);

        $data['page_title'] = 'Shakti Centers';
        view('page/shakti_centers', $data);
    }

    function faq() {
        $cats = $this->common->cats('FAQ');
        if ($cats) {
            foreach ($cats as &$c) {
                $c['faq'] = $this->page->faq($c['id']);
            }
        }
        $data['cats'] = array_chunk($cats, 2);

        $data['page_title'] = "FAQ's";
        view('page/faq', $data);
    }

    /** Courses * */
    function registeration($slug, $iBatchNo) {
        $aUserData = get_session(USR_SESSION_NAME);
        if (!isset($aUserData['aUserData'])) {
            redirect('login');
        }
        if (isset($aUserData['aUserData']) && !empty($aUserData['aUserData'])) {
            $aLoginUserData = $this->user->detail($aUserData['aUserData']['id']);
        }

        $this->load->library('paypal');
        $this->load->library('payu');
        $this->load->library('upload');

        $aCourseDetail = $this->page->course_dtl($slug);
        
        $aCategoryDetail = $this->common->cats_detail($aCourseDetail['cat_id']);
        $aBatchDetail = $this->page->course_batch_dtl(array('code' => $iBatchNo));

        $aCountryList = $this->common->country_phonecodes();
        if (empty($aCourseDetail) || empty($aBatchDetail)) {
            show_404();
        }

        $aUserSession = get_session(USR_SESSION_NAME);

        if (isset($_POST) && !empty($_POST)) {
//            redirect(URL . 'under-construction.html');

            $aPostedData = $_POST;
            $aUserSessionData = isset($aUserSession['aUserData']) && !empty($aUserSession['aUserData']) ? $aUserSession['aUserData'] : '';
            if (isset($aUserSessionData) && !empty($aUserSessionData)) {
                $aUserDetail = $this->user->detail($aUserSessionData['id']);

                if (isset($aUserDetail) && !empty($aUserDetail) && count($aUserDetail) > 0) {
                    $iUserID = $aUserDetail['id'];
                    $sUserName = $aUserDetail['name'];
                    $sEmailID = $aUserDetail['email'];
                } else {
                    show_404();
                }
            } else {
                $sEmailID = $aPostedData['email'];

                if (isset($sEmailID) && !empty($sEmailID)) {
                    $aUserDetail = $this->user->detail('', $sEmailID);

                    if (isset($aUserDetail) && !empty($aUserDetail) && count($aUserDetail) > 0) {
                        $iUserID = $aUserDetail['id'];
                        $sUserName = $aUserDetail['name'];
                        $sEmailID = $aUserDetail['email'];
                    } else {
                        $aUserData = array();
                        $sPassword = $this::generateRandomString(10);
                        $aUserData['name'] = $aPostedData['name'];
                        $aUserData['email'] = $aPostedData['email'];
                        $aUserData['password'] = encrypt_text($sPassword);
                        $aUserData['email'] = $aPostedData['email'];
                        $aUserData['dob'] = $aPostedData['dob'];
                        $aUserData['gender'] = $aPostedData['gender'];
                        $aUserData['address'] = $aPostedData['address'];
                        $aUserData['city'] = $aPostedData['city'];
                        $aUserData['state'] = $aPostedData['state'];
                        $aUserData['country'] = $aPostedData['country'];
                        $aUserData['is_deleted'] = 'n';
                        $aUserData['status'] = '1';

                        $iUserID = $this->user->save($aPostedData);
                        $sUserName = $aPostedData['name'];
                        $sEmailID = $aPostedData['email'];
                        /* Send Email Start */
                        $sSendEmail = true;
                        if (isset($iUserID) && !empty($iUserID) && $sSendEmail == true) {
                            $sFromEmail = sSITE_FROM_EMAIL;
                            $sFromName = sSITE_FROM_NAME;
                            $sEmailTo = $aPostedData['email'];
                            $aUserEmailData = array('name' => $aPostedData['name'], 'email' => $sEmailTo, 'password' => $sPassword);
                            $sHTMLContent = $this->load->view('templates/mail/register.php', $aUserEmailData, TRUE);

//Load email library 
                            $this->load->library('email');
                            $this->email->from($sFromEmail, $sFromName);
                            $this->email->to($sEmailTo);
                            $this->email->subject('Welcome to Shakti Multiversity');
                            $this->email->message($sHTMLContent);

//Send mail 
                            if ($this->email->send()) {
                                $sMessage = 'You have registered successfully. Please check your email for next step';
                            } else {
                                $sMessage = 'You have registered successfully. We are unable to share password on your email id';
                            }
                        }
                        /* Send Email End */
                    }
                }
            }

            if (isset($iUserID) && !empty($iUserID)) {

                $aCourseEnrollmentData = array();
                /* Genrate Formated Enrollment Number Start */
                $sNameCode = isset($sUserName) && !empty($sUserName) ? strtoupper(substr($sUserName, 0, 2)) : '';
                /* Genrate Formated Enrollment Number end */

                /* Get Selected Batch Information Start */
                $sBatchID = $aBatchDetail['id'];
                $sBatchTitle = $aBatchDetail['title'];
                $sBatchCode = $aBatchDetail['code'];
                $sBatchDate = $aBatchDetail['start_date'];
                $sBatchDays = $aBatchDetail['batch_days'];
                $sBatchTime = $aBatchDetail['batch_time'];
                $sDurationHours = $aBatchDetail['duration_hr'];
                $sDurationMin = $aBatchDetail['duration_min'];
                $sBatchStatus = $aBatchDetail['batch_status'];
                /* Get Selected Batch Information Start */
                $sAdminLogin = isset($aUserSession['sAdminLogin']) && !empty($aUserSession['sAdminLogin']) ? $aUserSession['sAdminLogin'] : false;

                $aCourseEnrollmentData['user_id'] = $iUserID;
                $aCourseEnrollmentData['course_id'] = $aCourseDetail['id'];
                $aCourseEnrollmentData['price_type'] = $aCourseDetail['price_type'];
                $aCourseEnrollmentData['batch_id'] = $sBatchID;
                $aCourseEnrollmentData['batch_title'] = $sBatchTitle;
                $aCourseEnrollmentData['batch_no'] = $iBatchNo;
                $aCourseEnrollmentData['batch_date'] = get_date($sBatchDate, false, 'Y-m-d');
                $aCourseEnrollmentData['batch_time'] = $sBatchTime;
                if ($sAdminLogin == true) {
                    $aCourseEnrollmentData['note'] = $aPostedData['note'];
                    $aCourseEnrollmentData['payment_status'] = 'not required';
                } else {
                    $aCourseEnrollmentData['payment_status'] = 'initiated';
                }
                $aCourseEnrollmentData['attandance'] = 'n';
                $aCourseEnrollmentData['email'] = $sEmailID;
                $aCourseEnrollmentData['status'] = '1';
                $aCourseEnrollmentData['deleted'] = 'n';


                $iEnrollmentID = $this->user->updateCourseEnrollment($aCourseEnrollmentData);

                if (isset($iEnrollmentID) && !empty($iEnrollmentID)) {

                    $aCourseEnrollmentData = array();
                    $aCourseEnrollmentData['id'] = $iEnrollmentID;

                    /* Genrate Formated Enrollment Number Start */
                    $sNameCode = isset($sUserName) && !empty($sUserName) ? strtoupper(substr($sUserName, 0, 2)) : '';
                    $sEnrollmentNumber = 'SM' . date('y') . $sNameCode . date('m') . date('d') . $iEnrollmentID;

                    /* Genrate Formated Enrollment Number end */
                    $aCourseEnrollmentData['enrollment_number'] = $sEnrollmentNumber;
                    $iEnrollmentID = $this->user->updateCourseEnrollment($aCourseEnrollmentData);

                    if ($iEnrollmentID) {
                        $sCurrencyType = get_session('sCurrencyType');

                        if ($sAdminLogin == true) {

                            /* Genrate Formated Enrollment Number end */

                            $aEnrollmentDetail = $this->user->userEnrollmentDetail($iEnrollmentID);
                            $aEnrollmentDetail = isset($aEnrollmentDetail['result'][0]) && !empty($aEnrollmentDetail['result'][0]) ? $aEnrollmentDetail['result'][0] : '';

                            $sSendEmail = true;
                            if ($sSendEmail == true && $aEnrollmentDetail != '') {
                                $sFromEmail = sSITE_FROM_EMAIL;
                                $sFromName = sSITE_FROM_NAME;
                                $this->load->library('email');
                                $this->email->set_newline("\r\n");

                                $this->email->from($sFromEmail, $sFromName);
                                $subject = 'Enrollment Confirmation of ' . $aEnrollmentDetail['course_title'];
                                $this->email->to($aEnrollmentDetail['email']);  // replace it with receiver mail id
                                $this->email->subject($subject); // replace it with relevant subject

                                $body = $this->load->view('templates/mail/paymentsucess.php', $aEnrollmentDetail, TRUE);

                                $this->email->message($body);
                                $this->email->send();

                                /* --------------------- Send Notification To Admin Start */
                                $aUserEmailData = array('course_name' => $aEnrollmentDetail['course_title'], 'aSessionDetail' => $aSessionDetail, 'comment' => $aEnrollmentDetail['comment'], 'status' => $status, 'email' => $aEnrollmentDetail['email']);
                                $sHTMLContent = $this->load->view('templates/mail/paymentsucess_admin.php', $aUserEmailData, TRUE);

//                                $sEmailTo = 'kuldeep.mca10@gmail.com';
                                $sEmailTo = sSITE_RECIEVE_EMAIL;
                                $this->load->library('email');
                                $this->email->from($sFromEmail, $sFromName);
                                $this->email->to($sEmailTo);
                                $this->email->subject('Enrollment Confirmation of ' . $aEnrollmentDetail['course_title']);
                                $this->email->message($sHTMLContent);
                                $this->email->send();
                                /* --------------------- Send Notification To Admin End */
                                set_session('sEnrollmentId', $iEnrollmentID);
                                redirect(URL . 'course-enrollment/complete');
                            }
                        } else if (isset($aCourseDetail['price_type']) && !empty($aCourseDetail['price_type']) && strtoupper($aCourseDetail['price_type']) == 'PAID' && $sAdminLogin == false) {
                            $aCourseEnrollmentData = array();
                            $aCourseEnrollmentData['id'] = $iEnrollmentID;
                            $aCourseEnrollmentData['payment_status'] = 'pending';
                            $aCourseEnrollmentData['price_type'] = $aCourseDetail['price_type'];
                            $aCourseEnrollmentData['course_fees'] = (strtoupper($sCurrencyType) == 'USD') ? $aCourseDetail['price_usd'] : $aCourseDetail['price'];
                            $aCourseEnrollmentData['currency_type'] = $sCurrencyType;

                            $sEnrollmentUpdated = $this->user->updateCourseEnrollment($aCourseEnrollmentData);

                            if (isset($sCurrencyType) && !empty($sCurrencyType) && strtoupper($sCurrencyType) == 'USD') {
                                set_session('sEnrollmentId', $iEnrollmentID);
                                if (isset($aLoginUserData) && !empty($aLoginUserData) && $aLoginUserData['isLifetimeMember'] == true) {
                                    $aPayableAmount = $aCourseDetail['price_usd'];
                                } else {
                                    $aPayableAmount = $aCourseDetail['mrp_usd'];
                                }
                                $aPaymentData = array();
                                $aPaymentData['item_name'] = $aCourseDetail['title'];
                                $aPaymentData['item_number'] = $sEnrollmentNumber;
                                $aPaymentData['payment_status'] = '';
                                $aPaymentData['payment_amount'] = $aPayableAmount;
                                $aPaymentData['amount'] = $aCourseDetail['price_usd'];
                                $aPaymentData['currency_code'] = 'USD';
                                $aPaymentData['cmd'] = '_xclick';
//                            $aPaymentData['no_note'] = '1';
                                $aPaymentData['lc'] = 'UK';
                                $aPaymentData['txn_id'] = $iEnrollmentID;
                                $aPaymentData['receiver_email'] = 'kuldeep.mca10@gmail.com';
                                $aPaymentData['payer_email'] = '';
                                $aPaymentData['custom'] = '';
                                $this->paypal->makepayment($aPaymentData);
                            } else {
                                if (isset($aLoginUserData) && !empty($aLoginUserData) && $aLoginUserData['isLifetimeMember'] == true) {
                                    $aPayableAmount = $aCourseDetail['price'];
                                } else {
                                    $aPayableAmount = $aCourseDetail['mrp'];
                                }
                                set_session('sEnrollmentId', $iEnrollmentID);
                                $aPaymentData = array();
                                $aPaymentData['transction_id'] = $sEnrollmentNumber;
                                $aPaymentData['amount'] = $aPayableAmount;
                                $aPaymentData['first_name'] = $sUserName;
                                $aPaymentData['email'] = $sEmailID;
                                $aPaymentData['phone'] = $aPostedData['contact_no'];
                                $aPaymentData['productinfo'] = $aCourseDetail['title'];

                                $sHashData = $this->payu->makepayment($aPaymentData);
//                            redirect(URL . 'course/thank-you');
                            }
                        } else {
                            /* Genrate Formated Enrollment Number end */
                            $aCourseEnrollmentData = array();
                            $aCourseEnrollmentData['id'] = $iEnrollmentID;
                            $aCourseEnrollmentData['payment_status'] = 'success';
                            $aCourseEnrollmentData['price_type'] = $aCourseDetail['price_type'];
                            $aCourseEnrollmentData['course_fees'] = 0;
                            $sEnrollmentUpdated = $this->user->updateCourseEnrollment($aCourseEnrollmentData);

                            $aEnrollmentDetail = $this->user->userEnrollmentDetail($iEnrollmentID);
                            $aEnrollmentDetail = isset($aEnrollmentDetail['result'][0]) && !empty($aEnrollmentDetail['result'][0]) ? $aEnrollmentDetail['result'][0] : '';

                            $sSendEmail = true;

                            if ($sSendEmail == true && $aEnrollmentDetail != '') {
                                $sFromEmail = sSITE_FROM_EMAIL;
                                $sFromName = sSITE_FROM_NAME;
//                                    $config = Array(
//                                        'protocol' => 'sendmail',
//                                        'smtp_host' => 'mail.shaktimultiversity.com',
//                                        'smtp_port' => 25,
//                                        'smtp_user' => 'admin@shaktimultiversity.com',
//                                        'smtp_pass' => 'Admin@123456',
//                                        'smtp_timeout' => '4',
//                                        'mailtype' => 'html',
//                                        'charset' => 'iso-8859-1'
//                                    );
//                                    $this->load->library('email', $config);
                                $this->load->library('email');
                                $this->email->set_newline("\r\n");

                                $this->email->from($sFromEmail, $sFromName);
                                $subject = 'Enrollment Confirmation of ' . $aEnrollmentDetail['course_title'];
                                $this->email->to($aEnrollmentDetail['email']);  // replace it with receiver mail id
                                $this->email->subject($subject); // replace it with relevant subject

                                $body = $this->load->view('templates/mail/paymentsucess.php', $aEnrollmentDetail, TRUE);

                                $this->email->message($body);
                                $this->email->send();

                                /* --------------------- Send Notification To Admin Start */
                                $aUserEmailData = array('course_name' => $aEnrollmentDetail['course_title'], 'aSessionDetail' => $aSessionDetail, 'comment' => $aEnrollmentDetail['comment'], 'status' => $status, 'email' => $aEnrollmentDetail['email']);
                                $sHTMLContent = $this->load->view('templates/mail/paymentsucess_admin.php', $aUserEmailData, TRUE);

//                                $sEmailTo = 'kuldeep.mca10@gmail.com';
                                $sEmailTo = sSITE_RECIEVE_EMAIL;
                                $this->load->library('email');
                                $this->email->from($sFromEmail, $sFromName);
                                $this->email->to($sEmailTo);
                                $this->email->subject('Enrollment Confirmation of ' . $aEnrollmentDetail['course_title']);
                                $this->email->message($sHTMLContent);
                                $this->email->send();
                                /* --------------------- Send Notification To Admin End */
                            }
//                            self::genrateCourseReciptPdf($aData);

                            set_session('sEnrollmentId', $iEnrollmentID);
                            redirect(URL . 'course-enrollment/complete');
                        }
                    }
                }
            }
        }
        $sCurrencyType = get_session('sCurrencyType');
        $data['CSS'] = array('theme/front/css/registration.css?v=' . VERSION);

        $data['aCountries'] = $this->common->countries();
        $data['aCourseDetail'] = $aCourseDetail;
        $data['aCategoryDetail'] = $aCategoryDetail;
        $data['aBatchDetail'] = $aBatchDetail;
        $data['aCountryList'] = $aCountryList;
        $data['sCurrencyType'] = $sCurrencyType;
        $data['page_title'] = 'Registeration';
        $data['aLoginUserData'] = isset($aLoginUserData) && !empty($aLoginUserData) ? $aLoginUserData : '';

        view('page/registeration', $data);
    }

    /** Courses * */
    function paypal_payment_response() {
        if (!$_REQUEST) {
            show_404();
        } else {

            $status = $_REQUEST["st"];

            if (isset($_REQUEST['item_number']) && !empty($_REQUEST['item_number']) && isset($status) && !empty($status)) {

                $aEnrollmentDetail = $this->user->userEnrollmentDetail('', '', '', $_REQUEST['item_number']);

                if (isset($aEnrollmentDetail['result'][0]) && !empty($aEnrollmentDetail['result'][0])) {
                    $aEnrollmentDetail = $aEnrollmentDetail['result'][0];

                    $aUserData = get_session(USR_SESSION_NAME);

                    if (isset($aEnrollmentDetail) && !empty($aEnrollmentDetail)) {
                        $aEnrollmentPaymentData = array();
                        $aEnrollmentPaymentData['id'] = $aEnrollmentDetail['id'];
                        $aEnrollmentPaymentData['payment_gateway_name'] = 'paypal';
                        $aEnrollmentPaymentData['payment_gateway_id'] = $_REQUEST['tx'];
                        $aEnrollmentPaymentData['payment_gateway_bank_ref_num'] = $_REQUEST['tx'];
                        $aEnrollmentPaymentData['payment_gateway_response'] = json_encode($_REQUEST);
                        $aEnrollmentPaymentData['payment_gateway_hit_count'] = $aEnrollmentDetail['payment_gateway_hit_count'] + 1;
                        $aEnrollmentPaymentData['payment_gateway_hit_date'] = currentDT();
                        $aEnrollmentPaymentData['payment_gateway_status'] = strtolower($status);


                        $sDetailUpdated = $this->user->updateCourseEnrollment($aEnrollmentPaymentData);

                        $sPaymentStatus = '';


                        $aEnrollmentPaymentData = array();
                        $aEnrollmentPaymentData['id'] = $aEnrollmentDetail['id'];
                        if (isset($aEnrollmentDetail['payment_status']) && !empty($aEnrollmentDetail['payment_status']) && $aEnrollmentDetail['payment_status'] == 'pending') {
                            set_session('sEnrollmentId', $aEnrollmentDetail['id']);

                            if (isset($status) && !empty($status) && strtolower($status) == 'completed') {

                                $sSendEmail = true;

                                if ($sSendEmail == true) {
                                    $sFromEmail = sSITE_FROM_EMAIL;
                                    $sFromName = sSITE_FROM_NAME;

//                                    $config = Array(
//                                        'protocol' => 'sendmail',
//                                        'smtp_host' => 'mail.shaktimultiversity.com',
//                                        'smtp_port' => 25,
//                                        'smtp_user' => 'admin@shaktimultiversity.com',
//                                        'smtp_pass' => 'Admin@123456',
//                                        'smtp_timeout' => '4',
//                                        'mailtype' => 'html',
//                                        'charset' => 'iso-8859-1'
//                                    );
//
//                                    $this->load->library('email', $config);

                                    $this->load->library('email');
                                    $this->email->set_newline("\r\n");
                                    $this->email->from($sFromEmail, $sFromName);
                                    $subject = 'Enrollment Confirmation of ' . $aEnrollmentDetail['course_title'];
                                    $this->email->to($aEnrollmentDetail['email']);  // replace it with receiver mail id
                                    $this->email->subject($subject); // replace it with relevant subject

                                    $body = $this->load->view('templates/mail/paymentsucess.php', $aEnrollmentDetail, TRUE);

                                    $this->email->message($body);
                                    $this->email->send();

                                    /* --------------------- Send Notification To Admin Start */
                                    $aUserEmailData = array('course_name' => $aEnrollmentDetail['course_title'], 'aSessionDetail' => $aSessionDetail, 'comment' => $aEnrollmentDetail['comment'], 'status' => $status, 'email' => $aEnrollmentDetail['email']);
                                    $sHTMLContent = $this->load->view('templates/mail/paymentsucess_admin.php', $aUserEmailData, TRUE);

//                                    $sEmailTo = 'kuldeep.mca10@gmail.com';
                                    $sEmailTo = sSITE_RECIEVE_EMAIL;
                                    $this->load->library('email');
                                    $this->email->from($sFromEmail, $sFromName);
                                    $this->email->to($sEmailTo);
                                    $this->email->subject('Enrollment Confirmation of ' . $aEnrollmentDetail['course_title']);
                                    $this->email->message($sHTMLContent);
                                    $this->email->send();
                                    /* --------------------- Send Notification To Admin End */
                                }
                                $sPaymentStatus = 'success';
                                $sPaymentPageURL = 'course-enrollment/complete';
                            } else if (isset($status) && !empty($status) && strtolower($status) == 'failure') {
                                $sPaymentStatus = 'failure';
                                $sPaymentPageURL = 'payment/failure';
                            } else {
                                $sPaymentStatus = 'failure';
                                $sPaymentPageURL = 'payment/failure';
                            }
                            $aEnrollmentPaymentData['payment_status'] = $sPaymentStatus;
                            $sDetailUpdated = $this->user->updateCourseEnrollment($aEnrollmentPaymentData);
                        } else {
                            if (isset($aEnrollmentDetail['status']) && !empty($aEnrollmentDetail['status']) && $aEnrollmentDetail['status'] == 'success') {
                                set_session('sEnrollmentId', $aEnrollmentDetail['id']);
                                $sPaymentStatus = 'success';
                                $sPaymentPageURL = 'course-enrollment/complete';
                            } else {
                                $sPaymentStatus = 'failure';
                                $sPaymentPageURL = 'payment/failure';
                            }

//                            $sPaymentStatus = 'failure';
//                            $sPaymentPageURL = 'payment/invalid';
                        }
                    }
                } else {
                    $sPaymentPageURL = 'payment/invalid';
                    show_404();
                }
                redirect(URL . $sPaymentPageURL);
            } else {
                show_404();
            }
        }
        die;
    }

    function pay_payment_response() {

        if (!$_REQUEST) {
            show_404();
        } else {

            $status = $_REQUEST["status"];
            $firstname = $_REQUEST["firstname"];
            $amount = $_REQUEST["amount"];
            $txnid = $_REQUEST["txnid"];
            $posted_hash = $_REQUEST["hash"];
            $key = $_REQUEST["key"];
            $productinfo = $_REQUEST["productinfo"];
            $email = $_REQUEST["email"];
            $salt = sPAYU_SALT;

            if (isset($_REQUEST["additionalCharges"])) {
                $additionalCharges = $_REQUEST["additionalCharges"];
                $retHashSeq = $additionalCharges . '|' . $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
            } else {
                $retHashSeq = $salt . '|' . $status . '|||||||||||' . $email . '|' . $firstname . '|' . $productinfo . '|' . $amount . '|' . $txnid . '|' . $key;
            }
            $hash = hash("sha512", $retHashSeq);

            $aEnrollmentDetail = $this->user->userEnrollmentDetail('', '', '', $_REQUEST['txnid']);
            $aEnrollmentDetail = isset($aEnrollmentDetail['result'][0]) && !empty($aEnrollmentDetail['result'][0]) ? $aEnrollmentDetail['result'][0] : '';
            if (isset($aEnrollmentDetail) && !empty($aEnrollmentDetail)) {
                if ($hash != $posted_hash) {
//            echo "Invalid Transaction. Please try again";
                    $sPaymentStatus = 'failure';
                    $aEnrollmentPaymentData = array();
                    $aEnrollmentPaymentData['id'] = $aEnrollmentDetail['id'];
                    $aEnrollmentPaymentData['payment_status'] = $sPaymentStatus;
                    $aEnrollmentPaymentData['payment_gateway_status'] = 'invalid';
                    $aEnrollmentPaymentData['payment_gateway_error'] = 'invalid transaction';
                    $sDetailUpdated = $this->user->updateCourseEnrollment($aEnrollmentPaymentData);
                    $sPaymentPageURL = 'payment/invalid';
                    view($sPaymentPageURL);
                } else {
                    $aUserData = get_session(USR_SESSION_NAME);


                    if (isset($aEnrollmentDetail) && !empty($aEnrollmentDetail)) {
                        $aEnrollmentPaymentData = array();
                        $aEnrollmentPaymentData['id'] = $aEnrollmentDetail['id'];
                        $aEnrollmentPaymentData['payment_gateway_name'] = 'payu';
                        $aEnrollmentPaymentData['payment_gateway_id'] = $_REQUEST['mihpayid'];
                        $aEnrollmentPaymentData['payment_gateway_bank_ref_num'] = $_REQUEST['bank_ref_num'];
                        $aEnrollmentPaymentData['payment_gateway_response'] = json_encode($_REQUEST);
                        $aEnrollmentPaymentData['payment_gateway_mode'] = $_REQUEST['mode'];
                        $aEnrollmentPaymentData['payment_gateway_error'] = isset($_REQUEST['mihpayid']) && !empty($_REQUEST['mihpayid']) ? $_REQUEST['mihpayid'] : $_REQUEST['field9'];
                        $aEnrollmentPaymentData['payment_gateway_hit_count'] = $aEnrollmentDetail['payment_gateway_hit_count'] + 1;
                        $aEnrollmentPaymentData['payment_gateway_hit_date'] = currentDT();
                        $aEnrollmentPaymentData['payment_gateway_status'] = $_REQUEST['status'];
                        $aEnrollmentPaymentData['payment_status'] = $_REQUEST['status'];

                        $sDetailUpdated = $this->user->updateCourseEnrollment($aEnrollmentPaymentData);

                        $sPaymentStatus = '';

                        $aEnrollmentPaymentData = array();
                        $aEnrollmentPaymentData['id'] = $aEnrollmentDetail['id'];
                        if (isset($aEnrollmentDetail['payment_status']) && !empty($aEnrollmentDetail['payment_status']) && $aEnrollmentDetail['payment_status'] == 'pending') {
                            set_session('sEnrollmentId', $aEnrollmentDetail['id']);

                            if (isset($status) && !empty($status) && strtolower($status) == 'success') {
                                $sSendEmail = true;

                                if ($sSendEmail == true) {
                                    $sFromEmail = sSITE_FROM_EMAIL;
                                    $sFromName = sSITE_FROM_NAME;
//                                    $config = Array(
//                                        'protocol' => 'sendmail',
//                                        'smtp_host' => 'mail.shaktimultiversity.com',
//                                        'smtp_port' => 25,
//                                        'smtp_user' => 'admin@shaktimultiversity.com',
//                                        'smtp_pass' => 'Admin@123456',
//                                        'smtp_timeout' => '4',
//                                        'mailtype' => 'html',
//                                        'charset' => 'iso-8859-1'
//                                    );
//                                    $this->load->library('email', $config);
                                    $this->load->library('email');
                                    $this->email->set_newline("\r\n");

                                    $this->email->from($sFromEmail, $sFromName);
                                    $subject = 'Enrollment Confirmation of ' . $aEnrollmentDetail['course_title'];
                                    $this->email->to($aEnrollmentDetail['email']);  // replace it with receiver mail id
                                    $this->email->subject($subject); // replace it with relevant subject
                                    $body = $this->load->view('templates/mail/paymentsucess.php', $aEnrollmentDetail, TRUE);
                                    $this->email->message($body);
                                    $this->email->send();

                                    /* --------------------- Send Notification To Admin Start */
                                    $aUserEmailData = array('course_name' => $aEnrollmentDetail['course_title'], 'aSessionDetail' => $aSessionDetail, 'comment' => $aEnrollmentDetail['comment'], 'status' => $status, 'email' => $aEnrollmentDetail['email']);
                                    $sHTMLContent = $this->load->view('templates/mail/paymentsucess_admin.php', $aUserEmailData, TRUE);

//                                    $sEmailTo = 'kuldeep.mca10@gmail.com';
                                    $sEmailTo = sSITE_RECIEVE_EMAIL;
                                    $this->load->library('email');
                                    $this->email->from($sFromEmail, $sFromName);
                                    $this->email->to($sEmailTo);
                                    $this->email->subject('Enrollment Confirmation of ' . $aEnrollmentDetail['course_title']);
                                    $this->email->message($sHTMLContent);
                                    $this->email->send();
                                    /* --------------------- Send Notification To Admin End */
                                }
                                $sPaymentStatus = 'success';
                                $sPaymentPageURL = 'course-enrollment/complete';
                            } else if (isset($status) && !empty($status) && strtolower($status) == 'failure' && strtolower($_REQUEST["field9"]) == 'cancelled by user') {
                                $sPaymentStatus = 'failure';
                                $sPaymentPageURL = 'payment/cancelled';
                            } else {
                                $sPaymentStatus = 'failure';
                                $sPaymentPageURL = 'payment/failure';
                            }
                            $aEnrollmentPaymentData['payment_status'] = $sPaymentStatus;
                            $sDetailUpdated = $this->user->updateCourseEnrollment($aEnrollmentPaymentData);
                        } else {
                            if (isset($aEnrollmentDetail['status']) && !empty($aEnrollmentDetail['status']) && $aEnrollmentDetail['status'] == 'success') {
                                set_session('sEnrollmentId', $aEnrollmentDetail['id']);
                                $sPaymentStatus = 'success';
                                $sPaymentPageURL = 'course-enrollment/complete';
                            } else {
                                $sPaymentStatus = 'failure';
                                $sPaymentPageURL = 'payment/failure';
                            }

//                            $sPaymentStatus = 'failure';
//                            $sPaymentPageURL = 'payment/invalid';
                        }
                        redirect(URL . $sPaymentPageURL);
                    }
                }
            } else {
                show_404();
            }
        }

        die;
    }

    public function genrateCourseReciptPdf($aData) {

        if (isset($aData) && !empty($aData) && is_array($aData)) {
            $filename = $aData['sEnrollmentId'] . "." . "pdf";
            $this->load->library('m_pdf');
            $html = $this->load->view('templates/pdf/recipt', $aData, true);

            $stylesheet = file_get_contents(URL . '/theme/front/css/main.css?v=1508657485'); // external css
            $this->m_pdf->pdf->WriteHTML($stylesheet, 1);
            $this->m_pdf->pdf->WriteHTML($html, 2);

            $this->m_pdf->pdf->Output("./uploads/course-enrollment/reciept/" . $filename, "F");
            return true;
        } else {
            return false;
        }
    }

    public function downloadCourseReciptPdf($aData) {

        if (isset($aData) && !empty($aData) && is_array($aData)) {
            $filename = $aData['sEnrollmentId'] . "." . "pdf";
            $this->load->library('M_pdf');
            $html = $this->load->view('templates/pdf/recipt', $aData, true);
            $stylesheet = file_get_contents(URL . '/theme/front/css/main.css?v=1508657485'); // external css
            $this->m_pdf->pdf->WriteHTML($stylesheet, 1);
            $this->m_pdf->pdf->WriteHTML($html, 2);
            $this->m_pdf->pdf->Output($filename, "D");
            return true;
        } else {
            return false;
        }
    }

    public function download_brochure($slug) {


        $aEnrollmentDetail = $this->user->userEnrollmentDetail('', '', '', $slug);
        if (isset($aEnrollmentDetail['result']) && !empty($aEnrollmentDetail['result'])) {
            $aEnrollmentDetail = $aEnrollmentDetail['result'][0];

            $aData = array();
            $aData['sEnrollmentId'] = $aEnrollmentDetail['enrollment_number'];
            $aData['sCourseType'] = $aEnrollmentDetail['course_type'];
            $aData['sCourseTitle'] = isset($aEnrollmentDetail['course_title']) ? $aEnrollmentDetail['course_title'] : 'N/A';
            $aData['sUsername'] = $aEnrollmentDetail['name'];
            $aData['sEmailId'] = $aEnrollmentDetail['email'];
            $aData['sCountry'] = 'India';
            $aData['sCourseCategory'] = $aEnrollmentDetail['category_name'];
            self::downloadCourseReciptPdf($aData);
        } else {
            show_404();
        }
    }

    public function download_recipt($slug) {

        $aEnrollmentDetail = $this->user->userEnrollmentDetail('', '', '', self::decrypt($slug));
        if (isset($aEnrollmentDetail['result']) && !empty($aEnrollmentDetail['result'])) {
            $aEnrollmentDetail = $aEnrollmentDetail['result'][0];

            $aData = array();
            $aData['sEnrollmentId'] = $aEnrollmentDetail['enrollment_number'];
            $aData['sCourseType'] = $aEnrollmentDetail['course_type'];
            $aData['sCourseTitle'] = isset($aEnrollmentDetail['course_title']) ? $aEnrollmentDetail['course_title'] : 'N/A';
            $aData['sUsername'] = $aEnrollmentDetail['name'];
            $aData['sEmailId'] = $aEnrollmentDetail['email'];
            $aData['sCourseCategory'] = $aEnrollmentDetail['category_name'];
            $aData['sCountry'] = 'India';
            self::downloadCourseReciptPdf($aData);
        } else {
            show_404();
        }
    }

    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    private function encrypt($sString) {
        if (!$sString) {
            return false;
        }
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, sENC_KEY, $sString, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext));

        /* Encrypt Enrollment Id Start */
        $sEnrollmentNumber = $this->encryption->encrypt($sString);
        $sEnrollmentNumber = strtr($sEnrollmentNumber, array('+' => '.', '=' => '-', '/' => '~'));
        return $sEnrollmentNumber;
        /* Encrypt Enrollment Id End */
    }

    private function decrypt($sString) {
        if (!$sString) {
            return false;
        }
        $crypttext = $this->safe_b64decode($sString);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, sENC_KEY, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }

    public function safe_b64encode($string) {

        $data = base64_encode($string);
        $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
        return $data;
    }

    public function safe_b64decode($string) {
        $data = str_replace(array('-', '_'), array('+', '/'), $string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    function contactus() {

        $post = trim_array($this->input->post());
        if ($post) {
            $sName = $post['name'];
            $sEmail = $post['email'];
            $sUserMessage = $post['message'];

            if (isset($sEmail) && !empty($sEmail)) {

                $aPostedData = array();
                $aPostedData['name'] = $sName;
                $aPostedData['email'] = $sEmail;
                $aPostedData['message'] = $sUserMessage;
                $aPostedData['type'] = 'contact us';
                $aPostedData['created_date'] = date('Y-m-d H:i:s');
                $aPostedData['status'] = '1';
                $aPostedData['is_deleted'] = 'n';

                $iUserID = $this->user->save($aPostedData, 'contact_us');


                if (isset($iUserID) && !empty($iUserID) && $iUserID > 0) {
                    $sStatus = true;
                    $sTitle = 'Thank You';
                    $sIcon = '<i class="fa fa-thumbs-o-up"></i>';
                    /* Send Email Start */
                    $sSendEmail = false;
                    if (isset($iUserID) && !empty($iUserID) && $sSendEmail == true) {
                        /* Sent Email to admin start */
                        $sFromEmail = sSITE_FROM_EMAIL;
                        $sFromName = sSITE_FROM_NAME;
                        $sEmailTo = sSITE_RECIEVE_EMAIL;
                        $aContactUs = array('name' => $sName, 'email' => $sEmail, 'message' => $sUserMessage);
                        $sHTMLContent = $this->load->view('templates/mail/contactus-admin.php', $aContactUs, TRUE);

                        $this->load->library('email');
                        $this->email->from($sFromEmail, $sFromName);
                        $this->email->to($sEmailTo);
                        $this->email->subject('A New Enquiry has been recieved on your Website');
                        $this->email->message($sHTMLContent);

                        if ($this->email->send()) {
                            $sMessage = 'Your query has been submitted, Allow us 24-48 hrs to reply back to you';

                            $aContactUs = array('name' => $sName, 'email' => $sEmail, 'message' => $sUserMessage);
                            $sHTMLContent = $this->load->view('templates/mail/contactus-customer.php', $aContactUs, TRUE);

                            $this->load->library('email');
                            $this->email->from($sFromEmail, $sFromName);
                            $this->email->to($sEmail);
                            $this->email->subject('The Shakti Multiversity : Enquiry');
                            $this->email->message($sHTMLContent);
                            $this->email->send();
                        } else {
                            $sMessage = 'Your query has been submitted, Allow us 24-48 hrs to reply back to you';
                        }
                        /* Sent Email to admin end */


                        $aResponse = array('status' => 'success', 'message' => $sMessage);
                    } else {
                        $sMessage = 'Your query has been submitted, Allow us 24-48 hrs to reply back to you';
                    }

                    /* Send Email End */
                } else {
                    $sStatus = false;
                    $sTitle = 'ERROR';
                    $sIcon = '<i class="fa fa-exclamation"></i>';
                    $sMessage = 'Please provide email id';
                }
            } else {
                $sStatus = false;
                $sTitle = 'ERROR';
                $sIcon = '<i class="fa fa-exclamation"></i>';
                $sMessage = 'Please provide email id';
            }

            $aResponse = array();
            $aResponse['status'] = $sStatus;
            $aResponse['icon'] = $sIcon;
            $aResponse['title'] = $sTitle;
            $aResponse['message'] = $sMessage;

            echo json_encode($aResponse);
            die;
        }
    }

    function course_enquiry() {

        $post = trim_array($this->input->post());

        if ($post) {
            $sName = $post['sEnquiryName'];
            $sEmail = $post['sEnquiryEmail'];
            $sCountry = $post['sEnquiryCountry'];
            $sUserMessage = $post['sEnquiryMessage'];
            $sCourseId = $post['sCourseId'];
            $sCourseName = $post['sCourseName'];


            if (isset($sEmail) && !empty($sEmail)) {
                $aPostedData = array();
                $aPostedData['name'] = $sName;
                $aPostedData['country'] = $sCountry;
                $aPostedData['type'] = 'batch enquiry';
                $aPostedData['course_id'] = $sCourseId;
                $aPostedData['email'] = $sEmail;
                $aPostedData['message'] = $sUserMessage;
                $aPostedData['created_date'] = date('Y-m-d H:i:s');
                $aPostedData['status'] = '1';
                $aPostedData['is_deleted'] = 'n';

                $iUserID = $this->user->save($aPostedData, 'contact_us');

                if (isset($iUserID) && !empty($iUserID) && $iUserID > 0) {
                    $sStatus = true;
                    $sTitle = 'Thank You';
                    $sIcon = '<i class="fa fa-thumbs-o-up"></i>';
                    /* Send Email Start */
                    $sSendEmail = true;
                    if (isset($iUserID) && !empty($iUserID) && $sSendEmail == true) {
                        $sFromEmail = sSITE_FROM_EMAIL;
                        $sFromName = sSITE_FROM_NAME;
                        $sEmailTo = sSITE_RECIEVE_EMAIL;
                        $aContactUs = array('name' => $sName, 'course_name' => $sCourseName, 'email' => $sEmail, 'message' => $sUserMessage, 'course' => $sCourseId, 'county' => $sCountry);
                        $sHTMLContent = $this->load->view('templates/mail/contactus-admin.php', $aContactUs, TRUE);

                        $this->load->library('email');
                        $this->email->from($sFromEmail, $sFromName);
                        $this->email->to($sEmailTo);
                        $this->email->subject('Welcome to Shakti Multiversity');
                        $this->email->message($sHTMLContent);
                        $this->email->send();
                        $sMessage = 'Your query has been submitted, Allow us 24-48 hrs to reply back to you';
                        /* Sent Email to admin start */

                        $sHTMLContent = $this->load->view('templates/mail/contactus-customer.php', $aContactUs, TRUE);

                        $this->load->library('email');
                        $this->email->from($sFromEmail, $sFromName);
                        $this->email->to($sEmail);
                        $this->email->subject('The Shakti Multiversity : Batch Enquiry');
                        $this->email->message($sHTMLContent);
                        $this->email->send();
                        $aResponse = array('status' => 'success', 'message' => $sMessage);
                    } else {
                        $sMessage = 'Your query has been submitted, Allow us 24-48 hrs to reply back to you';
                    }

                    /* Send Email End */
                } else {
                    $sStatus = false;
                    $sTitle = 'ERROR';
                    $sIcon = '<i class="fa fa-exclamation"></i>';
                    $sMessage = 'Please provide email id';
                }
            } else {
                $sStatus = false;
                $sTitle = 'ERROR';
                $sIcon = '<i class="fa fa-exclamation"></i>';
                $sMessage = 'Please provide email id';
            }

            $aResponse = array();
            $aResponse['status'] = $sStatus;
            $aResponse['icon'] = $sIcon;
            $aResponse['title'] = $sTitle;
            $aResponse['message'] = $sMessage;

            echo json_encode($aResponse);
            die;
        }
    }

    function service_enquiry() {

        $post = trim_array($this->input->post());
        if ($post) {
            $sName = $post['sEnquiryName'];
            $sEmail = $post['sEnquiryEmail'];
            $sCountry = $post['sEnquiryCountry'];
            $sUserMessage = $post['sEnquiryMessage'];
            $sCourseId = $post['sCourseId'];
            $sCourseName = $post['sCourseName'];


            if (isset($sEmail) && !empty($sEmail)) {
                $aPostedData = array();
                $aPostedData['name'] = $sName;
                $aPostedData['country'] = $sCountry;
                $aPostedData['type'] = 'service request';
                $aPostedData['course_id'] = $sCourseId;
                $aPostedData['email'] = $sEmail;
                $aPostedData['message'] = $sUserMessage;
                $aPostedData['created_date'] = date('Y-m-d H:i:s');
                $aPostedData['status'] = '1';
                $aPostedData['is_deleted'] = 'n';

                $iUserID = $this->user->save($aPostedData, 'service_request');

                if (isset($iUserID) && !empty($iUserID) && $iUserID > 0) {
                    $sStatus = true;
                    $sTitle = 'Thank You';
                    $sIcon = '<i class="fa fa-thumbs-o-up"></i>';
                    /* Send Email Start */
                    $sSendEmail = true;
                    if (isset($iUserID) && !empty($iUserID) && $sSendEmail == true) {
                        $sFromEmail = sSITE_FROM_EMAIL;
                        $sFromName = sSITE_FROM_NAME;
                        $sEmailTo = sSITE_RECIEVE_EMAIL;
                        $aContactUs = array('name' => $sName, 'course_name' => $sCourseName, 'email' => $sEmail, 'message' => $sUserMessage, 'course' => $sCourseId, 'county' => $sCountry);
                        $sHTMLContent = $this->load->view('templates/mail/service-request-admin.php', $aContactUs, TRUE);


                        $this->load->library('email');
                        $this->email->from($sFromEmail, $sFromName);
                        $this->email->to($sEmailTo);
                        $this->email->subject('Lifetime Guidance Programme request recieved!');
                        $this->email->message($sHTMLContent);
                        $this->email->send();
                        $sMessage = 'Your query has been recieved, Allow us 24-48 hrs to reply back to you';
                        /* Sent Email to admin start */

                        $sHTMLContent = $this->load->view('templates/mail/service-request-customer.php', $aContactUs, TRUE);

                        $this->load->library('email');
                        $this->email->from($sFromEmail, $sFromName);
                        $this->email->to($sEmail);
                        $this->email->subject('Regarding your Query on Lifetime Guidance Programme');
                        $this->email->message($sHTMLContent);
                        $this->email->send();
                        $aResponse = array('status' => 'success', 'message' => $sMessage);
                    } else {
                        $sMessage = 'Your request has been recieved, Allow us 24-48 hrs to reply back to you';
                    }

                    /* Send Email End */
                } else {
                    $sStatus = false;
                    $sTitle = 'ERROR';
                    $sIcon = '<i class="fa fa-exclamation"></i>';
                    $sMessage = 'Please provide email id';
                }
            } else {
                $sStatus = false;
                $sTitle = 'ERROR';
                $sIcon = '<i class="fa fa-exclamation"></i>';
                $sMessage = 'Please provide email id';
            }

            $aResponse = array();
            $aResponse['status'] = $sStatus;
            $aResponse['icon'] = $sIcon;
            $aResponse['title'] = $sTitle;
            $aResponse['message'] = $sMessage;

            echo json_encode($aResponse);
            die;
        }
    }

    function book_enquiry() {

        $post = trim_array($this->input->post());
        if ($post) {
            $sBookEnquiryName = $post['sBookEnquiryName'];
            $sBookEnquiryEmail = $post['sBookEnquiryEmail'];
            $sBookEnquiryMobile = $post['sBookEnquiryMobile'];
            $sBookEnquiryQuantity = $post['sBookEnquiryQuantity'];
            $sBookEnquiryCountry = $post['sBookEnquiryCountry'];
            $sBookEnquiryAddress = $post['sBookEnquiryAddress'];
            $sBookEnquiryPinCode = $post['sBookEnquiryPinCode'];
            $sBooksEnquiryMessage = $post['sBooksEnquiryMessage'];

            $sBookName = $post['sBookName'];
            $sBookId = $post['sBookId'];


            if (isset($sBookEnquiryEmail) && !empty($sBookEnquiryEmail)) {
                $aPostedData = array();
                $aPostedData['name'] = $sBookEnquiryName;
                $aPostedData['country'] = $sBookEnquiryCountry;
                $aPostedData['book_id'] = $sBookId;
                $aPostedData['email'] = $sBookEnquiryEmail;
                $aPostedData['address'] = $sBookEnquiryAddress;
                $aPostedData['pin_code'] = $sBookEnquiryPinCode;
                $aPostedData['quantity'] = $sBookEnquiryQuantity;
                $aPostedData['mobile'] = $sBookEnquiryMobile;
                $aPostedData['message'] = $sBooksEnquiryMessage;
                $aPostedData['created_date'] = date('Y-m-d H:i:s');
                $aPostedData['status'] = '1';
                $aPostedData['is_deleted'] = 'n';

                $iUserID = $this->user->save($aPostedData, 'book_order_request');

                if (isset($iUserID) && !empty($iUserID) && $iUserID > 0) {
                    $sStatus = true;
                    $sTitle = 'Thank You';
                    $sIcon = '<i class="fa fa-thumbs-o-up"></i>';
                    /* Send Email Start */
                    $sSendEmail = true;
                    if (isset($iUserID) && !empty($iUserID) && $sSendEmail == true) {
                        $sFromEmail = sSITE_FROM_EMAIL;
                        $sFromName = sSITE_FROM_NAME;
                        $sEmailTo = sSITE_RECIEVE_EMAIL;
                        $aContactUs = array('name' => $sBookEnquiryName, 'book_name' => $sBookName, 'address' => $sBookEnquiryAddress, 'pin_code' => $sBookEnquiryPinCode, 'email' => $sBookEnquiryEmail, 'message' => $sBooksEnquiryMessage, 'book_name' => $sBookName, 'country' => $sCountry);
                        $sHTMLContent = $this->load->view('templates/mail/book-request-admin.php', $aContactUs, TRUE);


                        $this->load->library('email');
                        $this->email->from($sFromEmail, $sFromName);
                        $this->email->to($sEmailTo);
                        $this->email->subject('Book enquiry request recieved!');
                        $this->email->message($sHTMLContent);
                        $this->email->send();
                        $sMessage = 'Your query has been recieved, Allow us 24-48 hrs to reply back to you';
                        /* Sent Email to admin start */

                        $sHTMLContent = $this->load->view('templates/mail/book-request-customer.php', $aContactUs, TRUE);

                        $this->load->library('email');
                        $this->email->from($sFromEmail, $sFromName);
                        $this->email->to($sBookEnquiryEmail);
                        $this->email->subject('You just made a Request at www.shaktimultiversity.com for Purchasing a Book!');
                        $this->email->message($sHTMLContent);
                        $this->email->send();
                        $aResponse = array('status' => 'success', 'message' => $sMessage);
                    } else {
                        $sMessage = 'Your request has been recieved, Allow us 24-48 hrs to reply back to you';
                    }

                    /* Send Email End */
                } else {
                    $sStatus = false;
                    $sTitle = 'ERROR';
                    $sIcon = '<i class="fa fa-exclamation"></i>';
                    $sMessage = 'Please provide email id';
                }
            } else {
                $sStatus = false;
                $sTitle = 'ERROR';
                $sIcon = '<i class="fa fa-exclamation"></i>';
                $sMessage = 'Please provide email id';
            }

            $aResponse = array();
            $aResponse['status'] = $sStatus;
            $aResponse['icon'] = $sIcon;
            $aResponse['title'] = $sTitle;
            $aResponse['message'] = $sMessage;

            echo json_encode($aResponse);
            die;
        }
    }

    function fnBatchCourseList($id, $slug) {
        $aCourseDetail = $this->page->course_dtl($slug, $id);
        ?>
        <div class="modal-body">

            <div class="hori-form lbleft">
                <?php
                if ($aCourseDetail['batches']) {
                    $i = 0;
                    foreach ($aCourseDetail['batches'] as $r) {
                        $aSessionList = $this->page->batch_session_list($r['id']);

                        $i++;
                        $sBatchStatus = '';
                        if ($r['batch_status'] == 'F'):
                            $sEnrollButton = '<span class="course_details_enroll enroll_it Batch Information" onclick="fnEnrollMentNow(NULL,' . $r['id'] . ')">Select</span>';
                            $sBatchStatus = 'Full';
                        elseif ($r['batch_status'] == 'FF'):
                            $sEnrollButton = '<span class="course_details_enroll enroll_it" onclick="fnEnrollMentNow(\'' . URL . 'registeration/' . $aCourseDetail['slug'] . '/' . $r['code'] . '\',' . $r['id'] . ')" >Select</span>';
                            $sBatchStatus = 'Few Seats Available';
                        else:
                            $sEnrollButton = '<span class="course_details_enroll enroll_it" onclick="fnEnrollMentNow(\'' . URL . 'registeration/' . $aCourseDetail['slug'] . '/' . $r['code'] . '\',' . $r['id'] . ')" >Select</span>';
                            $sBatchStatus = 'Available';
                        endif;
                        ?> 
                        <div class="row grid-<?php echo $i; ?>">
                            <div class="col-lg-4">
                                <span class="stitle"><?php echo ucwords(strtolower($r['title'])); ?></span>
                                <br>
                                <span class="subtitle">Seat Status:</span> 
                                <span class="<?php echo $r['batch_status']; ?>"><?php echo $sBatchStatus; ?></span>
                                </span>
                            </div>
                            <div class="col-lg-6">
                                <span class="subtitle">Start Date:</span> 
                                <?php echo get_date($r['start_date']) ?>
                                <br>
                               <!--                                <span class="stitle">Duration:</span> 
                                <?php echo isset($r['duration_hr']) ? $r['duration_hr'] . ' Hours' : ''; ?>
                                <?php echo isset($r['duration_mn']) ? $r['duration_mn'] . ' Mins' : ''; ?>
                                                               <br>-->
                                <div class="session-detail-link" id="show_session" onclick="showSession('<?php echo $r['id'] ?>')">Show Session Details</div>
                            </div>
                            <div class="col-lg-2 col-xs-12 mobile-margin-20">
                                <?php echo $sEnrollButton; ?>
                            </div>
                        </div>
                        <div id="sessionDetail_<?php echo $r['id'] ?>" class="sessionDetail" style="display:none">
                            <?php
                            $sSessionHtml = '<div class="session-detail-list" >';
                            if (isset($aSessionList) && !empty($aSessionList)) {
                                $sSessionHtml .= '<div class="row hidden-xs">'
                                        . '<div class="col-sm-3 session-detail-header ">Title</div>'
                                        . '<div class="col-sm-3 session-detail-header">Date</div>'
                                        . '<div class="col-sm-3 session-detail-header">Time</div>'
                                        . '<div class="col-sm-3 session-detail-header">Duration</div>'
                                        . '</div>';
                                foreach ($aSessionList as $aSession) {
                                    $sSessionHtml .= '<div class="row">';
                                    $sSessionHtml .= '<div class="col-sm-3 col-xs-12 session-detail-item session-name"><label class="visible-xs">Title</label>' . $aSession['name'] . '</div>';
                                    $sSessionHtml .= '<div class="col-sm-3 col-xs-12 session-detail-item session-date"><label class="visible-xs">Date</label>' . get_date($aSession['date']) . '</div>';
                                    $sSessionHtml .= '<div class="col-sm-3 col-xs-12 session-detail-item session-time"><label class="visible-xs">Time</label>' . $aSession['time'] . '</div>';
                                    $sSessionHtml .= '<div class="col-sm-3 col-xs-12 session-detail-item session-duration"><label class="visible-xs">Duration</label>' . $aSession['duration_hr'] . ' Hrs ' . $aSession['duration_mn'] . ' Mins</div>';
                                    $sSessionHtml .= '</div>';
                                }
                            } else {
                                $sSessionHtml .= 'No Session Available';
                            }
                            $sSessionHtml .= '</div>';
                            echo $sSessionHtml;
                            ?>
                        </div>
                        <?php
                        $i = (isset($i) && $i == 2) ? 0 : $i;
                    }
                } else {
                    $sCourseTitle = isset($aCourseDetail['title']) && !empty($aCourseDetail['title']) ? $aCourseDetail['title'] : '';
                    ?>
                    <div class="text-center" style="padding:40px 0">
                        <?php
                        echo "No Batch Avaliable. If you are interested please <a href=\"javascript:;\" onclick=\"sCourseEnquery('" . $id . "','" . $sCourseTitle . "')\">contact us</a>";
                        ?>
                    </div>
                <?php }; ?>
                <div class="modal-footer">
                    <div class="enroll-intro-text"><?php echo sMSG_100; ?></div>
                </div>
            </div>
        </div>
        <?php
    }

    function fnMarkAttendance($id, $batch_id) {
        $aUserData = get_session(USR_SESSION_NAME);
        $aSessionList = $this->page->batch_session_list($batch_id);
        ?>
        <div class="hori-form lbleft">            

            <div id="sessionDetail_<?php echo $r['id'] ?>" class="sessionDetail">
                <?php
                $sSessionHtml = '<div class="session-detail-list" >';
                if (isset($aSessionList) && !empty($aSessionList)) {
                    $sSessionHtml .= '<div class="row">'
                            . '<div class="col-sm-3 session-detail-header">Title</div>'
                            . '<div class="col-sm-2 session-detail-header">Date</div>'
                            . '<div class="col-sm-2 session-detail-header">Time</div>'
                            . '<div class="col-sm-2 session-detail-header">Duration</div>'
                            . '<div class="col-sm-3 session-detail-header">Action</div>'
                            . '</div>';
                    foreach ($aSessionList as $aSession) {
                        $aSessionStatus = $this->page->batch_user_session_list($aParams = array('session_id' => $aSession['id'], 'enrollment_id' => $id, 'batch_id' => $batch_id, 'user_id' => $aUserData['aUserData']['id']));

                        if (isset($aSessionStatus['result'][0]) && !empty($aSessionStatus['result'][0])) {
                            if (isset($aSessionStatus['result'][0]['status']) && $aSessionStatus['result'][0]['status'] == 1) {
                                $sStatusData = 'Attending';
                            } else if (isset($aSessionStatus['result'][0]['status']) && $aSessionStatus['result'][0]['status'] == 2) {
                                $sStatusData = 'Completed';
                            } else {
                                $sStatusData = '<select name="markAttandence" id="markAttandence" onchange="markAttandence(this.value,' . $batch_id . ',' . $id . ',' . $aSession['id'] . ')">
                                <option value="">Please Select</option>
                                <option value="1">Attending</option>
                                </select>';
                            }
                        } else {
                            $sStatusData = '<select name="markAttandence" id="markAttandence" onchange="markAttandence(this.value,' . $batch_id . ',' . $id . ',' . $aSession['id'] . ')">
                                <option value="">Please Select</option>
                                <option value="1">Attending</option>
                                </select>';
                        }
                        $sSessionHtml .= '<div class="row">';
                        $sSessionHtml .= '<div class="col-sm-3 session-detail-item session-name">' . $aSession['name'] . '</div>';
                        $sSessionHtml .= '<div class="col-sm-2 session-detail-item session-date">' . get_date($aSession['date']) . '</div>';
                        $sSessionHtml .= '<div class="col-sm-2 session-detail-item session-time">' . $aSession['time'] . '</div>';
                        $sSessionHtml .= '<div class="col-sm-2 session-detail-item session-duration">' . $aSession['duration_hr'] . ' Hrs ' . $aSession['duration_mn'] . ' Mins</div>';
                        $sSessionHtml .= '<div class="col-sm-3 session-detail-item session-duration">' . $sStatusData . '</div>';
                        $sSessionHtml .= '</div>';
                    }
                } else {
                    $sSessionHtml .= 'No Session Available';
                }
                $sSessionHtml .= '</div>';
                echo $sSessionHtml;
                ?>
            </div>
        </div>
        <?php
    }

}
