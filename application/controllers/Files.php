<?php

class Files extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("files_model", "files");
    }

    function upload($p = 1) {
        ini_set('max_execution_time', 600);
        ini_set('memory_limit', '200M');

        $dir = UP_PATH . "files/";
        if ($_FILES['file']['name']) {
            $file = append_to_filename($_FILES['file']['name'], time());
            if (check_image_ext($_FILES['file']['name'])) {
                $this->load->library('Image');
                $this->image->resize($_FILES['file']['tmp_name'], $dir . 'images/' . $file, 2000);
                $this->image->resize($_FILES['file']['tmp_name'], $dir . "thumbs/" . $file, 50);
            } else {
                move_uploaded_file($_FILES['file']['tmp_name'], $dir . 'others/' . $file);
            }

            if ($this->files->save(array('filename' => $file))) {
                
            }

            $this->lists();
        }
    }

    function lists($p = 1) {
        $data = $this->files->lists($p, 50);
        $this->load->view("file/list", $data);
    }

}

//EOF