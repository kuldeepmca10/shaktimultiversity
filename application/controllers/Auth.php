<?php

class Auth extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("auth_model", "auth");
        $this->load->model("user_model", "user");
    }

    function img_ext_check($v, $param = 'image') {
        if ($_FILES[$param]['name'] and ! check_image_ext($_FILES[$param]['name'])) {
            $this->form_validation->set_message('img_ext_check', 'Please upload .jpg, .jpeg, .gif or .png file only');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function subscribe_newsletter() {
        $post = trim_array($this->input->post());
        if ($post) {
            $sEmail = $post['subscriber_email'];
            if (isset($sEmail) && !empty($sEmail)) {
                $aUserData = $this->user->newsletter_detail($sEmail);
                if (isset($aUserData) && !empty($aUserData)) {
                    $sStatus = false;
                    $sTitle = 'INFORMATION';
                    $sIcon = '<i class="fa fa-exclamation"></i>';
                    $sMessage = 'You are already subscribed for newsletter';
                } else if (isset($aUserData) && empty($aUserData)) {
                    $aPostedData = array();
                    $aPostedData['email'] = $sEmail;
                    $aPostedData['created'] = date('Y-m-d H:i:s');
                    $aPostedData['status'] = '1';
                    $aPostedData['is_deleted'] = 'n';

                    $iUserID = $this->user->save($aPostedData, 'newsletter_subscriber');
                    if (isset($iUserID) && !empty($iUserID) && $iUserID > 0) {
                        $sStatus = true;
                        $sTitle = 'CONGRATULATION';
                        $sIcon = '<i class="fa fa-thumbs-o-up"></i>';
                        $sMessage = 'You have subscribed for newsletter successfully';
                    } else {
                        $sStatus = false;
                        $sTitle = 'ERROR';
                        $sIcon = '<i class="fa fa-exclamation"></i>';
                        $sMessage = 'Please try again';
                    }
                } else {
                    $sStatus = false;
                    $sTitle = 'ERROR';
                    $sIcon = '<i class="fa fa-exclamation"></i>';
                    $sMessage = 'Please provide email id';
                }
            } else {
                $sStatus = false;
                $sTitle = 'ERROR';
                $sIcon = '<i class="fa fa-exclamation"></i>';
                $sMessage = 'Please provide email id';
            }

            $aResponse = array();
            $aResponse['status'] = $sStatus;
            $aResponse['icon'] = $sIcon;
            $aResponse['title'] = $sTitle;
            $aResponse['message'] = $sMessage;

            echo json_encode($aResponse);
            die;
        }
    }

    function login() {
        $sRedirectTo = get_session('sRedirectTo');

        $aUserData = get_session(USR_SESSION_NAME);
        if (isset($aUserData['aUserData']) && !empty($aUserData['aUserData'])) {
            redirect(URL);
        }
        $post = trim_array($this->input->post());
        if ($post) {

            $sEmail = $post['login_email'];
            $sPassword = encrypt_text($post['login_password']);
            $aUserData = $this->user->detail('', $sEmail, $sPassword);
            set_session(USR_SESSION_NAME, array('aUserData' => $aUserData));
            $aUserData = get_session(USR_SESSION_NAME);

            if (isset($aUserData['aUserData']) && !empty($aUserData['aUserData'])) {
                $sStatus = true;
                $sMessage = 'You are login successfully. We are redirecting you.';
                $sRedirectTo = isset($sRedirectTo) && !empty($sRedirectTo) ? $sRedirectTo : false;
            } else {
                $sStatus = false;
                $sRedirectTo = false;
                $sMessage = 'Please provide valid login details';
            }
            $aResponse = array();
            $aResponse['status'] = $sStatus;
            $aResponse['redirect'] = $sRedirectTo;
            $aResponse['icon'] = $sIcon;
            $aResponse['title'] = $sTitle;
            $aResponse['message'] = $sMessage;

            echo json_encode($aResponse);
            die;
        }

        $data['CSS'] = array('theme/front/css/auth.css');
//        $data['JS'] = array('theme/front/js/auth.js');
        $data['page_title'] = 'Login';
        view('auth/login', $data);
    }

    function forgot_password() {
//        echo  $sPassword = encrypt_text(123456);
        $aUserData = get_session(USR_SESSION_NAME);
        if (isset($aUserData['aUserData']) && !empty($aUserData['aUserData'])) {
            redirect(URL);
        }
        $post = trim_array($this->input->post());
        if ($post) {
            $sEmail = $post['forgot_email'];
            $sOTP = $post['forgot_otp'];
            $sPassword = $post['forgot_password'];

            $aUserData = $this->user->detail('', $sEmail, '');

            if (isset($aUserData) && !empty($aUserData)) {
                if (isset($sEmail) && !empty($sEmail) && empty($sOTP)) {
                    /* Send Email Start */
                    $sSendEmail = true;
                    if (isset($sEmail) && !empty($sEmail) && $sSendEmail == true) {
                        $sOTP = rand(111111, 999999);
//                        $sOTP = 992193;
                        set_session(USR_SESSION_NAME, array('aForgotPassword' => array('sOTP' => $sOTP, 'sEmail' => $sEmail)));

                        $sFromEmail = sSITE_FROM_EMAIL;
                        $sFromName = sSITE_FROM_NAME;
                        $sEmailTo = $sEmail;
                        $aUserEmailData = array('name' => $aUserData['name'], 'email' => $sEmail, 'sOTP' => $sOTP);
                        $sHTMLContent = $this->load->view('templates/mail/forgot.php', $aUserEmailData, TRUE);


//                                    $config = Array(
//                                        'protocol' => 'sendmail',
//                                        'smtp_host' => 'mail.shaktimultiversity.com',
//                                        'smtp_port' => 25,
//                                        'smtp_user' => 'admin@shaktimultiversity.com',
//                                        'smtp_pass' => 'Admin@123456',
//                                        'smtp_timeout' => '4',
//                                        'mailtype' => 'html',
//                                        'charset' => 'iso-8859-1'
//                                    );
//                                    $this->load->library('email', $config);
                        $this->load->library('email');
                        $this->email->from($sFromEmail, $sFromName);
                        $this->email->to($sEmailTo);
                        $this->email->subject('The Shakti Multiversity Account Verification code for Password reset');
                        $this->email->message($sHTMLContent);
                        $sData = $this->email->send();
//Send mail 
                        if ($this->email->send()) {
                            $sMessage = 'You have registered successfully. Please check your email for next step';
                        } else {
                            $sMessage = 'You have registered successfully. We are unable to share password on your email id';
                        }
                    }
                    /* Send Email End */

                    $sStatus = true;
                    $sTitle = 'INFORMATION';
                    $sIcon = '<i class="fa fa-exclamation"></i>';
                    $sMessage = 'We have sent you 6 digit OTP on your email id. Please enter here.';
                } else if (isset($sEmail) && !empty($sEmail) && !empty($sOTP)) {
                    $aUserSession = get_session(USR_SESSION_NAME);
                    if ($aUserSession['aForgotPassword']['sOTP'] == $sOTP && $aUserSession['aForgotPassword']['sEmail'] == $sEmail) {
                        $aUserInformation = array();
                        $aUserInformation['id'] = $aUserData['id'];
                        $aUserInformation['password'] = encrypt_text($sPassword);
                        $sUserUpdated = $this->user->save($aUserInformation);

                        /* Password Has Been Set Email Start */
                        $sFromEmail = sSITE_FROM_EMAIL;
                        $sFromName = sSITE_FROM_NAME;
                        $sEmailTo = $sEmail;
                        $aUserEmailData = array('name' => $aUserData['name'], 'email' => $sEmail);
                        $sHTMLContent = $this->load->view('templates/mail/password_changed.php', $aUserEmailData, TRUE);

                        /* Load email library */

                        $this->load->library('email');
                        $this->email->from($sFromEmail, $sFromName);
                        $this->email->to($sEmailTo);
                        $this->email->subject('The Shakti Multiversity Account Password Reset Confirmation');
                        $this->email->message($sHTMLContent);

                        /* Send email */

                        if ($this->email->send()) {
                            
                        }
                        /* Password Has Been Set Email End */
                        $sStatus = true;
                        $sTitle = 'Congratulation';
                        $sIcon = '<i class="fa fa-thumbs-o-up"></i>';
                        $sMessage = 'You have update your password successfully.';
                        $this->session->set_flashdata('item', array('message' => $sMessage, 'class' => 'success'));
                    } else {
                        $sStatus = false;
                        $sTitle = 'ERROR';
                        $sIcon = '<i class="fa fa-exclamation"></i>';
                        $sMessage = 'Your OTP has been expired OR invalid.';
                    }
                } else {
                    $sStatus = false;
                    $sTitle = 'ERROR';
                    $sIcon = '<i class="fa fa-exclamation"></i>';
                    $sMessage = 'Your OTP has been expired OR invalid.';
                }
            } else {
                $sStatus = false;
                $sTitle = 'ERROR';
                $sIcon = '<i class="fa fa-exclamation"></i>';
                $sMessage = 'Please check your email id, No account regsitered with this email id.';
            }
            $aResponse = array();
            $aResponse['status'] = $sStatus;
            $aResponse['icon'] = $sIcon;
            $aResponse['title'] = $sTitle;
            $aResponse['message'] = $sMessage;
            echo json_encode($aResponse);
            die;
        }


        $data['CSS'] = array('theme/front/css/auth.css');
//        $data['JS'] = array('theme/front/js/auth.js');
        $data['page_title'] = 'Forgot Password';
        view('auth/forgot_password', $data);
    }

    function confirm_password($repass) {
        if ($repass != $this->input->post('password')) {
            $this->form_validation->set_message('confirm_password', 'Password mismatch');
            return FALSE;
        } else
            return TRUE;
    }

    function logout() {
        $aUserData = get_session(USR_SESSION_NAME);
        if (isset($aUserData['aUserData']) && !empty($aUserData['aUserData'])) {
            destroy_session(USR_SESSION_NAME);
        }
        redirect(URL);
    }

    function register() {
//        echo encrypt_text('12345678');

        $aUserData = get_session(USR_SESSION_NAME);
        if (isset($aUserData['aUserData']) && !empty($aUserData['aUserData'])) {
            redirect(URL);
        }
        $aPostedData = trim_array($this->input->post());
        $aCountryList = $this->common->country_phonecodes();

        $sStatus = false;
        $sType = 'none';


        if (isset($aPostedData) && !empty($aPostedData)) {

            $sEmailID = isset($aPostedData['email']) && !empty($aPostedData['email']) ? $aPostedData['email'] : $aPostedData['email'];

            if (isset($sEmailID) && !empty($sEmailID)) {
                $aStateList = $this->common->state($aPostedData['country']);
                $aUserDetail = $this->user->detail('', $sEmailID);

                if (isset($aUserDetail) && !empty($aUserDetail) && count($aUserDetail) > 0) {
                    $sMessage = 'This email id is already registered';
                    $sStatus = false;
                    $sTitle = 'INFORMATION';
                    $sIcon = '<i class="fa fa-exclamation"></i>';
                } else if (isset($aUserDetail) && empty($aUserDetail) && empty($aPostedData['email_otp'])) {

                    /* Send Email Start */
                    $sSendEmail = true;
                    if (isset($sEmailID) && !empty($sEmailID) && $sSendEmail == true) {
                        $sOTP = rand(111111, 999999);
                        set_session(USR_SESSION_NAME, array('aSignUp' => array('sOTP' => $sOTP, 'sTime' => date('Y-m-d H:i:s'), 'sEmail' => $sEmailID)));

                        $sFromEmail = sSITE_FROM_EMAIL;
                        $sFromName = sSITE_FROM_NAME;
                        $sEmailTo = $sEmailID;
                        $aUserEmailData = array('name' => $aPostedData['name'], 'email' => $sEmailID, 'sOTP' => $sOTP);
                        $sHTMLContent = $this->load->view('templates/mail/verifyemail.php', $aUserEmailData, TRUE);
                        $this->load->library('email');
                        $this->email->from($sFromEmail, $sFromName);
                        $this->email->to($sEmailTo);
                        $this->email->subject('The Shakti Multiversity Account Registration OTP Email');
                        $this->email->message($sHTMLContent);

                        if ($this->email->send()) {
                            $sMessage = 'We have sent an OTP on email id. Please enter here';
                        } else {
                            $sMessage = 'Please try again later';
                        }
                    }
                    /* Send Email End */

                    $sStatus = true;
                    $sTitle = 'INFORMATION';
                    $sIcon = '<i class="fa fa-exclamation"></i>';
                    $sType = 'otp';
                } else if (isset($aUserDetail) && empty($aUserDetail) && !empty($aPostedData['email_otp'])) {

                    $aUserSession = get_session(USR_SESSION_NAME);

                    if ($aUserSession['aSignUp']['sOTP'] == $aPostedData['email_otp'] && $aUserSession['aForgotPassword']['sEmail'] == $sEmail) {

                        $start_date = new DateTime($aUserSession['aSignUp']['sTime']);
                        $aDifference = $start_date->diff(new DateTime(date('Y-m-d H:i:s')));

                        if ($aDifference->i < 10) {
                            $aUserData['name'] = $aPostedData['name'];
                            $aUserData['email'] = $aPostedData['email'];
                            $aUserData['password'] = encrypt_text($aPostedData['password']);
                            $aUserData['phone'] = $aPostedData['contact_no'];
                            $aUserData['dob'] = $aPostedData['dob'];
                            $aUserData['gender'] = $aPostedData['gender'];
                            $aUserData['address'] = $aPostedData['address'];
                            $aUserData['city'] = $aPostedData['city'];
                            $aUserData['state'] = $aPostedData['state'];
                            $aUserData['country_code'] = $aPostedData['country_code'];
                            $aUserData['country'] = $aPostedData['country'];
                            $aUserData['is_deleted'] = 'n';
                            $aUserData['status'] = '1';
                            $aUserData['ipAddress'] = $sIPAddress = isset($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] : isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
                            $iUserID = $this->user->save($aUserData);
                            /* Send Email Start */
                            $sSendEmail = true;
                            if (isset($iUserID) && !empty($iUserID) && $sSendEmail == true) {

                                $aStudentData = array();
                                $aStudentData['studentId'] = 'SMU' . date('dmY') . $iUserID;
                                $aStudentData['id'] = $iUserID;
                                $this->user->save($aStudentData);


                                $sFromEmail = sSITE_FROM_EMAIL;
                                $sFromName = sSITE_FROM_NAME;
                                $sEmailTo = $aPostedData['email'];
                                $aUserEmailData = array('name' => $aPostedData['name'], 'email' => $sEmailTo, 'password' => $aPostedData['password']);
                                $sHTMLContent = $this->load->view('templates/mail/register.php', $aUserEmailData, TRUE);

                                $this->load->library('email');
                                $this->email->from($sFromEmail, $sFromName);
                                $this->email->to($sEmailTo);
                                $this->email->subject('Welcome to The Shakti Multiversity');
                                $this->email->message($sHTMLContent);
                                $this->email->send();
                                $sMessage = 'You have registered successfully. Please login here';

                                /* --------------------- Send Notification To Admin Start */
                                $aUserEmailData = array('email' => $sEmailTo, 'ipaddress' => $sIPAddress);
                                $sHTMLContent = $this->load->view('templates/mail/register-admin.php', $aUserEmailData, TRUE);

//                            $sEmailTo = 'kuldeep.mca10@gmail.com';
                                $sEmailTo = sSITE_RECIEVE_EMAIL;
                                $this->load->library('email');
                                $this->email->from($sFromEmail, $sFromName);
                                $this->email->to($sEmailTo);
                                $this->email->subject('ALERT - A Member Registration just happened on Shakti Multiversity!');
                                $this->email->message($sHTMLContent);
                                $this->email->send();
                                /* --------------------- Send Notification To Admin End */


                                $sStatus = true;
                                $sTitle = 'CONGRATULATION';
                                $sIcon = '<i class="fa fa-thumbs-o-up"></i>';
                                $sType = 'register';
                                $this->session->set_flashdata('item', array('message' => $sMessage, 'class' => 'success'));
                            }
                        } else {
                            $sMessage = 'OTP has been expired';
                            $sStatus = false;
                            $sTitle = 'INFORMATION';
                            $sIcon = '<i class="fa fa-exclamation"></i>';
                            $sType = 'register';
                        }
                    } else {
                        $sMessage = 'OTP has been expired OR invalid';
                        $sStatus = false;
                        $sTitle = 'INFORMATION';
                        $sIcon = '<i class="fa fa-exclamation"></i>';
                        $sType = 'register';
                    }
                    /* Send Email End */
                }
            }

            $aResponse = array();
            $aResponse['status'] = $sStatus;
            $aResponse['type'] = $sType;
            $aResponse['icon'] = $sIcon;
            $aResponse['title'] = $sTitle;
            $aResponse['message'] = $sMessage;

            echo json_encode($aResponse);
            die;
        }

//        $data['CSS'] = array('theme/front/css/auth.css?v=' . VERSION);
        $data['CSS'] = array('theme/front/css/registration.css?v=' . VERSION);

//        $data['JS'] = array('theme/front/js/auth.js');
        $data['jquery_ui'] = TRUE;
        $data['aUserData'] = $aPostedData;
        $data['page_title'] = 'Register';
        $data['aStateList'] = $aStateList;
        $data['aCountries'] = $this->common->countries();
        view('auth/register', $data);
    }

    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}

//EOF