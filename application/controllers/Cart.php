<?php

class Cart extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("page_model", "page");
    }

    function checkout() {
        $aCartSessionInformation = get_session(USR_SESSION_NAME);
        
        echo '<pre>';print_r($aCartSessionInformation);echo '</pre>';die;
        
    }

    function addtocart() {
        $aPostedData = $this->input->post();
        if (isset($aPostedData) && !empty($aPostedData)) {
            $iCourseId = $aPostedData['id'];
            $aCourseData = $this->page->course_dtl('', $iCourseId);

            $aCartSessionInformation = get_session(USR_SESSION_NAME);

            $aTempCartDetail = array();
            $aTemoMenuDetail = array();
            $aMenuDetail = array();
            if (isset($aCartSessionInformation) && !empty($aCartSessionInformation) && !empty(($aCartSessionInformation['cart']))) {
                $iCartItem = $aCartSessionInformation['cart']['cartItem'];
                $iCartItem++;
                if ($aCartSessionInformation['cart']['menu'] && !empty($aCartSessionInformation['cart']['menu'])) {

                    if ($aCartSessionInformation['cart']['menu'][$iCourseId] && !empty($aCartSessionInformation['cart']['menu'][$iCourseId])) {
                        $aMenuDetail = array();
                        $aMenuDetail = $aCartSessionInformation['cart']['menu'];
                        $aMenuDetail[$iCourseId]['qty'] = ($aMenuDetail[$iCourseId]['qty'] + 1);
                    } else {
                        $aMenuDetail = array();
                        $aMenuDetail = $aCartSessionInformation['cart']['menu'];

                        $aMenuDetail[$iCourseId] = array(
                            'id' => $aCourseData['id'],
                            'title' => $aCourseData['title'],
                            'qty' => 1,
                            'code' => $aCourseData['code'],
                            'type' => $aCourseData['type'],
                            'slug' => $aCourseData['slug'],
                            'short_description' => $aCourseData['short_description'],
                            'price' => $aCourseData['price'],
                            'mrp' => $aCourseData['mrp'],
                            'image' => $aCourseData['image']
                        );
                    }
                } else {
                    $aMenuDetail[$iCourseId] = array(
                        'id' => $aCourseData['id'],
                        'title' => $aCourseData['title'],
                        'qty' => 1,
                        'code' => $aCourseData['code'],
                        'type' => $aCourseData['type'],
                        'slug' => $aCourseData['slug'],
                        'short_description' => $aCourseData['short_description'],
                        'price' => $aCourseData['price'],
                        'mrp' => $aCourseData['mrp'],
                        'image' => $aCourseData['image']
                    );
                }
            } else {
                $iCartItem = 1;
                $aMenuDetail[$iCourseId] = array(
                    'id' => $aCourseData['id'],
                    'title' => $aCourseData['title'],
                    'qty' => 1,
                    'code' => $aCourseData['code'],
                    'type' => $aCourseData['type'],
                    'slug' => $aCourseData['slug'],
                    'short_description' => $aCourseData['short_description'],
                    'price' => $aCourseData['price'],
                    'mrp' => $aCourseData['mrp'],
                    'image' => $aCourseData['image']
                );
            }


            $aCartSessionInformation = array();
            $aCartSessionInformation['cart'] = array(
                'cartItem' => $iCartItem,
                'session_id' => session_id(),
                'discount' => 0,
                'subTotalAmount' => '',
                'taxAmount' => '',
                'totalAmount' => '',
                'menu' => $aMenuDetail
            );

            set_session(USR_SESSION_NAME, $aCartSessionInformation);

            $aUserData = get_session(USR_SESSION_NAME);
            $aResponse = array();
            $aResponse['itemCount'] = $iCartItem;
            $aResponse['title'] = 'Congratulations';
            $aResponse['message'] = 'Course added into cart ';
            echo json_encode($aResponse);
            die;
        }
    }

    function home() {
        $data['dtl'] = $this->common->page_dtl('home');
        $data['sliders'] = $this->common->page_sliders($data['dtl']['id']);
        $data['courses'] = $this->common->selected_courses('show_on_home');
        $data['news'] = $this->common->selected_happening('show_on_home');
        $data['products'] = $this->common->selected_products('show_on_home');
        $data['teachers'] = $this->common->home_teachers();

        $data['seo_description'] = $data['dtl']['seo_description'];
        $data['seo_keywords'] = $data['dtl']['seo_keywords'];
        $page_title = $data['dtl']['seo_title'] ? $data['dtl']['seo_title'] : SITE_NAME;
        $data['page_title'] = $page_title;

        view('page/home', $data);
    }

    function page_dtl($slug = '') {
        $data['dtl'] = $this->common->page_dtl($slug);
        if (!$data['dtl']) {
            show_404();
        }
        $data['sliders'] = $this->common->page_sliders($data['dtl']['id']);

        $data['seo_description'] = $data['dtl']['seo_description'];
        $data['seo_keywords'] = $data['dtl']['seo_keywords'];
        $page_title = $data['dtl']['seo_title'] ? $data['dtl']['seo_title'] : $data['dtl']['title'];
        $data['page_title'] = $page_title;

        if ($data['dtl']['layout'] == 'CONTACT') {
            view('page/cms_contact', $data);
        } else {
            view('page/cms', $data);
        }
    }

    /** Courses * */
    function courses($p = 1) {
        $data = $this->page->courses($p);
        $data['page_title'] = 'Our Courses';
        view('page/courses', $data);
    }

    function free_courses($p = 1) {
        $data = $this->page->courses($p, 50, 0, TRUE);
        $data['page_title'] = 'Free Courses';
        view('page/free_courses', $data);
    }

    function cat_courses($cat = '', $p = 1) {
        $cat_dtl = $this->common->cat_dtl($cat);
        if (!$cat_dtl) {
            show_404();
        }
        $data = $this->page->courses($p, 50, $cat_dtl['id']);

        $data['cat'] = $cat_dtl;

        $data['seo_description'] = $cat_dtl['seo_description'];
        $data['seo_keywords'] = $cat_dtl['seo_keywords'];
        $page_title = $cat_dtl['seo_title'] ? $cat_dtl['seo_title'] : $cat_dtl['title'] . ' Courses';
        $data['page_title'] = $page_title;

        view('page/courses', $data);
    }

    function course_dtl($slug = '') {
        $data['dtl'] = $this->page->course_dtl($slug);
        if (!$data['dtl']) {
            show_404();
        }

        $data['sliders'] = $this->common->page_sliders($data['dtl']['id'], 'COURSE');

        $data['seo_description'] = $data['dtl']['seo_description'];
        $data['seo_keywords'] = $data['dtl']['seo_keywords'];
        $page_title = $data['dtl']['seo_title'] ? $data['dtl']['seo_title'] : $data['dtl']['title'];
        $data['page_title'] = $page_title;

        view('page/course_dtl', $data);
    }

    /** Products * */
    function products($p = 1) {
        $data = $this->page->products($p);
        $data['page_title'] = 'Our Products';
        view('page/products', $data);
    }

    function product_dtl($slug = '') {
        $data['dtl'] = $this->page->product_dtl($slug);
        if (!$data['dtl']) {
            show_404();
        }

        $data['seo_description'] = $data['dtl']['seo_description'];
        $data['seo_keywords'] = $data['dtl']['seo_keywords'];
        $page_title = $data['dtl']['seo_title'] ? $data['dtl']['seo_title'] : $data['dtl']['title'];
        $data['page_title'] = $page_title;

        view('page/product_dtl', $data);
    }

    /** News * */
    function news($p = 1) {
        $data = $this->page->news($p);
        $data['page_title'] = 'News';
        view('page/news', $data);
    }

    function news_dtl($slug = '') {
        $data['dtl'] = $this->page->news_dtl($slug);
        if (!$data['dtl']) {
            show_404();
        }

        $data['seo_description'] = $data['dtl']['seo_description'];
        $data['seo_keywords'] = $data['dtl']['seo_keywords'];
        $page_title = $data['dtl']['seo_title'] ? $data['dtl']['seo_title'] : $data['dtl']['title'];
        $data['page_title'] = $page_title;

        view('page/news_dtl', $data);
    }

    /** Gallery * */
    function gallery($p = 1) {
        $data = $this->page->gallery($p);
        $data['cats'] = key_val_array($this->common->cats('Gallery'), 'id', 'title');
        $data['page_title'] = 'Gallery';
        view('page/gallery', $data);
    }

    /** Testimonials * */
    function testimonials($p = 1) {
        $data = $this->page->testimonials($p);
        $data['page_title'] = 'Testimonials';
        view('page/testimonials', $data);
    }

    /**     * */
    function shakti_centers() {
        $data['page_title'] = 'Shakti Centers';
        view('page/shakti_centers', $data);
    }

    function faq() {
        $cats = $this->common->cats('FAQ');
        if ($cats) {
            foreach ($cats as &$c) {
                $c['faq'] = $this->page->faq($c['id']);
            }
        }
        $data['cats'] = array_chunk($cats, 2);

        $data['page_title'] = "FAQ's";
        view('page/faq', $data);
    }

}

//EOF