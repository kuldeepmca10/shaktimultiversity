<?php

class Pro extends MY_Controller {

    function __construct() {
        parent::__construct();
        not_logged_res();
        $this->load->model("admin/pro_model", "pro");
        $this->load->model("admin/master_model", "master");
    }

    function img_ext_check($v, $param = 'image') {
        if ($_FILES[$param]['name'] and ! check_image_ext($_FILES[$param]['name'])) {
            $this->form_validation->set_message('img_ext_check', 'Please upload .jpg, .jpeg, .gif or .png file only');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /** Courses * */
    function courses($init = '') {
        $data = $this->pro->courses();
        if ($init) {
            $data['cats'] = $this->master->cats('Course');
            $data['teachers'] = $this->master->all_teachers();
        }
        json_data($data);
    }

    function save_course() {
        $post = trim_array($this->input->post());
        $inf = array('success' => 'F', 'msg' => '');

        $id = $post['id'] = intval($post['id']);
        $this->load->library('form_validation');
        $v = $this->form_validation;
        $req = array('required' => '%s required', 'is_unique' => 'This %s is already used', 'numeric' => '%s must be numeric', 'integer' => '%s must be numeric');
        $v->set_rules('cat_id', 'Category', "required", $req);
        $v->set_rules('title', 'Course Name', "required|is_unique[courses.title.id!='$id']", $req);
        $v->set_rules('code', 'Course Code', "required|is_unique[courses.code.id!='$id']", $req);
        $v->set_rules('duration_hr', 'Duration', "required|integer", $req);
        if ($post['price_type'] == 'paid') {
            $v->set_rules('price', 'Price', "required|numeric", $req);
            $v->set_rules('mrp', 'Mrp', "required|numeric", $req);
            $v->set_rules('price_usd', 'Price', "required|numeric", $req);
            $v->set_rules('mrp_usd', 'Mrp', "required|numeric", $req);
        }

        $inf['success'] = 'F';
        if ($v->run() == FALSE) {
            $inf['errors'] = $v->get_errors();
        } else {
            $data = filter_post_data($post, array('id', 'cat_id', 'title', 'code', 'teacher_id', 'display_order', 'duration_hr', 'duration_mn', 'price_type', 'price', 'mrp', 'price_usd', 'mrp_usd', 'show_on_home', 'featured', 'status', 'short_description', 'description', 'caricullaum', 'type', 'accommodation_dtl', 'how_to_apply', 'eligibility_creteria', 'terms', 'benefit', 'faq', 'no_of_learners', 'no_of_enrolled', 'seo_title', 'seo_keywords', 'seo_description'));

            $data['slug'] = url_title($post['slug'] ? $post['slug'] : $post['title'], '-', TRUE);
            if ($_FILES['image']['name']) {
                $this->load->library("image");
                $data['image'] = time() . uniqid() . "." . get_ext($_FILES['image']['name']);
                $this->image->resize($_FILES['image']['tmp_name'], UP_PATH . "course-sm/" . $data['image'], 300);
                $this->image->resize($_FILES['image']['tmp_name'], UP_PATH . "course-lg/" . $data['image'], 1000);
                if ($post['oldimage']) {
                    del_file(UP_PATH . "course-sm/" . $post['oldimage']);
                    del_file(UP_PATH . "course-lg/" . $post['oldimage']);
                }
            }
            if ($this->pro->save($data, "courses")) {
                $inf['success'] = 'T';
            }
        }

        json_data($inf);
    }

    function course_dtl($id) {
        $dtl = $this->pro->course_dtl($id);
        json_data($dtl);
    }

    function delete_course() {
        $post = $this->input->post();
        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        if (!$id) {
            json_data($inf);
        }

        if ($this->dba->delete("courses", "id='$id'")) {
            if ($post['image']) {
                del_file(UP_PATH . "course-sm/" . $post['image']);
                del_file(UP_PATH . "course-lg/" . $post['image']);
            }
            $inf = array('success' => 'T', 'msg' => 'Course deleted successfully');
        }

        json_data($inf);
    }

    function delete_batch() {
        $post = $this->input->post();

        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);

        if (!$id) {
            json_data($inf);
        }
        if ($this->dba->update("batches", array('is_deleted' => 'y'), "id='" . intval($id) . "'")) {
            $inf = array('success' => 'T', 'msg' => 'Batch deleted successfully');
        }

        json_data($inf);
    }

    function delete_session() {
        $post = $this->input->post();

        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        $batch_id = intval($post['batch_id']);

        if (!$id) {
            json_data($inf);
        }
        if ($this->dba->update("batch_sessions", array('is_deleted' => 'y'), "id='" . intval($id) . "'")) {

            $inf = array('success' => 'T', 'msg' => 'Session deleted successfully');
            $inf['sessions'] = $this->pro->batch_sessions($batch_id);
        }

        json_data($inf);
    }

    function delete_lession() {
        $post = $this->input->post();
        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        if (!$id) {
            json_data($inf);
        }

        if ($this->dba->delete("course_lession", "id='$id'")) {
            $inf = array('success' => 'T', 'msg' => 'Lession deleted successfully');
            $inf['lession'] = $this->pro->course_lession($post['course_id']);
        }

        json_data($inf);
    }

    function course_batches($course_id = 0) {
        $data['batches'] = $this->pro->course_batches($course_id);
        json_data($data);
    }

    function batch_sessions($batch_id = 0, $course_id = 0) {
        $data['sessions'] = $this->pro->batch_sessions($batch_id);
        $data['lession'] = $this->pro->course_lession($course_id);
        $data['lessionCount'] = count($data['lession']);
        json_data($data);
    }

    function course_lession($course_id = 0) {
        $data['lession'] = $this->pro->course_lession($course_id);
        json_data($data);
    }

    function save_lession() {
        $post = trim_array($this->input->post());

        $inf = array('success' => 'F', 'msg' => '');
        $id = $post['id'] = intval($post['id']);
        $courseId = $post['course_id'] = intval($post['course_id']);

        $cdtl = $this->pro->course_dtl($post['course_id']);
        if (!$cdtl) {
            $inf['msg'] = "Invalid course";
            json_data($inf);
        }

        if (!$post['title']) {
            $inf['msg'] = "Lession name is required";
            json_data($inf);
        } else {
            if ($id) {

                $ldtl = $this->pro->lession_dtl('', $post['title'], $post['course_id']);

                if (isset($ldtl) && !empty($ldtl) && $ldtl['id'] != $id) {
                    $inf['msg'] = "This Lession is already used.";
                    json_data($inf);
                }
            } else {
                if ($this->dba->val("course_lession", "title='" . escape_str($post['title']) . "' AND course_id = '$courseId'", "id")) {
                    $inf['msg'] = "This Lession is already used.";
                    json_data($inf);
                }
            }
        }
        if (!$post['type']) {
            $inf['msg'] = "Lession type is required";
            json_data($inf);
        }

        if ($post['type'] == 'VIDEO LESSONS') {
            if (isset($post['link']) && empty($post['link'])) {
                $inf['msg'] = "Link is required";
                json_data($inf);
            } else {
                $post['description'] = '';
                $post['file'] = '';
            }
        } else
        if ($post['type'] == 'STUDY MATERIAL' && (empty($post['file']) || !empty($data['oldFile']))) {
            if ((isset($post['file']) && empty($_FILES['file'])) && (isset($post['oldFile']) && empty($_FILES['oldFile']))) {
                $inf['msg'] = "File is required";
                json_data($inf);
            } else {
                $post['description'] = '';
                $post['link'] = '';
            }
        } else {
            if (!$post['description']) {
                $inf['msg'] = "Lession description is required";
                json_data($inf);
            } else {
                $post['link'] = '';
                $post['file'] = '';
            }
        }

        if (!$post['status']) {
            $inf['msg'] = "Lession status is required";
            json_data($inf);
        }

        $data = filter_post_data($post, array('id', 'course_id', 'title', 'type', 'description', 'link', 'is_deleted', 'status'));

        $data['created_on'] = date('Y-m-d H:i:s');
        $data['is_deleted'] = 'n';

        if ($id = $this->pro->save($data, "course_lession")) {
            if ($data['type'] == 'study' && ($_FILES['file'])) {
                $filename = 'session_' . $id . "." . get_ext($_FILES['file']['name']);
                move_uploaded_file($_FILES['file']['tmp_name'], UP_PATH . "lesson/" . $filename);
                $this->dba->update("course_lession", array('file' => $filename), "id='" . intval($id) . "'");
            } else if ($data['type'] == 'study' && isset($data['oldFile']) && !empty($data['oldFile'])) {
                $filename = $data['oldFile'];
                $this->dba->update("course_lession", array('file' => $filename), "id='" . intval($id) . "'");
            }
            $inf['success'] = 'T';
            $inf['msg'] = $post['id'] ? "Lesson updated successfully" : "Lesson added successfully";
            $inf['lession'] = $this->pro->course_lession($post['course_id']);
        }
        json_data($inf);
    }

    function save_batch() {
        $post = trim_array($this->input->post());
        $inf = array('success' => 'F', 'msg' => '');
        $id = $post['id'] = intval($post['id']);

        $cdtl = $this->pro->course_dtl($post['course_id']);
        if (!$cdtl) {
            $inf['msg'] = "Invalid course";
            json_data($inf);
        }

        if (!$post['title']) {
            $inf['msg'] = "Batch name is required";
            json_data($inf);
        }

        if (!$post['code']) {
            $inf['msg'] = "Batch code is required";
            json_data($inf);
        } else {
            if ($this->dba->val("batches", "code='" . escape_str($post['code']) . "' AND id!='$id' AND is_deleted = 'n' ", "id")) {
                $inf['msg'] = "This Batch code is already used.";
                json_data($inf);
            }
        }

        if (!$post['no_of_seats']) {
            $inf['msg'] = "No. of seats is required";
            json_data($inf);
        }

        if (!$post['start_date']) {
            $inf['msg'] = "Start date is required";
            json_data($inf);
        }

        if (!$post['end_date']) {
            $inf['msg'] = "End date is required";
            json_data($inf);
        }

        if (isset($post['registeration_close_before']) && $post['registeration_close_before'] == '') {
            $inf['msg'] = "Provide registeration close before hrs";
            json_data($inf);
        }

//        if (!$post['batch_days']) {
//            $inf['msg'] = "Batch days is required";
//            json_data($inf);
//        }
//
//        if (!$post['time_hr'] or ! $post['time_mn']) {
//            $inf['msg'] = "Batch time is required";
//            json_data($inf);
//        }
//
//        if ($post['time_hr'] > 12 or $post['time_mn'] > 59) {
//            $inf['msg'] = "Batch time is not valid";
//            json_data($inf);
//        }
//
//        if (!$post['duration_hr'] and ! $post['duration_mn']) {
//            $inf['msg'] = "Duration is required";
//            json_data($inf);
//        }

        $data = filter_post_data($post, array('id', 'course_id', 'title', 'code', 'no_of_seats', 'start_date', 'end_date', 'batch_days', 'registeration_close_before', 'duration_hr', 'duration_mn', 'batch_status', 'status', 'is_deleted'));

//        $dt = strtotime($data['start_date']);

        /* if($dt<time()){
          $inf['msg']="Start date must be greater than today";
          json_data($inf);
          } */

//        $day = date('l', $dt);
//        if (!in_array($day, $data['batch_days'])) {
//            $inf['msg'] = "Start date is not matching with batch days";
//            json_data($inf);
//        }
//
//        $cdur = ($cdtl['duration_hr'] * 60) + $cdtl['duration_mn'];
//        $bdur = ($data['duration_hr'] * 60) + $data['duration_mn'];
//        $n = ceil($cdur / $bdur);
//        $dates = array(date('Y-m-d', $dt));
//        $c = 1;
//        while ($c < $n) {
//            $dt = strtotime("+1 days", $dt);
//            $day = date('l', $dt);
//            if (in_array($day, $data['batch_days'])) {
//                $dates[] = date('Y-m-d', $dt);
//                $c++;
//            }
//        }

        $data['start_date'] = to_date_format($data['start_date']);
//        $data['batch_days'] = implode(",", $data['batch_days']);
//
//        $t = str_pad($post['time_hr'], 2, '0', STR_PAD_LEFT) . ':' . str_pad($post['time_mn'], 2, '0', STR_PAD_LEFT) . ' ' . $post['time_type'];
//        $data['batch_time'] = date('H:i:s', strtotime($t));
//
//        $data['end_date'] = end($dates);
        $data['end_date'] = to_date_format($data['end_date']);
        $data['is_deleted'] = 'n';
        if ($id = $this->pro->save($data, "batches")) {
//            $this->dba->delete("batch_dates", "batch_id='$id'");
//
//            $dates_inf = array();
//            foreach ($dates as $d) {
//                $dates_inf[] = array('batch_id' => $id, 'bdate' => $d);
//            }
//            if ($dates_inf) {
//                $this->db->insert_batch("batch_dates", $dates_inf);
//            }
            $mindate = $this->dba->val("batches", "course_id='" . intval($post['course_id']) . "'", "MIN(start_date)");
            $this->dba->update("courses", array('batch_start_date' => $mindate), "id='" . intval($post['course_id']) . "'");

            $inf['success'] = 'T';
            $inf['msg'] = $post['id'] ? "Batch updated successfully" : "Batch added successfully";
            $inf['batches'] = $this->pro->course_batches($post['course_id']);
        }
        json_data($inf);
    }

    function batch_dtl($id) {
        $dtl = $this->pro->batch_dtl($id);
        json_data($dtl);
    }

    function batch_session_dtl($id, $batch_id, $course_id) {

        $dtl = $this->pro->batch_session_dtl($id);
        $dtl['selectedLession'] = explode(',', $dtl['lesson_ids']);
        $dtl['lession'] = $this->pro->course_lession($course_id);
        json_data($dtl);
    }

    function lession_dtl($id) {
        $dtl = $this->pro->lession_dtl($id, '', '');
        json_data($dtl);
    }

    /** Products * */
    function products($init = '') {
        $data = $this->pro->products();
        if ($init) {
            $data['cats'] = $this->master->cats('Product');
        }
        json_data($data);
    }

    function save_product() {
        $post = trim_array($this->input->post());
        $inf = array('success' => 'F', 'msg' => '');

        $id = $post['id'] = intval($post['id']);
        $this->load->library('form_validation');
        $v = $this->form_validation;
        $req = array('required' => '%s required', 'is_unique' => 'This %s is already used', 'numeric' => '%s must be numeric', 'integer' => '%s must be numeric');
        $v->set_rules('cat_id', 'Category', "required", $req);
        $v->set_rules('title', 'Title', "required|is_unique[products.title.id!='$id']", $req);
        $v->set_rules('price_type', 'Price Type', "required", $req);
        if ($post['price_type'] == 'paid') {
            $v->set_rules('price', 'INR Price', "required|numeric", $req);
            $v->set_rules('mrp', 'INR MRP', "numeric", $req);
            $v->set_rules('price_usd', 'INR Price', "required|numeric", $req);
            $v->set_rules('mrp_usd', 'INR MRP', "numeric", $req);
        }

        $inf['success'] = 'F';
        if ($v->run() == FALSE) {
            $inf['errors'] = $v->get_errors();
        } else {
            $data = filter_post_data($post, array('id', 'cat_id', 'title', 'writer', 'display_order', 'price_type', 'price', 'mrp', 'price_usd', 'mrp_usd', 'show_on_home', 'status', 'short_description', 'description', 'seo_title', 'seo_keywords', 'seo_description'));
            $data['slug'] = url_title($post['slug'] ? $post['slug'] : $post['title'], '-', TRUE);

            if ($this->pro->save($data, "products")) {
                $inf['success'] = 'T';
            }
        }

        json_data($inf);
    }

    function product_dtl($id) {
        $dtl = $this->pro->product_dtl($id);
        json_data($dtl);
    }

    function delete_product() {
        $post = $this->input->post();
        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        if (!$id) {
            json_data($inf);
        }

        $images = $this->dba->rows("product_images", "pro_id='$id'", "image");

        if ($this->dba->delete("products", "id='$id'")) {
            if ($images) {
                foreach ($images as $r) {
                    del_file(UP_PATH . "pro-sm/" . $r['image']);
                    del_file(UP_PATH . "pro-lg/" . $r['image']);
                }
            }
            $inf = array('success' => 'T', 'msg' => 'Product deleted successfully');
        }

        json_data($inf);
    }

    function product_images($pro_id = 0) {
        $rs = $this->pro->product_images($pro_id);
        json_data($rs);
    }

    function save_product_image() {
        $post = trim_array($this->input->post());
        $inf = array('success' => 'F', 'msg' => '');

        $id = $post['id'] = intval($post['id']);
        $this->load->library('form_validation');
        $v = $this->form_validation;
        $req = array('required' => '%s required', 'is_unique' => 'This %s is already used', 'numeric' => '%s must be numeric', 'integer' => '%s must be numeric');
        $v->set_rules('pro_id', 'Product ID', "required|integer", $req);

        if ($_FILES['image']['name']) {
            $v->set_rules('image', '', 'callback_img_ext_check[image]');
        } elseif (!$id) {
            $v->set_rules('image', 'Image', 'required', $req);
        }

        $inf['success'] = 'F';
        if ($v->run() == FALSE) {
            $inf['errors'] = $v->get_errors();
        } else {
            $data = filter_post_data($post, array('id', 'pro_id', 'caption', 'display_order'));
            if ($_FILES['image']['name']) {
                $this->load->library("image");
                $data['image'] = append_to_filename($_FILES['image']['name'], time());
                $this->image->resize($_FILES['image']['tmp_name'], UP_PATH . "pro-lg/" . $data['image'], 1000);
                $this->image->resize($_FILES['image']['tmp_name'], UP_PATH . "pro-sm/" . $data['image'], 300);
                if ($post['oldimage']) {
                    del_file(UP_PATH . "pro-lg/" . $post['oldimage']);
                    del_file(UP_PATH . "pro-sm/" . $post['oldimage']);
                }
            }

            if ($this->pro->save($data, "product_images")) {
                $inf['success'] = 'T';
                $inf['images'] = $this->pro->product_images($post['pro_id']);
            }
        }

        json_data($inf);
    }

    function product_image_dtl($id) {
        $dtl = $this->pro->product_image_dtl($id);
        json_data($dtl);
    }

    function delete_product_image() {
        $post = $this->input->post();
        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        if ($this->dba->delete("product_images", "id='$id'")) {
            if ($post['image']) {
                del_file(UP_PATH . "pro-lg/" . $post['image']);
                del_file(UP_PATH . "pro-sm/" . $post['image']);
            }
            $inf = array('success' => 'T', 'msg' => 'Image deleted successfully', 'images' => $this->pro->product_images($post['pro_id']));
        }

        json_data($inf);
    }

    /** Happenings * */
    function happenings() {
        $data = $this->pro->happenings();
        $data['cats'] = $this->master->cats('Happenings');
        json_data($data);
    }

    function save_happenings() {
        $post = trim_array($this->input->post());
        $inf = array('success' => 'F', 'msg' => '');

        $id = $post['id'] = intval($post['id']);
        $this->load->library('form_validation');
        $v = $this->form_validation;
        $req = array('required' => '%s required', 'is_unique' => 'This %s is already used', 'numeric' => '%s must be numeric', 'integer' => '%s must be numeric');
        $v->set_rules('cat_id', 'Category', "required", $req);
        $v->set_rules('title', 'Title', "required|is_unique[happenings.title.id!='$id']", $req);
        $v->set_rules('publish_date', 'Publish Date', "required", $req);

        $inf['success'] = 'F';
        if ($v->run() == FALSE) {
            $inf['errors'] = $v->get_errors();
        } else {
            $data = filter_post_data($post, array('id', 'cat_id', 'title', 'source', 'source_subtitle', 'display_order', 'show_on_home', 'status', 'short_description', 'description', 'publish_date', 'seo_title', 'seo_keywords', 'seo_description'));

            $data['slug'] = url_title($post['slug'] ? $post['slug'] : $post['title'], '-', TRUE);
            if ($_FILES['image']['name']) {
                $this->load->library("image");
                $data['image'] = time() . uniqid() . "." . get_ext($_FILES['image']['name']);
                $this->image->resize($_FILES['image']['tmp_name'], UP_PATH . "happenings-sm/" . $data['image'], 300);
                $this->image->resize($_FILES['image']['tmp_name'], UP_PATH . "happenings-lg/" . $data['image'], 1000);
                if ($post['oldimage']) {
                    del_file(UP_PATH . "happenings-sm/" . $post['oldimage']);
                    del_file(UP_PATH . "happenings-lg/" . $post['oldimage']);
                }
            }
            $data['publish_date'] = to_date_format($data['publish_date']);
            if ($this->pro->save($data, "happenings")) {
                $inf['success'] = 'T';
            }
        }

        json_data($inf);
    }

    function happenings_dtl($id) {
        $dtl = $this->pro->happenings_dtl($id);
        $dtl['publish_date'] = get_date($dtl['publish_date']);
        json_data($dtl);
    }

    function delete_happenings() {
        $post = $this->input->post();
        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        if (!$id) {
            json_data($inf);
        }

        if ($this->dba->delete("happenings", "id='$id'")) {
            if ($post['image']) {
                del_file(UP_PATH . "happenings-sm/" . $post['image']);
                del_file(UP_PATH . "happenings-lg/" . $post['image']);
            }
            $inf = array('success' => 'T', 'msg' => 'Happenings deleted successfully');
        }

        json_data($inf);
    }

    /** Gallery * */
    function galleries($init = '') {
        $data = $this->pro->galleries();
        if ($init) {
            $data['cats'] = $this->master->cats('Gallery');
        }
        json_data($data);
    }

    /** Gallery * */
    function sidebar_banner($init = '') {
        $data = $this->pro->fnSidebarBannerList();
        if ($init) {
            $data['cats'] = $this->master->cats('Gallery');
        }
        json_data($data);
    }

    function save_gallery() {
        $post = trim_array($this->input->post());
        $inf = array('success' => 'F', 'msg' => '');

        $id = $post['id'] = intval($post['id']);
        $this->load->library('form_validation');
        $v = $this->form_validation;
        $req = array('required' => '%s required', 'is_unique' => 'This %s is already used', 'numeric' => '%s must be numeric', 'integer' => '%s must be numeric');
        $v->set_rules('cat_id', 'Category', "required", $req);
        $v->set_rules('title', 'Title', "required", $req);

        if ($post['type'] == 'I') {
            unset($post['video_url']);
            if ($_FILES['image']['name']) {
                $v->set_rules('image', '', 'callback_img_ext_check[image]');
            } elseif (!$id) {
                $v->set_rules('image', 'Image', 'required');
            }
        } else {
            $v->set_rules('video_url', 'Video url', 'required');
        }

        $inf['success'] = 'F';
        if ($v->run() == FALSE) {
            $inf['errors'] = $v->get_errors();
        } else {
            $data = filter_post_data($post, array('id', 'cat_id', 'type', 'title', 'video_url', 'display_order', 'publish_date', 'status', 'seo_title', 'seo_keywords', 'seo_description'));

            $data['slug'] = url_title($post['slug'] ? $post['slug'] : $post['title'], '-', TRUE);
            if ($post['type'] == 'I' and $_FILES['image']['name']) {
                $this->load->library("image");
                $data['image'] = time() . uniqid() . "." . get_ext($_FILES['image']['name']);
                $this->image->resize($_FILES['image']['tmp_name'], UP_PATH . "gal-sm/" . $data['image'], 250);
                $this->image->resize($_FILES['image']['tmp_name'], UP_PATH . "gal-lg/" . $data['image'], 1000);
                if ($post['oldimage']) {
                    del_file(UP_PATH . "gal-sm/" . $post['oldimage']);
                    del_file(UP_PATH . "gal-lg/" . $post['oldimage']);
                }
            }

            if ($post['video_url']) {
                parse_str(parse_url($post['video_url'], PHP_URL_QUERY), $obj);
                $data['video_id'] = $obj['v'] ? $obj['v'] : '';
            }
            $data['publish_date'] = to_date_format($data['publish_date']);
            if ($this->pro->save($data, "gallery")) {
                $inf['success'] = 'T';
            }
        }

        json_data($inf);
    }

    function save_gallery_images() {
        $post = trim_array($this->input->post());
        $inf = array('success' => 'F', 'msg' => '');
        $id = $post['id'] = intval($post['id']);
        $gallery_id = $post['gallery_id'] = intval($post['gallery_id']);
        $this->load->library('form_validation');
        $v = $this->form_validation;
        $req = array('required' => '%s required', 'is_unique' => 'This %s is already used', 'numeric' => '%s must be numeric', 'integer' => '%s must be numeric');
//        $v->set_rules('cat_id', 'Category', "required", $req);
        $v->set_rules('title', 'Title', "required", $req);

        if ($_FILES['image']['name']) {
            $v->set_rules('image', '', 'callback_img_ext_check[image]');
        } elseif (!$id) {
            $v->set_rules('image', 'Image', 'required');
        }

        $inf['success'] = 'F';
        if ($v->run() == FALSE) {
            $inf['errors'] = $v->get_errors();
        } else {
            $data = filter_post_data($post, array('id', 'gallery_id', 'title', 'image', 'display_order', 'status'));


            $this->load->library("image");
            $data['image'] = time() . uniqid() . "." . get_ext($_FILES['image']['name']);
            $this->image->resize($_FILES['image']['tmp_name'], UP_PATH . "gal-sm/" . $data['image'], 250);
            $this->image->resize($_FILES['image']['tmp_name'], UP_PATH . "gal-lg/" . $data['image'], 1000);
            if ($post['oldimage']) {
                del_file(UP_PATH . "gal-sm/" . $post['oldimage']);
                del_file(UP_PATH . "gal-lg/" . $post['oldimage']);
            }

            if ($this->pro->save($data, "gallery_images")) {
                $inf['success'] = 'T';
                $aResultData = $this->get_gallery_images_list($id, $gallery_id);
                $inf['imageList'] = $aResultData;
            }
        }
        json_data($inf);
    }

    function get_gallery_images_list($id, $gallery_id) {

        $aResultData = array();
        $dtl = $this->pro->gallery_images_dtl($id, $gallery_id);

        if ($dtl) {
            foreach ($dtl as &$r) {
                if ($r['image']) {
                    $r['image_url'] = UP_URL . "gal-sm/" . $r['image'];
                }
                $r['publish_date'] = get_date($r['publish_date']);
                $aResultData[] = $r;
            }
        }
        return $aResultData;
    }

    function gallery_images_list($gallery_id) {

        $aResultData = array();
        $dtl = $this->pro->gallery_images_dtl($id = '', $gallery_id);

        if ($dtl) {
            foreach ($dtl as &$r) {
                if ($r['image']) {
                    $r['image_url'] = UP_URL . "gal-sm/" . $r['image'];
                }
                $r['publish_date'] = get_date($r['publish_date']);
                $aResultData[] = $r;
            }
        }
        json_data($aResultData);
    }

    function gallery_images_dtl($id) {
        $aResultData = $this->get_gallery_images_list($id, '');
        $aResultData = isset($aResultData) && !empty($aResultData[0]) ? json_data($aResultData[0]) : array();
        json_data($aResultData);
    }

    function gallery_dtl($id) {
        $dtl = $this->pro->gallery_dtl($id);
        $dtl['publish_date'] = get_date($dtl['publish_date']);
        json_data($dtl);
    }

    function delete_gallery_image() {
        $post = $this->input->post();
        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        $gallery_id = intval($post['gallery_id']);
        if (!$id) {
            json_data($inf);
        }

        if ($this->dba->delete("gallery_images", "id='$id'")) {
            if ($post['image']) {
                del_file(UP_PATH . "gal-sm/" . $post['image']);
                del_file(UP_PATH . "gal-lg/" . $post['image']);
            }
            $aResultData = $this->get_gallery_images_list($id = '', $gallery_id);
            $inf = array('success' => 'T', 'msg' => 'Gallery deleted successfully', 'imageList' => $aResultData);
        }

        json_data($inf);
    }

    function delete_gallery() {
        $post = $this->input->post();
        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        if (!$id) {
            json_data($inf);
        }

        if ($this->dba->delete("gallery", "id='$id'")) {
            if ($post['image']) {
                del_file(UP_PATH . "gal-sm/" . $post['image']);
                del_file(UP_PATH . "gal-lg/" . $post['image']);
            }
            $inf = array('success' => 'T', 'msg' => 'Gallery deleted successfully');
        }

        json_data($inf);
    }

    /** CMS Pages * */
    function cms_widget() {
        $data = $this->pro->cms_widget();
        json_data($data);
    }

    /** CMS Pages * */
    function cms_pages() {
        $data = $this->pro->cms_pages();
        json_data($data);
    }

    function save_cms_page() {
        $post = trim_array($this->input->post());
        $inf = array('success' => 'F', 'msg' => '');

        $id = $post['id'] = intval($post['id']);
        $this->load->library('form_validation');
        $v = $this->form_validation;
        $req = array('required' => '%s required', 'is_unique' => 'This %s is already used', 'numeric' => '%s must be numeric', 'integer' => '%s must be numeric');
        $v->set_rules('title', 'Page Name', "required|is_unique[cms_pages.title.id!='$id']", $req);

        $inf['success'] = 'F';
        if ($v->run() == FALSE) {
            $inf['errors'] = $v->get_errors();
        } else {
            $data = filter_post_data($post, array('id', 'parent_id', 'title', 'display_order', 'heading', 'layout', 'status', 'description', 'seo_title', 'seo_keywords', 'seo_description'));

            $data['slug'] = url_title($post['slug'] ? $post['slug'] : $post['title'], '-', TRUE);
            if ($_FILES['image']['name']) {
                $this->load->library("image");
                $data['image'] = time() . uniqid() . "." . get_ext($_FILES['image']['name']);
                $this->image->resize($_FILES['image']['tmp_name'], UP_PATH . "cms/" . $data['image'], 1600);
                if ($post['oldimage']) {
                    del_file(UP_PATH . "cms/" . $post['oldimage']);
                }
            }

            if ($id == 1) {
                unset($data['title']);
                unset($data['slug']);
                unset($data['display_order']);
                unset($data['status']);
            }

            if ($this->pro->save($data, "cms_pages")) {
                $inf['success'] = 'T';
            }
        }

        json_data($inf);
    }

    function cms_page_dtl($id) {
        $dtl = $this->pro->cms_page_dtl($id);
        json_data($dtl);
    }

    function delete_cms_page() {
        $post = $this->input->post();
        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        if (!$id or $id == 1) {
            json_data($inf);
        }

        if ($this->dba->delete("cms_pages", "id='$id'")) {
            if ($post['image']) {
                del_file(UP_PATH . "cms/" . $post['image']);
            }
            $inf = array('success' => 'T', 'msg' => 'Page deleted successfully');
        }

        json_data($inf);
    }

    function delete_cms_image() {
        $post = $this->input->post();
        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        if (!$id or $id == 1) {
            json_data($inf);
        }

        if ($this->dba->update("cms_pages", array('image' => ''), "id='$id'")) {
            if ($post['image']) {
                del_file(UP_PATH . "cms/" . $post['image']);
            }
            $inf = array('success' => 'T', 'msg' => 'Image deleted successfully');
        }

        json_data($inf);
    }

    /** Page slider * */
    function slides($page_id = 0, $type = 'CMS') {
        $rs = $this->pro->slides($page_id, $type);
        json_data($rs);
    }

    function save_slide() {
        $post = trim_array($this->input->post());
        $inf = array('success' => 'F', 'msg' => '');

        $id = $post['id'] = intval($post['id']);
        $this->load->library('form_validation');
        $v = $this->form_validation;
        $req = array('required' => '%s required', 'is_unique' => 'This %s is already used', 'numeric' => '%s must be numeric', 'integer' => '%s must be numeric');
        $v->set_rules('page_id', 'Page ID', "required|integer", $req);
        $v->set_rules('title', 'Title', "required", $req);

        if ($_FILES['image']['name']) {
            $v->set_rules('image', '', 'callback_img_ext_check[image]');
        } elseif (!$id) {
            $v->set_rules('image', 'Image', 'required');
        }

        $inf['success'] = 'F';
        if ($v->run() == FALSE) {
            $inf['errors'] = $v->get_errors();
        } else {
            $data = filter_post_data($post, array('id', 'page_id', 'type', 'title', 'subtitle', 'link', 'link_caption', 'display_order'));
            if (!$data['type']) {
                $data['type'] = 'CMS';
            }
            if ($_FILES['image']['name']) {
                $this->load->library("image");
                $data['image'] = append_to_filename($_FILES['image']['name'], time());
                $this->image->resize($_FILES['image']['tmp_name'], UP_PATH . "slider/" . $data['image'], 1600);
                if ($post['oldimage']) {
                    del_file(UP_PATH . "slider/" . $post['oldimage']);
                }
            }

            if ($this->pro->save($data, "page_slider")) {
                $inf['success'] = 'T';
                $inf['slides'] = $this->pro->slides($post['page_id'], 'COURSE');
            }
        }

        json_data($inf);
    }

    function slide_dtl($id) {
        $dtl = $this->pro->slide_dtl($id);
        json_data($dtl);
    }

    function delete_slide() {
        $post = $this->input->post();
        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        if (!$id or $id == 1) {
            json_data($inf);
        }

        if (!$post['type']) {
            $post['type'] = 'CMS';
        }

        if ($this->dba->delete("page_slider", "id='$id'")) {
            if ($post['image']) {
                del_file(UP_PATH . "slider/" . $post['image']);
            }
            $inf = array('success' => 'T', 'msg' => 'Slider image deleted successfully', 'slides' => $this->pro->slides($post['page_id'], $post['type']));
        }

        json_data($inf);
    }

    function save_banner() {
        $post = trim_array($this->input->post());
        $inf = array('success' => 'F', 'msg' => '');

        $id = $post['id'] = intval($post['id']);
        $this->load->library('form_validation');
        $v = $this->form_validation;
        $req = array('required' => '%s required', 'is_unique' => 'This %s is already used', 'numeric' => '%s must be numeric', 'integer' => '%s must be numeric');
        $v->set_rules('title', 'Title', "required", $req);
        $v->set_rules('type', 'Type', "required", $req);
        if ($post['type'] == 'link' && $post['type'] == '') {
            $v->set_rules('url', 'URL', "required", $req);
        }

        if ($_FILES['image']['name']) {
            $v->set_rules('image', '', 'callback_img_ext_check[image]');
        } elseif (!$id) {
            $v->set_rules('image', 'Image', 'required');
        }


        $inf['success'] = 'F';
        if ($v->run() == FALSE) {
            $inf['errors'] = $v->get_errors();
        } else {
            $data = filter_post_data($post, array('id', 'type', 'title', 'image', 'url', 'description', 'section', 'display_order', 'show_title', 'status', 'created_date', 'updated_date', 'is_deleted'));

            if (isset($_FILES['image']) && $_FILES['image']['size'] > 0) {
                $this->load->library("image");
                $data['image'] = time() . uniqid() . "." . get_ext($_FILES['image']['name']);
                $this->image->resize($_FILES['image']['tmp_name'], UP_PATH . "sidebar-sm/" . $data['image'], 250);
                $this->image->resize($_FILES['image']['tmp_name'], UP_PATH . "sidebar-lg/" . $data['image'], 1000);
                if ($post['oldimage']) {
                    del_file(UP_PATH . "sidebar-sm/" . $post['oldimage']);
                    del_file(UP_PATH . "sidebar-lg/" . $post['oldimage']);
                }
            }

            $data['created_date'] = to_date_format($data['created_date']);
            if ($this->pro->save($data, "sidebar")) {
                $inf['success'] = 'T';
            }
        }

        json_data($inf);
    }

    function sidebar_banner_dtl($id) {
        $dtl = $this->pro->fnSidebarBannerDetail($id);
        json_data($dtl);
    }

    function save_session() {
        $post = trim_array($this->input->post());
        $inf = array('success' => 'F', 'msg' => '');
        $id = $post['id'] = intval($post['id']);


        $cdtl = $this->pro->batch_dtl($post['batch_id']);
        $aCourseDetail = $this->pro->course_dtl($cdtl['course_id']);
        $sSessionDate = isset($post['date']) ? date('Y-m-d', strtotime($post['date'])) : '';

        if (!$cdtl) {
            $inf['msg'] = "Invalid batch";
            json_data($inf);
        }

        if (!$post['name']) {
            $inf['msg'] = "Session name is required";
            json_data($inf);
        }

        if (!$post['duration_hr'] and ! $post['duration_mn']) {
            $inf['msg'] = "Duration is required";
            json_data($inf);
        } else {
            $aSessionDuration = $this->pro->sessionDuration(array('id' => $id, 'batch_id' => $post['batch_id']));
            $sSessionPreDurationMin = convertTimeInMinutes($aSessionDuration['duration_hr'], $aSessionDuration['duration_mn']);
            $sSessionPostDurationMin = convertTimeInMinutes($aSessionDuration['duration_hr'] + $post['duration_hr'], $aSessionDuration['duration_mn'] + $post['duration_mn']);
            $sCourseDurationMin = convertTimeInMinutes($aCourseDetail['duration_hr'], $aCourseDetail['duration_mn']);
            if ($sSessionPreDurationMin >= $sCourseDurationMin) {
                $inf['msg'] = "Course duration already assigned for this batch";
                json_data($inf);
            } else if ($sSessionPostDurationMin > $sCourseDurationMin) {
                $sRemainingMin = convertTimeInHours($sCourseDurationMin - $sSessionPreDurationMin);
                $inf['msg'] = "Only " . $sRemainingMin . " duration remaining for this batch";
                json_data($inf);
            }
        }

        if (!$post['date']) {
            $inf['msg'] = "Date is required";
            json_data($inf);
        } else {
            if (isset($sSessionDate) &&
                    $sSessionDate >= date('Y-m-d', strtotime($cdtl['start_date'])) &&
                    $sSessionDate <= date('Y-m-d', strtotime($cdtl['end_date']))) {
                // True;
            } else {
                $inf['msg'] = "Date must be between " . $cdtl['start_date'] . " and " . $cdtl['end_date'] . "";
                json_data($inf);
            }
        }

        if (!$post['time_hr'] or ! $post['time_mn']) {
            $inf['msg'] = "Time is required";
            json_data($inf);
        }

        if ($post['time_hr'] > 12 or $post['time_mn'] > 59) {
            $inf['msg'] = "Time is not valid";
            json_data($inf);
        }



        if (!$post['lesson']) {
            $inf['msg'] = "Lesson is required";
            json_data($inf);
        } else {
            if ($this->dba->val("batch_sessions", "name='" . escape_str($post['name']) . "' AND batch_id = '" . $post['batch_id'] . "' AND is_deleted = 'n' AND id!='$id'", "id")) {
                $inf['msg'] = "Session with same name is already used.";
                json_data($inf);
            }
        }


        $data = filter_post_data($post, array('id', 'batch_id', 'lesson_ids', 'name', 'date', 'time', 'duration_hr', 'duration_mn', 'status', 'is_deleted'));

        $t = str_pad($post['time_hr'], 2, '0', STR_PAD_LEFT) . ':' . str_pad($post['time_mn'], 2, '0', STR_PAD_LEFT) . ' ' . $post['time_type'];
        $data['time'] = date('H:i:s', strtotime($t));

        $data['date'] = to_date_format($data['date']);
        $data['lesson_ids'] = isset($post['lesson']) && !empty($post['lesson']) ? implode(',', $post['lesson']) : '';
        $data['status'] = '1';
        $data['is_deleted'] = 'n';

        if ($id = $this->pro->save($data, "batch_sessions")) {
            $mindate = $this->dba->val("batch_sessions", "batch_id='" . intval($post['batch_id']) . "' AND date > '" . date('Y-m-d') . "'", "MIN(date)");
            $this->dba->update("courses", array('batch_start_date' => $mindate), "id='" . intval($post['course_id']) . "'");

            $inf['success'] = 'T';
            $inf['msg'] = $post['id'] ? "Session updated successfully" : "Session added successfully";
            $inf['sessions'] = $this->pro->batch_sessions($post['batch_id']);
//            $inf['batches'] = $this->pro->course_batches($post['course_id']);
        }
        json_data($inf);
    }

}

//EOF