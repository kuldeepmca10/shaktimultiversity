<?php
class Dashboard extends MY_Controller {
	function __construct() {
        parent::__construct();
		not_logged_res();
    }
	
	function index(){
		$data['page_title']="Dashboard | ".SITE_NAME;
		admin_view('admin/user/dashboard', $data);
	}
	
	function angular_layout(){
		admin_view('');
	}
}

//EOF