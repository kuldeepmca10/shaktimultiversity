<?php

class Student extends MY_Controller {

    function __construct() {
        parent::__construct();
        not_logged_res();
        $this->load->model("admin/student_model", "student");
        $this->load->model("user_model", "user");
        $this->load->model("admin/master_model", "master");
    }

    function profile() {
        $dtl = $this->student->detail(ADM_USER_ID);
        json_data($dtl);
    }

    function update_student() {

        $post = trim_array($this->input->post());
        $inf = array('success' => 'F', 'msg' => '');

        $id = $post['id'] = intval($post['id']);

        /** Validation * */
        $this->load->library('form_validation');
        $v = $this->form_validation;


        $req = array('required' => '%s required', 'is_unique' => 'This %s is already used', 'integer' => '%s must be numeric',
            'min_length' => '%s must be 10 digits', 'max_length' => '%s must be 10 digits');

        $v->set_rules('name', 'Name', 'required', $req);

        if (!isset($id) && empty($id)) {
            $v->set_rules('email', 'Email ID', "required|valid_email|is_unique[users.email]", $req);
        } else {
            $v->set_rules('email', 'Email ID', "required|valid_email", $req);
        }

        $v->set_rules('country', 'Country', 'required', $req);
        $v->set_rules('skype_id', 'Skype ID', 'required', $req);
        $v->set_rules('other_id', 'Whatsup ID', 'required', $req);
        $v->set_rules('phone_code', 'Phone Code', 'required', $req);
        $v->set_rules('phone', 'Phone Number', 'required|min_length[10]|max_length[10]', $req);

        $inf['success'] = 'F';
        if ($v->run() == FALSE) {
            $inf['errors'] = $v->get_errors();
        } else {

            if (isset($id) && !empty($id)) {
                $post['id'] = $id;
            } else {
                $sPassword = $this::generateRandomString(10);
                $post['password'] = encrypt_text($sPassword);
            }
            $iUserID = $this->user->save($post);

            if (isset($iUserID) && !empty($iUserID)) {
                $inf['success'] = 'T';
                $sMessage = 'You have registered successfully. Please check your email for next step.' . $sPassword;
                /* Send Email Start */

                $from_email = "kuldeep.mca10@gmail.com";
                $to_email = $this->input->post('email');
                //Load email library 
                $this->load->library('email');
                $this->email->from($from_email, 'Kuldeep Singh');
                $this->email->to($to_email);
                $this->email->subject('Email Test');
                $this->email->message('Testing the email class.');

                //Send mail 
                if ($this->email->send()) {
                    $sMessage = 'You have registered successfully. Please check your email for next step.' . $sPassword;
                } else {
                    $sMessage = 'You have registered successfully. We are unable to share password on your email id';
                }
                $inf['msg'] = $sMessage;
            }
        }
        json_data($inf);
    }

    function save_student() {

        $post = trim_array($this->input->post());
        $inf = array('success' => 'F', 'msg' => '');

        $id = $post['id'] = intval($post['id']);

        /** Validation * */
        $this->load->library('form_validation');
        $v = $this->form_validation;


        $req = array('required' => '%s required', 'is_unique' => 'This %s is already used', 'integer' => '%s must be numeric',
            'min_length' => '%s must be 10 digits', 'max_length' => '%s must be 10 digits');

        $v->set_rules('name', 'Name', 'required', $req);
        $v->set_rules('email', 'Email ID', "required|valid_email|is_unique[users.email]", $req);
        $v->set_rules('country', 'Country', 'required', $req);
        $v->set_rules('skype_id', 'Skype ID', 'required', $req);
        $v->set_rules('other_id', 'Whatsup ID', 'required', $req);
        $v->set_rules('phone', 'Phone Number', 'required|min_length[10]|max_length[10]', $req);

        $inf['success'] = 'F';
        if ($v->run() == FALSE) {
            $inf['errors'] = $v->get_errors();
        } else {
            $sIPAddress = $post['ipAddress'] = isset($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] : isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
            if (isset($id) && !empty($id)) {
                $post['id'] = $id;
            } else {
                $sPassword = $this::generateRandomString(10);
                $post['password'] = encrypt_text($sPassword);
            }

            $iUserID = $this->user->save($post);

            if (isset($iUserID) && !empty($iUserID)) {

                $aStudentData = array();
                $aStudentData['studentId'] = 'SMU' . date('dmY') . $iUserID;
                $aStudentData['id'] = $iUserID;
                $this->user->save($aStudentData);

                $sFromEmail = sSITE_FROM_EMAIL;
                $sFromName = sSITE_FROM_NAME;
                $sEmailTo = $post['email'];
                $aUserEmailData = array('name' => $post['name'], 'email' => $sEmailTo, 'password' => $sPassword);
                $sHTMLContent = $this->load->view('templates/mail/register.php', $aUserEmailData, TRUE);

                $this->load->library('email');
                $this->email->from($sFromEmail, $sFromName);
                $this->email->to($sEmailTo);
                $this->email->subject('Welcome to The Shakti Multiversity');
                $this->email->message($sHTMLContent);
                $this->email->send();
                $sMessage = 'You have registered successfully. Please check your email for next step.';

                /* --------------------- Send Notification To Admin Start */
                $aUserEmailData = array('email' => $sEmailTo, 'ipaddress' => $sIPAddress);
                $sHTMLContent = $this->load->view('templates/mail/register-admin.php', $aUserEmailData, TRUE);

                $sEmailTo = sSITE_RECIEVE_EMAIL;
                $this->load->library('email');
                $this->email->from($sFromEmail, $sFromName);
                $this->email->to($sEmailTo);
                $this->email->subject('ALERT - A Member Registration just happened on Shakti Multiversity!');
                $this->email->message($sHTMLContent);
                $this->email->send();
                /* --------------------- Send Notification To Admin End */

                $inf['success'] = 'T';
                $inf['msg'] = $sMessage;
            }
        }
        json_data($inf);
    }

    /** Users * */
    function students($p = 1) {
        $data = $this->student->students();

        $data['roles'] = $this->common->roles();
        json_data($data);
    }

    /** Users * */
    function country_phonecodes() {
        $data = $this->student->country_phonecodes();
        json_data($data);
    }

    function student_dtl($id = 0) {
        if (ctype_digit($id) and $id) {
            $data = $this->student->detail($id);
        }
        json_data($data);
    }

    function login_as_user($id = 0) {
        $data = array();
        if (ctype_digit($id) and $id) {
            $data = $this->student->detail($id);
            set_session(USR_SESSION_NAME, array('aUserData' => $data, 'sAdminLogin' => true));
            $aUserData = get_session(USR_SESSION_NAME);
            if (isset($aUserData) && !empty($aUserData)) {
                $data['redirect'] = URL;
            }
        }
        json_data($data);
    }

    function save_user() {
        $post = trim_array($this->input->post());

        $id = $post['id'] = intval($post['id']);
        $this->load->library('form_validation');
        $v = $this->form_validation;
        $req = array('required' => '%s required', 'is_unique' => 'This %s is already used', 'integer' => '%s must be numeric', 'min_length' => '%s must be 10 digits',
            'max_length' => '%s must be 10 digits');
        $v->set_rules('fname', 'Name', 'required', $req);
        $v->set_rules('username', 'Username', "required|is_unique[system_users.username.id!='$id']", $req);
        $v->set_rules('email', 'Email', "required|valid_email|is_unique[system_users.email.id!='$id']", $req);

        if (!$id) {
            $v->set_rules('password', 'Password', 'required', $req);
            $v->set_rules('repassword', 'Confirm Password', 'required|callback_passwordmatch', $req);
        }

        $v->set_rules('mobile', 'Mobile No.', "required|integer|min_length[10]|max_length[10]", $req);
        $v->set_rules('role_id', 'Role', 'required|callback_checkadminroles', $req);

        $inf['success'] = 'F';
        if ($v->run() == FALSE) {
            $inf['errors'] = $v->get_errors();
        } else {
            $data = filter_post_data($post, array('id', 'role_id', 'fname', 'lname', 'username', 'email', 'mobile', 'phone', 'status'));
            if (!$id) {
                $data['password'] = encrypt_text($post['password']);
            }

            if ($id = $this->student->save($data)) {
                $inf['id'] = $id;
                $inf['success'] = 'T';
            }
        }
        json_data($inf);
    }

    function checkadminroles($v) {
        $id = intval($this->input->post('id'));
        $client_id = intval($this->input->post('client_id'));
        $locid = intval($this->input->post('reference_id'));
        switch ($v) {
            case 1:
                if ($this->dba->val("system_users", "role_id=1 AND id!='$id'", "id")) {
                    $this->form_validation->set_message('checkadminroles', 'Administrator is already added');
                    return FALSE;
                }
                break;
        }

        return TRUE;
    }

    function course_enrollment($iUserID = 0) {
        $data['enrollmentlist'] = $this->student->userEnrollmentDetail('', $iUserID);
        $data['cats'] = $this->master->cats('Course');
        json_data($data);
    }

    function course_enrollment_detail($iId = 0, $iUserId = 0) {
        $data['enrollmentlist'] = $this->student->userEnrollmentDetail($iId, $iUserID);
        json_data($data);
    }

    function export_enrollment() {
        // Load the Library
        $this->load->library("excel");

        $aResponse = $this->student->userEnrollmentDetail();

        $this->excel->setActiveSheetIndex(0);
        // Gets all the data using MY_Model.php

        $filename = 'enrollment_report.xls';
        $this->excel->stream($filename, $aResponse['result']);
        die;
    }

    function delete_student() {
        $post = $this->input->post();
        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);

        if (!$id) {
            json_data($inf);
        }
        if ($this->dba->delete("users", "id='$id'")) {
            $inf = array('success' => 'T', 'msg' => 'Student deleted successfully');
        }

        json_data($inf);
    }

    function update_student_status() {
        $post = $this->input->post();
        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        $status = intval($post['status']);


        if (!$id) {
            json_data($inf);
        }
        $post = array();
        $post['id'] = $id;
        $post['status'] = isset($status) && $status == '0' ? 1 : 0;

        if ($this->user->save($post)) {
            $sStatusMessage = isset($status) && $status == '0' ? 'Activated' : 'Inactivated';
            $inf = array('success' => 'T', 'msg' => 'Student has been ' . $sStatusMessage . ' successfully');
        }

        json_data($inf);
    }

    function update_membership_status() {
        $post = $this->input->post();
        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        $status = intval($post['status']);


        if (!$id) {
            json_data($inf);
        }
        $post = array();
        $post['id'] = $id;
        $post['isLifetimeMember'] = isset($status) && $status == '0' ? 1 : 0;

        if ($this->user->save($post)) {
            $sStatusMessage = isset($status) && $status == '0' ? 'Activated' : 'Inactivated';
            if (isset($status) && $status == 0) {
                $dtl = $this->student->detail($id);

                $sFromEmail = sSITE_FROM_EMAIL;
                $sFromName = sSITE_FROM_NAME;

                /* --------------------- Send Notification To Member Start */
                $sEmailTo = $dtl['email'];
                $aUserEmailData = array('name' => $dtl['name'], 'email' => $sEmailTo, 'membershipid' => $dtl['studentId']);

                $sHTMLContent = $this->load->view('templates/mail/memebrship-confirmation-customer.php', $aUserEmailData, TRUE);

                $this->load->library('email');
                $this->email->from($sFromEmail, $sFromName);
                $this->email->to($sEmailTo);
                $this->email->subject('Congratulations...You are now under Lifetime Guidance of the Masters!');
                $this->email->message($sHTMLContent);
                $this->email->send();
                /* --------------------- Send Notification To Member End */

                /* --------------------- Send Notification To Admin Start */
                $aUserEmailData = array('name' => $dtl['name'], 'email' => $sEmailTo, 'membershipid' => $dtl['studentId']);
                $sHTMLContent = $this->load->view('templates/mail/memebrship-confirmation-admin.php', $aUserEmailData, TRUE);

                $sEmailTo = sSITE_RECIEVE_EMAIL;
                $this->load->library('email');
                $this->email->from($sFromEmail, $sFromName);
                $this->email->to($sEmailTo);
                $this->email->subject('WARNING!!!!! New Lifetime Membership Activated on your Website!');
                $this->email->message($sHTMLContent);
                $this->email->send();
                /* --------------------- Send Notification To Admin End */

                $inf['success'] = 'T';
                $inf['msg'] = $sMessage;
            }
            $inf = array('success' => 'T', 'msg' => 'Student has been ' . $sStatusMessage . ' successfully');
        }

        json_data($inf);
    }

    function update_enrollment_status() {
        $post = $this->input->post();

        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        $status = ($post['status']);
        $comment = ($post['comment']);

        if (!$id) {
            json_data($inf);
        }
        $post = array();
        $post['id'] = $id;
        $post['approval_status'] = $status;
        $post['comment'] = $comment;

        $sStatusMessage = $status;
        if ($this->student->save_enrollment($post)) {
            $aEnrollmentDetail = $this->student->userEnrollmentDetail($id);
            $aEnrollmentDetail = isset($aEnrollmentDetail['result'][0]) && !empty($aEnrollmentDetail['result'][0]) ? $aEnrollmentDetail['result'][0] : '';

            $aSessionDetail = $this->student->batch_session_list($aEnrollmentDetail['batch_no']);

            /* Send Email Start */
            $sSendEmail = true;
            if (isset($aEnrollmentDetail['email']) && !empty($aEnrollmentDetail['email']) && $sSendEmail == true) {
                $sFromEmail = sSITE_FROM_EMAIL;
                $sFromName = sSITE_FROM_NAME;
                $sEmailTo = $aEnrollmentDetail['email'];
                $aUserEmailData = array('name' => $aEnrollmentDetail['name'], 'enrollment_number' => $aEnrollmentDetail['enrollment_number'], 'course_name' => $aEnrollmentDetail['course_title'], 'aSessionDetail' => $aSessionDetail, 'comment' => $aEnrollmentDetail['comment'], 'status' => $status, 'email' => $aEnrollmentDetail['email']);
                $sHTMLContent = $this->load->view('templates/mail/coursestatusupdate.php', $aUserEmailData, TRUE);

//Load email library 
                $this->load->library('email');
                $this->email->from($sFromEmail, $sFromName);
                $this->email->to($sEmailTo);
                $this->email->subject(ucwords(strtolower($aEnrollmentDetail['course_title'])) . ' : Your ' . $aEnrollmentDetail['enrollment_number'] . ' has been ' . ucwords(strtolower($status)));
                $this->email->message($sHTMLContent);
//Send mail 
                $this->email->send();


                /* --------------------- Send Notification To Admin Start */
                $aUserEmailData = array('enrollment_number' => $aEnrollmentDetail['enrollment_number'], 'course_name' => $aEnrollmentDetail['course_title'], 'aSessionDetail' => $aSessionDetail, 'comment' => $aEnrollmentDetail['comment'], 'status' => $status, 'email' => $aEnrollmentDetail['email']);
                $sHTMLContent = $this->load->view('templates/mail/coursestatusupdate-admin.php', $aUserEmailData, TRUE);

//                $sEmailTo = 'kuldeep.mca10@gmail.com';
                $sEmailTo = sSITE_RECIEVE_EMAIL;
                $this->load->library('email');
                $this->email->from($sFromEmail, $sFromName);
                $this->email->to($sEmailTo);
                $this->email->subject(ucwords(strtolower($aEnrollmentDetail['course_title'])) . ' : Your ' . $aEnrollmentDetail['enrollment_number'] . ' has been ' . ucwords(strtolower($status)));
                $this->email->message($sHTMLContent);
                $this->email->send();
                /* --------------------- Send Notification To Admin End */
            }
            /* Send Email End */

            $inf = array('success' => 'T', 'msg' => 'Enrollment status has been changed to ' . $sStatusMessage . ' successfully');
        }

        json_data($inf);
    }

    private function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}

//EOF