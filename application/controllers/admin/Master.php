<?php

class Master extends MY_Controller {

    function __construct() {
        parent::__construct();
        not_logged_res();
        $this->load->model("admin/master_model", "master");
    }

    function cat_init() {
        $data['ccats'] = $this->master->cats('Course');
        $data['pcats'] = $this->master->cats('Product');
        $data['tcats'] = $this->master->cats('Teacher');
        $data['gcats'] = $this->master->cats('Gallery');
        $data['hcats'] = $this->master->cats('Happenings');
        $data['fcats'] = $this->master->cats('FAQ');

        json_data($data);
    }

    function save_cat() {
        $post = trim_array($this->input->post());
        $inf = array('success' => 'F', 'msg' => '');

        $id = $post['id'] = intval($post['id']);
        $type = $post['type'] = $post['type'];
        $this->load->library('form_validation');
        $v = $this->form_validation;
        $req = array('required' => '%s required', 'is_unique' => 'This %s is already used');
        $v->set_rules('type', 'Category Type', "required", $req);
        $v->set_rules('title', 'Category Name', "required|is_unique[cats.title.id!='$id' and type = '$type']", $req);

        $inf['success'] = 'F';
        if ($v->run() == FALSE) {
            $inf['errors'] = $v->get_errors();
        } else {
            $data = filter_post_data($post, array('id', 'type', 'title', 'icon_class', 'display_order', 'seo_title', 'seo_keywords', 'seo_description'));
            $data['slug'] = url_title($post['slug'] ? $post['slug'] : $post['title'], '-', TRUE);

            if ($this->master->save($data, "cats")) {
                $inf['success'] = 'T';
                $inf['msg'] = $id ? "Category updated successfully" : "Category added successfully";
                $inf['type'] = $post['type'];
                $inf['cats'] = $this->master->cats($post['type']);
            }
        }

        json_data($inf);
    }

    function cat_dtl($id = 0) {
        $dtl = $this->master->cat_dtl($id);
        json_data($dtl);
    }

    function delete_cat() {
        $post = $this->input->post();
        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        if (!$id) {
            json_data($inf);
        }

        /* if($this->dba->row("songs", "FIND_IN_SET('$id', artists)", "id")){
          $inf=array('success'=>'F', 'msg'=>'Please delete all songs added for this Artist first!');
          json_data($inf);
          } */

        if ($this->dba->delete("cats", "id='$id'")) {
            $inf['success'] = 'T';
            $inf['msg'] = 'Category deleted successfully';
            $inf['cats'] = $this->master->cats($post['type']);
        }

        json_data($inf);
    }

    /** Teachers * */
    function teachers($init = '') {
        $data = $this->master->teachers();
        if ($init) {
            $data['cats'] = $this->master->cats('Teacher');
        }
        json_data($data);
    }

    function save_teacher() {
        $post = trim_array($this->input->post());
        $inf = array('success' => 'F', 'msg' => '');

        $id = $post['id'] = intval($post['id']);
        $this->load->library('form_validation');
        $v = $this->form_validation;
        $req = array('required' => '%s required', 'is_unique' => 'This %s is already used', 'numeric' => '%s must be numeric', 'integer' => '%s must be numeric');
        $v->set_rules('cat_id', 'Category', "required", $req);
        $v->set_rules('title', 'Name', "required", $req);

        $inf['success'] = 'F';
        if ($v->run() == FALSE) {
            $inf['errors'] = $v->get_errors();
        } else {
            $data = filter_post_data($post, array('id', 'cat_id', 'title', 'display_order', 'show_on_home', 'status', 'about_lineage', 'contribution', 'social_responsibility', 'in_the_media', 'short_description', 'workshop_seminar', 'experience', 'sessions', 'avaliablity', 'facebook_url', 'twitter_url', 'linkedin_url', 'google_plus_url', 'seo_title', 'seo_keywords', 'seo_description'));

            $data['slug'] = url_title($post['slug'] ? $post['slug'] : $post['title'], '-', TRUE);
            if ($_FILES['image']['name']) {
                $this->load->library("image");
                $data['image'] = append_to_filename($_FILES['image']['name'], time());
                $this->image->resize($_FILES['image']['tmp_name'], UP_PATH . "teachers/" . $data['image'], 600);
                if ($post['oldimage']) {
                    del_file(UP_PATH . "teachers/" . $post['oldimage']);
                }
            }
            if ($this->master->save($data, "teachers")) {
                $inf['success'] = 'T';
            }
        }

        json_data($inf);
    }

    function teacher_dtl($id) {
        $dtl = $this->master->teacher_dtl($id);
        json_data($dtl);
    }

    function delete_teacher() {
        $post = $this->input->post();
        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        if (!$id) {
            json_data($inf);
        }

        if ($this->dba->delete("teachers", "id='$id'")) {
            if ($post['image']) {
                del_file(UP_PATH . "teachers/" . $post['image']);
            }
            $inf = array('success' => 'T', 'msg' => 'Teacher deleted successfully');
        }

        json_data($inf);
    }

    /** Testimonials * */
    function testimonials($init = '') {
        $data = $this->master->testimonials();
        if ($init) {
            $data['cats'] = $this->master->cats('course');
        }
        json_data($data);
    }

    function save_testimonial() {
        $post = trim_array($this->input->post());
        $inf = array('success' => 'F', 'msg' => '');

        $id = $post['id'] = intval($post['id']);
        $this->load->library('form_validation');
        $v = $this->form_validation;
        $req = array('required' => '%s required', 'is_unique' => 'This %s is already used', 'numeric' => '%s must be numeric', 'integer' => '%s must be numeric');
        $v->set_rules('title', 'Name', "required", $req);
        $v->set_rules('publish_date', 'Date', "required", $req);

        $inf['success'] = 'F';
        if ($v->run() == FALSE) {
            $inf['errors'] = $v->get_errors();
        } else {

            $data = filter_post_data($post, array('id', 'cat_id', 'title', 'subtitle', 'display_order', 'footer_display', 'status', 'description', 'publish_date'));


            if ($_FILES['image']['name']) {
                $this->load->library("image");
                $data['image'] = append_to_filename($_FILES['image']['name'], time());
                $this->image->resize($_FILES['image']['tmp_name'], UP_PATH . "testimonials/" . $data['image'], 600);
                if ($post['oldimage']) {
                    del_file(UP_PATH . "testimonials/" . $post['oldimage']);
                }
            }
            $data['publish_date'] = to_date_format($data['publish_date']);
            if ($this->master->save($data, "testimonials")) {
                $inf['success'] = 'T';
            }
        }

        json_data($inf);
    }

    function testimonial_dtl($id) {
        $dtl = $this->master->testimonial_dtl($id);
        $dtl['publish_date'] = get_date($dtl['publish_date']);
        json_data($dtl);
    }

    function delete_testimonial() {
        $post = $this->input->post();
        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        if (!$id) {
            json_data($inf);
        }

        if ($this->dba->delete("testimonials", "id='$id'")) {
            if ($post['image']) {
                del_file(UP_PATH . "testimonials/" . $post['image']);
            }
            $inf = array('success' => 'T', 'msg' => 'Testimonial deleted successfully');
        }

        json_data($inf);
    }

    /** FAQ * */
    function faq($init = '') {
        $data = $this->master->faq();
        if ($init) {
            $data['cats'] = $this->master->cats('FAQ');
        }
        json_data($data);
    }

    function save_faq() {
        $post = trim_array($this->input->post());
        $inf = array('success' => 'F', 'msg' => '');

        $id = $post['id'] = intval($post['id']);
        $this->load->library('form_validation');
        $v = $this->form_validation;
        $req = array('required' => '%s required', 'is_unique' => 'This %s is already used', 'numeric' => '%s must be numeric', 'integer' => '%s must be numeric');
        $v->set_rules('cat_id', 'Category', "required", $req);
        $v->set_rules('title', 'Question', "required", $req);

        $inf['success'] = 'F';
        if ($v->run() == FALSE) {
            $inf['errors'] = $v->get_errors();
        } else {
            $data = filter_post_data($post, array('id', 'cat_id', 'title', 'display_order', 'status', 'description'));
            if ($this->master->save($data, "faq")) {
                $inf['success'] = 'T';
            }
        }
        json_data($inf);
    }

    function faq_dtl($id) {
        $dtl = $this->master->faq_dtl($id);
        json_data($dtl);
    }

    function delete_faq() {
        $post = $this->input->post();
        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        if (!$id) {
            json_data($inf);
        }

        if ($this->dba->delete("faq", "id='$id'")) {
            $inf = array('success' => 'T', 'msg' => 'FAQ deleted successfully');
        }
        json_data($inf);
    }

    /** Menu * */
    function menu_init() {
        $data['hmenues'] = $this->master->menues('Header');
        $data['fmenues'] = $this->master->menues('Footer');
        $data['cms_pages'] = $this->master->cms_pages();
        $data['other_pages'] = array(
            'products' => 'Books & Products',
            'testimonials' => 'Testimonials',
            'gallery' => 'Image Gallery',
            'courses' => 'Courses',
            'free_courses' => 'Free Courses',
            'login' => 'Login Page',
            'registration' => 'Registration Page',
            'faq' => "FAQ's",
            'shakti_centers' => 'Shakti Centers'
        );

        $data['link_open_type'] = array('_blank' => 'New Tab', '' => 'Same Tab');
        $data['quicklinks'] = $this->master->quick_links();

        json_data($data);
    }

    function save_menu() {
        $post = trim_array($this->input->post());
        $inf = array('success' => 'F', 'msg' => '');

        $id = $post['id'] = intval($post['id']);
        $this->load->library('form_validation');
        $v = $this->form_validation;
        $req = array('required' => '%s required', 'is_unique' => 'This %s is already used');
        $v->set_rules('type', 'Type', "required", $req);
        $v->set_rules('title', 'Menu Title', "required", $req);

        $inf['success'] = 'F';
        if ($v->run() == FALSE) {
            $inf['errors'] = $v->get_errors();
        } else {
            $data = filter_post_data($post, array('id', 'parent_id', 'type', 'menu_type', 'link_open_type', 'title', 'page_id', 'other_page', 'link', 'display_order'));
            if ($post['type'] == 'Submenu') {
                $type = $this->dba->val("menues", "id='" . intval($post['parent_id']) . "'", "type");
                $data['type'] = $type ? $type : '';
            }

            if ($this->master->save($data, "menues")) {
                $inf['success'] = 'T';
                $inf['msg'] = $id ? "Menu updated successfully" : "Menu added successfully";
                $inf['type'] = $post['type'];
                if ($post['type'] == 'Submenu') {
                    $inf['submenues'] = $this->master->sub_menues($post['parent_id']);
                } else {
                    $inf['menues'] = $this->master->menues($post['type']);
                }
            }
        }

        json_data($inf);
    }

    function menu_dtl($id) {
        $dtl = $this->master->menu_dtl($id);
        json_data($dtl);
    }

    function delete_menu() {
        $post = $this->input->post();
        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        if (!$id) {
            json_data($inf);
        }

        if ($this->dba->delete("menues", "id='$id'")) {
            $this->dba->delete("menues", "parent_id='$id'");
            $inf['success'] = 'T';
            $inf['msg'] = 'Menu deleted successfully';

            if ($post['type'] == 'Submenu') {
                $inf['submenues'] = $this->master->sub_menues($post['parent_id']);
            } else {
                $inf['menues'] = $this->master->menues($post['type']);
            }
        }

        json_data($inf);
    }

    function sub_menues($parent_id = 0) {
        $data['submenues'] = $this->master->sub_menues($parent_id);
        json_data($data);
    }

    /** Quick Links * */
    function quick_link_dtl($id) {
        $dtl = $this->master->quick_link_dtl($id);
        json_data($dtl);
    }

    function save_quick_link() {
        $post = trim_array($this->input->post());
        $inf = array('success' => 'F', 'msg' => '');

        $id = $post['id'] = intval($post['id']);
        $this->load->library('form_validation');
        $v = $this->form_validation;
        $req = array('required' => '%s required', 'is_unique' => 'This %s is already used');
        $v->set_rules('title', 'Title', "required", $req);
        $v->set_rules('link', 'Link', "required", $req);

        $inf['success'] = 'F';
        if ($v->run() == FALSE) {
            $inf['errors'] = $v->get_errors();
        } else {
            $data = filter_post_data($post, array('id', 'title', 'link', 'icon_class', 'display_order'));

            if ($this->master->save($data, "quick_links")) {
                $inf['success'] = 'T';
                $inf['msg'] = $id ? "Quick Link updated successfully" : "Quick Link added successfully";
                $inf['quicklinks'] = $this->master->quick_links();
            }
        }

        json_data($inf);
    }

    function delete_quick_link() {
        $post = $this->input->post();
        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        if (!$id) {
            json_data($inf);
        }

        if ($this->dba->delete("quick_links", "id='$id'")) {
            $inf['success'] = 'T';
            $inf['msg'] = 'Quick Link deleted successfully';
            $inf['quicklinks'] = $this->master->quick_links();
        }

        json_data($inf);
    }

    /** Testimonials * */
    function shakticenters($init = '') {
        $data = $this->master->shakticenters();
        if ($init) {
            $data['cats'] = $this->master->cats('course');
        }
        json_data($data);
    }

    function save_shakticenters() {
        $post = trim_array($this->input->post());
        $inf = array('success' => 'F', 'msg' => '');

        $id = $post['id'] = intval($post['id']);
        $this->load->library('form_validation');
        $v = $this->form_validation;
        $req = array('required' => '%s required', 'is_unique' => 'This %s is already used', 'numeric' => '%s must be numeric', 'integer' => '%s must be numeric');
        $v->set_rules('title', 'Title', "required", $req);
        $v->set_rules('subtitle', 'Sub Title', "required", $req);
        $v->set_rules('description', 'Description', "required", $req);
        $v->set_rules('location', 'Location', "required", $req);

        $inf['success'] = 'F';
        if ($v->run() == FALSE) {
            $inf['errors'] = $v->get_errors();
        } else {

            $data = filter_post_data($post, array('id', 'title', 'subtitle', 'location', 'display_order', 'status', 'description'));

            if ($_FILES['image']['name']) {
                $this->load->library("image");
                $data['image'] = append_to_filename($_FILES['image']['name'], time());
                $this->image->resize($_FILES['image']['tmp_name'], UP_PATH . "shakticenters/" . $data['image'], 600);
                if ($post['oldimage']) {
                    del_file(UP_PATH . "shakticenters/" . $post['oldimage']);
                }
            }
//            $data['publish_date'] = to_date_format($data['publish_date']);
            if ($this->master->save($data, "shakti_centers")) {
                $inf['success'] = 'T';
            }
        }

        json_data($inf);
    }

    function shakticenters_dtl($id) {
        $dtl = $this->master->shakticenters_dtl($id);
        json_data($dtl);
    }

    function delete_shakticenters() {
        $post = $this->input->post();
        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        if (!$id) {
            json_data($inf);
        }

        if ($this->dba->delete("shakti_centers", "id='$id'")) {
            if ($post['image']) {
                del_file(UP_PATH . "shakticenters/" . $post['image']);
            }
            $inf = array('success' => 'T', 'msg' => 'Shakti Centers deleted successfully');
        }

        json_data($inf);
    }

}

//EOF