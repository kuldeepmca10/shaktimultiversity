<?php

class Auth extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/auth_model', 'auth');
        $this->load->model("admin/user_model", "user");
    }

    function login() {
        redirect_logged();
        $data = array();
        $post = $this->input->post();

        if (isset($post['username'])) {
            $this->load->library('form_validation');
            $v = $this->form_validation;
            $req = array('required' => '%s required');
            $v->set_rules('username', 'Username', 'required', $req);
//            $v->set_rules('password', 'Password', 'required', $req);
            $data['dtl'] = $post;
            if ($v->run() == FALSE) {
                $data['errors'] = $v->get_errors();
            } else {
                $dtl = $this->auth->checkEmail($post);
                if ($dtl) {
                    if ($dtl['status'] == 1) {

                        $aUserInformation = array();
                        $sOTP = rand(111111, 999999);
                        $aUserInformation['id'] = $dtl['id'];
                        $aUserInformation['password'] = encrypt_text($sOTP);
                        $sUserUpdated = $this->user->save($aUserInformation);


                        /* Send Email Start */
                        $sSendEmail = true;
                        $sEmailID = $dtl['email'];
                        if (isset($sEmailID) && !empty($sEmailID) && $sSendEmail == true) {

                            set_session(USR_SESSION_NAME, array('aAdminLogin' => array('sOTP' => $sOTP, 'sTime' => date('Y-m-d H:i:s'), 'username' => $dtl['username'])));

                            $sFromEmail = sSITE_FROM_EMAIL;
                            $sFromName = sSITE_FROM_NAME;
                            $sEmailTo = $sEmailID;
                            $aUserEmailData = array('name' => $dtl['fname'], 'email' => $sEmailID, 'sOTP' => $sOTP);
                            $sHTMLContent = $this->load->view('templates/mail/adminverifyaccount.php', $aUserEmailData, TRUE);
                            $this->load->library('email');
                            $this->email->from($sFromEmail, $sFromName);
                            $this->email->to($sEmailTo);
                            $this->email->subject('The Shakti Multiversity Admin OTP Email');
                            $this->email->message($sHTMLContent);

                            if ($this->email->send()) {
                                $data['msg'] = '<div class="alert alert-info">Password has been sent to email</div>';
                                $data['passwordSent'] = 'sent';
                            } else {
                                $data['msg'] = '<div class="alert alert-danger">Please try again later</div>';
                            }
                        }
                        /* Send Email End */
                    } else {
                        $data['msg'] = '<div class="alert alert-danger">Your account is inactive!</div>';
                    }
                } else {
                    $data['msg'] = '<div class="alert alert-danger">Invalid username or password!</div>';
                }
            }
        } else if (isset($post['password'])) {
            $aUserSession = get_session(USR_SESSION_NAME);
            $dtl = $this->auth->checkEmail($aUserSession['aAdminLogin']);

            if ($aUserSession['aAdminLogin']['sOTP'] == $post['password'] && isset($dtl) && !empty($dtl['username'])) {
                $start_date = new DateTime($aUserSession['aAdminLogin']['sTime']);
                $aDifference = $start_date->diff(new DateTime(date('Y-m-d H:i:s')));

                if ($aDifference->i < 10) {
                    session_regenerate_id(TRUE);
                    set_session(ADM_SESSION_NAME, $dtl);
                    redirect_logged();
                } else {
                    $data['msg'] = '<div class="alert alert-danger">OTP has been expired</div>';
                    $data['refresh'] = true;
                }
            } else {
                $data['msg'] = '<div class="alert alert-danger">OTP has been expired</div>';
                $data['refresh'] = true;
            }
        }
        $data['page_title'] = "Login | " . SITE_NAME;
        admin_view('admin/user/login', $data, 'admin_login');
    }

    function verify() {
        redirect_logged();
        $data = array();
        $post = $this->input->post();
        if (isset($post['username'])) {
            $this->load->library('form_validation');
            $v = $this->form_validation;
            $req = array('required' => '%s required');
            $v->set_rules('username', 'Username', 'required', $req);
//            $v->set_rules('password', 'Password', 'required', $req);
            $data['dtl'] = $post;
            if ($v->run() == FALSE) {
                $data['errors'] = $v->get_errors();
            } else {
                $dtl = $this->auth->checkEmail($post);
                if ($dtl) {
                    if ($dtl['status'] == 1) {
                        $dtl = $this->auth->checkEmail($post);
                        session_regenerate_id(TRUE);
                        set_session(ADM_SESSION_NAME, $dtl);
                        redirect_logged();
                    } else {
                        $data['msg'] = '<div class="alert alert-danger">Your account is inactive!</div>';
                    }
                } else {
                    $data['msg'] = '<div class="alert alert-danger">Invalid username or password!</div>';
                }
            }
        }
        $data['page_title'] = "Login | " . SITE_NAME;
        admin_view('admin/user/login', $data, 'admin_login');
    }

    function logout() {
        delete_session(ADM_SESSION_NAME);
        redirect(ADM_URL);
    }

}

//EOF