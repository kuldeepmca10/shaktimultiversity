<?php

class User extends MY_Controller {

    function __construct() {
        parent::__construct();
        not_logged_res();
        $this->load->model("admin/user_model", "user");
    }

    function profile() {
        $dtl = $this->user->detail(ADM_USER_ID);
        json_data($dtl);
    }

    function update_profile() {
        $post = trim_array($this->input->post());

        if ($post) {
            $id = ADM_USER_ID;
            $this->load->library('form_validation');
            $v = $this->form_validation;
            $req = array('required' => '%s required', 'is_unique' => 'This %s is already used', 'integer' => '%s must be numeric', 'min_length' => '%s must be 10 digits',
                'max_length' => '%s must be 10 digits');
            $v->set_rules('fname', 'Name', 'required', $req);
            $v->set_rules('username', 'Username', "required|is_unique[system_users.username.id!='$id']", $req);
            $v->set_rules('email', 'Email', "required|valid_email|is_unique[system_users.email.id!='$id']", $req);
            $v->set_rules('mobile', 'Mobile No.', "required|integer|min_length[10]|max_length[10]", $req);

            $inf['success'] = 'F';
            if ($v->run() == FALSE) {
                $inf['errors'] = $v->get_errors();
            } else {
                $post['id'] = $id;
                if ($id = $this->user->save($post)) {
                    $inf['id'] = $id;
                    $inf['success'] = 'T';
                }
            }
            json_data($inf);
        }
    }

    function change_pass() {
        $post = trim_array($this->input->post());

        if ($post) {
            $this->load->library('form_validation');
            $v = $this->form_validation;
            $req = array('required' => '%s required');
            $v->set_rules('password', 'Password', 'required', $req);
            $v->set_rules('repassword', 'Confirm Password', 'required|callback_passwordmatch', $req);

            $post['success'] = 'F';
            if ($v->run() == FALSE) {
                $post['errors'] = $v->get_errors();
            } else {
                $inf = array('id' => $post['id'] ? intval($post['id']) : ADM_USER_ID, 'password' => encrypt_text($post['password']));
                if ($this->user->save($inf)) {
                    $post['success'] = 'T';
                }
            }
            json_data($post);
        }
    }

    function passwordmatch($v) {
        $post = trim_array($this->input->post());
        if ($post['password'] != $post['repassword']) {
            $this->form_validation->set_message('passwordmatch', 'Password mismatch!');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /** Roles * */
    function roles($p = 1) {
        $data = $this->user->roles($p, 20);
        json_data($data);
    }

    function role_detail($id = 0) {
        if (ctype_digit($id) and $id) {
            $data['dtl'] = $this->user->role_detail($id);
        }
        json_data($data);
    }

    function save_role() {
        $post = trim_array($this->input->post());

        if ($post) {
            $id = $post['id'] = intval($post['id']);
            $this->load->library('form_validation');
            $v = $this->form_validation;
            $req = array('required' => '%s required', 'is_unique' => 'This %s is already used', 'numeric' => '%s must be numeric', 'integer' => '%s must be numeric');
            $v->set_rules('role_name', 'Role Name', "required|is_unique[system_users_roles.role_name.id!='$id']", $req);

            $inf['success'] = 'F';
            if ($v->run() == FALSE) {
                $inf['errors'] = $v->get_errors();
            } else {
                $data = filter_post_data($post, array('id', 'role_name'));
                if ($id = $this->user->save_role($data)) {
                    $inf['id'] = $id;
                    $inf['success'] = 'T';
                }
            }
            json_data($inf);
        }
    }

    function role_access_detail($id) {
        $data = array();
        if (ctype_digit($id) and $id) {
            $data['role_dtl'] = $this->user->role_detail($id);
            $data['access_controls'] = $this->user->access_controls();
            $data['permissions'] = $this->user->access_permissions($id);
        }

        json_data($data);
    }

    function save_permissions() {
        $post = $this->input->post();
        $role_id = intval($post['role_id']);
        if ($role_id == 1 or ! $role_id) {
            return;
        }

        $inf['success'] = 'F';
        if ($this->user->save_permissions($post, $role_id)) {
            $inf['success'] = 'T';
        }

        json_data($inf);
    }

    /** Users * */
    function users($p = 1) {
        $data = $this->user->users();
        $data['roles'] = $this->common->roles();
        json_data($data);
    }

    function user_detail($id = 0) {
        if (ctype_digit($id) and $id) {
            $data['dtl'] = $this->user->detail($id);
        }
        $data['roles'] = $this->common->roles();

        json_data($data);
    }

    function save_user() {
        $post = trim_array($this->input->post());

        $id = $post['id'] = intval($post['id']);
        $this->load->library('form_validation');
        $v = $this->form_validation;
        $req = array('required' => '%s required', 'is_unique' => 'This %s is already used', 'integer' => '%s must be numeric', 'min_length' => '%s must be 10 digits',
            'max_length' => '%s must be 10 digits');
        $v->set_rules('fname', 'Name', 'required', $req);
        $v->set_rules('username', 'Username', "required|is_unique[system_users.username.id!='$id']", $req);
        $v->set_rules('email', 'Email', "required|valid_email|is_unique[system_users.email.id!='$id']", $req);

        if (!$id) {
            $v->set_rules('password', 'Password', 'required', $req);
            $v->set_rules('repassword', 'Confirm Password', 'required|callback_passwordmatch', $req);
        }

        $v->set_rules('mobile', 'Mobile No.', "required|integer|min_length[10]|max_length[10]", $req);
        $v->set_rules('role_id', 'Role', 'required|callback_checkadminroles', $req);

        $inf['success'] = 'F';
        if ($v->run() == FALSE) {
            $inf['errors'] = $v->get_errors();
        } else {
            $data = filter_post_data($post, array('id', 'role_id', 'fname', 'lname', 'username', 'email', 'mobile', 'phone', 'status'));
            if (!$id) {
                $data['password'] = encrypt_text($post['password']);
            }

            if ($id = $this->user->save($data)) {
                $inf['id'] = $id;
                $inf['success'] = 'T';
            }
        }
        json_data($inf);
    }

    function checkadminroles($v) {
        $id = intval($this->input->post('id'));
        $client_id = intval($this->input->post('client_id'));
        $locid = intval($this->input->post('reference_id'));
        switch ($v) {
            case 1:
                if ($this->dba->val("system_users", "role_id=1 AND id!='$id'", "id")) {
                    $this->form_validation->set_message('checkadminroles', 'Administrator is already added');
                    return FALSE;
                }
                break;
        }

        return TRUE;
    }

    function newsletter() {
        $aResponse = $this->user->newsletter_list();

        echo json_encode($aResponse);
        die;
    }

    function newsletter_detail($id) {

        $aResponse = $this->user->newsletter_detail($id);

        echo json_encode($aResponse);
        die;
    }

    function save_newsletter() {
        $post = trim_array($this->input->post());

        if ($post) {
            $id = $post['id'];
            $this->load->library('form_validation');
            $v = $this->form_validation;

            $v->set_rules('email', 'Email', "required|valid_email|is_unique[newsletter_subscriber.email.id!='$id']", $req);

            $inf['success'] = 'F';
            if ($v->run() == FALSE) {
                $inf['errors'] = $v->get_errors();
            } else {

                if ($id = $this->user->save($post, 'newsletter_subscriber')) {
                    $inf['id'] = $id;
                    $inf['success'] = 'T';
                }
            }
            json_data($inf);
        }
    }

    function export_newsletter() {
        // Load the Library
        $this->load->library("excel");

        $aResponse = $this->user->newsletter_list();

        $this->excel->setActiveSheetIndex(0);
        // Gets all the data using MY_Model.php

        $filename = 'newsletter_subscriber.xls';
        $this->excel->stream($filename, $aResponse['result']);
        die;

        echo json_encode($aResponse);
        die;
    }

    function delete_newsletter() {
        $post = $this->input->post();
        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        if (!$id) {
            json_data($inf);
        }

        if ($this->dba->delete("newsletter_subscriber", "id='$id'")) {
            $inf = array('success' => 'T', 'msg' => 'Newsletter subscriber has been deleted successfully');
        }

        json_data($inf);
    }

    function book_request() {
        $aResponse = $this->user->book_request();

        echo json_encode($aResponse);
        die;
    }

    function service() {
        $aResponse = $this->user->service_request();

        echo json_encode($aResponse);
        die;
    }

    function service_detail($id) {

        $aResponse = $this->user->service_request_detail($id);

        echo json_encode($aResponse);
        die;
    }

    function save_service() {
        $post = trim_array($this->input->post());

        if ($post) {
            $id = $post['id'];
            $this->load->library('form_validation');
            $v = $this->form_validation;

            $v->set_rules('email', 'Email', "required|valid_email|is_unique[newsletter_subscriber.email.id!='$id']", $req);

            $inf['success'] = 'F';
            if ($v->run() == FALSE) {
                $inf['errors'] = $v->get_errors();
            } else {

                if ($id = $this->user->save($post, 'newsletter_subscriber')) {
                    $inf['id'] = $id;
                    $inf['success'] = 'T';
                }
            }
            json_data($inf);
        }
    }

    function export_service() {
        // Load the Library
        $this->load->library("excel");

        $aResponse = $this->user->service_request();

        $this->excel->setActiveSheetIndex(0);
        // Gets all the data using MY_Model.php

        $filename = 'service_request.xls';
        $this->excel->stream($filename, $aResponse['result']);
        die;

        echo json_encode($aResponse);
        die;
    }

    function delete_service() {
        $post = $this->input->post();
        $inf = array('success' => 'F', 'msg' => 'Error!');
        $id = intval($post['id']);
        if (!$id) {
            json_data($inf);
        }

        if ($this->dba->delete("service_request", "id='$id'")) {
            $inf = array('success' => 'T', 'msg' => 'Request has been deleted successfully');
        }

        json_data($inf);
    }

}

//EOF