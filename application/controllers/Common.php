<?php

class Common extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function error404() {
        $this->load->view('layouts/error404');
    }

    function getLatLong() {
        $loc = $this->input->post('loc');
        $c = get_lat_long($loc);
        echo json_encode($c);
    }

    function getstate($id) {
        $aStateList = $this->common->state($id);
        if (isset($aStateList) && !empty($aStateList)) {
            $sStateHtml = '<option value=""> -- Select --</option>';
            foreach ($aStateList as $aState) {
                $sStateHtml .='<option value="' . $aState['id'] . '">' . $aState['name'] . '</option>';
            }
            echo $sStateHtml;
            die;
        }
    }

    function test() {
        parse_str(parse_url("https://www.youtube.com/watch?v=DXYJ8gIzMjw", PHP_URL_QUERY), $obj);
        pr($obj);
    }

}

//EOF