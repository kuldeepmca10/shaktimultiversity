<?php

class User extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model("user_model", "user");
        $this->load->model("page_model", "page");
        $this->load->model("common_model", "common");
    }

    function img_ext_check($v, $param = 'image') {
        if ($_FILES[$param]['name'] and ! check_image_ext($_FILES[$param]['name'])) {
            $this->form_validation->set_message('img_ext_check', 'Please upload .jpg, .jpeg, .gif or .png file only');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function dashboard() {
        $aUserData = get_session(USR_SESSION_NAME);
        if (!isset($aUserData['aUserData']) || empty($aUserData['aUserData'])) {
            redirect(URL);
        }

//$aUserData['aUserData'] = $this->user->detail($aUserData['aUserData']['id']);
//        $aUserData['aStats'] = $this->user->fnCourseEnrollmentStats($aUserData['aUserData']['id']);
//
//        $aPostData = array();
//        if (isset($aUserData['aStats']) && !empty($aUserData['aStats'])) {
//            $aPostData['totalEnrollment'] = $aUserData['aStats'][0]['totalEnrollment'];
//
//            foreach ($aUserData['aStats'] as $aStats) {
//                $aPostData[$aStats['approval_status']] = $aStats['totalEnrollment'];
//            }
//        }

        $post = trim_array($this->input->post());
        $aUserData['CSS'] = array('theme/front/css/dashboard.css?v=' . VERSION);
        $aUserData['page_title'] = 'User Dashboard';
        $aUserData['aStats'] = $aPostData;


        $aUserData['aEnrollmentList'] = $this->user->courseEnrollmentList('', $aUserData['aUserData']['id'], '', true);

        $post = trim_array($this->input->post());
        $aUserData['CSS'] = array('theme/front/css/dashboard.css?v=' . VERSION);
        $aUserData['page_title'] = 'User Dashboard';
        view('user/dashboard', $aUserData);
    }

    function update_profile() {

        /* Initalized Variables Start */
        $aUserData = array();
        $aData = array();
        $sStatus = false;
        $sTitle = 'ERROR';
        $sIcon = '<i class="fa fa-thumbs-o-up"></i>';
        /* Initalized Variables End */

        /* Get And Fetch User Start */
        $aUserSessionData = get_session(USR_SESSION_NAME);
        $aCountryList = $this->common->countries();


        if (isset($aUserSessionData['aUserData']['id']) && !empty($aUserSessionData['aUserData']['id'])) {
            $aUserData['aUserData'] = $this->user->detail($aUserSessionData['aUserData']['id']);
        } else {
            redirect(URL);
        }
        $aStateList = $this->common->state($aUserData['aUserData']['country']);
        /* Get And Fetch User Start */

        $post = trim_array($this->input->post());
        if (isset($post) && !empty($post)) {

            $iUserID = $aUserSessionData['aUserData']['id'];
            $aUserInformation = array();
            $aUserInformation = $post;
            $aUserInformation['id'] = $iUserID;
            if (isset($aUserInformation['country']) && !empty($aUserInformation['country'])) {
                $sCountry = explode('_', $aUserInformation['country']);
                $aUserInformation['country'] = $sCountry[0];
            }

            if (isset($aUserInformation['date']) && isset($aUserInformation['month']) && isset($aUserInformation['year'])) {
                $aUserInformation['dob'] = $aUserInformation['date'] . '/' . $aUserInformation['month'] . '/' . $aUserInformation['year'];
            }

            if (isset($_FILES['profile_photo'])) {
                $errors = array();
                $sFileName = $_FILES['profile_photo']['name'];
                $sFileSize = $_FILES['profile_photo']['size'];
                $sFileTemp = $_FILES['profile_photo']['tmp_name'];
                $sFileType = $_FILES['profile_photo']['type'];
                $aFileExtension = explode('.', $sFileName);
                $sFileExtension = strtolower(end($aFileExtension));

                $aAllowedExtension = array("jpeg", "jpg");

                if (in_array($sFileExtension, $aAllowedExtension) === false) {
                    $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
                }

                if ($sFileSize > 2097152) {
                    $errors[] = 'File size must be excately 2 MB';
                }

                if (empty($errors) == true) {
                    $sFileName = "user_" . $iUserID . "." . $sFileExtension;
                    move_uploaded_file($sFileTemp, "uploads/user/" . $sFileName);
                    $aUserInformation['profile_photo'] = $sFileName;
                } else {
                    print_r($errors);
                }
            }

            if (isset($_FILES['id_proof'])) {
                $errors = array();
                $sFileName = $_FILES['id_proof']['name'];
                $sFileSize = $_FILES['id_proof']['size'];
                $sFileTemp = $_FILES['id_proof']['tmp_name'];
                $sFileType = $_FILES['id_proof']['type'];

                $aFileExtension = explode('.', $sFileName);
                $sFileExtension = strtolower(end($aFileExtension));

                $aAllowedExtension = array("jpeg", "jpg");

                if (in_array($sFileExtension, $aAllowedExtension) === false) {
                    $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
                }

                if ($sFileSize > 2097152) {
                    $errors[] = 'File size must be excately 2 MB';
                }

                if (empty($errors) == true) {
                    move_uploaded_file($sFileTemp, "uploads/user/id_proof_" . $iUserID . "." . $sFileExtension);
                    $sFileName = "id_proof_" . $iUserID . "." . $sFileExtension;
                    move_uploaded_file($sFileTemp, "uploads/user/" . $sFileName);
                    $aUserInformation['id_proof'] = $sFileName;
                } else {
                    print_r($errors);
                }
            }
            $aUserInformation['profile_complete'] = '1';
            $sUserUpdated = $this->user->save($aUserInformation);
            //            $this->session->set_flashdata('message', 'You have updated your information');
            if ($sUserUpdated) {
                $sStatus = true;
                $sTitle = 'CONGRATULATION';
                $sIcon = '<i class="fa fa-thumbs-o-up"></i>';
                $sMessage = 'You have updated your profile successfully';
                $aData = $this->user->detail($aUserSessionData['aUserData']['id']);
            } else {
                $sMessage = 'We are unable to find your profile';
            }
            $this->session->set_flashdata('item', array('message' => $sMessage, 'class' => 'success'));
            redirect(URL . 'user/update-profile');
            die;

            /* Prepare And Return Response Start */
            $aResponse = array();
            $aResponse['status'] = $sStatus;
            $aResponse['icon'] = $sIcon;
            $aResponse['title'] = $sTitle;
            $aResponse['message'] = $sMessage;
            $aResponse['data'] = $aData;
            echo json_encode($aResponse);
            die;
            /* Prepare And Return Response End */
        }

        $aUserData['aCountries'] = $aCountryList;
        $aUserData['aStateList'] = $aStateList;
        $aUserData['CSS'] = array('theme/front/css/dashboard.css?v=' . VERSION);
        $aUserData['page_title'] = 'Update Profile';
        view('user/update_profile', $aUserData);
    }

    function change_password() {

        /* Initalized Variables Start */
        $aUserData = array();
        $aData = array();
        $sStatus = false;
        $sTitle = 'ERROR';
        $sIcon = '<i class="fa fa-exclamation"></i>';
        /* Initalized Variables End */

        /* Get And Fetch User Start */
        $aUserSessionData = get_session(USR_SESSION_NAME);
        $aUserData['aUserData'] = $this->user->detail($aUserSessionData['aUserData']['id']);
        if (!isset($aUserData['aUserData']) || empty($aUserData['aUserData'])) {
            redirect(URL);
        }
        /* Get And Fetch User Start */


        $post = trim_array($this->input->post());
        if (isset($post) && !empty($post)) {
            if (empty($post['sCurrentPassword'])) {
                $sMessage = 'Please provide current password';
            } else if (empty($post['sNewPassword'])) {
                $sMessage = 'Please provide new password';
            } else {
                $aUserSessionData = get_session(USR_SESSION_NAME);
                $aUserData = $this->user->detail($aUserSessionData['aUserData']['id'], '', encrypt_text($post['sCurrentPassword']));
                if (isset($aUserData) && !empty($aUserData) && count($aUserData) > 0) {
                    $aUserInformation = array();
                    $aUserInformation['id'] = $aUserSessionData['aUserData']['id'];
                    $aUserInformation['password'] = encrypt_text($post['sNewPassword']);
                    $sUserUpdated = $this->user->save($aUserInformation);
                    if ($sUserUpdated) {
                        $sStatus = true;
                        $sTitle = 'CONGRATULATION';
                        $sIcon = '<i class="fa fa-thumbs-o-up"></i>';
                        $sMessage = 'You have updated your pasword successfully';

                        $sFromEmail = sSITE_FROM_EMAIL;
                        $sFromName = sSITE_FROM_NAME;
                        $sEmailTo = $aUserData['email'];

                        $sHTMLContent = $this->load->view('templates/mail/customer-change-password.php', $aUserData, TRUE);

                        $this->load->library('email');
                        $this->email->from($sFromEmail, $sFromName);
                        $this->email->to($sEmailTo);
                        $this->email->subject('The Shakti Multiversity - Your Password has been changed!');
                        $this->email->message($sHTMLContent);
                        $this->email->send();
                        $sMessage = 'Your query has been recieved, Allow us 24-48 hrs to reply back to you';
                        /* Sent Email to admin start */
                    } else {
                        $sMessage = 'We are currently experiencing server issues, working towards resolution. Please try again later.';
                    }
                } else {
                    $sMessage = 'Current password is incorrect';
                }
                /* Prepare And Return Response Start */
            }
            $aResponse = array();
            $aResponse['status'] = $sStatus;
            $aResponse['icon'] = $sIcon;
            $aResponse['title'] = $sTitle;
            $aResponse['message'] = $sMessage;
            $aResponse['data'] = $aData;
            echo json_encode($aResponse);
            die;
            /* Prepare And Return Response Start */
        }

        $aUserData['aCuntryListWithPhoneCode'] = $this->common->countries();
        $aUserData['CSS'] = array('theme/front/css/dashboard.css?v=' . VERSION);
        $aUserData['page_title'] = 'Update Profile';
        view('user/change_password', $aUserData);
    }

    function unsubscribe_enrollment($iEnrollmentID) {

        /* Initalized Variables Start */
        $aUserData = array();
        $aData = array();
        $sStatus = false;
        $sTitle = 'ERROR';
        $sIcon = '<i class="fa fa-thumbs-o-up"></i>';
        /* Initalized Variables End */

        /* Get And Fetch User Start */
        $aUserSessionData = get_session(USR_SESSION_NAME);
        $aUserData['aUserData'] = $this->user->detail($aUserSessionData['aUserData']['id']);
        if (!isset($aUserData['aUserData']) || empty($aUserData['aUserData'])) {
            redirect(URL);
        }
        /* Get And Fetch User Start */

        if (isset($iEnrollmentID) && !empty($iEnrollmentID)) {
            $aEnrollemtDetail = $this->user->userEnrollmentDetail($iEnrollmentID, $aUserData['id']);

            if (isset($aEnrollemtDetail['result']) && !empty($aEnrollemtDetail['result'])) {
                if ($aEnrollemtDetail['result'][0]['status'] == 1) {
                    $aCourseEnrollmentData['id'] = $iEnrollmentID;

                    $aCourseEnrollmentData['status'] = '2';
                    $iEnrollmentID = $this->user->updateCourseEnrollment($aCourseEnrollmentData);

                    $sStatus = true;
                    $sTitle = 'CONGRATULATION';
                    $sIcon = '<i class="fa fa-thumbs-o-up"></i>';
                    $sMessage = 'You have unsubscribed for this course successfully';
                } else if ($aEnrollemtDetail['result'][0]['status'] == 2) {
                    $sMessage = 'You are already unsubscribed for this course';
                } else if ($aEnrollemtDetail['result'][0]['status'] == 0) {
                    $sMessage = 'Request is pending for approval';
                }
            } else {
                $sMessage = 'No Detail Found';
            }
        } else {
            $sMessage = 'No Detail Found';
        }

        /* Prepare And Return Response Start */
        $aResponse = array();
        $aResponse['status'] = $sStatus;
        $aResponse['icon'] = $sIcon;
        $aResponse['title'] = $sTitle;
        $aResponse['message'] = $sMessage;
        $aResponse['data'] = $aData;
        echo json_encode($aResponse);
        die;
        /* Prepare And Return Response Start */

        $aUserData['aEnrollmentList'] = $this->user->courseEnrollmentList($aUserData['id']);

        $post = trim_array($this->input->post());
        $aUserData['CSS'] = array('theme/front/css/dashboard.css?v=' . VERSION);
        $aUserData['page_title'] = 'User Dashboard';
        view('user/course_enrollment', $aUserData);
    }

    function enrollment_feedback() {

        /* Initalized Variables Start */
        $aUserData = array();
        $aData = array();
        $sStatus = false;
        $sTitle = 'ERROR';
        $sIcon = '<i class="fa fa-thumbs-o-up"></i>';
        /* Initalized Variables End */

        /* Get And Fetch User Start */
        $aUserSessionData = get_session(USR_SESSION_NAME);
        $aUserData['aUserData'] = $this->user->detail($aUserSessionData['aUserData']['id']);
        if (!isset($aUserData['aUserData']) || empty($aUserData['aUserData'])) {
            redirect(URL);
        }
        /* Get And Fetch User Start */
        $post = trim_array($this->input->post());
        $sFeedbackMessage = $post['sFeedbackMessage'];
        $iEnrollmentID = $post['sEnrollmentID'];


        if (isset($iEnrollmentID) && !empty($iEnrollmentID)) {
            $aEnrollemtDetail = $this->user->userEnrollmentDetail($iEnrollmentID, $aUserData['id']);

            if (isset($aEnrollemtDetail['result'][0]) && !empty($aEnrollemtDetail['result'][0])) {

                //$sFeedbackMessage Send Email
                if (isset($aEnrollemtDetail['result'][0]['enrollment_number']) && !empty($aEnrollemtDetail['result'][0]['enrollment_number'])) {
                    $sFromEmail = sSITE_FROM_EMAIL;
                    $sFromName = sSITE_FROM_NAME;
                    $sEmailTo = sSITE_RECIEVE_NAME;
                    $aUserEmailData = array('name' => 'Admin', 'sMessage' => $sFeedbackMessage);
                    $sHTMLContent = $this->load->view('templates/mail/send_query.php', $aUserEmailData, TRUE);
                    $this->load->library('email');
                    $this->email->from($sFromEmail, $sFromName);
                    $this->email->to($sEmailTo = 'kuldeep.mca10@gmail.com');
                    $this->email->subject('ACTION REQUIRED! QUERY REGARDING ENROLLMENT NUMBER :- ' . $aEnrollemtDetail['result'][0]['enrollment_number']);
                    $this->email->message($sHTMLContent);

                    if ($this->email->send()) {
                        $sStatus = true;
                        $sTitle = 'Thank You!';
                        $sIcon = '<i class="fa fa-thumbs-o-up"></i>';
                        $sMessage = 'We have sent your query to our team. We will get back to you soon';
                    } else {
                        $sStatus = true;
                        $sTitle = 'Warning';
                        $sIcon = '<i class="fa fa-exclamation"></i>';
                        $sMessage = 'Please try again later';
                    }
                } else {
                    $sMessage = 'No Detail Found';
                }
            } else {
                $sMessage = 'No Detail Found';
            }
        } else {
            $sMessage = 'No Detail Found';
        }

        /* Prepare And Return Response Start */
        $aResponse = array();
        $aResponse['status'] = $sStatus;
        $aResponse['icon'] = $sIcon;
        $aResponse['title'] = $sTitle;
        $aResponse['message'] = $sMessage;
        $aResponse['data'] = $aData;
        echo json_encode($aResponse);
        die;
        /* Prepare And Return Response Start */

        $aUserData['aEnrollmentList'] = $this->user->courseEnrollmentList($aUserData['id']);

        $post = trim_array($this->input->post());
        $aUserData['CSS'] = array('theme/front/css/dashboard.css?v=' . VERSION);
        $aUserData['page_title'] = 'User Dashboard';
        view('user/course_enrollment', $aUserData);
    }

    function course_enrollment() {
        $aUserData = get_session(USR_SESSION_NAME);

        if (!isset($aUserData['aUserData']) || empty($aUserData['aUserData'])) {
            redirect(URL);
        }

        $aUserData['aEnrollmentList'] = $this->user->courseEnrollmentList('', $aUserData['aUserData']['id']);

        $post = trim_array($this->input->post());
        $aUserData['CSS'] = array('theme/front/css/dashboard.css?v=' . VERSION);
        $aUserData['page_title'] = 'User Dashboard';
        view('user/course_enrollment', $aUserData);
    }

    function mark_your_attendance() {
        $aUserData = get_session(USR_SESSION_NAME);

        if (!isset($aUserData['aUserData']) || empty($aUserData['aUserData'])) {
            redirect(URL);
        }

        $aUserData['aEnrollmentList'] = $this->user->courseEnrollmentList('', $aUserData['aUserData']['id'], '', true);

        $post = trim_array($this->input->post());
        $aUserData['CSS'] = array('theme/front/css/dashboard.css?v=' . VERSION);
        $aUserData['page_title'] = 'Mark Your Attendance';
        view('user/mark_your_attendance', $aUserData);
    }

    function course_enrollment_detail($id) {
        $aUserData = get_session(USR_SESSION_NAME);
        if (!isset($aUserData['aUserData']) || empty($aUserData['aUserData'])) {
            redirect(URL);
        }
        $aEnrollmentDetail = $this->user->userEnrollmentDetail($id);


        $aUserData['aEnrollmentDetail'] = isset($aEnrollmentDetail['result'][0]) ? $aEnrollmentDetail['result'][0] : '';

        $aUserData['aSessionList'] = $this->page->batch_session_list($aUserData['aEnrollmentDetail']['batch_id']);

        $post = trim_array($this->input->post());
        $aUserData['CSS'] = array('theme/front/css/dashboard.css?v=' . VERSION);
        $aUserData['page_title'] = 'User Dashboard';
        $aUserData['sEnrollmentNumber'] = self::encrypt($aUserData['aEnrollmentDetail']['enrollment_number']);

        view('user/course_enrollment_detail', $aUserData);
    }

    function course_information($id) {

        $aUserData = get_session(USR_SESSION_NAME);
        if (!isset($aUserData['aUserData']) || empty($aUserData['aUserData'])) {
            redirect(URL);
        }
        $aEnrollmentDetail = $this->user->userEnrollmentDetail($id);


        $aUserData['aEnrollmentDetail'] = isset($aEnrollmentDetail['result'][0]) ? $aEnrollmentDetail['result'][0] : '';

        $aUserData['aSessionList'] = $this->page->batch_session_lession_list($aUserData['aEnrollmentDetail']['batch_id']);

        $post = trim_array($this->input->post());
        $aUserData['CSS'] = array('theme/front/css/dashboard.css?v=' . VERSION);
        $aUserData['page_title'] = 'User Dashboard';
        $aUserData['sEnrollmentNumber'] = self::encrypt($aUserData['aEnrollmentDetail']['enrollment_number']);

        view('user/course_information', $aUserData);
    }

    function subscribe_newsletter() {
        $post = trim_array($this->input->post());
        if ($post) {
            $sEmail = $post['subscriber_email'];
            if (isset($sEmail) && !empty($sEmail)) {
                $aUserData = $this->user->newsletter_detail($sEmail);
                if (isset($aUserData) && !empty($aUserData)) {
                    $sStatus = false;
                    $sTitle = 'INFORMATION';
                    $sIcon = '<i class="fa fa-exclamation"></i>';
                    $sMessage = 'You are already subscribed for newsletter';
                } else if (isset($aUserData) && empty($aUserData)) {
                    $aPostedData = array();
                    $aPostedData['email'] = $sEmail;
                    $aPostedData['created'] = date('Y-m-d H:i:s');
                    $aPostedData['status'] = '1';
                    $aPostedData['is_deleted'] = 'n';

                    $iUserID = $this->user->save($aPostedData, 'newsletter_subscriber');
                    if (isset($iUserID) && !empty($iUserID) && $iUserID > 0) {
                        $sStatus = true;
                        $sTitle = 'CONGRATULATION';
                        $sIcon = '<i class="fa fa-thumbs-o-up"></i>';
                        $sMessage = 'You have subscribed for newsletter successfully';
                    } else {
                        $sStatus = false;
                        $sTitle = 'ERROR';
                        $sIcon = '<i class="fa fa-exclamation"></i>';
                        $sMessage = 'Please try again';
                    }
                } else {
                    $sStatus = false;
                    $sTitle = 'ERROR';
                    $sIcon = '<i class="fa fa-exclamation"></i>';
                    $sMessage = 'Please provide email id';
                }
            } else {
                $sStatus = false;
                $sTitle = 'ERROR';
                $sIcon = '<i class="fa fa-exclamation"></i>';
                $sMessage = 'Please provide email id';
            }

            $aResponse = array();
            $aResponse['status'] = $sStatus;
            $aResponse['icon'] = $sIcon;
            $aResponse['title'] = $sTitle;
            $aResponse['message'] = $sMessage;

            echo json_encode($aResponse);
            die;
        }
    }

    function logout() {
        $aUserData = get_session(USR_SESSION_NAME);
        if (isset($aUserData['aUserData']) && !empty($aUserData['aUserData'])) {
            destroy_session(USR_SESSION_NAME);
        }
        redirect(URL);
    }

    private function encrypt($sString) {
        if (!$sString) {
            return false;
        }
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, sENC_KEY, $sString, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext));

        /* Encrypt Enrollment Id Start */
        $sEnrollmentNumber = $this->encryption->encrypt($sString);
        $sEnrollmentNumber = strtr($sEnrollmentNumber, array('+' => '.', '=' => '-', '/' => '~'));
        return $sEnrollmentNumber;
        /* Encrypt Enrollment Id End */
    }

    private function decrypt($sString) {
        if (!$sString) {
            return false;
        }
        $crypttext = $this->safe_b64decode($sString);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, sENC_KEY, $crypttext, MCRYPT_MODE_ECB, $iv);
        return trim($decrypttext);
    }

    public function safe_b64encode($string) {

        $data = base64_encode($string);
        $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
        return $data;
    }

    public function safe_b64decode($string) {
        $data = str_replace(array('-', '_'), array('+', '/'), $string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    function update_attendance() {

        /* Initalized Variables Start */
        $aUserData = array();
        $aData = array();
        $sStatus = false;
        $sTitle = 'ERROR';
        $sIcon = '<i class="fa fa-thumbs-o-up"></i>';
        /* Initalized Variables End */

        /* Get And Fetch User Start */
        $aUserSessionData = get_session(USR_SESSION_NAME);
        $aUserData['aUserData'] = $this->user->detail($aUserSessionData['aUserData']['id']);
        if (!isset($aUserData['aUserData']) || empty($aUserData['aUserData'])) {
            redirect(URL);
        }
        /* Get And Fetch User Start */

        if (isset($_REQUEST['enrollment_id']) && !empty($_REQUEST['enrollment_id'])) {
            $aData['id'] = $_REQUEST['id'];
            $aData['user_id'] = $aUserSessionData['aUserData']['id'];
            $aData['status'] = $_REQUEST['status'];
            $aData['batch_id'] = $_REQUEST['batch_id'];
            $aData['enrollment_id'] = $_REQUEST['enrollment_id'];
            $aData['session_id'] = $_REQUEST['session_id'];

            $aEnrollemtDetail = $this->user->updateAttandence($aData);

            if (isset($aEnrollemtDetail) && !empty($aEnrollemtDetail)) {
                $sStatus = true;
                $sTitle = 'CONGRATULATION';
                $sIcon = '<i class="fa fa-thumbs-o-up"></i>';
                $sMessage = 'You have mark your attendance for this session';
            } else {
                $sMessage = 'No Detail Found';
            }
        } else {
            $sMessage = 'No Detail Found';
        }

        /* Prepare And Return Response Start */
        $aResponse = array();
        $aResponse['status'] = $sStatus;
        $aResponse['icon'] = $sIcon;
        $aResponse['title'] = $sTitle;
        $aResponse['message'] = $sMessage;
        $aResponse['data'] = $aData;
        echo json_encode($aResponse);
        die;
        /* Prepare And Return Response Start */
    }

}
