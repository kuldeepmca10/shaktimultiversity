<?php
class MY_Controller extends CI_Controller {
    function __construct() {
        parent::__construct();
		$this->load->model("common_model", "common");
		
		define('IS_AJAX', $this->input->is_ajax_request()?TRUE:FALSE);
		
		define('USER_ID', 		get_session(USR_SESSION_NAME, 'id'));
		define('ADM_USER_ID', 	get_session(ADM_SESSION_NAME, 'id'));
    }
}

//EOF