   

function validateEmail(Email)
{
	var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if(reg.test(Email) == false)
	{
		return false;
	}
	return true;
}

$(document).ready(function(){
	var isClicked = true;
	var firsttime = 1;
	$("#btn_login").click(function()
	{
		var Email = $('#login_email').val();
		var Password = $('#login_password').val();
		if($("#remember").is(':checked')){
			var remember = 1;
		}
		else{
			var remember = 0;
		}
		if(Email == '') {
			displayError('email',$('#login_email'));
			/*$('#form-error').html('Enter Email'); 
			$('#form-error').show(100); */
			return false;
		}
		else if(!validateEmail(Email)) {
			displayError('email',$('#login_email'));
		  /* $('#form-error').html("Please Enter Valid Email");
		   $('#form-error').show(100); */
			return false;
		}
		else if(Password == '') {
			removeError('email',$('#login_email'));
			displayError('password',$('#login_password'));
			/*$('#form-error').html('Enter Password'); 
			$('#form-error').show(100); */
			return false;
		}
		else{
			removeError('email',$('#login_email'));
			removeError('password',$('#login_password'));

			if($('#captcha_code').length){
				var captcha = $('#captcha_code').val();
				if(captcha=='')
				{
					displayError('captcha',$('#captcha_code'));
					return false;
				}
			}
			else{
				var captcha = "no";
			}
			if(isClicked){
				isClicked = false;
				$("#loader-login").show();
				$.ajax({
					method: "POST",
					url: "http://nerdyturtlez.com/login.php",
					data: {
						'login_email': Email,
						'login_password': Password,
						'remember': remember,
						'captcha': captcha,
						'action': 'login',
					},
					success: function(data){
						
						isClicked = true;
						//alert(data);
						if(data=="2"){
							$("#loader-login").hide();
							window.location.href='http://nerdyturtlez.com/tutorexam/assessment-start.php';
						}else if(data=="1"){
							removeError('email',$('#login_email'));
							removeError('password',$('#login_password'));
							removeError('captcha',$('#captcha_code'));
							$.ajax({
								method: "POST",   
								url: "http://nerdyturtlez.com/tutor/nerdylogin.php",
								data:{
									'Email': Email,
									'remember': remember,
									'Password': Password
								},
								success: function(data){
									$("#loader-login").hide();
									window.location.reload();
								} 
							})
						//window.location.href='http://nerdyturtlez.com/tutor/nerdylogin.php?Email='+Email+'&Password='+Password;
						}else if(data=="4"){
							$("#loader-login").hide();
							$('#form-error').html("<p class='alert alert-danger'>You are an unauthorized user. Please contact admin.</p>");
							$('#form-error').show(100); 
							return false;
						}
						else if(data=="9"){
							$("#loader-login").hide();
							$('#form-error').html("<p class='alert alert-danger'>You are an unauthorized user. Please contact admin.</p>");
							$('#form-error').show(100); 
							return false;
						}
						else if(data=="10"){
							$("#loader-login").hide();
							removeError('email',$('#login_email'));
							removeError('password',$('#login_password'));
							displayError('captcha',$('#captcha_code'));
							/*$('#form-error').html("Please enter captcha code");
							$('#form-error').show(100); */
							return false;
						}
						else if(data=="11"){
							$("#loader-login").hide();
							removeError('email',$('#login_email'));
							removeError('password',$('#login_password'));
							displayError('captcha',$('#captcha_code'));
							/*$('#form-error').html("Captcha not matched");
							$('#form-error').show(100); */
							return false;
						}
						else if(data=="12"){
							$("#loader-login").hide();
							removeError('email',$('#login_email'));
							removeError('password',$('#login_password'));
							displayError('captcha',$('#captcha_code'));
							/*$('#form-error').html("Captcha error");
							$('#form-error').show(100);*/ 
							return false;
						}
						//else if(data=="0"){
						else{
							$("#loader-login").hide();

							displayError('email',$('#login_email'));
							displayError('password',$('#login_password'));

							var c = data.split(",");
							/*$('#form-error').html("Invalid email or password!");*/
							// append catcha code start
							
							if(parseInt(c[1])>=5){
								
								if(firsttime==1){
									firsttime = 0;
									$('#captcha').html('<div class="row"><div class="col-md-6 cptcha-div"><fieldset><img src="securitycode.php" id="scode3" alt="securitycode" class="cptcha-img"/><a class="pull-right captcha-refresh" href="javascript:void(0)" tabindex="-1" onclick="$(\'#scode3\').attr(\'src\',\'securitycode.php?id=\'+Math.random())" class="pull-right"><i class="fa fa-refresh fa-lg"></i></a></fieldset></div><div class="col-md-6"><div class="rgstr-form"><div class="form-group"><div class=""><input name="captcha_code" id="captcha_code" required="" aria-required="true" type="text"><label for="input" class="control-label">Security code</label><span style="display:none;" class="pull-right fm-chk"><i class="fa fa-check-circle fa-lg" aria-hidden="true"></i></span><i class="bar"></i></div></div></div></div></div><span></span>');
								}
								$("input:not(:checkbox):not(:button):not(.otp), textarea").focus(function(){
								 //$(this).parent('div').removeClass('rgst-c');
								 if($(this).parent().parent().hasClass('has-error')){
									 $(this).parent('div').addClass('rgs-fm2');
								 }
								 else{
									 $(this).parent('div').addClass('rgs-fm');
								 }
							  }).blur(function(){
								  if($(this).val()==""){
									  $(this).parent('div').removeClass('rgs-fm');
									  $(this).parent('div').removeClass('rgs-fm2');
								  }
								 //$(this).parent('div').addClass('rgst-c');
							  });
							  $('.form-inner-containt').addClass('captcha-add');
							} else{ $('.form-inner-containt').removeClass('captcha-add'); }
							// append captcha code end	
						}
					}
				});
			}
			return false;
			
		}
		
	});
});



function displayForget(){
	$('#forgotform').show();
	$('#loginform').hide();
	if($('#fpecaptcha').children().length > 1)
	{
		$('.form-inner-containt').addClass('captcha-add');
	}
	else
	{
		$('.form-inner-containt').removeClass('captcha-add');
	}
}
function displayForget1() {
	$('#forgotform').hide();
	$('#loginform').show();
	if($('#captcha').children().length > 1)
	{
		$('.form-inner-containt').addClass('captcha-add');
	}
	else
	{
		$('.form-inner-containt').removeClass('captcha-add');
	}
}



function forgetvalidate(){
	var optType=$("input[name='optType']:checked").val();
	var fpecode='0';
	if(optType =='Email')
	{
		var forgetEmail = $('#forget_email').val();
		if(forgetEmail == '') {
			displayError('email',$('#forget_email'));
			/*$('#forgot-error').html("<p style='color: red;'>Enter Email</p>"); 		
			$('#forgot-error').show(100);*/ 		
		return false;
		}
		else if(!validateEmail(forgetEmail)) {
			displayError('email',$('#forget_email'));
			/*$('#forgot-error').html("<p style='color: red;'>Please Enter Valid Email</p>");	   
			$('#forgot-error').show(100);*/ 		
			return false;	
		}
		else
		{
			removeError('email',$('#forget_email'));
			if($('#fpecode').length>0)
			{
				fpecode=$('#fpecode').val();
				if(fpecode=='')
				{
					displayError('captcha',$('#fpecode'));
					return false;
				}
				else
				{
					removeError('captcha',$('#fpecode'));
				}
			}
			$('#loaderEmail').show();
			$.ajax({
				method: "GET",
				url: "login.php",
				data: {
				'forget_email': forgetEmail,
				'action': 'forgot',
				'optType': optType,
				'captchacode':fpecode
				},
				success: function(data){
					$('#loaderEmail').hide();
					var dt =data.split(',');
					if(dt[0]=="1")
					{
						$('#forget_email').val('');
						if($('#fpecode').length>0){$('#fpecode').val('');}
						$('#forgot-error').html("<p style='color: green;'>Thank you for connecting with us your password has been sent to your Registered Email Address/Registered Phone Number</p>");
						$('#forgotform').hide();
						$('#forgot-error').show();
						setTimeout(function(){ displayForget1(); $('#forgot-error').hide();}, 5000);
					}
					if(dt[0]=="0")
					{
						/*$('#forget_email').val('');*/
						displayError('email',$('#forget_email'));
						/*$('#forgot-error').html("<p style='color: red;'>This email id not registered!</p>");*/
					}
					else if(dt[0]=="2")
					{
						/*$('#forget_email').val('');*/
						displayError('email',$('#forget_email'));
						/*$('#forgot-error').html("<p style='color: red;'>Invalid email address!</p>");*/
					}
					else if(dt[0]=="3")
					{
						/*$('#forget_email').val('');*/
						displayError('email',$('#forget_email'));
						/*$('#forgot-error').html("<p style='color: red;'>This email id not registered!</p>");*/
					}
					else if(dt[0]=="4")
					{
						$('#forgot-error').html("");
						/*$('#forget_email').val('');*/
						removeError('email',$('#forget_email'));
						displayError('captcha',$('#fpecode'));
						/*$('#forgot-error').html("<p style='color: red;'>Invalid captcha code !</p>");*/
					}
					else if(dt[0]=="6")
					{
						$('#forget_email').val('');
						$('#forgot-error').html("<p style='color: red;'>Please select an option!</p>");
					}
					if(dt[1]>3)
					{
					$('#fpecaptcha').html('<div class="row"><div class="col-md-6 cptcha-div"><fieldset><img src="securitycode.php" id="scode1" alt="securitycode" class="cptcha-img"> <a  class="pull-right captcha-refresh" href="javascript:void(0);" tabindex="-1" onclick="$(\'#scode1\').attr(\'src\',\'securitycode.php?id=\'+Math.random());"><i class="fa fa-refresh  fa-lg"></i> </a></fieldset></div><div class="col-md-6"><div class="rgstr-form"><div class="form-group"><div class=""><input type="text" name="fpecode" id="fpecode" required aria-required="true"><label for="input" class="control-label">Security code</label><span style="display:none;" class="pull-right fm-chk"><i class="fa fa-check-circle fa-lg" aria-hidden="true"></i></span> <i class="bar"></i> </div></div></div></div></div><span></span>');

						$("input:not(:checkbox):not(:button):not(.otp), textarea").focus(function(){
						//$(this).parent('div').removeClass('rgst-c');
						if($(this).parent().parent().hasClass('has-error')){
						$(this).parent('div').addClass('rgs-fm2');
						}
						else{
						$(this).parent('div').addClass('rgs-fm');
						}
						}).blur(function(){
						if($(this).val()==""){ 
						$(this).parent('div').removeClass('rgs-fm');
						$(this).parent('div').removeClass('rgs-fm2');
						}
						//$(this).parent('div').addClass('rgst-c');
						});
						$('.form-inner-containt').addClass('captcha-add');
						if(dt[0]=="4"){ displayError('captcha',$('#fpecode')); }
					}
					/*else{ $('.form-inner-containt').removeClass('captcha-add'); }*/
				}
			});
		}
		return false;
	}
	else if(optType=='Phone')
	{
		var fppcode=0;
		var countryCode = $('#countryCode').val();
		var contactno = $('#contact_no').val();
		var reg=/[^0-9]/g;
		if(countryCode == '') {
			/*displayError('phone',$('#countryCode'));*/
			/*$('#forgot-error').html("<p style='color: red;'>Enter country code</p>"); 		
			$('#forgot-error').show(100);*/ 		
			return false;
		}
		else if(contactno=='')
		{
			displayError('phone',$('#contact_no'));
			/*$('#forgot-error').html("<p style='color: red;'>Please Enter phone number</p>");	   
			$('#forgot-error').show(100);*/ 		
			return false;
		}
		else if(reg.test(contactno)){
			displayError('phone',$('#contact_no'));
			/*$('#forgot-error').html("<p style='color: red;'>Please Enter Valid number</p>");	   
			$('#forgot-error').show(100);*/ 		
			return false;	
		}
		else
		{
			removeError('phone',$('#contact_no'));
			removeError('countryCode',$('#fppcode'));
			if($('#fppcode').length>0)
			{
				fppcode= $('#fppcode').val();
				if(fppcode=='')
				{
					$('#fppcode').parent().parent().addClass('has-error');
					displayError('captcha',$('#fppcode'));
					return false;
				}
				else
				{
					removeError('captcha',$('#fppcode'));
					/*$('#fppcode').parent().parent().removeClass('has-error');*/
				}
			}
			$('#loaderPhone').show();
			$.ajax({
				method:'get',
				url:'forgot-password.php',
				data: {
				'ISD_Id': countryCode,
				'forgot_phone': contactno,
				'action': 'forgot',
				'optType': optType,
				'captchacode':fppcode
				},
				success: function(data){
					$('#loaderPhone').hide();
					var dt =data.split(',');
					if(dt[0]=="1")
					{
						$('#contact_no').val('');
						removeError('phone',$('#contact_no'));
						$('#forgot-error').html("<p style='color: green;'>Thank you for connecting with us your password has been sent to your Registered Email Address/Registered Phone Number</p>");
						$('#forgotform').hide();
						$('#forgot-error').show();
						setTimeout(function(){ displayForget1(); $('#forgot-error').hide();}, 5000);
					}
					else if(dt[0]=="2")
					{
						displayError('phone',$('#contact_no'));
						/*$('#forget_email').val('');
						$('#forgot-error').html("<p style='color: red;'>Invalid phone number!</p>");*/
					}
					else if(dt[0]=="3")
					{
						displayError('phone',$('#contact_no'));
						/*$('#forget_email').val('');
						$('#forgot-error').html("<p style='color: red;'>phone number is not registered!</p>");*/
					}
					else if(dt[0]=="4")
					{
						removeError('phone',$('#contact_no'));
						displayError('captcha',$('#fppcode'));
						
						/*$('#forget_email').val('');
						$('#forgot-error').html("<p style='color: red;'>Invalid captcha code !</p>");*/
					}
					else if(dt[0]=="6")
					{
						$('#contact_no').val('');
						$('#forgot-error').html("<p style='color: red;'>Please select an option!</p>");
					}
					if(dt[1]>3)
					{
						$('#fppcaptcha').html('<div class="row"><div class="col-md-6 cptcha-div"><fieldset><img src="securitycode.php" id="scode2" alt="securitycode" class="cptcha-img"> <a  class="pull-right captcha-refresh" href="javascript:void(0);" tabindex="-1" onclick="$(\'#scode2\').attr(\'src\',\'securitycode.php?id=\'+Math.random());"><i class="fa fa-refresh  fa-lg"></i> </a></fieldset></div><div class="col-md-6"><div class="rgstr-form"><div class="form-group"><div class=""><input type="text" name="fppcode" id="fppcode" required aria-required="true"><label for="input" class="control-label">Security code</label><span style="display:none;" class="pull-right fm-chk"><i class="fa fa-check-circle fa-lg" aria-hidden="true"></i></span> <i class="bar"></i> </div></div></div></div></div><span></span>');

						$("input:not(:checkbox):not(:button):not(.otp), textarea").focus(function(){
						//$(this).parent('div').removeClass('rgst-c');
						if($(this).parent().parent().hasClass('has-error')){
						$(this).parent('div').addClass('rgs-fm2');
						}
						else{
						$(this).parent('div').addClass('rgs-fm');
						}
						}).blur(function(){
						if($(this).val()==""){ 
						$(this).parent('div').removeClass('rgs-fm');
						$(this).parent('div').removeClass('rgs-fm2');
						}
						//$(this).parent('div').addClass('rgst-c');
						});
						$('.form-inner-containt').addClass('captcha-add');
						if(dt[0]=="4"){ displayError('captcha',$('#fppcode')); }
					}
					/*else{ $('.form-inner-containt').removeClass('captcha-add'); }*/
				}
			});
		}
		return false;
	}
	return false;
}
	function SelectSubject(obj)
	{
		if(obj.value=="Others")
		{
			//$("#header-query").addClass("new-query");
			$('#fieldOthers').show();
		}
		else

		{

			//$("#header-query").removeClass("new-query");

			$('#fieldOthers').hide();

		}

	}

	function resetForm()

	{

		$('#txtName').val('');

		$('#txtEmail').val('');

		$('#QuerySubject').val('');

		$('#txtMessage').val('');

		$('#OtherSubject').val('');

		$('#Security_Code').val('');

	}

	$(document).ready(function(){

	$("#btn_submit").click(function()

	{

		var TutorName = $.trim($('#txtName').val());

		var Email = $.trim($('#txtEmail').val());

		var Subject = $.trim($('#QuerySubject').val());

		var Message = $.trim($('#txtMessage').val());

		var Security_Code = $.trim($('#Security_Code').val());

		var CSCode="";

		var flag=true;

		if(Subject=="Others")

		{

			Subject=$.trim($('#OtherSubject').val());

		}

		if(Security_Code=='')

		{

			$('#Security_Code').css("border-color","red");

			flag =  false;

		}

		else

		{

			$('#Security_Code').css("border-color","green");

		}

		if(TutorName == '')

		{

			$('#txtName').css("border-color","red");

			flag =  false;

		}

		else

		{

			$('#txtName').css("border-color","green");

		}

		if(Email == '')

		{

			$('#txtEmail').css("border-color","red");

			flag =  false;

		}

		else if(!validateEmail(Email)) {

		   $('#txtEmail').css("border-color","red");

			flag =  false;

		}

		else

		{

			$('#txtEmail').css("border-color","green");

		}

		if(Subject == '')

		{

			if($('#QuerySubject').val()=="Others")

			{

				$('#OtherSubject').css("border-color","red");

			}

			else

			{

				$('#QuerySubject').css("border-color","red");

			}

			flag =  false;

		}

		else

		{

			$('#QuerySubject').css("border-color","green");

			$('#OtherSubject').css("border-color","green");

		}

		if(Message == '')

		{

			$('#txtMessage').css("border-color","red");

			flag =  false;

		}

		else

		{

			$('#txtMessage').css("border-color","green");

		}

		if(flag)

		{

			$("#ldr").show();

			$("#btn_submit").prop("disabled",true);

			$.ajax({

			method: "POST",

			url: "login.php",

			data:{

				'Tutor_Name': TutorName,

				'Tutor_Email':Email,

				'QuerySubject':Subject,

				'QueryMessage':Message,

				'Security_Code':Security_Code,

				'Action': 'SubmitQuery',

			},

			success: function(data)

			{

				$("#ldr").hide();

				if(data=='1')

				{

					resetForm();

					$('#scode').attr('src','securitycode.php?id='+Math.random());

					$("#hideForm").hide();

					$("#recId").show();

					$("#recId").html("<div class='query-top'><span>Thank you</span><br><br>Message has been sent, we will contact you soon</div>");

					setTimeout(function(){ $("#recId").hide(); $("#sidebar1").click();$("#hideForm").show();}, 5000);

					$("#btn_submit").prop("disabled",false);

				}

				else if(data=='2')

				{

					$('#txtEmail').css("border-color","red");

					$("#btn_submit").prop("disabled",false);

				}

				else if(data=='3')

				{

					$('#txtEmail').css("border-color","red");

					$("#btn_submit").prop("disabled",false);

				}

				else if(data=='4')

				{

					$("#recId").html("<div class='alert alert-danger'><i class='icon-warning-sign'></i> Some error occurred, please try again</div>");

					setTimeout(function(){ $("#recId").hide();$("#hideForm").show();}, 5000);

					$("#btn_submit").prop("disabled",false);

				}

				else if(data=='5')

				{

					$('#Security_Code').css("border-color","red");

					$("#btn_submit").prop("disabled",false);

				}

			}

			});

			return false;

			}

		});

	});

